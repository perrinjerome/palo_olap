@request_path /cell/area

@short_description Shows values of cube cells
 
@long_description 


@param database
@param_type identifier
@param_description Identifier of a database
 
@param cube
@param_type identifier
@param_description Identifier of a cube

@param area
@param_type area
@param_description Comma separated list of element identifiers list. Each element identifiers list is colon separated. The area is the cartesian product.
 
@param show_rule
@param_type boolean
@param_description If 1, then additional information about the cell value is returned, in case the value originates from an enterprise rule.

@param show_lock_info
@param_type boolean
@param_description If 1, then additional information about the cell lock is returned.
 
@param sid
@param_type string
@param_description Session identifier for a server connection. Use the <a href="/api/server/login">/server/login</a> request to get a valid session identifier.


@result type
@result_type integer
@result_description Type of the value (1=NUMERIC, 2=STRING)

@result exists
@result_type boolean
@result_description 1 if at least base cell for the path exists

@result value
@result_type double/string
@result_description Value of the cell

@result path
@result_type path
@result_description Comma separate list of element identifier (path of cube cell)

@result rule
@result_type identifier
@result_description Identifier of the rule, this cell values originates from or empty. Only available if show_rule is 1.

@result lock_info
@result_type identifier
@result_description Lock info (0 - cell is not locked, 1 - cell is locked by user wich sent request, 2 - cell is locked by another user). Only available if show_lock_info is 1.



@example database=1&cube=7&area=0:1,1:2,2:3,3:4,0,0
@example_description Show values of cells area "(0,1) x (1,2) x (2,3) x (3,4) x 0 x 0"
