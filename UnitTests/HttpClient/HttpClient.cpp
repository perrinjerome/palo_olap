////////////////////////////////////////////////////////////////////////////////
/// @brief http client
///
/// @file
///
/// Copyright (C) 2006-2009 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo.txt">
///   http://www.jedox.com/license_palo.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox 
/// AG. Intellectual property rights for these portions has triagens GmbH, 
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation 
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler
/// @author Copyright 2006, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#define _CRT_SECURE_NO_DEPRECATE 1

#include <stdio.h>
#include "HttpClient.h"

#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER

////////////////////////////////////////////////////////////////////////////////
// defines for windows
////////////////////////////////////////////////////////////////////////////////

#include <Windows.h>

#define usleep(v) Sleep(v/1000)
#define errno_socket WSAGetLastError()
#define EINTR_SOCKET WSAEINTR
#define EWOULDBLOCK_SOCKET WSAEWOULDBLOCK
#define snprintf _snprintf

#else

////////////////////////////////////////////////////////////////////////////////
// defines for unix
////////////////////////////////////////////////////////////////////////////////

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <netdb.h>
#include <errno.h>
#include <fcntl.h>

#define errno_socket errno
#define EINTR_SOCKET EINTR
#define EWOULDBLOCK_SOCKET EWOULDBLOCK
#define closesocket close

#endif

namespace triagens
{
using namespace std;



static int subtract (const timeval& l, const timeval& r)
{
    time_t      sec = l.tv_sec  - r.tv_sec;
    suseconds_t msc = l.tv_usec - r.tv_usec;

    while (msc < 0)
    {
        msc += 1000000;
        sec -= 1;
    }

    return (sec * 1000000LL) + msc;
}



HttpClient::HttpClient (const string& host, unsigned int port)
        : lastError(HTTP_CLIENT_NO_ERROR), sheep(0), host(host), port(port), connected(false),
        totalSend(0), totalReceive(0), totalReads(0), totalWrites(0), totalBlocks(0), nrCalls(0)
{
    resolveHostname();
}



HttpClient::HttpClient (const string& host, const string& port)
        : lastError(HTTP_CLIENT_NO_ERROR), sheep(0), host(host), port(atoi(port.c_str())), connected(false),
        totalSend(0), totalReceive(0), totalReads(0), totalWrites(0), totalBlocks(0), nrCalls(0)
{
    resolveHostname();
}


void HttpClient::resolveHostname ()
{
    sheep = ::gethostbyname(host.c_str());

    if (sheep == 0)
    {
        lastError = CANNOT_RESOLVE_HOSTNAME;
        setLastErrorMessage(h_errno);
    }
}



bool HttpClient::connect ()
{
    if (sheep == 0)
    {
        return false;
    }

    // open a socket
    fd = ::socket(AF_INET, SOCK_STREAM, 0);

    if (fd < 0)
    {
        lastError = CANNOT_CREATE_SOCKET;
        setLastErrorMessage(errno);
        return false;
    }

    // and try to connect the socket to the given address
    struct sockaddr_in saddr;

    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    memcpy(&(saddr.sin_addr.s_addr), sheep->h_addr, sheep->h_length);
    saddr.sin_port = htons(port);

    int res = ::connect(fd, (struct sockaddr*)&saddr, sizeof(saddr));

    if (res < 0)
    {
       // ::closesocket(fd);
        lastError = CANNOT_CONNECT;
        setLastErrorMessage(errno);
        return false;
    }

    // disable nagle's algorithm
    int n = 1;
    res = setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char*)&n, sizeof(n));

    if (res != 0 )
    {
        //::closesocket(fd);
        lastError = NO_DELAY_FAILED;
        setLastErrorMessage(errno);
        return false;
    }

    // we have a connection
    connected = true;

    return true;
}



void HttpClient::setLastErrorMessage (int err)
{
#ifdef _MSC_VER
    switch (lastError)
    {
        case HTTP_CLIENT_NO_ERROR:
            lastErrorMessage = "no error";
            break;
        case CONNECTION_CLOSED:
            lastErrorMessage = "connection closed";
            break;
        case CANNOT_RESOLVE_HOSTNAME:
            lastErrorMessage = "cannot resolve hostname";
            break;
        case CANNOT_CREATE_SOCKET:
            lastErrorMessage = "cannot create socket";
            break;
        case CANNOT_CONNECT:
            lastErrorMessage = "cannot connect";
            break;
        case WRITE_FAILED:
            lastErrorMessage = "write failed";
            break;
        case READ_FAILED:
            lastErrorMessage = "read failed";
            break;
        case NON_BLOCKING_FAILED:
            lastErrorMessage = "cannot set socket to non-blocking";
            break;
        case NO_DELAY_FAILED:
            lastErrorMessage = "cannot disable nagle's algorithm";
            break;
    }
#else
    lastErrorMessage = strerror(err);
#endif
}



bool HttpClient::get (const string& path, const string& parameters)
{
    if (! connected)
    {
        return false;
    }

    bool ok = sendGet(path, parameters);

    if (! ok)
    {
        return false;
    }

    return receive();
}



bool HttpClient::post (const string& path, const string& parameters)
{
    if (! connected)
    {
        return false;
    }

    bool ok = sendPost(path, parameters);

    if (! ok)
    {
        return false;
    }

    return receive();
}



bool HttpClient::sendGet (const string& path, const string& parameters)
{
    string message = "GET " + path;

    if (! parameters.empty())
    {
        message += "?" + parameters;
    }

    message += " HTTP/1.1\r\nContent-Length: 0\r\nHost: " + host + "\r\n\r\n";
    return send(message);
}



bool HttpClient::sendPost (const string& path, const string& parameters)
{
    char buffer[1024];

    string message = "POST " + path;

    snprintf(buffer, sizeof(buffer)-1, "%d", (int) parameters.size());

    message += " HTTP/1.1\r\nContent-Length: ";
    message += buffer;
    message += "\r\nHost: " + host + "\r\n\r\n" + parameters;
    return send(message);
}



bool HttpClient::send (const string& message)
{
    ::timeval t1;
    ::timeval t2;
    int writes = 0;

    gettimeofday(&t1, 0);

    const char * ptr = message.c_str();
    size_t length = message.length();

    while (0 < length)
    {
        int nr = ::send(fd, ptr, (int) length, 0);
        writes++;

        if (nr < 0)
        {
            if (errno_socket == EINTR_SOCKET)
            {
            }
            else
            {
                //::closesocket(fd);
                lastError = WRITE_FAILED;
                setLastErrorMessage(errno);
                connected = false;
                cerr << "send failed with " << errno << endl;
                return false;
            }
        }
        else
        {
            ptr    += nr;
            length -= nr;
        }
    }

    gettimeofday(&t2, 0);
    double d = subtract(t2, t1) / 1000.0;

    totalSend += d;
    totalWrites += writes;

    return true;
}



bool HttpClient::receive ()
{
    ::timeval t1;
    ::timeval t2;
    int blocks = 0;
    int reads = 0;

    gettimeofday(&t1, 0);

    resultString.clear();
    resultHeader.clear();
    resultBody.clear();

    bodyLength = 0;

    readingHeader = true;

    while (1)
    {
        char buffer [100000];
        int need = sizeof(buffer);

        if (! readingHeader)
        {
            int missing = (int) (bodyLength - resultBody.size());

            if (missing == 0)
            {
                break;
            }

            if (missing < need)
            {
                need = missing;
            }
        }


        int nr = ::recv(fd, buffer, need, 0);
        reads++;

        if (nr < 0)
        {
            if (errno_socket == EINTR_SOCKET)
            {
            }
            else if (errno_socket == EWOULDBLOCK_SOCKET)
            {
                blocks++;
            }
            else
            {
                //::closesocket(fd);
                lastError = READ_FAILED;
                setLastErrorMessage(errno);
                connected = false;
                cerr << "receive failed with " << errno << endl;
                return false;
            }
        }
        else if (nr == 0)
        {
            //::closesocket(fd);
            lastError = READ_FAILED;
            setLastErrorMessage(errno);
            connected = false;
            cerr << "receive failed with " << errno << endl;
            return false;
        }
        else
        {
            processInput(buffer, nr);
        }
    }

    gettimeofday(&t2, 0);
    double d = subtract(t2, t1) / 1000;

    totalReceive += d;
    totalBlocks += blocks;
    totalReads += reads;
    nrCalls++;

    return true;
}



void HttpClient::processInput (const char * buffer, size_t length)
{
    if (readingHeader)
    {
        resultString.append(buffer, length);

        size_t pos  = resultString.find("\r\n");
        size_t last = 0;

        while (pos != string::npos)
        {
            if (pos == last)
            {
                readingHeader = false;

                if (0 < last)
                {
                    resultBody.append(resultString.substr(last + 2));
                }

                resultString.clear();

                return;
            }
            else
            {
                string line = resultString.substr(last, pos - last);

                resultHeader.push_back(line);

                last = pos + 2;
                pos  = resultString.find("\r\n", last);

                if (line.compare(0, 15, "Content-Length:", 15) == 0)
                {
                    size_t numpos = line.find_first_not_of(" \r", 15);

                    if (numpos == string::npos)
                    {
                        bodyLength = 0;
                    }
                    else
                    {
                        bodyLength = atoi(line.substr(numpos).c_str());
                    }
                }
            }
        }

        if (0 < last)
        {
            resultString = resultString.substr(last);
        }
    }
    else
    {
        resultBody.append(buffer, length);
    }
}
}
