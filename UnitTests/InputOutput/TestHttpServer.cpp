#include <iostream>

#include "palo.h"

#include "HttpServer/DirectHttpResponse.h"
#include "HttpServer/HttpRequest.h"
#include "HttpServer/HttpRequestHandler.h"
#include "HttpServer/HttpResponse.h"
#include "HttpServer/HttpServer.h"
#include "Scheduler/ListenTask.h"
#include "Scheduler/Scheduler.h"
#include "Scheduler/SignalTask.h"
#include "Scheduler/Task.h"

using namespace palo;
using namespace std;

Scheduler* scheduler = Scheduler::getScheduler();



class TestHandler : public HttpRequestHandler
{
public:
    HttpJobRequest * handleHttpRequest (HttpRequest * request)
    {
        HttpResponse * response = new HttpResponse(HttpResponse::OK);

        response->getBody().appendText("Hallo World");

        HttpJobRequest * job = new DirectHttpResponse(request->getRequestPath(), response);
        return job;
    }
};



class ExitHandler : public HttpRequestHandler
{
public:
    HttpJobRequest * handleHttpRequest (HttpRequest * request)
    {
        HttpResponse * response = new HttpResponse(HttpResponse::OK);

        scheduler->beginShutdown();

        HttpJobRequest * job = new DirectHttpResponse(request->getRequestPath(), response);
        return job;
    }
};



class ControlCHandler : public SignalTask
{
public:
    void handleSignal (signal_t signal)
    {
        scheduler->beginShutdown();
    }
};



class NotFoundHandler : public HttpRequestHandler
{
public:
    HttpJobRequest * handleHttpRequest (HttpRequest * request)
    {
        HttpResponse * response = new HttpResponse(HttpResponse::NOT_FOUND);

        response->getBody().appendText("File not found!");

        HttpJobRequest * job = new DirectHttpResponse(request->getRequestPath(), response);
        return job;
    }
};



int main (int argc, char * argv [])
{
    int port = 7777;

    if (argc == 2)
    {
        port = ::atoi(argv[1]);
    }

    HttpServer httpServer(scheduler);

    TestHandler* testHandler = new TestHandler();
    ExitHandler* exitHandler = new ExitHandler();
    NotFoundHandler* notFound = new NotFoundHandler();

    httpServer.addHandler("/hello/world", testHandler);
    httpServer.addHandler("/exit",        exitHandler);
    httpServer.addNotFoundHandler(notFound);

    scheduler->addListenTask(&httpServer, port);

    ControlCHandler controlCHandler;
    scheduler->addSignalTask(&controlCHandler, SIGINT);

    cout << "please try the server on port " << port << "\n"
         << "using /hello/world or /exit" << endl;

    scheduler->run();
}
