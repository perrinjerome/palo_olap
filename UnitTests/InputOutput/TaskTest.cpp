#include <iostream>

#include "palo.h"

#include "Logger/Logger.h"
#include "Scheduler/Scheduler.h"
#include "Scheduler/ReadWriteTask.h"
#include "Scheduler/SignalTask.h"

using namespace palo;
using namespace std;

Scheduler* scheduler = Scheduler::getScheduler();

class ControlCHandler : public SignalTask
{
public:
    void handleSignal (signal_t signal)
    {
        scheduler->beginShutdown();
    }
};


class TaskTestHandler : public ReadWriteTask
{
public:
    TaskTestHandler (socket_t readSocket, socket_t writeSocket, Scheduler* scheduler)
            : IoTask(readSocket, writeSocket),
            ReadWriteTask(readSocket, writeSocket),
            scheduler(scheduler),
            readPosition(0)
    {

        writeBuffer = new StringBuffer();
        writeBuffer->initialize();
    }

    ~TaskTestHandler ()
    {

    }

public:
    bool handleRead ()
    {
        bool res = fillReadBuffer();

        if (! res)
        {
            return false;
        }

        return processRead();
    }

    bool processRead()
    {
        const char * ptr = readBuffer.c_str() + readPosition;
        const char * end = readBuffer.end();

        for (;  ptr < end;  ptr++)
        {
            readPosition++;
            writeBuffer->appendChar(*ptr);
        }

        return true;
    }

    void completedWriteBuffer()
    {
        writeBuffer = new StringBuffer();
        writeBuffer->initialize();
    }


private:

    Scheduler* scheduler;
    size_t readPosition;
};



int main (int argc, char * argv [])
{

    Logger::setLogLevel("trace");

    ControlCHandler controlCHandler;
    scheduler->addSignalTask(&controlCHandler, SIGINT);

    socket_t readSocket = 0;
    socket_t writeSocket = 1;
    TaskTestHandler* task = new TaskTestHandler (readSocket, writeSocket, scheduler);
    scheduler->addIoTask(task);

    scheduler->run();
}
