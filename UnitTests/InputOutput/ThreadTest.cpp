#include <iostream>
#include <string>

#include "palo.h"

#include "Logger/Logger.h"
#include "Thread/Thread.h"
#include "Thread/ReadWriteLock.h"
#include "Thread/ReadLocker.h"
#include "Thread/WriteLocker.h"

using namespace palo;
using namespace std;

ReadWriteLock myLock;

class MyThread : public Thread
{
public:
    MyThread () : Thread("MyThread")
    {
    }

protected:

    void readSomething (const string &name)
    {
        Logger::info << name << " wait read" << endl;
        ReadLocker r (&myLock);
        Logger::info << name << " is reading" << endl;
        waitRand();
        Logger::info << name << " is finished" << endl;
    }

    void writeSomething (const string &name)
    {
        Logger::info << name << " wait write" << endl;
        WriteLocker w (&myLock);
        Logger::info << name << " is writing" << endl;
        waitRand();
        Logger::info << name << " is finished" << endl;
    }

    void work (const string &name)
    {
        int j= 1 + (int) (10.0*rand()/(RAND_MAX+1.0));
        if (j > 5)
        {
            readSomething(name);
        }
        else
        {
            writeSomething(name);
        }
    }

    void waitRand ()
    {
        int j=1+(int) (10.0*rand()/(RAND_MAX+1.0));
        sleep(j);
    }
};

class ThreadA : public MyThread
{
protected:
    void run()
    {
        while (1)
        {
            work("Thread A");
            sleep(1);
        }
    }
};

class ThreadB : public MyThread
{
protected:
    void run()
    {
        while (1)
        {
            work("\t\t\tThread B");
            sleep(1);
        }
    }
};

class ThreadC : public MyThread
{
protected:
    void run()
    {
        while (1)
        {
            work("\t\t\t\t\t\tThread C");
            sleep(1);
        }
    }
};

int main(int argc, char * argv [])
{
    srand((int) time(0));

    Logger::setLogLevel("trace");

    ThreadA a;
    ThreadB b;
    ThreadC c;
    b.start();
    a.start();
    c.start();
    sleep(150);
}
