#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>

#if defined(_MSC_VER)
#include "Windows.h"
#endif

using namespace std;

static const string authentication = "AUTHENTICATION;";
static const string authorization = "AUTHORIZATION;";
static const string double_1_13 = "DOUBLE;1;13;";
static const string session = "SESSION";

static string currentSession = "----";

#if ! defined(_MSC_VER)
namespace
{
void split (const string& text, const string& separators, vector<string>& words)
{
    size_t n = text.length();
    size_t start = text.find_first_not_of(separators);

    while (start != string::npos)
    {
        size_t stop = text.find_first_of(separators, start);

        if (stop == string::npos)
        {
            stop = n;
        }

        words.push_back(text.substr(start, stop - start));
        start = text.find_first_not_of(separators, stop + 1);
    }
}

void executeReplace (const string& what)
{
    vector<string> a;
    split(what, ";", a);

    cerr << "RECEIVED REPLACED\n"
         << "  PATH = " << a[2] << "\n"
         << "  VALUE = " << a[3] << "\n"
         << "  SOURCE = " << a[1] << "\n"
         << "  SID = " << currentSession << "\n";

    string line = "curl -o /dev/stderr http://localhost:7777/server/databases?sid=" + currentSession;

    system(line.c_str());

    cout << "ERROR;\"Positive\";\"Positive Desktop Pro XL | Germany | Jan | 2002 | Budget | Units Ab, 69\"" << endl;
}
}
#endif

int main (int argc, char* argv[])
{
    int st = 10;

    if (argc == 2)
    {
        st = atoi(argv[1]);
    }

    cerr << "WORKER started" << endl;

    while (! cin.eof())
    {
        string line;

        getline(cin, line);

        cerr << "WORKER received '" << line << "'" << endl;

        if (st != 0)
        {
#if defined(_MSC_VER)
            Sleep(st * 1000);
#else
            sleep(st);
#endif
        }

        string response;

        if (line == "CUBE;2;5")
        {
            response += "AREA;Bereich1;2;5;*;*\n";
        }
        else if (line == "CUBE;1;13")
        {
            response += "AREA;Bereich2;1;13;*;*;*;*;*;*\n";
        }
        else if (line.substr(0, authentication.size()) == authentication)
        {
            response += "LOGIN;TRUE\n";
        }
        else if (line.substr(0, authorization.size()) == authorization)
        {
            response += "LOGIN;TRUE\n";
            response += "GROUPS;admin\n";
        }
        else if (line == "TERMINATE")
        {
            exit(0);
        }
        else if (line.substr(0, session.size()) == session)
        {
            currentSession = line.substr(session.size() + 1);
        }
#if !defined(_MSC_VER)
        else if (line.substr(0, double_1_13.size()) == double_1_13)
        {
            executeReplace(line.substr(double_1_13.size()));
        }
#endif

        response += "DONE";

        cerr << "WORKER replied '" << response << "'" << endl;
        cout << response << "\n";
    }
}
