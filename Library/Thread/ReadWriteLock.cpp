////////////////////////////////////////////////////////////////////////////////
/// @brief Read-Write Lock
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "Thread/ReadWriteLock.h"

namespace palo {

// /////////////////////////////////////////////////////////////////////////////
// constructors and destructora
// /////////////////////////////////////////////////////////////////////////////

ReadWriteLock::ReadWriteLock() :
	nReaderCount(0), hWriterEvent(0), hNoReadersEvent(0)
{

	// Signaled:     writer has no access
	// Non-Signaled: writer has access, block readers

	hWriterEvent = CreateEvent(0, TRUE, TRUE, 0);

	// Signaled:     no readers
	// Non-Signaled: some readers have access, block writer

	hNoReadersEvent = CreateEvent(0, TRUE, TRUE, 0);

	InitializeCriticalSection(&csLockWriter);
	InitializeCriticalSection(&csReaderCount);
}

ReadWriteLock::~ReadWriteLock()
{
	DeleteCriticalSection(&csLockWriter);
	DeleteCriticalSection(&csReaderCount);

	CloseHandle( hWriterEvent);
	CloseHandle( hNoReadersEvent);
}

// /////////////////////////////////////////////////////////////////////////////
// public methods
// /////////////////////////////////////////////////////////////////////////////

void ReadWriteLock::readLock()
{
	while (true) {
		WaitForSingleObject(hWriterEvent, INFINITE);

		incrementReaderCount();

		if (WaitForSingleObject(hWriterEvent, 0) != WAIT_OBJECT_0) {
			decrementReaderCount();
		} else {
			break;
		}
	}
}

void ReadWriteLock::releaseReadLock()
{
	decrementReaderCount();
}

void ReadWriteLock::writeLock()
{
	EnterCriticalSection(&csLockWriter);

	WaitForSingleObject(hWriterEvent, INFINITE);

	ResetEvent( hWriterEvent);

	WaitForSingleObject(hNoReadersEvent, INFINITE);

	LeaveCriticalSection(&csLockWriter);
}

void ReadWriteLock::releaseWriteLock()
{
	SetEvent( hWriterEvent);
}

void ReadWriteLock::incrementReaderCount()
{
	EnterCriticalSection(&csReaderCount);

	nReaderCount++;

	ResetEvent( hNoReadersEvent);

	LeaveCriticalSection(&csReaderCount);
}

void ReadWriteLock::decrementReaderCount()
{
	EnterCriticalSection(&csReaderCount);

	nReaderCount--;

	if (nReaderCount == 0) {
		SetEvent( hNoReadersEvent);
	} else if (nReaderCount < 0) {
		throw "reader count is negative";
	}

	LeaveCriticalSection(&csReaderCount);
}
}
