////////////////////////////////////////////////////////////////////////////////
/// @brief http server
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUTm_available_handlers

/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo.txt">
///   http://www.jedox.com/license_palo.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
///
/// @author Radu Ialovoi
////////////////////////////////////////////////////////////////////////////////

#ifndef THREAD_POOL_H
#define THREAD_POOL_H 1

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

namespace palo {
template<class T>
class ThreadPool {
private:
	class ThreadedHandler {
		friend class ThreadPool;
	public:
		ThreadedHandler(ThreadPool* pool)
		{
		}

		~ThreadedHandler()
		{
		}

		void operator()()
		{
		}
	};

	boost::thread_group this_group;

	void do_one_handler()
	{
	}

	bool could_steal_one_handler()
	{
		return true;
	}
public:
	ThreadPool(size_t size)
	{
	}

	~ThreadPool()
	{
	}

	size_t total_handlers()
	{
	}

	void asynch_execute(T* job)
	{
	}
};
}

#endif
