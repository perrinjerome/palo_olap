////////////////////////////////////////////////////////////////////////////////
/// @brief cube browser
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "PaloBrowser/CubeBrowserJob.h"

#include "Olap/AreaStorage.h"
#include "PaloDocumentation/CubeBrowserDocumentation.h"
#include "PaloDocumentation/HtmlFormatter.h"

namespace palo {

// /////////////////////////////////////////////////////////////////////////////
// PaloBrowserJob methods
// /////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// {@inheritDoc}
////////////////////////////////////////////////////////////////////////////////

bool CubeBrowserJob::initialize()
{
	bool ok = PaloJob::initialize();

	if (!ok) {
		return false;
	}

	if (jobRequest->action) {
		action = *(jobRequest->action);
	}

	if (!action.empty()) {
		if (action == "load" || action == "save") {
			jobType = WRITE_JOB;
		}
	}
	return true;
}

void CubeBrowserJob::compute()
{
	findCube();

	// handle required action
	string message1;

	if (!action.empty()) {
		try {
			if (action == "load") {
				database->loadCube(cube, user);
				message1 = "cube loaded";
			} else if (action == "save") {
				database->saveCube(cube, user);
				message1 = "cube saved";
			}

			if (!message1.empty()) {
				message1 = createHtmlMessage("Info", message1);
			}
		} catch (const ErrorException& e) {
			message1 = createHtmlMessage("Error", e.getMessage());
		}
	}

	// get the path
	const vector<Dimension*>* dimensions = cube->getDimensions();
	uint32_t numResult = 0;
	vector<IdentifiersType> cellPaths;
	vector<IdentifiersType> cubeArea;
	string pathStringMessage = "";

	if (jobRequest->area != 0 || jobRequest->areaName != 0) {
		try {
			if (jobRequest->area) {
				cubeArea = area(jobRequest->area, dimensions, numResult, false);
			} else if (jobRequest->areaName) {
				cubeArea = area(jobRequest->areaName, dimensions, numResult, false);
			}

			for (size_t i = 0; i < cube->getDimensions()->size(); i++) {
				IdentifiersType& dimElements = cubeArea[i];

				if (i > 0) {
					pathStringMessage += ",";
				}

				if (dimElements.size() == dimensions->at(i)->sizeElements()) {
					pathStringMessage += "*";
				} else {
					pathStringMessage += StringUtils::convertToString(dimElements[0]);

					for (size_t j = 1; j < dimElements.size(); j++) {
						pathStringMessage += ":";
						pathStringMessage += StringUtils::convertToString(dimElements[j]);
					}
				}
			}
		} catch (ParameterException e) {
			cubeArea.resize(dimensions->size());
			numResult = 0;
			message1 = createHtmlMessage("Error", e.getMessage());
		}
	} else {
		pathStringMessage = "0";

		for (size_t i = 1; i < cube->getDimensions()->size(); i++) {
			pathStringMessage += ",0";
		}

		numResult = 0;
		cubeArea.resize(dimensions->size());
	}

	AreaStorage storage(dimensions, &cubeArea, numResult, false);
	cube->getAreaCellValues(&storage, &cubeArea, &cellPaths, user, &memory_context);
	CubeBrowserDocumentation sbd(database, cube, &storage, &cellPaths, pathStringMessage, message1);
	HtmlFormatter hf(templatePath + "/browser_cube.tmpl");

	generateResult(hf.getDocumentation(&sbd));
}
}
