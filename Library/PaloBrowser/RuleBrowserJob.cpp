////////////////////////////////////////////////////////////////////////////////
/// @brief provides cube rule browser
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////


#include "PaloBrowser/RuleBrowserJob.h"

#include "Exceptions/ParameterException.h"

#include "Collections/StringUtils.h"

#include "PaloDocumentation/HtmlFormatter.h"

#include "Olap/Dimension.h"
#include "Olap/Rule.h"

#include "Parser/RuleParserDriver.h"

#include "PaloDocumentation/RuleBrowserDocumentation.h"

namespace palo {

bool RuleBrowserJob::initialize()
{
	bool ok = PaloJob::initialize();

	if (!ok) {
		return false;
	}

	if (jobRequest->action) {
		action = *(jobRequest->action);
	}

	if (!action.empty()) {
		if (action == "delete" || action == "activate") {
			jobType = WRITE_JOB;
		}
	}
	return true;
}

void RuleBrowserJob::compute()
{
	findCube();

	string message;
	string ruleStringMessage;

	if (jobRequest->action) {
		string action = *(jobRequest->action);
		findRule();

		if (action == "delete") {
			cube->deleteRule(rule->getIdentifier(), 0, &memory_context);
			rule = 0;
			message = createHtmlMessage("Info", "rule deleted");
		} else if (action == "activate") {
			if (rule->isActive()) {
				cube->activateRule(rule, false, 0);
				message = createHtmlMessage("Info", "rule deactivated");
			} else {
				cube->activateRule(rule, true, 0);
				message = createHtmlMessage("Info", "rule activated");
			}
		}
	}

	if (jobRequest->definition) {
		string definition = *(jobRequest->definition);

		ruleStringMessage = StringUtils::escapeHtml(definition);

		if (!ruleStringMessage.empty()) {
			RuleParserDriver driver;

			driver.parse(definition);
			RuleNode* r = driver.getResult();

			if (r) {
				// validate parse tree
				string errorMsg;
				bool ok = r->validate(server, database, cube, errorMsg);

				if (!ok) {
					delete r;
					message = createHtmlMessage("Error", errorMsg);
				} else {
					cube->createRule(r, "PALO", "generated via server browser", true, 0);
					message = createHtmlMessage("Info", "added new rule");
				}
			} else {
				message = createHtmlMessage("Error", driver.getErrorMessage());
			}
		}
	}
	server->invalidateCache();

	RuleBrowserDocumentation sbd(database, cube, ruleStringMessage, message);
	HtmlFormatter hf(templatePath + "/browser_cube_rule.tmpl");

	generateResult(hf.getDocumentation(&sbd));
}

}
