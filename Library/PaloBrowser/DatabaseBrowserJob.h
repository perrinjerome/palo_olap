////////////////////////////////////////////////////////////////////////////////
/// @brief database browser
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_BROWSER_DATABASE_BROWSER_JOB_H
#define PALO_BROWSER_DATABASE_BROWSER_JOB_H 1

#include "palo.h"

#include <iostream>

#include "PaloDispatcher/PaloBrowserJob.h"
#include "PaloDocumentation/HtmlFormatter.h"
#include "PaloDocumentation/DatabaseBrowserDocumentation.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief database browser
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS DatabaseBrowserJob : public PaloBrowserJob {

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new DatabaseBrowserJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	DatabaseBrowserJob(Server * server, PaloJobRequest* jobRequest) :
		PaloBrowserJob(server, jobRequest)
	{
		jobType = READ_JOB;
		action = "";
	}

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return jobType;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	bool initialize()
	{
		bool ok = PaloJob::initialize();

		if (!ok) {
			return false;
		}

		if (jobRequest->action) {
			action = *(jobRequest->action);
		}

		if (!action.empty()) {
			if (action == "load" || action == "save" || action == "delete" || action == "unload" || action == "load_database" || action == "save_database" || action == "delete_dimension") {
				jobType = WRITE_JOB;
			}
		}
		return true;
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		string message;

		findDatabase();

		if (!action.empty()) {
			try {
				if (action == "load") {
					findCube(false);
					database->loadCube(cube, user);
					message = "cube loaded";
				} else if (action == "save") {
					findCube();
					server->saveServer(0);
					server->saveDatabase(database, 0);
					database->saveCube(cube, 0);
					message = "cube saved";
				} else if (action == "delete") {
					findCube();
					database->deleteCube(cube, 0);
					message = "cube deleted";
				} else if (action == "unload") {
					findCube();
					database->unloadCube(cube, 0);
					message = "cube unloaded";
				} else if (action == "load_database") {
					server->loadDatabase(database, 0);
					message = "database loaded";
				} else if (action == "save_database") {
					server->saveServer(0);
					server->saveDatabase(database, 0);
					message = "database saved";
				} else if (action == "delete_dimension") {
					findDimension();
					database->deleteDimension(dimension, 0);
					message = "dimension deleted";
				}

				if (!message.empty()) {
					message = createHtmlMessage("Info", message);
				}
			} catch (ErrorException e) {
				message = createHtmlMessage("Error", e.getMessage());
			}
		}

		DatabaseBrowserDocumentation sbd(database, message);
		HtmlFormatter hf(templatePath + "/browser_database.tmpl");

		generateResult(hf.getDocumentation(&sbd));
	}

private:
	string action;
	JobType jobType;
};

}

#endif
