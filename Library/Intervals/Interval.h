#ifndef YALOS_INTERVAL_H
#define YALOS_INTERVAL_H 1
////////////////////////////////////////////////////////////////////////////////
/// @brief palo Interval class
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Radu Racariu (radu@yalos-solutions.com), yalos solutions, Bucharest, Romania
////////////////////////////////////////////////////////////////////////////////

#include <sstream>
#include <stdexcept>
#include <string>

/*
 * Safe for multi threading use
 */
template<typename T>
class Interval {
	// Methods
public:
	Interval() :
		m_start(0), m_end(0)
	{

	}
	Interval(const T& start, const T& end) :
		m_start(start), m_end(end)
	{
		if (start > end) {
			std::ostringstream os;
			os << "invalid range: " << start << " > " << end << std::endl;
			throw std::logic_error(os.str());
		}
	}

	inline bool contains(const T& value) const
	{
		if (value >= m_start && value <= m_end)
			return true;
		else
			return false;
	}

	inline bool contains(const Interval& interval) const
	{
		if (interval.start() >= m_start && interval.end() <= m_end)
			return true;
		else
			return false;
	}

	inline const Interval operator=(const Interval& interval)
	{
		return Interval(interval.start(), interval.end());
	}

	inline bool operator==(const Interval& interval) const
	{
		return (interval.start() == m_start && interval.end() == m_end);
	}

	inline bool operator<(const Interval& interval) const
	{
		return (m_end < interval.start());
	}

	inline bool operator>(const Interval& interval) const
	{
		return (m_start > interval.end());
	}

	inline const T& start() const
	{
		return m_start;
	}

	inline const T& end() const
	{
		return m_end;
	}

	// Members
private:
	const T m_start;
	const T m_end;
};
#endif
