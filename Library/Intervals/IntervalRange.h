#ifndef YALOS_INTERVAL_RANGE_H
#define YALOS_INTERVAL_RANGE_H 1
////////////////////////////////////////////////////////////////////////////////
/// @brief palo IntervalRange class
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Radu Racariu (radu@yalos-solutions.com), yalos solutions, Bucharest, Romania
////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include "Intervals/Interval.h"
#include "Intervals/IntervalSet.h"

template<typename T>
class IntervalVectorSet : public std::vector<IntervalSet<T> > {
public:
	// if at least 1 element is non-empty, flip
	bool valid;
	T first;
	T last;
    typedef typename std::vector<IntervalSet<T> >::const_iterator const_iterator;

	IntervalVectorSet() :
		valid(false), first(0), last(0)
	{
	}

	template<typename V>
	IntervalVectorSet(const V& items)
	{
		add(items);
	}

	template<typename V>
	void add(const V& items)
	{
		add<V> (items.begin(), items.end());
	}

	template<class V, typename InputIterator>
	void add(InputIterator begin, InputIterator end)
	{
		for (InputIterator it = begin; it != end; ++it) {
			IntervalSet<T> set;

			typename V::value_type::const_iterator begin = it->begin();
			typename V::value_type::const_iterator end = it->end();

			set.addIntervals(begin, end);

			if (!it->empty()) {
				valid = true;

				if (set.first() < this->first)
					first = set.first();

				if (set.last() > last)
					last = set.last();
			}

			this->push_back(set);
		}
	}

	void clear()
	{
		std::vector<IntervalSet<T> >::clear();
		valid = false;
		first = last = 0;
	}

	template<class K>
	bool contains(const K& item)
	{
		if (!valid)
			return false;

		typename K::const_iterator crt = item.begin();
		for (const_iterator i = this->begin(); i != this->end(); i++, crt++) {
			if (i->empty())
				continue;

			if (i->contains(*crt)) {
				return true;
			}
		}
		return false;
	}

};
#endif
