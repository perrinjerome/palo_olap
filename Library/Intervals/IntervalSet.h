#ifndef YALOS_INTERVAL_SET_H
#define YALOS_INTERVAL_SET_H 1
////////////////////////////////////////////////////////////////////////////////
/// @brief palo IntervalSet
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Radu Racariu (radu@yalos-solutions.com), yalos solutions, Bucharest, Romania
////////////////////////////////////////////////////////////////////////////////

#include <set>
#include <vector>
#include <climits>
#include <limits>

#include "Intervals/Interval.h"

template<typename T>
class IntervalSet : public std::set<Interval<T> > {
	// Methods
public:
	typedef std::set<Interval<T> > IntervalSetType;
	typedef std::vector<T> vectorTType;

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

	IntervalSet(const int setId = INT_MIN) :
		m_id(setId), m_start(std::numeric_limits<T>::min()), m_end(std::numeric_limits<T>::max())
	{
	}

	void addInterval(const T& first, const T& last)
	{
		Interval<T> i(first, last);
		addInterval(i);
	}

	void addInterval(const Interval<T> & interval)
	{
		this->insert(interval);
		typename IntervalSetType::const_iterator it = this->begin();
		m_start = (*it).start();

		it = this->end();
		m_end = (*(--it)).end();
	}

	template<typename InputIterator>
	void addIntervals(InputIterator first, InputIterator last)
	{
		if (first != last) {
			T left = *first;
			T right = *first;

			for (InputIterator i = ++first; i != last; i++) {
				T crt = *i;
				if (crt != right + 1) {
					addInterval(left, right);
					left = crt;
				}
				right = crt;
			}

			addInterval(left, right);
		}
	}

	inline bool contains(const T& value) const
	{
		if (this->empty())
			return false;

		if (!(value >= m_start && value <= m_end))
			return false;

		for (typename IntervalSetType::const_iterator it = this->begin(); it != this->end(); ++it) {
			if ((*it).contains(value))
				return true;
		}
		return false;
	}

	bool contains(const vectorTType& value) const
	{
		if (this->empty())
			return false;

		IntervalSet<T> set;
		set.addIntervals(value.begin(), value.end());

		return contains(set);
	}

	bool contains(const IntervalSet& set) const
	{
		if (this->empty())
			return false;

		typename IntervalSetType::const_iterator it = set.begin();
		typename IntervalSetType::const_iterator end = set.end();

		for (typename IntervalSetType::const_iterator base = this->begin(); base != this->end(); ++base) {
			for (; it != end; ++it) {
				if ((*base).contains(*it))
					break;
			}
			return false;
		}
		return true;
	}

	bool contains(const Interval<T> & interval) const
	{
		if (this->empty())
			return false;

		bool found = false;
		typename IntervalSetType::const_iterator fi = find(interval);

		if (fi != this->end())
			found = true;
		else {
			typename IntervalSetType::const_iterator it;
			for (it = this->begin(); it != this->end(); ++it) {
				if (interval.start() > (*it).start() && interval.end() < (*it).end()) {
					found = true;
					break;
				}
			}
		}
		return found;
	}

	bool operator==(const IntervalSet& set) const
	{
		if (set.size() != this->size())
			return false;

		typename IntervalSetType::const_iterator it;
		for (it = set.begin(); it != set.end(); ++it) {
			if (!contains(*it))
				return false;
		}
		return true;
	}

	inline const T& first() const
	{
		return m_start;
	}

	inline const T& last() const
	{
		return m_end;
	}

	inline const int id() const
	{
		return m_id;
	}

	// Members
private:
	int m_id;
	T m_start;
	T m_end;
};
#endif
