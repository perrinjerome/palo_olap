////////////////////////////////////////////////////////////////////////////////
/// @brief write a data file
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef INPUT_OUTPUT_FILE_WRITER_H
#define INPUT_OUTPUT_FILE_WRITER_H 1

#include "palo.h"

#include <fstream>

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief file a data writer
///
/// The file write generates CSV files to store the OLAP server, database, dimension
/// and cube data.
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS FileWriter {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructs a new file writer object for a given filename, path and file extension
	////////////////////////////////////////////////////////////////////////////////

	FileWriter(const FileName& fileName, bool bufferOutput);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief deletes a file writer object
	////////////////////////////////////////////////////////////////////////////////

	virtual ~FileWriter();

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief opens the file for writing
	////////////////////////////////////////////////////////////////////////////////

	virtual void openFile();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief close the file for writing
	////////////////////////////////////////////////////////////////////////////////

	virtual void closeFile();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends a comment line
	////////////////////////////////////////////////////////////////////////////////

	void appendComment(const string& value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends a section line
	////////////////////////////////////////////////////////////////////////////////

	void appendSection(const string& value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends a string to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendString(const string& value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief escapes a string and appends the result to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendEscapeString(const string& value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief escapes a list of strings and appends the result to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendEscapeStrings(const vector<string>* value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends an integer to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendInteger(const int32_t value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends list of integers to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendIntegers(const vector<int32_t>* value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends list of integers to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendIntegers(const vector<size_t>* value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends an identifier to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendIdentifier(const IdentifierType value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends list of identifiers to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendIdentifiers(const IdentifiersType* value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends a double to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendDouble(const double value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends list of doubles to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendDoubles(const vector<double>* value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends a boolean to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendBool(const bool value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief appends a timestamp to the actual line
	////////////////////////////////////////////////////////////////////////////////

	void appendTimeStamp();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief finalize the actual line
	////////////////////////////////////////////////////////////////////////////////

	void nextLine();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief flush buffers
	////////////////////////////////////////////////////////////////////////////////

	void writeBuffer();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief delete a file
	////////////////////////////////////////////////////////////////////////////////

	static void deleteFile(const FileName& fileName);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief get file size in bytes
	////////////////////////////////////////////////////////////////////////////////

	static int32_t getFileSize(const FileName& fileName);

private:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief escape a string and return the escaped string
	////////////////////////////////////////////////////////////////////////////////

	string escapeString(const string& text);

protected:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief actual line is empty
	////////////////////////////////////////////////////////////////////////////////

	bool isFirstValue;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief the output stream
	////////////////////////////////////////////////////////////////////////////////

	ofstream* outputFile;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief the filename of the output stream
	////////////////////////////////////////////////////////////////////////////////

	FileName fileName;

private:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief true if the buffer should be flushed after a new line
	////////////////////////////////////////////////////////////////////////////////

	bool isBuffered;
};

}

#endif
