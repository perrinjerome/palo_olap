////////////////////////////////////////////////////////////////////////////////
/// @brief Direct Palo Job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "PaloDispatcher/DirectPaloJob.h"

#include "Collections/StringUtils.h"
#include "Exceptions/ErrorException.h"
#include "Exceptions/WorkerException.h"
#include "HttpServer/HttpResponse.h"
#include "InputOutput/Statistics.h"

namespace palo {

// /////////////////////////////////////////////////////////////////////////////
// constructors and destructors
// /////////////////////////////////////////////////////////////////////////////

DirectPaloJob::DirectPaloJob(Server* server, PaloJobRequest* jobRequest) :
	PaloJob(server, jobRequest)
{
}

// /////////////////////////////////////////////////////////////////////////////
// Job methods
// /////////////////////////////////////////////////////////////////////////////

void DirectPaloJob::work()
{
	try {
		Statistics::Timer timer(getName(), "direct");

		compute();
	} catch (const WorkerException& e) {
		response = new HttpResponse(HttpResponse::BAD);

		StringBuffer& body = response->getBody();

		body.appendCsvInteger((int32_t)ErrorException::ERROR_WORKER_MESSAGE);
		body.appendText(e.getMessage());
		body.appendEol();
	} catch (const ErrorException& e) {

		if (ErrorException::ERROR_OUT_OF_MEMORY == e.getErrorType()) {
			cube->FreeEngineCube(&memory_context);
			memory_context.cleanup();
		}

		response = new HttpResponse(HttpResponse::BAD);

		StringBuffer& body = response->getBody();

		body.appendCsvInteger((int32_t)e.getErrorType());
		body.appendCsvString(StringUtils::escapeString(ErrorException::getDescriptionErrorType(e.getErrorType())));
		body.appendCsvString(StringUtils::escapeString(e.getMessage()));
		if (e.getDetails() != "") {
			body.appendCsvString(StringUtils::escapeString(e.getDetails()));
		}
		body.appendEol();

		Logger::warning << "error code: " << (int32_t)e.getErrorType() << " description: " << ErrorException::getDescriptionErrorType(e.getErrorType()) << " message: " << e.getMessage() << endl;

	}
}
}
