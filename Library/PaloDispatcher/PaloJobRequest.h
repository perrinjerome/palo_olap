////////////////////////////////////////////////////////////////////////////////
/// @brief palo job request
///
/// @file
///
/// Copyright (C) 2006-2011 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
/// @author Jiri Junek, qBicon s.r.o., Prague, Czech Republic
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_DISPATCHER_PALO_JOB_REQUEST_H
#define PALO_DISPATCHER_PALO_JOB_REQUEST_H 1

#include "palo.h"

#include "HttpServer/HttpJobRequest.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief palo job request
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS PaloJobRequest : public HttpJobRequest {

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	PaloJobRequest(const string& name);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief destructor
	////////////////////////////////////////////////////////////////////////////////

	~PaloJobRequest();

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets response
	////////////////////////////////////////////////////////////////////////////////

	HttpResponse* getResponse()
	{
		return response;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets response
	////////////////////////////////////////////////////////////////////////////////

	void setResponse(HttpResponse* response)
	{
		this->response = response;
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	void handleDone(Job*);

private:
	void initialize();

public:

	// session
	IdentifierType session;
	bool hasSession;

	// https port
	int httpsPort;

	// directory paths
	string browserPath;

	// tokens
	uint32_t * serverToken;
	uint32_t * databaseToken;
	uint32_t * dimensionToken;
	uint32_t * cubeToken;
	uint32_t * clientCacheToken;

	// identifiers
	IdentifierType cube; // default NO_IDENTIFIER
	IdentifierType database; // default NO_IDENTIFIER
	IdentifierType dimension; // default NO_IDENTIFIER
	IdentifierType element; // default NO_IDENTIFIER
	IdentifierType lock; // default NO_IDENTIFIER
	IdentifierType rule; // default NO_IDENTIFIER
    IdentifierType parent; // default ALL_IDENTIFIERS
    IdentifierType limitStart; // default 0
    IdentifierType limitCount; // default NO_IDENTIFIER

	// booleans
	bool activate; // default TRUE
	bool add; // default FALSE
	bool baseOnly; // default FALSE
	bool complete; // default FALSE
	bool eventProcess; // default TRUE
	bool showAttribute; // default FALSE
	bool showInfo; // default FALSE
	bool showLockInfo; // default FALSE
	bool showNormal; // default TRUE
	bool showRule; // default FALSE
	bool showSystem; // default FALSE
	bool skipEmpty; // default TRUE
	bool useIdentifier; // default FALSE
	bool useRules; // default FALSE
	bool showUserInfo; // default FALSE

	// strings
	string * action;
	string * comment;
	string * condition;
	string * cubeName;
	string * databaseName;
	string * definition;
	string * dimensionName;
	string * elementName;
	string * event;
	string * externPassword;
	string * externalIdentifier;
	string * functions;
	string * newName;
	string * password;
	string * source;
	string * user;
	string * value;

	// unsigned integers
	uint32_t blockSize; // default 1000
	uint32_t mode; // default 0
	uint32_t position; // default 0
	uint32_t splash; // default 1
	uint32_t steps; // default 0
	uint32_t type; // default 0

	// list of identifiers
	vector<IdentifierType> * dimensions;
	vector<IdentifierType> * path;
	vector<IdentifierType> * pathTo;
	vector<IdentifierType> * elements;

	// list of unsigned integers
	vector<uint32_t> * types;

	// list of doubles
	vector<vector<double> > * weights;

	// list of strings
	vector<string> * dimensionsName;
	vector<string> * elementsName;
	vector<string> * pathName;
	vector<string> * pathToName;
	vector<string> * values;

	// list of list of identifiers
	vector<vector<IdentifierType> > * area;
	vector<vector<IdentifierType> > * children;
	vector<vector<IdentifierType> > * paths;

	// list of strings
	vector<vector<string> > * areaName;
	vector<vector<string> > * childrenName;
	vector<vector<string> > * pathsName;

private:
	HttpResponse* response;
};
}

#endif
