////////////////////////////////////////////////////////////////////////////////
/// @brief Palo Job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "PaloDispatcher/PaloJob.h"

#include "Collections/StringBuffer.h"
#include "Olap/AttributesCube.h"
#include "Olap/CubeDimension.h"
#include "Olap/Element.h"
#include "Olap/Lock.h"
#include "Olap/NormalDimension.h"
#include "Olap/PaloSession.h"
#include "Olap/RightsCube.h"
#include "Olap/Rule.h"
#include "Olap/SubsetViewDimension.h"
#include "Olap/SystemDimension.h"
#include "Olap/UserInfoDimension.h"
#include "PaloDispatcher/PaloJobRequest.h"

namespace palo {

// /////////////////////////////////////////////////////////////////////////////
// constructors and destructors
// /////////////////////////////////////////////////////////////////////////////

PaloJob::PaloJob(Server* server, PaloJobRequest* jobRequest) :
	Job(jobRequest), server(server), jobRequest(jobRequest)
{
	response = 0;
	database = 0;
	cube = 0;
	cellPath = 0;
	cellPathTo = 0;
	user = 0;
	dimension = 0;
	element = 0;
	rule = 0;
	cellPaths = 0;
}

PaloJob::~PaloJob()
{
	if (cellPath) {
		delete cellPath;
	}
	if (cellPathTo) {
		delete cellPathTo;
	}
	if (cellPaths) {
		delete cellPaths;
	}
}

// /////////////////////////////////////////////////////////////////////////////
// Job methods
// /////////////////////////////////////////////////////////////////////////////

bool PaloJob::initialize()
{
	response = 0;

	if (jobRequest->hasSession) {
		try {
			PaloSession * session = PaloSession::findSession(jobRequest->session);

			if (session) {
				user = session->getUser();
			}
		} catch (const ErrorException& e) {
			response = new HttpResponse(HttpResponse::BAD);

			StringBuffer& body = response->getBody();

			body.appendCsvInteger((int32_t)e.getErrorType());
			body.appendCsvString(StringUtils::escapeString(ErrorException::getDescriptionErrorType(e.getErrorType())));
			body.appendCsvString(StringUtils::escapeString(e.getMessage()));
			body.appendEol();

			Logger::warning << "error code: " << (int32_t)e.getErrorType() << " description: " << ErrorException::getDescriptionErrorType(e.getErrorType()) << " message: " << e.getMessage() << endl;

			return false;
		}
	} else {
		user = 0;
	}

	return true;
}

// /////////////////////////////////////////////////////////////////////////////
// check token methods
// /////////////////////////////////////////////////////////////////////////////

void PaloJob::checkToken(Server* server)
{
	if (jobRequest->serverToken == 0) {
		return;
	}

	if (server->getToken() != *jobRequest->serverToken) {
		throw ParameterException(ErrorException::ERROR_SERVER_TOKEN_OUTDATED, "server token outdated", "server", "palo server");
	}
}

void PaloJob::checkToken(Database* database)
{
	if (jobRequest->databaseToken == 0) {
		return;
	}

	if (database->getToken() != *jobRequest->databaseToken) {
		throw ParameterException(ErrorException::ERROR_DATABASE_TOKEN_OUTDATED, "database token outdated", "database", database->getName());
	}
}

void PaloJob::checkToken(Dimension* dimension)
{
	if (jobRequest->dimensionToken == 0) {
		return;
	}

	if (dimension->getToken() != *jobRequest->dimensionToken) {
		throw ParameterException(ErrorException::ERROR_DIMENSION_TOKEN_OUTDATED, "dimension token outdated", "dimension", dimension->getName());
	}
}

void PaloJob::checkToken(Cube* cube)
{
	if (jobRequest->cubeToken == 0) {
		return;
	}

	if (cube->getToken() != *jobRequest->cubeToken) {
		throw ParameterException(ErrorException::ERROR_CUBE_TOKEN_OUTDATED, "cube token outdated", "cube", cube->getName());
	}
}

// /////////////////////////////////////////////////////////////////////////////
// generate response
// /////////////////////////////////////////////////////////////////////////////

void PaloJob::generateLoginResponse(PaloSession* session)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	// append session identifier
	body.appendCsvString(session->getEncodedIdentifier());

	// append time to live
	body.appendCsvInteger((uint32_t)session->getTtlIntervall());

	body.appendEol();

	setToken(server);
}

void PaloJob::generateOkResponse()
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	body.appendText("1;");
	body.appendEol();
}

void PaloJob::generateCellValueResponse(const Cube::CellValueType& value, bool found)
{
	response = new HttpResponse(HttpResponse::OK);

	setToken(cube);

	// check for locks
	Cube::CellLockInfo lockInfo = 0;

	if (jobRequest->showLockInfo) {
		IdentifierType userId = user ? user->getIdentifier() : 0;
		lockInfo = cube->getCellLockInfo(cellPath, userId);
	}

	appendCell(response->getBody(), value, jobRequest->showRule, jobRequest->showLockInfo, lockInfo, found);
}

void PaloJob::generateDatabaseResponse(Database* database)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(database);

	appendDatabase(&body, database);
}

void PaloJob::generateDatabasesResponse(Server * server, vector<Database*>* databases, bool showNormal, bool showSystem, bool showUserInfo)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(server);

	for (vector<Database*>::iterator i = databases->begin(); i != databases->end(); i++) {
		Database* database = (*i);

		bool normal = (database->getType() == NORMAL) && showNormal;
		bool system = (database->getType() == SYSTEM) && showSystem;
		bool userinfo = (database->getType() == USER_INFO) && showUserInfo;

		if (normal || system || userinfo) {
			appendDatabase(&body, database);
		}
	}
}

void PaloJob::generateCubeResponse(Cube* cube)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(cube);
	setSecondToken(cube);

	appendCube(&body, cube);
}

void PaloJob::generateCubesResponse(Database* database, vector<Cube*>* cubes, bool showNormal, bool showSystem, bool showAttribute, bool showInfo)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(database);

	for (vector<Cube*>::iterator i = cubes->begin(); i != cubes->end(); i++) {
		Cube* cube = (*i);

		ItemType it = cube->getType();
		bool normal = false;
		bool system = false;
		bool attribute = false;
		bool userinfo = false;

		switch (it) {
		case NORMAL:
			normal = showNormal;
			break;
		case USER_INFO:
			userinfo = showInfo;
			break;
		case SYSTEM: {
			SystemCube* systemCube = dynamic_cast<SystemCube*> (cube);
			SystemCube::SystemCubeType sdt = systemCube->getSubType();

			system = (sdt != SystemCube::ATTRIBUTES_CUBE) && showSystem;
			attribute = (sdt == SystemCube::ATTRIBUTES_CUBE) && showAttribute;
			break;
		}
		default:
			break;
		}

		/*  if (it == NORMAL) {
		 normal = showNormal;
		 }
		 else if (it == USER_INFO) {
		 userinfo = showInfo;
		 }
		 else if (it == SYSTEM) {
		 SystemCube* systemCube =  dynamic_cast<SystemCube*>(cube);
		 SystemCube::SystemCubeType sdt = systemCube->getSubType();

		 system    = (sdt != SystemCube::ATTRIBUTES_CUBE) && showSystem;
		 attribute = (sdt == SystemCube::ATTRIBUTES_CUBE) && showAttribute;
		 }*/

		if (normal || system || attribute || userinfo ) {
			appendCube(&body, cube);
		}
	}
}

void PaloJob::generateDimensionResponse(Dimension* dimension)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(dimension);

	appendDimension(&body, dimension);
}

void PaloJob::generateDimensionsResponse(Database* database, vector<Dimension*>* dimensions, bool showNormal, bool showSystem, bool showAttribute, bool showInfo)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(database);

	for (vector<Dimension*>::iterator i = dimensions->begin(); i != dimensions->end(); i++) {
		Dimension* dimension = (*i);

		ItemType it = dimension->getType();
		bool normal = false;
		bool system = false;
		bool attribute = false;
		bool userInfo = false;

		if (it == NORMAL) {
			normal = showNormal;
		} else if (it == USER_INFO) {
			userInfo = showInfo;
		} else if (it == SYSTEM) {
			SystemDimension* systemDimension = dynamic_cast<SystemDimension*> (dimension);
			SystemDimension::SystemDimensionType sdt = systemDimension->getSubType();

			system = (sdt != SystemDimension::ATTRIBUTE_DIMENSION) && showSystem;
			attribute = (sdt == SystemDimension::ATTRIBUTE_DIMENSION) && showAttribute;
		}

		if (normal || system || attribute || userInfo) {
			appendDimension(&body, dimension);
		}
	}
}

void PaloJob::generateElementResponse(Dimension* dimension, Element* element)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(dimension);

	appendElement(&body, dimension, element);
}

void PaloJob::generateElementsResponse(Dimension* dimension, vector<Element*>* elements)
{
    IdentifierType skipCount = jobRequest->limitStart;
    IdentifierType responseCount = jobRequest->limitCount;

	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(dimension);

    if ( !responseCount ) {
        // return just number of elements in selection
        body.appendInteger( elements->size() );
        body.appendEol();
        return;
    }
	for (vector<Element*>::iterator i = elements->begin(); i != elements->end(); i++) {
        if ( skipCount ) {
            skipCount--;
            continue;
        }
		Element* element = (*i);

		appendElement(&body, dimension, element);

        if ( !--responseCount ) {
            break;
        }
	}
}

void PaloJob::generateElementsValuesResponse(Dimension* dimension, list<Element*>* elements, vector<Cube::CellValueType> *vals)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(dimension);

	vector<Cube::CellValueType>::iterator vit = vals->begin();
	for (list<Element*>::iterator i = elements->begin(); i != elements->end(); i++, vit++) {
		Element* element = (*i);

		appendElement(&body, dimension, element);
		appendCell(body, *vit, false, false, false, true);
	}
}

void PaloJob::generateRuleResponse(Rule* rule, bool useIdentifier)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(cube);

	appendRule(&body, rule, useIdentifier);
}

void PaloJob::generateRulesResponse(Cube* cube, const vector<Rule*>* rules, bool useIdentifier)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(cube);

	for (vector<Rule*>::const_iterator iter = rules->begin(); iter != rules->end(); iter++) {
		appendRule(&body, *iter, useIdentifier);
	}
}

void PaloJob::generateLocksResponse(Server* server, Cube* cube, User* user, bool completeContainsArea)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(cube);

	const vector<Lock*>& locks = cube->getCubeLocks(user);
	for (vector<Lock*>::const_iterator i = locks.begin(); i != locks.end(); i++) {
		appendLock(server, &body, *i, completeContainsArea);
	}
}

void PaloJob::generateLockResponse(Server* server, Cube* cube, Lock* lock, bool completeContainsArea)
{
	response = new HttpResponse(HttpResponse::OK);
	StringBuffer& body = response->getBody();

	setToken(cube);

	if (lock) {
		appendLock(server, &body, lock, completeContainsArea);
	}
}

// /////////////////////////////////////////////////////////////////////////////
// find palo objects
// /////////////////////////////////////////////////////////////////////////////

void PaloJob::findDatabase(bool requireLoad)
{
	if (database) {
		return;
	}

	IdentifierType id = jobRequest->database;

	if (id != NO_IDENTIFIER) {
		database = server->findDatabase(id, user, requireLoad);
	} else if (jobRequest->databaseName) {
		string name = *(jobRequest->databaseName);
		database = server->findDatabaseByName(name, user, requireLoad);
	} else {
		throw ParameterException(ErrorException::ERROR_DATABASE_NOT_FOUND, "database not found", PaloRequestHandler::ID_DATABASE, "");
	}

	checkToken(database);
}

void PaloJob::findCube(bool requireLoad)
{
	findDatabase();

	if (cube) {
		return;
	}

	IdentifierType id = jobRequest->cube;

	if (id != NO_IDENTIFIER) {
		cube = database->findCube(id, user, requireLoad);
	} else if (jobRequest->cubeName) {
		string name = *(jobRequest->cubeName);
		cube = database->findCubeByName(name, user, requireLoad);
	} else {
		throw ParameterException(ErrorException::ERROR_CUBE_NOT_FOUND, "cube not found", PaloRequestHandler::ID_CUBE, "");
	}

	checkToken(cube);
}

void PaloJob::findPath()
{
	findCube();

	const vector<Dimension*> * dimensions = cube->getDimensions();
	size_t numDimensions = dimensions->size();

	if (jobRequest->path) {
		if (jobRequest->path->size() != numDimensions) {
			throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", PaloRequestHandler::ID_PATH, "");
		}

		IdentifiersType path(numDimensions);

		for (size_t i = 0; i < numDimensions; i++) {
			Dimension* d = dimensions->at(i);
			path[i] = d->findElement(jobRequest->path->at(i), 0)->getIdentifier();
		}

		cellPath = new CellPath(cube, &path);
	} else if (jobRequest->pathName) {
		if (jobRequest->pathName->size() != numDimensions) {
			throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", PaloRequestHandler::NAME_PATH, "");
		}

		IdentifiersType path(numDimensions);

		for (size_t i = 0; i < numDimensions; i++) {
			Dimension* d = dimensions->at(i);
			path[i] = d->findElementByName(jobRequest->pathName->at(i), 0)->getIdentifier();
		}

		cellPath = new CellPath(cube, &path);
	} else {
		throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "path is empty, list of element identifiers is missing", PaloRequestHandler::ID_PATH, "");
	}
}

void PaloJob::findPathTo()
{
	findCube();

	const vector<Dimension*> * dimensions = cube->getDimensions();
	size_t numDimensions = dimensions->size();

	if (jobRequest->pathTo) {
		if (jobRequest->pathTo->size() != numDimensions) {
			throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", PaloRequestHandler::ID_PATH_TO, "");
		}

		IdentifiersType path(numDimensions);

		for (size_t i = 0; i < numDimensions; i++) {
			Dimension* d = dimensions->at(i);
			path[i] = d->findElement(jobRequest->pathTo->at(i), 0)->getIdentifier();
		}

		cellPathTo = new CellPath(cube, &path);
	} else if (jobRequest->pathToName) {
		if (jobRequest->pathToName->size() != numDimensions) {
			throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", PaloRequestHandler::NAME_PATH_TO, "");
		}

		IdentifiersType path(numDimensions);

		for (size_t i = 0; i < numDimensions; i++) {
			Dimension* d = dimensions->at(i);
			path[i] = d->findElementByName(jobRequest->pathToName->at(i), 0)->getIdentifier();
		}

		cellPathTo = new CellPath(cube, &path);
	} else {
		throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "path to is empty, list of element identifiers is missing", PaloRequestHandler::ID_PATH_TO, "");
	}
}

void PaloJob::findDimension()
{
	findDatabase();

	if (dimension) {
		return;
	}

	IdentifierType id = jobRequest->dimension;

	if (id != NO_IDENTIFIER) {
		dimension = database->findDimension(id, user);
	} else if (jobRequest->dimensionName) {
		string name = *(jobRequest->dimensionName);
		dimension = database->findDimensionByName(name, user);
	} else {
		throw ParameterException(ErrorException::ERROR_DIMENSION_NOT_FOUND, "dimension not found", PaloRequestHandler::ID_DIMENSION, "");
	}

	checkToken(dimension);
}

void PaloJob::findElement()
{
	findDimension();

	if (element) {
		return;
	}

	IdentifierType id = jobRequest->element;

	if (id != NO_IDENTIFIER) {
		element = dimension->findElement(id, user);
	} else if (jobRequest->elementName) {
		string name = *(jobRequest->elementName);
		element = dimension->findElementByName(name, user);
	} else {
		throw ParameterException(ErrorException::ERROR_ELEMENT_NOT_FOUND, "element not found", PaloRequestHandler::ID_ELEMENT, "");
	}
}

void PaloJob::findRule()
{
	findCube();

	rule = cube->findRule(jobRequest->rule, user);
}

void PaloJob::findPaths()
{
	findCube();

	IdentifiersType emptyPath;

	const vector<Dimension*> * dimensions = cube->getDimensions();
	size_t numDimensions = dimensions->size();

	if (jobRequest->paths) {
		cellPaths = new vector<IdentifiersType> ();

		for (size_t j = 0; j < jobRequest->paths->size(); j++) {
			IdentifiersType& it = jobRequest->paths->at(j);

			if (it.size() != numDimensions) {
				throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", PaloRequestHandler::ID_PATHS, "");
			}

			try {
				IdentifiersType path(numDimensions);

				for (size_t i = 0; i < numDimensions; i++) {
					Dimension* d = dimensions->at(i);
					path[i] = d->findElement(it.at(i), 0)->getIdentifier();
				}

				cellPaths->push_back(path);
			} catch (const ParameterException&) {
				cellPaths->push_back(emptyPath);
			}

		}

	} else if (jobRequest->pathsName) {
		cellPaths = new vector<IdentifiersType> ();

		for (size_t j = 0; j < jobRequest->pathsName->size(); j++) {
			vector<string>& names = jobRequest->pathsName->at(j);

			if (names.size() != numDimensions) {
				throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", PaloRequestHandler::NAME_PATHS, "");
			}

			try {
				IdentifiersType path(numDimensions);

				for (size_t i = 0; i < numDimensions; i++) {
					Dimension* d = dimensions->at(i);
					path[i] = d->findElementByName(names.at(i), 0)->getIdentifier();
				}

				cellPaths->push_back(path);
			} catch (const ParameterException&) {
				cellPaths->push_back(emptyPath);
			}

		}

	} else {
		throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "path is empty, list of element identifiers is missing", PaloRequestHandler::ID_PATHS, "");
	}
}

// /////////////////////////////////////////////////////////////////////////////
// check permissions
// /////////////////////////////////////////////////////////////////////////////

void PaloJob::checkPathPermission(CellPath* cp, RightsType minimumRight)
{
	cube->checkPathAccessRight(user, cp, minimumRight);
}

// /////////////////////////////////////////////////////////////////////////////
// private methods
// /////////////////////////////////////////////////////////////////////////////

void PaloJob::appendDatabase(StringBuffer* sb, Database* database)
{
	sb->appendCsvInteger((int32_t)database->getIdentifier());
	sb->appendCsvString(StringUtils::escapeString(database->getName()));
	sb->appendCsvInteger((int32_t)database->sizeDimensions());
	sb->appendCsvInteger((int32_t)database->sizeCubes());
	sb->appendCsvInteger((int32_t)database->getStatus());
	sb->appendCsvInteger((int32_t)database->getType());
	sb->appendCsvInteger((int32_t)database->getToken());
	sb->appendEol();
}

void PaloJob::appendDimension(StringBuffer* sb, Dimension* dimension)
{
	sb->appendCsvInteger((int32_t)dimension->getIdentifier());
	sb->appendCsvString(StringUtils::escapeString(dimension->getName()));
	sb->appendCsvInteger((int32_t)dimension->sizeElements());
	sb->appendCsvInteger((int32_t)dimension->getLevel());
	sb->appendCsvInteger((int32_t)dimension->getIndent());
	sb->appendCsvInteger((int32_t)dimension->getDepth());

	ItemType it = dimension->getType();

	// dimension is a normal dimension
	if (it == NORMAL) {
		NormalDimension* nd = dynamic_cast<NormalDimension*> (dimension);

		sb->appendCsvInteger((int32_t)0);

		AttributesDimension* ad = nd->getAttributesDimension();

		if (ad) {
			sb->appendCsvInteger((int32_t)ad->getIdentifier());
		} else {
			sb->appendChar(';');
		}

		AttributesCube* ac = nd->getAttributesCube();

		if (ac) {
			sb->appendCsvInteger((int32_t)ac->getIdentifier());
		} else {
			sb->appendCsvString("");
		}

		RightsCube* rc = nd->getRightsCube();

		if (rc) {
			sb->appendCsvInteger((int32_t)rc->getIdentifier());
		} else {
			sb->appendCsvString("");
		}

	}

	// dimension is a user dimension
	else if (it == USER_INFO) {
		sb->appendCsvInteger((int32_t)3);
		UserInfoDimension* cd = dynamic_cast<UserInfoDimension*> (dimension);

		if (cd) {
			AttributesDimension* ad = cd->getAttributesDimension();

			if (ad) {
				sb->appendCsvInteger((int32_t)ad->getIdentifier());
			} else {
				sb->appendChar(';');
			}

			AttributesCube* ac = cd->getAttributesCube();

			if (ac) {
				sb->appendCsvInteger((int32_t)ac->getIdentifier());
			} else {
				sb->appendCsvString("");
			}

			// ";" for rights cube
			sb->appendChar(';');
		} else {
			// three ";" for attributes dimension and attributes and rights cube
			sb->appendChar(';');
			sb->appendChar(';');
			sb->appendChar(';');
		}
	}

	// dimension is a system dimension
	else {
		SystemDimension* systemDimension = dynamic_cast<SystemDimension*> (dimension);

		// cube dimension has attributes dimension and cube
		if (systemDimension->getSubType() == SystemDimension::CUBE_DIMENSION) {
			sb->appendCsvInteger((int32_t)1);
			CubeDimension* cd = dynamic_cast<CubeDimension*> (dimension);

			if (cd) {
				AttributesDimension* ad = cd->getAttributesDimension();

				if (ad) {
					sb->appendCsvInteger((int32_t)ad->getIdentifier());
				} else {
					sb->appendChar(';');
				}

				AttributesCube* ac = cd->getAttributesCube();

				if (ac) {
					sb->appendCsvInteger((int32_t)ac->getIdentifier());
				} else {
					sb->appendCsvString("");
				}

				// ";" for rights cube
				sb->appendChar(';');
			} else {
				// three ";" for attributes dimension and attributes and rights cube
				sb->appendChar(';');
				sb->appendChar(';');
				sb->appendChar(';');
			}
		}

		// subset and view dimensions have attributes dimension and cube
		else if (systemDimension->getSubType() == SystemDimension::SUBSET_VIEW_DIMENSION) {
			sb->appendCsvInteger((int32_t)1);
			SubsetViewDimension* cd = dynamic_cast<SubsetViewDimension*> (dimension);

			if (cd) {
				AttributesDimension* ad = cd->getAttributesDimension();

				if (ad) {
					sb->appendCsvInteger((int32_t)ad->getIdentifier());
				} else {
					sb->appendChar(';');
				}

				AttributesCube* ac = cd->getAttributesCube();

				if (ac) {
					sb->appendCsvInteger((int32_t)ac->getIdentifier());
				} else {
					sb->appendCsvString("");
				}

				// ";" for rights cube
				sb->appendChar(';');
			} else {
				// three ";" for attributes dimension and attributes and rights cube
				sb->appendChar(';');
				sb->appendChar(';');
				sb->appendChar(';');
			}
		}

		// other system dimensions
		else {

			// attributes dimension is treated as a normal dimension
			if (systemDimension->getSubType() == SystemDimension::ATTRIBUTE_DIMENSION) {
				sb->appendCsvInteger((int32_t)2);

				AttributesDimension* a = dynamic_cast<AttributesDimension*> (dimension);
				NormalDimension* nd = a->getNormalDimension();

				if (nd) {
					sb->appendCsvInteger((int32_t)nd->getIdentifier());
				} else {
					sb->appendChar(';');
				}
			} else {
				// other system dimensions
				sb->appendCsvInteger((int32_t)1);
				sb->appendChar(';');
			}

			// two ";" for attributes and rights cube
			sb->appendChar(';');
			sb->appendChar(';');
		}
	}

	sb->appendCsvInteger((int32_t)dimension->getToken());

	sb->appendEol();
}

void PaloJob::appendElement(StringBuffer* sb, Dimension* dimension, Element* element)
{
	sb->appendCsvInteger((int32_t)element->getIdentifier());
	sb->appendCsvString(StringUtils::escapeString(element->getName()));
	sb->appendCsvInteger((int32_t)element->getPosition());
	sb->appendCsvInteger((int32_t)element->getLevel(dimension));
	sb->appendCsvInteger((int32_t)element->getIndent(dimension));
	sb->appendCsvInteger((int32_t)element->getDepth(dimension));
	sb->appendCsvInteger((int32_t)element->getElementType());

	const Dimension::ParentsType* parents = dimension->getParents(element);

	sb->appendCsvInteger((int32_t)parents->size());

	// append parent identifier
	bool b = false;

	for (Dimension::ParentsType::const_iterator pi = parents->begin(); pi != parents->end(); pi++) {
		if (b) {
			sb->appendChar(',');
		}

		sb->appendInteger((uint32_t)(*pi)->getIdentifier());
		b = true;
	}

	sb->appendChar(';');

	// append children identifier
	const ElementsWeightType children = dimension->getChildren(element);

	sb->appendCsvInteger((int32_t)children.size());

	b = false;

	for (ElementsWeightType::const_iterator ci = children.begin(); ci != children.end(); ci++) {
		if (b) {
			sb->appendChar(',');
		}

		sb->appendInteger((uint32_t)(*ci).first->getIdentifier());
		b = true;
	}

	sb->appendChar(';');

	// append children weight
	b = false;

	for (ElementsWeightType::const_iterator cw = children.begin(); cw != children.end(); cw++) {
		if (b) {
			sb->appendChar(',');
		}

		sb->appendDecimal((*cw).second);
		b = true;
	}

	sb->appendChar(';');
	sb->appendEol();

}

void PaloJob::appendCube(StringBuffer* sb, Cube* cube)
{
	sb->appendCsvInteger((int32_t)cube->getIdentifier());
	sb->appendCsvString(StringUtils::escapeString(cube->getName()));

	const vector<Dimension*>* dimensions = cube->getDimensions();

	sb->appendCsvInteger((int32_t)dimensions->size());

	bool b = false;
	uint64_t sizeCube = 1;

	for (vector<Dimension*>::const_iterator pi = dimensions->begin(); pi != dimensions->end(); pi++) {
		Dimension* dimension = *pi;

		if (b) {
			sb->appendChar(',');
		}

		sb->appendInteger(dimension->getIdentifier());
		b = true;

		sizeCube *= dimension->sizeElements();
	}

	sb->appendChar(';');

	sb->appendCsvInteger(sizeCube);
	sb->appendCsvInteger(cube->sizeFilledCells());

	sb->appendCsvInteger((int32_t)cube->getStatus());

	ItemType it = cube->getType();

	switch (it) {
	case USER_INFO:
		sb->appendCsvInteger((int32_t)3);
		break;
	case NORMAL:
		sb->appendCsvInteger((int32_t)0);
		break;
	case SYSTEM: {
		SystemCube* systemCube = dynamic_cast<SystemCube*> (cube);

		switch (systemCube->getSubType()) {
		case SystemCube::ATTRIBUTES_CUBE:
			sb->appendCsvInteger((uint32_t)2);
			break;
		default:
			sb->appendCsvInteger((uint32_t)1);
		}
	}
	default:
		break;
	}

	//	if (it == USER_INFO) {
	//      sb->appendCsvInteger((int32_t) 3);
	//    }
	//    else if (it != SYSTEM) {
	//      sb->appendCsvInteger((int32_t) 0);
	//    }
	//    else {
	//      SystemCube* systemCube =  dynamic_cast<SystemCube*>(cube);
	//
	//      switch (systemCube->getSubType()) {
	//        case SystemCube::ATTRIBUTES_CUBE :
	//          sb->appendCsvInteger((int32_t) 2);
	//          break;
	//
	//        default:
	//          sb->appendCsvInteger((int32_t) 1);
	//      }
	//    }

	sb->appendCsvInteger((int32_t)cube->getToken());

	sb->appendEol();
}

void PaloJob::appendRule(StringBuffer* sb, Rule* rule, bool useIdentifier)
{
	sb->appendCsvInteger((int32_t)rule->getIdentifier());

	StringBuffer sb2;
	sb2.initialize();
	rule->appendRepresentation(&sb2, !useIdentifier);
	sb->appendCsvString(StringUtils::escapeString(sb2.c_str()));
	sb2.free();

	sb->appendCsvString(StringUtils::escapeString(rule->getExternal()));
	sb->appendCsvString(StringUtils::escapeString(rule->getComment()));
	sb->appendCsvInteger((int32_t)rule->getTimeStamp());

	if (rule->isActive()) {
		sb->appendCsvString("1");
	} else {
		sb->appendCsvString("0");
	}

	sb->appendEol();
}

void PaloJob::appendLock(Server* server, StringBuffer* sb, Lock* lock, bool completeContainsArea)
{
	sb->appendCsvInteger((int32_t)lock->getIdentifier());
	if (completeContainsArea)
		appendArea(sb, lock->getContainsArea());
	else
		sb->appendCsvString(lock->getAreaString());

	// we need a system database
	SystemDatabase* sd = server->getSystemDatabase();

	User* user = sd->getUser(lock->getUserIdentifier());
	if (user) {
		sb->appendCsvString(user->getName());
	} else {
		sb->appendCsvString("");
	}
	sb->appendCsvInteger((int32_t)lock->getStorage()->getNumberSteps());

	sb->appendEol();
}

void PaloJob::appendArea(StringBuffer* sb, const vector<set<IdentifierType> > &area)
{
	//1:2:3,4:5:6,...
	for (vector<set<IdentifierType> >::const_iterator dit = area.begin(); dit != area.end(); dit++) {
		if (dit != area.begin())
			sb->appendChar(',');
		for (set<IdentifierType>::const_iterator it = dit->begin(); it != dit->end(); it++) {
			if (it != dit->begin())
				sb->appendChar(':');
			sb->appendInteger(*it);
		}
	}
	sb->appendChar(';');
}

void PaloJob::appendArea(StringBuffer* sb, const vector<IdentifiersType> &area)
{
	//1:2:3,4:5:6,...
	for (vector<IdentifiersType>::const_iterator dit = area.begin(); dit != area.end(); dit++) {
		if (dit != area.begin())
			sb->appendChar(',');
		for (IdentifiersType::const_iterator it = dit->begin(); it != dit->end(); it++) {
			if (it != dit->begin())
				sb->appendChar(':');
			sb->appendInteger(*it);
		}
	}
	sb->appendChar(';');
}

void PaloJob::appendCell(StringBuffer& body, const Cube::CellValueType& value, bool showRule, bool showLockInfo, Cube::CellLockInfo lockInfo, bool found)
{
	string s;

	body.appendCsvInteger((int32_t)value.type);

	if (found) {
		body.appendCsvString("1");
		switch (value.type) {
		case NUMERIC:
			body.appendCsvDouble(value.doubleValue);
			break;

		case STRING:
			s = value.charValue;
			body.appendCsvString(StringUtils::escapeString(s));
			break;

		default:
			body.appendCsvString("");
			break;
		}
	} else {
		body.appendCsvString("0;");
	}

	if (showRule) {
		if (value.rule != Rule::NO_RULE) {
			body.appendCsvInteger((uint32_t)value.rule);
		} else {
			body.appendChar(';');
		}
	}
	if (showLockInfo) {
		body.appendCsvInteger(lockInfo);
	}

	body.appendEol();
}

///////////////////////////////////////////////////////////////////////////////
// helper methods
///////////////////////////////////////////////////////////////////////////////

ElementType PaloJob::elementTypeByIdentifier(uint32_t type)
{
	ElementType elementType;

	if (type == 1) {
		elementType = NUMERIC;
	} else if (type == 2) {
		elementType = STRING;
	} else if (type == 4) {
		elementType = CONSOLIDATED;
	} else {
		elementType = UNDEFINED;
	}

	if (elementType == UNDEFINED) {
		throw ParameterException(ErrorException::ERROR_INVALID_ELEMENT_TYPE, "wrong value for element type", PaloRequestHandler::ID_TYPE, type);
	}

	return elementType;
}

vector<IdentifiersType> PaloJob::area(vector<IdentifiersType>* paths, const vector<Dimension*> * dimensions, uint32_t& numResult, bool useBaseOnly)
{
	if (dimensions->size() != paths->size()) {
		throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of area elements", PaloRequestHandler::ID_AREA, "");
	}

	size_t numDimensions = dimensions->size();

	vector<IdentifiersType> area(numDimensions);

	numResult = 1;

	for (uint32_t i = 0; i < numDimensions; i++) {
		IdentifiersType path;

		if (paths->at(i).size() == 0) {
			vector<Element*> elements = dimensions->at(i)->getElements(0);

			for (vector<Element*>::iterator j = elements.begin(); j != elements.end(); j++) {
				Element * e = *j;

				if (!(useBaseOnly && e->getElementType() == CONSOLIDATED)) {
					path.push_back(e->getIdentifier());
				}
			}
		} else {
			for (size_t j = 0; j < paths->at(i).size(); j++) {
				IdentifierType id = (*dimensions)[i]->findElement(paths->at(i).at(j), 0)->getIdentifier();
				path.push_back(id);
			}
		}

		numResult *= (uint32_t)path.size();
		area[i] = path;
	}

	return area;
}

vector<IdentifiersType> PaloJob::area(vector<vector<string> >* paths, const vector<Dimension*> * dimensions, uint32_t& numResult, bool useBaseOnly)
{
	if (dimensions->size() != paths->size()) {
		throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of area elements", PaloRequestHandler::ID_AREA, "");
	}

	size_t numDimensions = dimensions->size();

	vector<IdentifiersType> area(numDimensions);

	numResult = 1;

	for (uint32_t i = 0; i < numDimensions; i++) {
		IdentifiersType path;

		if (paths->at(i).size() == 0) {
			vector<Element*> elements = dimensions->at(i)->getElements(0);

			for (vector<Element*>::iterator j = elements.begin(); j != elements.end(); j++) {
				Element * e = *j;

				if (!(useBaseOnly && e->getElementType() == CONSOLIDATED)) {
					path.push_back(e->getIdentifier());
				}
			}
		} else {
			for (size_t j = 0; j < paths->at(i).size(); j++) {
				IdentifierType id = (*dimensions)[i]->findElementByName(paths->at(i).at(j), 0)->getIdentifier();
				path.push_back(id);
			}
		}

		numResult *= (uint32_t)path.size();
		area[i] = path;
	}

	return area;
}

string PaloJob::areaToString(vector<vector<string> >* paths)
{
	string result = "";
	StringBuffer sb;
	sb.initialize();

	for (uint32_t i = 0; i < paths->size(); i++) {
		if (i > 0) {
			if (i > 0)
				sb.appendChar(',');
		}

		IdentifiersType path;

		if (paths->at(i).size() == 0) {
			sb.appendChar('*');
		} else {
			for (size_t j = 0; j < paths->at(i).size(); j++) {
				if (j > 0)
					sb.appendChar(':');
				sb.appendText(paths->at(i).at(j));
			}
		}
	}

	result = sb.c_str();
	sb.free();
	return result;

}

string PaloJob::areaToString(vector<IdentifiersType>* paths)
{
	string result = "";
	StringBuffer sb;
	sb.initialize();

	for (uint32_t i = 0; i < paths->size(); i++) {
		if (i > 0) {
			if (i > 0)
				sb.appendChar(',');
		}

		IdentifiersType path;

		if (paths->at(i).size() == 0) {
			sb.appendChar('*');
		} else {
			for (size_t j = 0; j < paths->at(i).size(); j++) {
				if (j > 0)
					sb.appendChar(':');
				IdentifierType id = paths->at(i).at(j);
				sb.appendInteger(id);
			}
		}
	}

	result = sb.c_str();
	sb.free();
	return result;

}

Cube::SplashMode PaloJob::splashMode(uint32_t splash)
{
	switch (splash) {
	case 0:
		return Cube::DISABLED;
	case 1:
		return Cube::DEFAULT;
	case 2:
		return Cube::ADD_BASE;
	case 3:
		return Cube::SET_BASE;
	default:
		throw ParameterException(ErrorException::ERROR_INVALID_SPLASH_MODE, "wrong value for splash mode", PaloRequestHandler::SPLASH, splash);
	}

}

}
