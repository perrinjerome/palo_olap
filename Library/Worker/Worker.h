////////////////////////////////////////////////////////////////////////////////
/// @brief worker
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef WORKER_WORKER_H
#define WORKER_WORKER_H 1

#include "palo.h"

#include "Collections/StringBuffer.h"
#include "Thread/Mutex.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief abstract base class for worker
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS Worker {
public:
	static const int WORKER_TIMEOUT_MSEC = 2000;

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	Worker(const string& session);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief destructor
	////////////////////////////////////////////////////////////////////////////////

	virtual ~Worker();

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets executable for workers
	////////////////////////////////////////////////////////////////////////////////

	static void setExecutable(const string& executable);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets arguments for workers
	////////////////////////////////////////////////////////////////////////////////

	static void setArguments(const vector<string>& arguments);

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief starts worker process and sets up communication
	////////////////////////////////////////////////////////////////////////////////

	virtual bool start();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sends a request to the worker and reads result
	////////////////////////////////////////////////////////////////////////////////

	virtual ResultStatus execute(const string& line, vector<string>& result);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sends a request to the worker and reads result
	////////////////////////////////////////////////////////////////////////////////

	virtual ResultStatus execute(const string& line, vector<string>& result, time_t timeout);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sends a terminate request to the worker and kills the process
	////////////////////////////////////////////////////////////////////////////////

	virtual void terminate(const string& line, time_t timeout);

protected:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief path to worker executable
	////////////////////////////////////////////////////////////////////////////////

	static string executable;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief arguments for worker
	////////////////////////////////////////////////////////////////////////////////

	static vector<string> arguments;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief maximal number of failures
	////////////////////////////////////////////////////////////////////////////////

	static int maxFailures;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief result lines
	////////////////////////////////////////////////////////////////////////////////

	vector<string> result;

private:
#if defined(_MSC_VER)

	bool startProcess (const string& executable, HANDLE, HANDLE);

	bool createPipes (HANDLE&, HANDLE&, HANDLE&, HANDLE&);

#else

	bool startProcess(const string& executable, char** args, int*, int*);

	bool createPipes(int*, int*);

#endif

	bool startProcess();

	void terminateProcess();

	bool sendSession();

	bool sendLine(const string& line, time_t timeout);

	bool restart();

	bool readLine(string& line, time_t timeout);

	bool fillReadBuffer(time_t timeout);

	bool readResult(time_t timeout);

protected:
	Mutex mutex;
	WorkerStatus status;

private:
	bool started;
	int numFailures;

	string session;

	StringBuffer readBuffer;
	size_t readPosition;

#if defined(_MSC_VER)

	HANDLE processHandle;
	HANDLE hChildStdoutRd;
	HANDLE hChildStdinWr;

	string commandLine;

#else

	socket_t writePipe;
	socket_t readPipe;
	pid_t processPid;

	char** args;

#endif
};
}

#endif
