////////////////////////////////////////////////////////////////////////////////
/// @brief cube worker
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "Worker/CubeWorker.h"

#include "Collections/StringBuffer.h"
#include "Collections/StringUtils.h"

#include "Exceptions/WorkerException.h"
#include "Logger/Logger.h"

#include "Olap/Cube.h"
#include "Olap/Database.h"
#include "Olap/Server.h"

#include <iterator>

namespace palo {
bool CubeWorker::useWorkers = false;

// /////////////////////////////////////////////////////////////////////////////
// constructors and destructors
// /////////////////////////////////////////////////////////////////////////////

CubeWorker::CubeWorker(const string& sessionString, Cube* cube) :
	Worker(sessionString), shutdownInProgress(false), cube(cube)
{
}

// /////////////////////////////////////////////////////////////////////////////
// getters and setters
// /////////////////////////////////////////////////////////////////////////////

void CubeWorker::setUseCubeWorker(bool use)
{
	useWorkers = use;
}

bool CubeWorker::useCubeWorker()
{
	return useWorkers;
}

// /////////////////////////////////////////////////////////////////////////////
// public methods
// /////////////////////////////////////////////////////////////////////////////

ResultStatus CubeWorker::setCellValue(const string& areaIdentifier, const string& sessionIdentifier, const IdentifiersType& path, double value)
{
	IdentifierType databaseId = cube->getDatabase()->getIdentifier();
	IdentifierType cubeId = cube->getIdentifier();

	StringBuffer sb;
	sb.initialize();

	sb.appendText("DOUBLE;");
	sb.appendInteger(databaseId);
	sb.appendChar(';');
	sb.appendInteger(cubeId);
	sb.appendChar(';');
	sb.appendText(StringUtils::escapeString(areaIdentifier));
	sb.appendChar(';');
	sb.appendText(StringUtils::escapeString(sessionIdentifier));
	sb.appendChar(';');
	sb.appendText(buildPathString(&path));
	sb.appendChar(';');
	sb.appendDecimal(value);

	// we need to change the job-type
	//    dispatcher->downgradeCurrentJobs();

	// send set-cell-value request to worker
	vector<string> result;

	ResultStatus status = execute(sb.c_str(), result);

	if (status != RESULT_OK) {
		return status;
	}

	if (result.size() >= 1 && result[0].substr(0, 6) == "ERROR;") {
		throw WorkerException(result[0].substr(6));
	}

	return RESULT_OK;
}

ResultStatus CubeWorker::setCellValue(const string& areaIdentifier, const string& sessionIdentifier, const IdentifiersType& path, const string& value)
{
	IdentifierType databaseId = cube->getDatabase()->getIdentifier();
	IdentifierType cubeId = cube->getIdentifier();

	StringBuffer sb;
	sb.initialize();

	sb.appendText("STRING;");
	sb.appendInteger(databaseId);
	sb.appendChar(';');
	sb.appendInteger(cubeId);
	sb.appendChar(';');
	sb.appendText(StringUtils::escapeString(areaIdentifier));
	sb.appendChar(';');
	sb.appendText(StringUtils::escapeString(sessionIdentifier));
	sb.appendChar(';');
	sb.appendText(buildPathString(&path));
	sb.appendChar(';');
	sb.appendText(StringUtils::escapeString(value));

	// we need to change the job-type
	//dispatcher->downgradeCurrentJobs();

	// send set-cell-value request to worker
	vector<string> result;

	ResultStatus status = execute(sb.c_str(), result);

	if (status != RESULT_OK) {
		return status;
	}

	if (result.size() >= 1 && result[0].substr(0, 6) == "ERROR;") {
		throw WorkerException(result[0].substr(6));
	}

	return RESULT_OK;
}

ResultStatus CubeWorker::notifyShutdown()
{
	shutdownInProgress = true;

	terminate("TERMINATE", WORKER_TIMEOUT_MSEC);

	return RESULT_OK;
}

// /////////////////////////////////////////////////////////////////////////////
// Worker methods
// /////////////////////////////////////////////////////////////////////////////

bool CubeWorker::start()
{
	if (shutdownInProgress) {
		return false;
	}

	{
		WriteLocker locker(&mutex);

		if (status == WORKER_RUNNING) {
			return true;
		}
	}

	Logger::trace << "starting cube worker for cube '" << cube->getName() << "'" << endl;

	bool ok = Worker::start();

	if (ok) {
		ResultStatus status = defineCubeAreas();

		if (status == RESULT_FAILED) {
			ok = false;
		}
	}

	return ok;
}

// /////////////////////////////////////////////////////////////////////////////
// private methods
// /////////////////////////////////////////////////////////////////////////////

ResultStatus CubeWorker::defineCubeAreas()
{
	vector<string> areaIds;
	vector<vector<IdentifiersType> > areas;

	message.clear();

	IdentifierType databaseId = cube->getDatabase()->getIdentifier();
	IdentifierType cubeId = cube->getIdentifier();

	vector<string> result;

	// send area request to cube worker
	ResultStatus status = execute("CUBE;" + StringUtils::convertToString(databaseId) + ";" + StringUtils::convertToString(cubeId), result);

	if (status == RESULT_FAILED) {
		return status;
	}

	// check result
	if (result.size() > 0) {
		if (result.at(0).substr(0, 6) == "ERROR;") {
			message = result[0].substr(6);
			return RESULT_FAILED;
		} else {
			status = readAreaLines(result, &areaIds, &areas);

			if (status == RESULT_FAILED) {
				notifyShutdown();
				return status;
			}
		}
	}

	if (areaIds.empty()) {
		Logger::info << "cube '" << cube->getName() << "' has no worker areas, shutting down worker" << endl;
		notifyShutdown();
		cube->removeWorker();
		return RESULT_OK;
	}

	// check areas
	bool ok = checkAreas(&areaIds, &areas);

	if (ok) {
		Logger::info << "got area for cube '" << cube->getName() << "'" << endl;

		if (Logger::isTrace()) {
			for (vector<string>::const_iterator i = result.begin(); i != result.end(); i++) {
				Logger::trace << *i << endl;
			}
		}

		cube->setWorkerAreas(&areaIds, &areas);
	} else {
		notifyShutdown();
		cube->removeWorker();
		return RESULT_FAILED;
	}

	return notifyAreaBuildOk();
}

ResultStatus CubeWorker::notifyAreaBuildOk()
{
	vector<string> result;

	return execute("AREA-BUILD;OK", result);
}

ResultStatus CubeWorker::notifyAreaBuildError(const string& id1, const string& id2)
{
	vector<string> result;

	return execute("AREA-BUILD;ERROR;" + StringUtils::escapeString(id1) + ";" + StringUtils::escapeString(id2), result);
}

string CubeWorker::buildPathString(const IdentifiersType* path)
{
	bool first = true;
	string pathString;

	for (IdentifiersType::const_iterator i = path->begin(); i != path->end(); i++) {
		if (first) {
			first = false;
		} else {
			pathString += ",";
		}

		pathString += StringUtils::convertToString(*i);
	}

	return pathString;
}

ResultStatus CubeWorker::readAreaLines(const vector<string>& result, vector<string>* areaIds, vector<vector<IdentifiersType> >* areas)
{
	for (vector<string>::const_iterator i = result.begin(); i != result.end(); i++) {
		vector<string> values;
		StringUtils::splitString(*i, &values, ';');

		// check for AREA string
		if (values[0] == "AREA" || values.size() < 6) {
			string areaId;
			vector<IdentifiersType> area;

			try {
				bool ok = computeArea(values, areaId, &area);

				if (ok) {
					if (area.size() > 0) {
						areaIds->push_back(areaId);
						areas->push_back(area);
					}
				} else {
					Logger::error << "error in worker response AREA: '" << *i << "'" << endl;
					return RESULT_FAILED;
				}
			} catch (...) {
				Logger::error << "error in worker response AREA: '" << *i << "'" << endl;
				return RESULT_FAILED;
			}
		} else {
			Logger::error << "error in worker response: '" << *i << "'" << endl;
			return RESULT_FAILED;
		}
	}

	return RESULT_OK;
}

bool CubeWorker::computeArea(vector<string>& answer, string& areadId, vector<IdentifiersType>* area)
{

	// has no area
	// AREA;<id_database>;<id_cube>

	// has area
	// AREA;<id_database>;<id_cube>;<id_area>;<list_elements>;<list_elements>;...;<list_elements>

	if (answer.size() < 4) {
		return false;
	}

	// we start one worker for each normal cube, so we can check the cube and databse identifier here
	IdentifierType databaseId = StringUtils::stringToInteger(answer[2]);

	if (databaseId != cube->getDatabase()->getIdentifier()) {
		Logger::error << "wrong database id '" << databaseId << "' in worker response" << endl;
		return false;
	}

	IdentifierType cubeId = StringUtils::stringToInteger(answer[3]);

	if (cubeId != cube->getIdentifier()) {
		Logger::error << "wrong cube id '" << cubeId << "' in worker response" << endl;
		return false;
	}

	// get area identifier and dimension elements
	if (answer.size() > 4) {
		areadId = answer[1];

		for (int i = 4; i < (int)answer.size(); i++) {

			// use empty vector for all elements
			if (answer[i] == "*") {
				vector<IdentifierType> ids;
				area->push_back(ids);
			}

			// convert to identifiers
			else {
				vector<string> idStrings;
				StringUtils::splitString(answer[i], &idStrings, ',');

				vector<IdentifierType> ids;

				for (vector<string>::iterator i = idStrings.begin(); i != idStrings.end(); i++) {
					ids.push_back(StringUtils::stringToInteger(*i));
				}

				area->push_back(ids);
			}
		}
	}

	return true;
}

bool CubeWorker::checkAreas(vector<string>* areaIds, vector<vector<IdentifiersType> >* areas)
{
	const vector<Dimension*>* dimensions = cube->getDimensions();
	size_t numDimension = dimensions->size();

	size_t num = areas->size();

	for (size_t i = 0; i < num; i++) {
		vector<IdentifiersType> area = areas->at(i);

		if (areaIds->at(i) == "") {
			Logger::error << "name of area is empty" << endl;
			return false;
		}

		if (numDimension != area.size()) {
			Logger::error << "error in size of dimension elements in AREA: '" << areaIds->at(i) << "'" << endl;
			return false;
		}
	}

	// check for overlapping areas
	for (size_t i = 0; i < num - 1; i++) {
		for (size_t j = i + 1; j < num; j++) {
			bool overlaps = isOverlapping(&(areas->at(i)), &(areas->at(j)));

			if (overlaps) {
				Logger::error << "error overlapping area '" << areaIds->at(i) << "' and '" << areaIds->at(j) << "'" << endl;
				notifyAreaBuildError(areaIds->at(i), areaIds->at(j));
				return false;
			}
		}
	}

	return true;
}

bool CubeWorker::isOverlapping(vector<IdentifiersType>* area1, vector<IdentifiersType>* area2)
{
	vector<IdentifiersType>::iterator a1 = area1->begin();
	vector<IdentifiersType>::iterator a2 = area2->begin();

	for (; a1 != area1->end(); a1++, a2++) {
		if (a1->size() > 0 && a2->size() > 0) {
			sort(a1->begin(), a1->end());
			sort(a2->begin(), a2->end());

			vector<IdentifierType> result;
			insert_iterator<vector<IdentifierType> > resIns(result, result.begin());

			set_intersection(a1->begin(), a1->end(), a2->begin(), a2->end(), resIns);

			// result is empty, no overlapping possible
			if (result.size() == 0) {
				return false;
			}

		}
	}
	return true;
}
}
