////////////////////////////////////////////////////////////////////////////////
/// @brief login worker
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "Worker/LoginWorker.h"

#include "Collections/StringUtils.h"
#include "Logger/Logger.h"
#include "Olap/CellPath.h"

namespace palo {

// /////////////////////////////////////////////////////////////////////////////
// Worker methods
// /////////////////////////////////////////////////////////////////////////////

bool LoginWorker::start()
{
	if (shutdownInProgress) {
		return false;
	}

	Logger::trace << "starting login worker" << endl;

	return Worker::start();
}

// /////////////////////////////////////////////////////////////////////////////
// public methods
// /////////////////////////////////////////////////////////////////////////////

ResultStatus LoginWorker::loginInformation(const string& username, const string& password)
{
	vector<string> result;

	return execute("LOGIN;" + StringUtils::escapeString(username), result);
}

ResultStatus LoginWorker::authenticateUser(const string& username, const string& password, bool* canLogin)
{
	vector<string> result;

	ResultStatus status = execute("AUTHENTICATION;" + StringUtils::escapeString(username) + ";" + StringUtils::escapeString(password), result);

	if (status != RESULT_OK) {
		return status;
	}

	if (result.size() != 1) {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer of login worker");
	}

	vector<string> answer;
	string line = result[0];
	StringUtils::splitString(line, &answer, ';');

	if (answer.size() != 2 && answer.size() != 4) {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer '" + line + "' of login worker");
	}

	if (answer[0] != "LOGIN") {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer '" + line + "' of login worker");
	}

	if (answer[1] == "TRUE") {
		*canLogin = true;
	} else if (answer[1] == "FALSE") {
		*canLogin = false;
	} else {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer '" + line + "' of login worker");
	}

	return RESULT_OK;
}

ResultStatus LoginWorker::authorizeUser(const string& username, const string& password, bool* canLogin, vector<string>* groups)
{
	vector<string> result;

	ResultStatus status = execute("AUTHORIZATION;" + StringUtils::escapeString(username) + ";" + StringUtils::escapeString(password), result);
	if (status != RESULT_OK) {
		return status;
	}

	if (result.size() != 1 && result.size() != 2) {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer of login worker");
	}

	vector<string> answer;
	string line = result[0];
	StringUtils::splitString(line, &answer, ';');

	if (answer.size() != 2 && answer.size() != 4) {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer '" + line + "' of login worker");
	}

	if (answer[0] != "LOGIN") {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer '" + line + "' of login worker");
	}

	if (answer[1] == "TRUE") {
		*canLogin = true;
	} else if (answer[1] == "FALSE") {
		*canLogin = false;

		return RESULT_OK;
	} else {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer '" + line + "' of login worker");
	}

	if (result.size() != 2) {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer of login worker");
	}

	answer.clear();
	line = result[1];
	StringUtils::splitString(line, &answer, ';');

	if (answer.size() < 2) {
		throw ErrorException(ErrorException::ERROR_INVALID_WORKER_REPLY, "error in answer '" + line + "' of login worker");
	}

	groups->clear();

	for (vector<string>::iterator i = answer.begin() + 1; i != answer.end(); i++) {
		groups->push_back(*i);
	}

	return RESULT_OK;
}

ResultStatus LoginWorker::notifyShutdown()
{
	shutdownInProgress = true;

	vector<string> result;

	return execute("SERVER SHUTDOWN", result, WORKER_TIMEOUT_MSEC);
}

ResultStatus LoginWorker::notifyDatabaseSaved(IdentifierType id)
{
	vector<string> result;

	return execute("DATABASE SAVED;" + StringUtils::convertToString(id), result, WORKER_TIMEOUT_MSEC);
}

ResultStatus LoginWorker::notifyLogout(const string& username)
{
	vector<string> result;

	return execute("USER LOGOUT;" + StringUtils::escapeString(username), result, WORKER_TIMEOUT_MSEC);
}

ResultStatus LoginWorker::cellDrillThrough(IdentifierType mode, IdentifierType database, IdentifierType cube, CellPath* cellPath, vector<string>& result)
{
	return execute("CELL DRILLTHROUGH;" + StringUtils::convertToString(mode) + ";" + StringUtils::convertToString(database) + ";" + StringUtils::convertToString(cube) + ";" + cellPath->toString(), result);
}
}
