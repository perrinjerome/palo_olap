////////////////////////////////////////////////////////////////////////////////
/// @brief cube worker
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef WORKER_CUBE_WORKER_H
#define WORKER_CUBE_WORKER_H 1

#include "palo.h"

#include "Worker/Worker.h"

namespace palo {
class Cube;

////////////////////////////////////////////////////////////////////////////////
/// @brief cube worker
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CubeWorker : public Worker {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief get for use-flag
	////////////////////////////////////////////////////////////////////////////////

	static bool useCubeWorker();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief setter for use-flag
	////////////////////////////////////////////////////////////////////////////////

	static void setUseCubeWorker(bool);

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CubeWorker(const string&, Cube*);

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets a double value
	////////////////////////////////////////////////////////////////////////////////

	ResultStatus setCellValue(const string& area, const string& session, const IdentifiersType& path, double value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets a double value
	////////////////////////////////////////////////////////////////////////////////

	ResultStatus setCellValue(const string& area, const string& session, const IdentifiersType& path, const string& value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief notifies worker about shutdown
	////////////////////////////////////////////////////////////////////////////////

	ResultStatus notifyShutdown();

public:

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	bool start();

private:
	ResultStatus defineCubeAreas();

	ResultStatus notifyAreaBuildOk();

	ResultStatus notifyAreaBuildError(const string& id1, const string& id2);

	string buildPathString(const IdentifiersType*);

	ResultStatus readAreaLines(const vector<string>& result, vector<string>* areaIds, vector<vector<IdentifiersType> >* areas);

	bool computeArea(vector<string>& answer, string& areaId, vector<IdentifiersType>* area);

	bool checkAreas(vector<string>* areaIds, vector<vector<IdentifiersType> >* areas);

	bool isOverlapping(vector<IdentifiersType>* area1, vector<IdentifiersType>* area2);

private:
	static bool useWorkers;

private:
	bool shutdownInProgress;

	string message;

	Cube* cube;

};

}

#endif
