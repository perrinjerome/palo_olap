/****************************************************************************
**
*A  list.c          1.02 2000-12-25     Lib                  Martin Schoenert
**
**  This file implements a linked list.
**
*H  1.02 2000-12-25 mS cleaned and extended the code
*H  1.01 2000-12-08 mS initial revision
*/

#include    "abattoir.h"                /* various stuff                   */

#include    "list.h"                    /* declaration part                */


/****************************************************************************
**
*F  IsDoubleLinkList(list)
*/

#define IsDoubleLinkList(list) \
    (((TypeList*)(list))->off_prev != -1)


/****************************************************************************
**
*F  SetNextItemList(list,item1,item2)
*/

#define SetNextItemList(list,item1,item2) \
    (NextItemList(list,item1) = (item2))


/****************************************************************************
**
*F  SetPrevItemList(list,item1,item2)
*/

#define SetPrevItemList(list,item1,item2) \
    (PrevItem2List(list,item1) = (item2))


/****************************************************************************
**
*F  InitList(<list>)  . . . . . . . . . . . . . . . . . . . initialize a list
**
**  'InitList' initializes the linked list <list>.
*/
Void                    InitList ARG3(
    TypeList *,             list,
    Int,                    off_next,
    Int,                    off_prev )
{
    list->first    = 0;
    list->last     = 0;
    list->nr_items = 0;
    list->off_next = off_next;
    list->off_prev = off_prev;
}


/****************************************************************************
**
*F  CopyList(<list1>,<list2>) . . . . . . . . . . . . . . . . . . copy a list
**
**  'CopyList' copies the linked list <list2> to <list1>.
*/
Void                    CopyList ARG2(
    TypeList *,             list1,
    TypeList *,             list2 )
{
    list1->first    = list2->first;
    list2->first    = 0;
    list1->last     = list2->last;
    list2->last     = 0;
    list1->nr_items = list2->nr_items;
    list2->nr_items = 0;
    list1->off_next = list2->off_next;
    list1->off_prev = list2->off_prev;
}


/****************************************************************************
**
*F  PrevItemList(<list>,<item1>)  . . . . . . . . . . previous item of a list
**
**  'PrevItemList' returns the item preceeding the item <item1> on the list
**  <list>. If <item1> is the first item on the list <list>, it returns 0.
**  Note that this function may take a long time on a singly linked list if
**  <item1> is not the first item of the list.
*/
extern
Void *                  PrevItemList ARG2(
    TypeList *,             list,
    Void *,                 item1 )
{
    Void *                  prev;
    Void *                  next;

    /* check arguments for sanity                                          */
    ASSERT( item1 != 0 )
    ASSERT( list->nr_items != 0 )
    ASSERT( list->first    != 0 )
    ASSERT( list->last     != 0 )

    /* on a doubly linked list, we need not search                         */
    if ( IsDoubleLinkList(list) ) {
        prev = PrevItem2List(list,item1);
    }

    /* on a forward linked list, we must search for the item               */
    else {
        if ( item1 == list->first && item1 == list->last ) {
            ASSERT( list->nr_items == 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            prev = 0;
        }
        else if ( item1 == list->first ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            prev = 0;
        }
        else if ( item1 == list->last ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            prev = 0;
            next = list->first;
            while ( next != item1 ) {
                prev = next;
                next = NextItemList( list, next );
                ASSERT( next != 0 )
            }
        }
        else {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            prev = 0;
            next = list->first;
            while ( next != item1 ) {
                prev = next;
                next = NextItemList( list, next );
                ASSERT( next != 0 )
            }
        }
    }

    /* return the previous item                                            */
    return prev;
}


/****************************************************************************
**
*F  InsertItemAfterList(<list>,<item1>,<item2>) . .  insert an item to a list
**
**  'InsertItemAfterList' inserts the item <item2> to the list <list>, so
**  that if comes directly after the item <item1>. If <item1> is 0, <item2>
**  becomes the new last item of <list>.
*/
extern
Void                    InsertItemAfterList ARG3(
    TypeList *,             list,
    Void *,                 item1,
    Void *,                 item2 )
{
    Void *                  next;

    /* special case if <item1> is 0                                        */
    if ( item1 == 0 ) {
        item1 = LastItemList( list );
    }

    /* handle a doubly linked list                                         */
    if ( IsDoubleLinkList(list) ) {
        if ( NrItemsList(list) == 0 ) {
            ASSERT( list->first == 0 )
            ASSERT( list->last  == 0 )
            ASSERT( item1       == 0 )
            list->last     = item2;
            SetNextItemList( list, item2, 0 );
            SetPrevItemList( list, item2, 0 );
            list->first    = item2;
        }
        else if ( item1 == list->first && item1 == list->last ) {
            ASSERT( list->nr_items == 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            ASSERT( PrevItem2List( list, item1 ) == 0 )
            list->last    = item2;
            SetNextItemList( list, item2, 0 );
            SetPrevItemList( list, item2, item1 );
            SetNextItemList( list, item1, item2 );
        }
        else if ( item1 == list->first ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            ASSERT( PrevItem2List( list, item1 ) == 0 )
            next = NextItemList( list, item1 );
            ASSERT( PrevItem2List( list, next ) == item1 )
            SetPrevItemList( list, next, item2 );
            SetNextItemList( list, item2, next );
            SetPrevItemList( list, item2, item1 );
            SetNextItemList( list, item1, item2 );
        }
        else if ( item1 == list->last ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            ASSERT( PrevItem2List( list, item1 ) != 0 )
            list->last    = item2;
            SetNextItemList( list, item2, 0 );
            SetPrevItemList( list, item2, item1 );
            SetNextItemList( list, item1, item2 );
        }
        else {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            ASSERT( PrevItem2List( list, item1 ) != 0 )
            next = NextItemList( list, item1 );
            ASSERT( PrevItem2List( list, next ) == item1 )
            SetPrevItemList( list, next, item2 );
            SetNextItemList( list, item2, next );
            SetPrevItemList( list, item2, item1 );
            SetNextItemList( list, item1, item2 );
        }
        list->nr_items++;
    }

    /* handle a singly linked list                                         */
    else {
        if ( NrItemsList(list) == 0 ) {
            ASSERT( list->first == 0 )
            ASSERT( list->last  == 0 )
            ASSERT( item1       == 0 )
            list->last     = item2;
            SetNextItemList( list, item2, 0 );
            list->first    = item2;
        }
        else if ( item1 == list->first && item1 == list->last ) {
            ASSERT( list->nr_items == 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            list->last    = item2;
            SetNextItemList( list, item2, 0 );
            SetNextItemList( list, item1, item2 );
        }
        else if ( item1 == list->first ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            next = NextItemList( list, item1 );
            SetNextItemList( list, item2, next );
            SetNextItemList( list, item1, item2 );
        }
        else if ( item1 == list->last ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            list->last    = item2;
            SetNextItemList( list, item2, 0 );
            SetNextItemList( list, item1, item2 );
        }
        else {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            next = NextItemList( list, item1 );
            SetNextItemList( list, item2, next );
            SetNextItemList( list, item1, item2 );
        }
        list->nr_items++;
    }

}


/****************************************************************************
**
*F  InsertItemBeforeList(<list>,<item1>,<item2>)  .  insert an item to a list
**
**  'InsertItemBeforeList' inserts the item <item2> to the list <list> so
**  that it comes directly before the item <item1>. If <item1> is 0, <item2>
**  becomes the new first item of <list>.
*/
extern
Void                    InsertItemBeforeList ARG3(
    TypeList *,             list,
    Void *,                 item1,
    Void *,                 item2 )
{
    Void *                  prev;
    Void *                  next;

    /* special case if <item1> is 0                                        */
    if ( item1 == 0 ) {
        item1 = FirstItemList( list );
    }

    /* handle a doubly linked list                                         */
    if ( IsDoubleLinkList(list) ) {
        if ( NrItemsList(list) == 0 ) {
            ASSERT( list->first == 0 )
            ASSERT( list->last  == 0 )
            ASSERT( item1       == 0 )
            list->first    = item2;
            SetPrevItemList( list, item2, 0 );
            SetNextItemList( list, item2, 0 );
            list->last     = item2;
        }
        else if ( item1 == list->first && item1 == list->last ) {
            ASSERT( list->nr_items == 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            ASSERT( PrevItem2List( list, item1 ) == 0 )
            list->first    = item2;
            SetPrevItemList( list, item2, 0 );
            SetNextItemList( list, item2, item1 );
            SetPrevItemList( list, item1, item2 );
        }
        else if ( item1 == list->first ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            ASSERT( PrevItem2List( list, item1 ) == 0 )
            list->first    = item2;
            SetPrevItemList( list, item2, 0 );
            SetNextItemList( list, item2, item1 );
            SetPrevItemList( list, item1, item2 );
        }
        else if ( item1 == list->last ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            ASSERT( PrevItem2List( list, item1 ) != 0 )
            prev = PrevItem2List( list, item1 );
            ASSERT( NextItemList( list, prev ) == item1 )
            SetNextItemList( list, prev, item2 );
            SetPrevItemList( list, item2, prev );
            SetNextItemList( list, item2, item1 );
            SetPrevItemList( list, item1, item2 );
        }
        else {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            ASSERT( PrevItem2List( list, item1 ) != 0 )
            prev = PrevItem2List( list, item1 );
            ASSERT( NextItemList( list, prev ) == item1 )
            SetNextItemList( list, prev, item2 );
            SetPrevItemList( list, item2, prev );
            SetNextItemList( list, item2, item1 );
            SetPrevItemList( list, item1, item2 );
        }
        list->nr_items++;
    }

    /* handle a singly linked list                                         */
    else {
        if ( NrItemsList(list) == 0 ) {
            ASSERT( list->first == 0 )
            ASSERT( list->last  == 0 )
            ASSERT( item1       == 0 )
            list->first    = item2;
            SetNextItemList( list, item2, 0 );
            list->last     = item2;
        }
        else if ( item1 == list->first && item1 == list->last ) {
            ASSERT( list->nr_items == 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            list->first    = item2;
            SetNextItemList( list, item2, item1 );
        }
        else if ( item1 == list->first ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            list->first    = item2;
            SetNextItemList( list, item2, item1 );
        }
        else if ( item1 == list->last ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            prev = 0;
            next = list->first;
            while ( next != item1 ) {
                prev = next;
                next = NextItemList( list, next );
                ASSERT( next != 0 )
            }
            ASSERT( NextItemList( list, prev ) == item1 )
            SetNextItemList( list, prev, item2 );
            SetNextItemList( list, item2, item1 );
        }
        else {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) != 0 )
            prev = 0;
            next = list->first;
            while ( next != item1 ) {
                prev = next;
                next = NextItemList( list, next );
                ASSERT( next != 0 )
            }
            ASSERT( NextItemList( list, prev ) == item1 )
            SetNextItemList( list, prev, item2 );
            SetNextItemList( list, item2, item1 );
        }
        list->nr_items++;
    }

}


/****************************************************************************
**
*F  RemoveItemList(<list>,<item1>)  . . . . . . .  remove an item from a list
**
**  'RemoveList' removes the item <item1> from the list <list>.  Note that
**  this function may take a long time on a singly linked list if <item1> is
**  not the first item of the list.
*/
extern
Void                    RemoveItemList ARG2(
    TypeList *,             list,
    Void *,                 item1 )
{
    Void *                  prev;
    Void *                  next;

    /* check arguments for sanity                                          */
    ASSERT( item1 != 0 )
    ASSERT( list->nr_items != 0 )
    ASSERT( list->first    != 0 )
    ASSERT( list->last     != 0 )

    /* on a doubly linked list, we need not search                         */
    if ( IsDoubleLinkList(list) ) {
        if ( item1 == list->first && item1 == list->last ) {
            ASSERT( list->nr_items == 1 )
            ASSERT( NextItemList( list, item1 )  == 0 )
            ASSERT( PrevItem2List( list, item1 ) == 0 )
            list->first = 0;
            list->last  = 0;
        }
        else if ( item1 == list->first ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( PrevItem2List( list, item1 ) == 0 )
            next = NextItemList( list, item1 );
            ASSERT( next != 0 )
            ASSERT( PrevItem2List( list, next ) == item1 )
            list->first = next;
            SetPrevItemList( list, next, 0 );
        }
        else if ( item1 == list->last ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            prev = PrevItem2List( list, item1 );
            ASSERT( prev != 0 )
            ASSERT( NextItemList( list, prev ) == item1 )
            list->last = prev;
            SetNextItemList( list, prev, 0 );
        }
        else {
            ASSERT( list->nr_items > 1 )
            next = NextItemList( list, item1 );
            ASSERT( next != 0 )
            ASSERT( PrevItem2List( list, next ) == item1 )
            prev = PrevItem2List( list, item1 );
            ASSERT( prev != 0 )
            ASSERT( NextItemList( list, prev ) == item1 )
            SetNextItemList( list, prev, next );
            SetPrevItemList( list, next, prev );
        }
        list->nr_items--;
    }

    /* on a forward linked list, we must search for the item               */
    else {
        if ( item1 == list->first && item1 == list->last ) {
            ASSERT( list->nr_items == 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            list->first = 0;
            list->last  = 0;
        }
        else if ( item1 == list->first ) {
            ASSERT( list->nr_items > 1 )
            next = NextItemList( list, item1 );
            ASSERT( next != 0 )
            list->first = next;
        }
        else if ( item1 == list->last ) {
            ASSERT( list->nr_items > 1 )
            ASSERT( NextItemList( list, item1 ) == 0 )
            prev = 0;
            next = list->first;
            while ( next != item1 ) {
                prev = next;
                next = NextItemList( list, next );
                ASSERT( next != 0 )
            }
            list->last = prev;
            SetNextItemList( list, prev, 0 );
        }
        else {
            ASSERT( list->nr_items > 1 )
            prev = 0;
            next = list->first;
            while ( next != item1 ) {
                prev = next;
                next = NextItemList( list, next );
                ASSERT( next != 0 )
            }
            next = NextItemList( list, item1 );
            ASSERT( next != 0 )
            SetNextItemList( list, prev, next );
        }
        list->nr_items = list->nr_items - 1;
    }


}


/****************************************************************************
**
*E  list.c  . . . . . . . . . . . . . . . . . . . . . . . . . . . . ends here
*/
