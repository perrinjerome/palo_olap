/****************************************************************************
**
*A  list.h          1.02 2000-12-25     Lib                  Martin Schoenert
**
**  This file declares a linked list.
**
*H  1.02 2000-12-25 mS cleaned and extended the code
*H  1.01 2000-12-08 mS initial revision
*/

#ifndef LIST_H
#define LIST_H

/****************************************************************************
**
*T  TypeList  . . . . . . . . . . . . . . . . . . . . . type of a linked list
**
**  'TypeList' is the type of a linked list. If a program wants to use a
**  linked list, it must allocate an object of this type and pass a pointer
**  to this object to each of the following functions.
*/
typedef struct StructList
{
    void *                  first;
    void *                  last;
    UInt                    nr_items;
    Int                     off_next;
    Int                     off_prev;
} TypeList;

#define List_t          TypeList


#ifdef __cplusplus
extern "C"
{
#endif


    /****************************************************************************
    **
    *F  InitList(<list>,...)  . . . . . . . . . . . . . . . . . initialize a list
    **
    **  'InitList' initializes the linked list <list>.
    */
    extern
    Void                    InitList ARG3(
        TypeList *,             list,
        Int,                    off_next,
        Int,                    off_prev );


    /****************************************************************************
    **
    *F  CopyList(<list1>,<list2>) . . . . . . . . . . . . . . . . . . copy a list
    **
    **  'CopyList' copies the linked list <list2> to <list1>.
    */
    extern
    Void                    CopyList ARG2(
        TypeList *,             list1,
        TypeList *,             list2 );


    /****************************************************************************
    **
    *F  FirstItemList(<list>) . . . . . . . . . . . . . . .  first item of a list
    **
    **  'FirstItemList' returns the first item of the list <list>.
    */

#define FirstItemList(list) \
    (((TypeList*)(list))->first)


    /****************************************************************************
    **
    *F  LastItemList(<list>)  . . . . . . . . . . . . . . . . last item of a list
    **
    **  'LastItemList' returns the last item of the list <list>.
    */

#define LastItemList(list) \
    (((TypeList*)(list))->last)


    /****************************************************************************
    **
    *F  NrItemsList(<list>) . . . . . . . . . . . . . . number of items of a list
    **
    **  'NrItemsList' returns the number of items on the list <list>.
    */

#define NrItemsList(list) \
    (((TypeList*)(list))->nr_items)


    /****************************************************************************
    **
    *F  NextItemList(<list>,<item1>)  . . . . . . . . . . . . next item of a list
    **
    **  'NextItemList' returns the item following the item <item1> on the list
    **  <list>. If <item1> is the last item on the list <list>, it returns 0.
    */

#define NextItemList(list,item1) \
    (*(Void**)(((Char*)(item1))+(list)->off_next))


    /****************************************************************************
    **
    *F  PrevItemList(<list>,<item1>)  . . . . . . . . . . previous item of a list
    *F  PrevItem2List(<list>,<item1>) . . . . . . . . . . previous item of a list
    **
    **  'PrevItemList' returns the item preceeding the item <item1> on the list
    **  <list>. If <item1> is the first item on the list <list>, it returns 0.
    **  Note that this function may take a long time on a singly linked list if
    **  <item1> is not the first item of the list.
    **
    **  'PrevItem2List' does the same but works only if the list is doubly
    **  linked.
    */
    extern
    Void *                  PrevItemList ARG2(
        TypeList *,             list,
        Void *,                 item1 );

#define PrevItem2List(list,item1) \
    (*(Void**)(((Char*)(item1))+(list)->off_prev))


    /****************************************************************************
    **
    *F  InsertItemAfterList(<list>,<item1>,<item2>) . .  insert an item to a list
    **
    **  'InsertItemAfterList' inserts the item <item2> to the list <list>, so
    **  that if comes directly after the item <item1>. If <item1> is 0, <item2>
    **  becomes the new last item of <list>.
    */
    extern
    Void                    InsertItemAfterList ARG3(
        TypeList *,             list,
        Void *,                 item1,
        Void *,                 item2 );


    /****************************************************************************
    **
    *F  InsertItemBeforeList(<list>,<item1>,<item2>)  .  insert an item to a list
    **
    **  'InsertItemBeforeList' inserts the item <item2> to the list <list> so
    **  that it comes directly before the item <item1>. If <item1> is 0, <item2>
    **  becomes the new first item of <list>.  Note that this function may take a
    **  long time on a singly linked list if <item1> is not the first item of the
    **  list.
    */
    extern
    Void                    InsertItemBeforeList ARG3(
        TypeList *,             list,
        Void *,                 item1,
        Void *,                 item2 );


    /****************************************************************************
    **
    *F  AppendItemList(<list>,<item1>)  . . . . . . . .  insert an item to a list
    **
    **  'AppendItemList' inserts the item <item1> to the list <list> so that it
    **  is the new last item in the list
    */

#define AppendItemList(list,item1) \
    InsertItemAfterList((list),(0),(item1))


    /****************************************************************************
    **
    *F  InsertItemList(<list>,<item1>) . . . . . . . .  insert an item to a list
    **
    **  'InsertItemList' insert the item <item1> to the list <list> so that it is
    **  the new first item in the list.
    */

#define InsertItemList(list,item1) \
    InsertItemBeforeList((list),(0),(item1))


    /****************************************************************************
    **
    *F  RemoveItemList(<list>,<item1>)  . . . . . . .  remove an item from a list
    **
    **  'RemoveList' removes the item <item1> from the list <list>.  Note that
    **  this function may take a long time on a singly linked list if <item1> is
    **  not the first item of the list.
    */
    extern
    Void                    RemoveItemList ARG2(
        TypeList *,             list,
        Void *,                 item1 );


#ifdef __cplusplus
}
#endif


#endif

/****************************************************************************
**
*E  list.h  . . . . . . . . . . . . . . . . . . . . . . . . . . . . ends here
*/
