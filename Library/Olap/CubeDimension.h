////////////////////////////////////////////////////////////////////////////////
/// @brief palo cube dimension
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_CUBE_DIMENSION_H
#define OLAP_CUBE_DIMENSION_H 1

#include "palo.h"

#include "Olap/SystemDimension.h"
#include "Olap/SystemDatabase.h"
#include "Olap/Server.h"
#include "Olap/AttributedDimension.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cube dimension
///
/// An OLAP dimension is an ordered list of elements
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CubeDimension : public SystemDimension, public AttributedDimension {
public:
	static const uint32_t DIMENSION_TYPE = 5;

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief creates new dimension with given identifier
	////////////////////////////////////////////////////////////////////////////////

	CubeDimension(IdentifierType identifier, const string& name, Database* database) :
		SystemDimension(identifier, name, database), AttributedDimension()
	{
		Logger::info << "created CubeDimension" << endl;
	}

public:
	void saveDimensionType(FileWriter* file);

public:
	Element* addElement(const string& name, ElementType elementType, User* user, bool useJournal = true);

	void deleteElement(Element * element, User* user, bool useJournal = true);

	void changeElementName(Element * element, const string& name, User* user);

	void changeElementType(Element * element, ElementType elementType, User* user, bool setConsolidated);

	// attributes
	void notifyAddDimension();
	void beforeRemoveDimension();
	void notifyRenameDimension(const string& oldName);
	AttributesDimension* getAttributesDimension();
	AttributesCube* getAttributesCube();

public:
	SystemDimensionType getSubType() const
	{
		return CUBE_DIMENSION;
	}

	void checkElements();

protected:

	void checkCubeAccessRight(User* user, RightsType minimumRight)
	{
		if (user != 0 && user->getRoleRightsRight() < minimumRight) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "insufficient access rights for rights cube", "user", (int)user->getIdentifier());
		}
	}

	void checkElementAccessRight(User* user, RightsType minimumRight)
	{

		SystemDatabase* system = (database->getServer())->getSystemDatabase();
		if (user == 0 || system == 0) {
			return;
		}

		RightsType rt = user->getRoleElementRight();

		if (rt < minimumRight) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "insufficient access rights for dimension element", "user", (int)user->getIdentifier());
		}
	}

};

}

#endif
