////////////////////////////////////////////////////////////////////////////////
/// @brief palo subcube list
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Ladislav Morva, qBicon s.r.o., Prague, Czech Republic
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_SUBCUBE_LIST_H
#define PALO_SUBCUBE_LIST_H 1

#include <boost/shared_ptr.hpp>

namespace palo {

typedef set<IdentifierType> SetID;
typedef boost::shared_ptr<SetID> RSetID;
typedef vector<RSetID> SubCube;
typedef boost::shared_ptr<SubCube> RSubCube;

////////////////////////////////////////////////////////////////////////////////
/// @brief SubCubeList
///
/// List of subcubes
////////////////////////////////////////////////////////////////////////////////

class SubCubeList {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	SubCubeList();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief destructor
	////////////////////////////////////////////////////////////////////////////////

	~SubCubeList();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief adds a subcube containing one cell to the list
	////////////////////////////////////////////////////////////////////////////////

	size_t size(){return subCubes.size();}

	void reset(){iter = subCubes.begin();}

	RSubCube next();

	void addCell(const IdentifiersType *cell);

	//void addSubCube(const RSubCube cube);
	//bool findCell(const IdentifiersType *cell); //returns true if there is a subcube which contains this cell

private:
	bool isJoinable(RSubCube cube, const IdentifiersType *cell);
	bool isJoinable(RSubCube cube1, RSubCube cube2);
	RSubCube createSubCube(const IdentifiersType *cell);
	RSubCube joinSubCubes(const RSubCube cube1, const RSubCube cube2);
	void mergeSubCubes(list<RSubCube> &subCubes, list<RSubCube>::iterator sit);

private:
	list<RSubCube> subCubes;
	list<RSubCube>::iterator iter;
};

}

#endif
