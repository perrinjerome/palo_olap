////////////////////////////////////////////////////////////////////////////////
/// @brief palo normal cube
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "Olap/NormalCube.h"

#include "Olap/Database.h"
#include "Olap/NormalDatabase.h"
#include "Olap/UserInfoDatabase.h"
#include "Olap/PaloSession.h"
#include "Olap/SystemCube.h"
#include "Olap/SystemDatabase.h"

namespace palo {

NormalCube::NormalCube(IdentifierType identifier, const string& name, Database* database, vector<Dimension*>* dimensions) :
	Cube(identifier, name, database, dimensions)
{

	if (CubeWorker::useCubeWorker()) {
		PaloSession* session = PaloSession::createSession(database->getServer(), 0, true, 0);
		cubeWorker = new CubeWorker(session->getEncodedIdentifier(), this);
	}
}

////////////////////////////////////////////////////////////////////////////////
// notification callbacks
////////////////////////////////////////////////////////////////////////////////

void NormalCube::notifyAddCube()
{
	Dimension *cubeDimension = 0;
	if (database->getType() == NORMAL) {
		NormalDatabase * db = dynamic_cast<NormalDatabase*> (database);

		if (db == 0) {
			Logger::error << "normal database is unnormal" << endl;
			return;
		}

		cubeDimension = db->getCubeDimension();
	} else if (database->getType() == USER_INFO) {
		UserInfoDatabase * db = dynamic_cast<UserInfoDatabase*> (database);

		if (db == 0) {
			Logger::error << "UserInfoDatabase dynamic cast failed" << endl;
			return;
		}

		cubeDimension = db->getCubeDimension();
	}
	if (cubeDimension) {
		// add cube to list of cubes in #_CUBE_
		cubeDimension->addElement(name, STRING, 0, false);
	}
}

void NormalCube::notifyRemoveCube()
{
	Dimension *cubeDimension = 0;
	if (database->getType() == NORMAL) {
		NormalDatabase * db = dynamic_cast<NormalDatabase*> (database);

		if (db == 0) {
			Logger::error << "normal database is unnormal" << endl;
			return;
		}

		cubeDimension = db->getCubeDimension();
	} else if (database->getType() == USER_INFO) {
		UserInfoDatabase * db = dynamic_cast<UserInfoDatabase*> (database);

		if (db == 0) {
			Logger::error << "UserInfoDatabase dynamic cast failed" << endl;
			return;
		}

		cubeDimension = db->getCubeDimension();
	}
	if (cubeDimension) {
		Element* element = cubeDimension->lookupElementByName(name);

		if (element != 0) {
			cubeDimension->deleteElement(element, 0, false);
		}
	}
}

void NormalCube::notifyRenameCube(const string& oldName)
{
	Dimension * cubeDimension = 0;
	if (database->getType() == NORMAL) {
		NormalDatabase * db = dynamic_cast<NormalDatabase*> (database);

		if (db == 0) {
			Logger::error << "normal database is unnormal" << endl;
			return;
		}

		cubeDimension = db->getCubeDimension();
	} else if (database->getType() == USER_INFO) {
		UserInfoDatabase * db = dynamic_cast<UserInfoDatabase*> (database);

		if (db == 0) {
			Logger::error << "UserInfoDatabase dynamic cast failed" << endl;
			return;
		}

		cubeDimension = db->getCubeDimension();
	}
	if (cubeDimension) {
		Element* element = cubeDimension->lookupElementByName(oldName);

		if (element != 0) {
			cubeDimension->changeElementName(element, name, 0);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// functions to load and save a dimension
////////////////////////////////////////////////////////////////////////////////

void NormalCube::saveCubeType(FileWriter* file)
{
	file->appendIdentifier(identifier);
	file->appendEscapeString(name);

	IdentifiersType identifiers;

	for (vector<Dimension*>::const_iterator i = dimensions.begin(); i != dimensions.end(); i++) {
		identifiers.push_back((*i)->getIdentifier());
	}

	file->appendIdentifiers(&identifiers);
	file->appendInteger(CUBE_TYPE);
	file->appendBool(isDeletable());
	file->appendBool(isRenamable());

	file->nextLine();
}
}
