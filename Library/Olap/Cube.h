////////////////////////////////////////////////////////////////////////////////
/// @brief palo cube
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
/// @author Zurab Khadikov, Jedox AG, Freiburg, Germany
/// @author Christoffer Anselm, Jedox AG, Freiburg, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_CUBE_H
#define OLAP_CUBE_H 1

#include "palo.h"

#include "InputOutput/JournalFileWriter.h"
#include "Logger/Logger.h"
#include "Olap/CellPath.h"
#include "Olap/CubePage.h"
#include "Olap/Dimension.h"
#include "Olap/Element.h"
#include "Thread/ReadWriteLock.h"
#include "Worker/CubeWorker.h"
#include "Olap/RulesList.h"

#include "Intervals/IntervalRange.h"
#include "Cache/SimpleCache.h"

namespace palo {
class AreaStorage;
class CubeLooper;
class CubeStorage;
class HashAreaStorage;
class Condition;
class Database;
class ExportStorage;
class PaloSession;
class Rule;
class RuleMarker;
class RuleNode;
class Lock;
class CubeWorker;
class EMemoryContext;

struct StructCube;

////////////////////////////////////////////////////////////////////////////////
/// @brief OLAP cube
///
/// An OLAP cube stores the data
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS Cube {
	friend class CubeLooper;


public:
	static double splashLimit1; // error
	static double splashLimit2; // warning
	static double splashLimit3; // info

	static int goalseekTimeoutMiliSec;
	static int goalseekCellLimit;

	static const double TOLERANCE;
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief status of the cube
	///
	/// UNLOADED: the cube was not loaded<br>
	/// LOADED:   the cube is loaded and not changed<br>
	/// CHANGED:  the cube is new or changed
	////////////////////////////////////////////////////////////////////////////////

	enum CubeStatus {
		UNLOADED, LOADED, CHANGED
	};

	////////////////////////////////////////////////////////////////////////////////
	/// @brief "splash mode" for setting numeric values in aggregations
	///
	/// DISABLED: do not set the value<br>
	/// DEFAULT:  <br>
	///           1. value = 0.0 <br>
	///              clears all base path cells<br>
	///           2. value <> 0.0 and old_value = 0.0<br>
	///              compute a "splash value" and distribute this value to all
	///              base path cells<br>
	///           3. value <> 0.0 and old_value <> 0.0<br>
	///              compute a scale factor and recalculate all base cells<br>
	/// SET_BASE: sets all base path elements to the same value<br>
	/// ADD_BASE: adds value to all base path elements
	////////////////////////////////////////////////////////////////////////////////

	enum SplashMode {
		DISABLED, DEFAULT, SET_BASE, ADD_BASE
	};

	////////////////////////////////////////////////////////////////////////////////
	/// @brief a cell value
	////////////////////////////////////////////////////////////////////////////////

	struct CellValueType {
		ElementType type;

		double doubleValue;
		string charValue;
		IdentifierType rule;
	};

	typedef uint32_t CellLockInfo;

	std::vector<std::pair<Dimension*, IdentifierType> > purgedList;

public:
	SimpleCache cache;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets flag for ignoring cell data (for debugging only)
	////////////////////////////////////////////////////////////////////////////////

	static void setIgnoreCellData(bool ignore)
	{
		ignoreCellData = ignore;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief creates a new cube from type line
	////////////////////////////////////////////////////////////////////////////////

	static Cube* loadCubeFromType(FileReader*, Database*, IdentifierType, const string& name, vector<Dimension*>* dimensions, uint32_t type);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief set the number of base elements for caching consolidated values
	////////////////////////////////////////////////////////////////////////////////

	static void setCacheBarrier(double barrier);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief get the number of base elements for caching consolidated values
	////////////////////////////////////////////////////////////////////////////////

	static double getCacheBarrier();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief set the number cells that clears a cube cache
	////////////////////////////////////////////////////////////////////////////////

	static void setCacheClearBarrierCells(double barrier);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief computes parameter for area mapping
	////////////////////////////////////////////////////////////////////////////////

	static bool computeAreaParameters(Cube* cube, vector<IdentifiersType>* area, vector<map<Element*, uint32_t> >& hashMapping, vector<ElementsType>& numericArea, vector<vector<vector<pair<uint32_t, double> > > >& numericMapping, vector<map<uint32_t, vector<pair<IdentifierType, double> > > >& reverseNumericMapping, vector<uint32_t>& hashSteps, vector<uint32_t>& lengths, bool ignoreUnknownElements);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets timeout for goalseek operations
	////////////////////////////////////////////////////////////////////////////////
	static void setGoalseekTimeout(int milisecs);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets cell limit for goalseek operations
	////////////////////////////////////////////////////////////////////////////////
	static void setGoalseekCellLimit(int cellCount);

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief Creates empty cube
	////////////////////////////////////////////////////////////////////////////////

	Cube(IdentifierType identifier, const string& name, Database* database, vector<Dimension*>* dimensions);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief Destructor
	////////////////////////////////////////////////////////////////////////////////

	virtual ~Cube();

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @{
	/// @name notification callbacks
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	/// @brief called after a cube has been added to a database
	////////////////////////////////////////////////////////////////////////////////

	virtual void notifyAddCube()
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief called after a cube has been removed from a database
	////////////////////////////////////////////////////////////////////////////////

	virtual void notifyRemoveCube()
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief called after a cube has been renamed
	////////////////////////////////////////////////////////////////////////////////

	virtual void notifyRenameCube(const string& oldName)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @}
	////////////////////////////////////////////////////////////////////////////////

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @{
	/// @name functions to save and load the dimension
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns true if cube is loadable
	////////////////////////////////////////////////////////////////////////////////

	bool isLoadable() const;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief changes cube path
	////////////////////////////////////////////////////////////////////////////////

	void setCubeFile(const FileName&);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief reads data from file
	////////////////////////////////////////////////////////////////////////////////

	virtual void loadCube(bool processJournal);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief reads journal data from file
	////////////////////////////////////////////////////////////////////////////////

	void processCubeJournal(CubeStatus cubeStatus);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief saves cube name and type to file
	////////////////////////////////////////////////////////////////////////////////

	virtual void saveCubeType(FileWriter* file) = 0;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief saves data to file
	////////////////////////////////////////////////////////////////////////////////

	virtual void saveCube();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief deletes cube file from disk
	////////////////////////////////////////////////////////////////////////////////

	void deleteCubeFiles();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief unloads saved cubes from memory
	////////////////////////////////////////////////////////////////////////////////

	void unloadCube();

	////////////////////////////////////////////////////////////////////////////////
	/// @}
	////////////////////////////////////////////////////////////////////////////////

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @{
	/// @name getter and setter
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns cube type
	////////////////////////////////////////////////////////////////////////////////

	virtual ItemType getType() const = 0;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets the token
	////////////////////////////////////////////////////////////////////////////////

	uint32_t getToken() const;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets the client cache token
	////////////////////////////////////////////////////////////////////////////////

	uint32_t getClientCacheToken();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets identifier of cube
	////////////////////////////////////////////////////////////////////////////////

	IdentifierType getIdentifier() const
	{
		return identifier;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets cube name
	////////////////////////////////////////////////////////////////////////////////

	void setName(const string& name)
	{
		this->name = name;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets cube name
	////////////////////////////////////////////////////////////////////////////////

	const string& getName() const
	{
		return name;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets deletable attribute
	////////////////////////////////////////////////////////////////////////////////

	void setDeletable(bool deletable)
	{
		this->deletable = deletable;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets deletable attribute
	////////////////////////////////////////////////////////////////////////////////

	bool isDeletable() const
	{
		return deletable;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets renamable attribute
	////////////////////////////////////////////////////////////////////////////////

	void setRenamable(bool renamable)
	{
		this->renamable = renamable;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets renamable attribute
	////////////////////////////////////////////////////////////////////////////////

	bool isRenamable() const
	{
		return renamable;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets cube dimension list
	////////////////////////////////////////////////////////////////////////////////

	const vector<Dimension*>* getDimensions() const
	{
		return &dimensions;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets rule list
	////////////////////////////////////////////////////////////////////////////////

	vector<Rule*> getRules(User*);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets cube status
	////////////////////////////////////////////////////////////////////////////////

	CubeStatus getStatus() const
	{
		return status;
	}
	;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets thedatabase
	////////////////////////////////////////////////////////////////////////////////

	Database* getDatabase() const
	{
		return database;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the number of filled cells
	////////////////////////////////////////////////////////////////////////////////

	int32_t sizeFilledCells();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the number of filled numeric cells
	////////////////////////////////////////////////////////////////////////////////

	virtual size_t sizeFilledNumericCells();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the number of filled string cells
	////////////////////////////////////////////////////////////////////////////////

	virtual size_t sizeFilledStringCells();

	////////////////////////////////////////////////////////////////////////////////
	/// @}
	////////////////////////////////////////////////////////////////////////////////

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @{
	/// @name functions to update internal structures
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	/// @brief creates a new rule
	////////////////////////////////////////////////////////////////////////////////

	Rule* createRule(RuleNode*, const string& external, const string& comment, bool activate, User*);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief modifies an existing rule
	////////////////////////////////////////////////////////////////////////////////

	bool modifyRule(Rule*, RuleNode*, const string& external, const string& comment, User*);

	////////////////////////////////////////////////////////////////////////////////
	/// @checks to see this rule has markers and the markers are okay
	////////////////////////////////////////////////////////////////////////////////

	bool testRuleMarkers(Rule*);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief activeate/deactivate an existing rule
	////////////////////////////////////////////////////////////////////////////////

	void activateRule(Rule*, bool activate, User*, bool bDefinitionChangedBefore = false);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief deletes a rule
	////////////////////////////////////////////////////////////////////////////////

	void deleteRule(IdentifierType, User*, EMemoryContext* mem_context);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief finds a rule
	////////////////////////////////////////////////////////////////////////////////

	Rule* findRule(IdentifierType, User*);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief evaluates a rule
	////////////////////////////////////////////////////////////////////////////////

	double computeRule(CubePage::element_t, double dflt, User*, EMemoryContext* mem_context);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief evaluates a marked rule for export
	////////////////////////////////////////////////////////////////////////////////

	double computeMarkeredRule(CubePage::element_t, double dflt, User*, const vector<Rule*>& rules, EMemoryContext* mem_context);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief increments the token
	////////////////////////////////////////////////////////////////////////////////

	void updateToken();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief increments the client cache token
	////////////////////////////////////////////////////////////////////////////////

	void updateClientCacheToken();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief increments JUST the token
	////////////////////////////////////////////////////////////////////////////////

	void updateJustToken()
	{
		token++;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief deletes element
	////////////////////////////////////////////////////////////////////////////////

	virtual void deleteElement(const string& username, const string& event, Dimension* dimension, IdentifierType element, bool processStorageDouble, bool processStorageString, bool deleteRules, EMemoryContext* mem_context, bool useJournal/* = true*/);

	void deleteElements(const string& username, const string& event, Dimension* dimension, IdentifiersType elements, bool processStorageDouble, bool processStorageString, bool deleteRules, EMemoryContext* mem_context, bool useJournal);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief opens journal
	////////////////////////////////////////////////////////////////////////////////

	void openJournal()
	{
		closeJournal();
		journal = new JournalFileWriter(FileName(*fileName, "log"), false);
		journal->openFile();
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief closes journal
	////////////////////////////////////////////////////////////////////////////////

	void closeJournal()
	{
		if (journal != 0) {
			delete journal;
			journal = 0;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief set the worker area of the cube
	////////////////////////////////////////////////////////////////////////////////

	void setWorkerAreas(vector<string>* areaIdentifiers, vector<vector<IdentifiersType> >* areas);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief remove unused worker
	////////////////////////////////////////////////////////////////////////////////

	void removeWorker();

	Lock* lockCube(vector<IdentifiersType>* area, const string& areaString, User* user);

	void commitCube(long int id, User* user);

	void rollbackCube(long int id, User* user, size_t numSteps);

	const vector<Lock*>& getCubeLocks(User* user);

	bool hasLockedArea()
	{
		return hasLock;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @}
	////////////////////////////////////////////////////////////////////////////////

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @{
	/// @name functions dealing with cells and cell values
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	/// @brief clears all cells
	////////////////////////////////////////////////////////////////////////////////

	virtual void clearCells(User* user);

	virtual void clearCells(vector<IdentifiersType> * baseElements, User * user);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets NUMERIC value to a cell
	////////////////////////////////////////////////////////////////////////////////

	virtual ResultStatus setCellValue(CellPath* cellPath, double value, User* user, PaloSession * session, bool checkArea, bool sepRight, bool addValue, SplashMode splashMode, // = DEFAULT,
	        Lock * lock);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets STRING value to a cell
	////////////////////////////////////////////////////////////////////////////////

	virtual ResultStatus setCellValue(CellPath* cellPath, const string& value, User* user, PaloSession * session, bool checkArea, bool sepRight, Lock * lock);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief clears value of cell in cube
	////////////////////////////////////////////////////////////////////////////////

	virtual ResultStatus clearCellValue(CellPath* cellPath, User* user, PaloSession * session, bool checkArea, bool sepRight, Lock* lock);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets a value from the cube
	///
	/// elementType contains the datatype of the returned value,
	/// found contains true if at least one entry exists
	////////////////////////////////////////////////////////////////////////////////

	CellValueType getCellValue(CellPath* cellPath, bool* found, User* user, PaloSession * session, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context, bool useEnterpriseRules = true);
	// version 3.0
	CellValueType getCellValueNew(CellPath* cellPath, bool* found, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context, bool isPreChecked = false);
	CellValueType getCellValueNew(CellPath* cellPath, CellValueType& value, bool* found, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context, bool isPreChecked = false);
	void resetCacheCounter();

	bool getCellValueFromDirectRule(CellPath* cellPath, Cube::CellValueType& cellValue, bool* found, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context);

	bool getCellValueFromIndirectRule(CellPath* cellPath, Cube::CellValueType& cellValue, bool* found, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context);

    virtual void getCellValueFromStorage(CellPath* cellPath, CellValueType& cellValue, bool* found) = 0;

	void getCellValueFromEngine(CellPath* cellPath, Cube::CellValueType& cellValue, bool* found, EMemoryContext* mem_context);

	bool hasRule()
	{
		return !rules.empty();
	}
	bool hasActiveRule()
	{
		return rules.hasActiveRule();
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief copies a cell value (or cell values) to an other cell
	////////////////////////////////////////////////////////////////////////////////

	virtual void copyCellValues(CellPath* cellPathFrom, CellPath* cellPathTo, User* user, EMemoryContext* mem_context, double factor = 1.0);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief copies a cell value (or cell values) to an other cell
	////////////////////////////////////////////////////////////////////////////////

	virtual void copyLikeCellValues(CellPath* cellPathFrom, CellPath* cellPathTo, User* user, EMemoryContext* mem_context, double value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets values from the cube
	///
	/// the storage returns the values
	/// area is a cube area given by the request
	/// cellPathes returns the cell pathes computed by the getAreaCellValues method
	/// return true if the storage is filled
	////////////////////////////////////////////////////////////////////////////////

	bool getAreaCellValues(AreaStorage* storage, vector<IdentifiersType>* area, vector<IdentifiersType>* resultPathes, User* user, EMemoryContext* mem_context);

	double getNumAreaBaseCells(vector<IdentifiersType>* area);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets values from the cube
	///
	/// the storage returns the values
	/// area is a cube area given by the request
	/// cellPathes returns the cell pathes computed by the getAreaCellValues method
	////////////////////////////////////////////////////////////////////////////////

	void getExportValues(ExportStorage* storage, vector<IdentifiersType>* area, const IdentifiersType* startPath, bool hasStartPath, bool useRules, bool baseElementsOnly, bool skipEmpty, Condition * condition, User* user, EMemoryContext* mem_context, uint32_t cellType);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets values from the cube
	///
	/// cell goal seek
	///
	///
	////////////////////////////////////////////////////////////////////////////////

	void cellGoalSeek(CellPath* cellPath, User* user, PaloSession * session, const double& value, EMemoryContext* mem_context);

	////////////////////////////////////////////////////////////////////////////////
	/// @}
	////////////////////////////////////////////////////////////////////////////////

public:
	void executeShutdown();

	static bool isInArea(CellPath* cellPath, vector<set<IdentifierType> >* area);

	static bool isInArea(const IdentifierType*, const vector<set<IdentifierType> >* area);

	static void checkZero(CellValueType &value)
	{
		if (NUMERIC == value.type && fabs(value.doubleValue) < TOLERANCE) {
			value.doubleValue = 0.0;
		}
	}

	static void checkZero(double &value)
	{
		if (fabs(value) < TOLERANCE) {
			value = 0.0;
		}
	}

	CellValueType getConsolidatedRuleValue(const CellPath*, bool* found, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context);

	CubeStorage* getStorageDouble()
	{
		return storageDouble;
	}
	CubeStorage* getStorageString()
	{
		return storageString;
	}

	void removeFromMarker(RuleMarker*);

	void removeToMarker(RuleMarker*);

	void addFromMarker(RuleMarker*);

	void addToMarker(RuleMarker*);

	void checkFromMarkers(CubePage::key_t key);

	void clearAllMarkers();

	void rebuildAllMarkers();

	const set<RuleMarker*>& getFromMarkers() const
	{
		return fromMarkers;
	}

	Mutex* getFromMarkersLock()
	{
		return &cubeMasterLock;
	}

	bool checkNewMarkerRules();

	Mutex* getToMarkersLock()
	{
		return &cubeMasterLock;
	}

	const set<RuleMarker*>& getToMarkers() const
	{
		return toMarkers;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief check the user access rights for a cell path
	////////////////////////////////////////////////////////////////////////////////

	virtual void checkPathAccessRight(User* user, const CellPath* cellPath, RightsType minimumRight);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief get the minimum access right for the cube
	////////////////////////////////////////////////////////////////////////////////

	virtual RightsType getMinimumAccessRight(User* user);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief get the cube access right
	////////////////////////////////////////////////////////////////////////////////

	virtual RightsType getCubeAccessRight(User*);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief get the minimum access right for the cube
	////////////////////////////////////////////////////////////////////////////////

	virtual RightsType getElementAccessRight(User*, Dimension*, Element*);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sort pages of cube storage
	////////////////////////////////////////////////////////////////////////////////

	void sortAllCubePages();

	// Version 3.0
	void fillHashAreaStorage2(HashAreaStorage* hashStorage, vector<vector<Element*> > * area, EMemoryContext* mem_context);

	struct StructCube * getEngineCube();

	void setEngineCube(struct StructCube * e)
	{
		ecube = e;
		ecubeToken = token;
	}

	void FreeEngineCube(EMemoryContext* mem_context);

public:
    virtual void loadCubeCells(FileReader* fr) = 0;

protected:
	void setBaseCellValue(const IdentifiersType * path, const string& value);

	void loadCubeOverview(FileReader*);

	void loadCubeCellsDefault(FileReader* fr);

    void getCellValueFromStorageDefault(CellPath* cellPath, CellValueType& cellValue, bool* found);

	bool loadCubeJournal();

	void loadCubeRuleInfo(FileReader*);

	void loadCubeRule(FileReader*, int version);

	void loadCubeRules();

	void saveCubeOverview(FileWriter*);

	void saveCubeDimensions(FileWriter*);

	void saveCubeCells(FileWriter* file);

	void saveCubeRule(FileWriter* file, Rule* rule);

	void saveCubeRules();

	void checkSepRight(User* user, RightsType minimumRight);

protected:

	virtual void checkCubeAccessRight(User* user, RightsType minimumRight)
	{
		if (user != 0 && user->getRoleCubeRight() < minimumRight) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "insufficient access rights for cube", "user", (int)user->getIdentifier());
		}
	}

	virtual void checkCubeRuleRight(User* user, RightsType minimumRight)
	{
		// check cube data right
		if (user != 0 && user->getCubeDataRight(database, this) < RIGHT_READ) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "insufficient access rights for cube", "user", (int)user->getIdentifier());
		}

		if (user != 0 && user->getRoleRuleRight() < minimumRight) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "insufficient access rights for cube rules", "user", (int)user->getIdentifier());
		}
	}

private:
	void removeRule(Rule* rule);

	bool isInArea(CellPath* cellPath, string& areaIdentifier);

	void setCellValueConsolidated(CellPath* cellPath, double value, SplashMode splashMode, User* user, Lock* lock);

	// does not count elements with weight 0.0
	double countBaseElements(const CellPath * path);

	double computeBaseElements(const CellPath * path, vector<IdentifiersWeightType> * baseElements, bool ignoreNullWeight);

	double computeBaseElements(const CellPath * path, vector<set<IdentifierType> > * baseElements);

	void deleteNumericBasePath(const vector<ElementsWeightType> * baseElements);

	void deleteNumericBasePathRecursive(size_t position, vector<ElementsWeightType>::const_iterator baseElementsIter, PathType * newPath);

	void setBaseElementsRecursive(vector<IdentifiersWeightType> *baseElements, double value, SplashMode splashMode, CellPath* cellPath, User* user, Lock* lock);

	void setBaseElementsRecursive(size_t position, vector<IdentifiersWeightType> *baseElements, IdentifiersType *path, double value, SplashMode splashMode, size_t * count, Lock* lock);

	void setBaseCellValue(const IdentifiersType * path, double value);

	void setBaseCellValue(const PathType * path, double value);

	void setBaseCellValue(const PathWeightType * path, double value);

	void setBaseCellValue(const PathType * path, const string& value);

	void setBaseCellValue(const PathWeightType * path, const string& value);

	void reorganizeCubeStorage(CubeStorage** storage);

	void makeOrderedElements(ElementsWeightType*);

	bool isEqualBaseElements(vector<ElementsWeightType>*, vector<ElementsWeightType>*);

	struct CopyElementInfo {
		CopyElementInfo(Element* e, bool check) :
			element(e), zero_check(check)
		{
		}
		;
		Element* element;
		bool zero_check;
		std::vector<CopyElementInfo> children;
	};

	bool computeCompatibleElements_m3(CellPath* cellPathFrom, vector<vector<CopyElementInfo> >& baseElementsFrom, CellPath* cellPathTo, vector<vector<CopyElementInfo> >& baseElementsTo);

	bool computeCompatibleElements_m3(Dimension* dimension, Element* from, set<Element*>& accessedFromElements, vector<CopyElementInfo>& baseElementsFrom, Element* to, set<Element*>& accessedToElements, vector<CopyElementInfo>& baseElementsTo);

	bool computeSplashElements_m3(CellPath* cellPathFrom, vector<vector<CopyElementInfo> >& baseElementsFrom);

	bool computeSplashElements_m3(Dimension* dimension, Element* from, set<Element*>& accessedFromElements, vector<CopyElementInfo>& baseElementsFrom);

	void copyCellValuesRecursive_m3(vector<CopyElementInfo*>& from, vector<CopyElementInfo*>& to, vector<Element*>& fromElements, vector<Element*>& toElements, int lastDiggDimIndex, int zeroCheckCount, User* user, double factor, size_t* count, bool* addCubeToChangedMarkers, Lock * lock, EMemoryContext* mem_context);

	void copyCellValues(vector<vector<Element*> >*, vector<vector<Element*> >*, User* user, EMemoryContext* mem_context, double, Lock* lock);

	void copyCellValues_m3(vector<vector<CopyElementInfo> >* elementsWeigthFrom, vector<vector<CopyElementInfo> >* elementsWeigthTo, User* user, double factor, Lock* lock, EMemoryContext* mem_context);

	void copyCellValuesRecursive(vector<vector<Element*> >::iterator fromIterator, vector<vector<Element*> >::iterator endIterator, vector<vector<Element*> >::iterator toIterator, vector<Element*>* fromElements, vector<Element*>* toElements, int pos, User* user, EMemoryContext* mem_context, double factor, Lock * lock);

	void getParentElements(Dimension* dimension, Element* child, set<IdentifierType>* parents);

	double computeAreaBaseElements(vector<IdentifiersType>* paths, vector<map<IdentifierType, map<IdentifierType, double> > > *baseElements, bool ignoreWrongIdentifiers);
	void computeAreaBaseElementsRecursive(Dimension * dimension, IdentifierType elementId, Element * element, double weight, map<IdentifierType, map<IdentifierType, double> > * mapping);

	void computeVectorSets(vector<IdentifiersType>* area, IntervalVectorSet<IdentifierType>& consolidationVectorSet, bool ignoreWrongIdentifiers);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets cell/area and cell/values values from the cube
	///
	/// the storage contains the values
	/// baseElements contains the mapping of base elements to requested elements
	/// cellPathes is the list of cell pathes
	/// buildCellPathes should be true for (cell/area)
	/// updateConsolidationsStorage should be true
	////////////////////////////////////////////////////////////////////////////////

	void getCellValues(AreaStorage* storage, vector<IdentifiersType>* area, vector<IdentifiersType>* cellPathes, bool buildCellPathes, User* user, EMemoryContext* mem_context);

	void addSiblingsAndParents(set<IdentifierType>* identifiers, Dimension* dimension, IdentifierType id);
	void addElementAndParents(set<IdentifierType>* identifiers, Dimension* dimension, Element* child);

	Lock* lookupLockedArea(CellPath* cellPath, User* user);

	void setCellMarker(const uint32_t*);

	Lock* findCellLock(CellPath* cellPath);

public:
	CellLockInfo getCellLockInfo(CellPath* cellPath, IdentifierType userId);

	CubeWorker* getCubeWorker()
	{
		return cubeWorker;
	}

    vector<uint32_t>& getDimensionsMap()
    {
        return dimensionsMap;
    }


public:
	Mutex ecubeLock;

private:
	Mutex cubeMasterLock;
	Mutex cubeJurnalLock;
	Mutex storageStringLock;
protected:
	uint32_t token; // token for changes
	uint32_t clientCacheToken; // token for client cache changes
	IdentifierType identifier; // identifier of the cube
	string name; // name of the cube

	vector<Dimension*> dimensions; // list of dimensions used for the cube
	vector<size_t> sizeDimensions; // list of dimensions sizes

	//      vector<Rule*> rules; // list of rules
	RulesList rules;
	IdentifierType maxRuleIdentifier;

	Database * database; // database of cube

	bool deletable;
	bool renamable;

	CubeWorker* cubeWorker;
	bool hasArea;
	vector<string> workerAreaIdentifiers;
	vector<vector<set<IdentifierType> > > workerAreas;

	CubeStorage* storageDouble; // cell storage for NUMERIC values
	CubeStorage* storageString; // cell storage for STRING values

	CubeStatus status; // the status of the cell

	FileName * fileName; // file name of the cube
	FileName * ruleFileName; // file of the rules

	JournalFileWriter * journal; // journal writer of the cube

	uint32_t invalidateCacheCounter;

	static double cacheBarrier;
	static double cacheClearBarrierCells;

	static bool ignoreCellData;

	bool hasLock;
	vector<Lock*> locks;
	uint32_t maxLockId;

	set<RuleMarker*> fromMarkers;
	set<RuleMarker*> toMarkers;
	set<Rule*> newMarkerRules;

	struct StructCube * ecube;
	uint32_t ecubeToken;

    vector<uint32_t> dimensionsMap;             // list of dimensions map
};

}

#endif
