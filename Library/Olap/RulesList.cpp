////////////////////////////////////////////////////////////////////////////////
/// @brief palo cube
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Radu Ialovoi, yalos solutions ltd, Bucharest, Romania
////////////////////////////////////////////////////////////////////////////////

#include "Olap/RulesList.h"
#include "Olap/Rule.h"

namespace palo {

const std::vector<Rule *>& RulesList::ReadLock()
{
	m_Lock.readLock();
	return m_RulesContainer;
}

std::vector<Rule *>& RulesList::WriteLock()
{
	m_Lock.writeLock();
	return m_RulesContainer;
}

void RulesList::UnLock(bool for_write)
{
	if (for_write) {
		m_Lock.releaseWriteLock();
	} else {
		m_Lock.releaseReadLock();
	}
}

bool RulesList::empty()
{
	bool result;
	m_Lock.readLock();
	result = 0 == m_RulesContainer.size();
	m_Lock.releaseReadLock();

	return result;
}

void RulesList::push_back(Rule* rule)
{
	m_Lock.writeLock();
	m_RulesContainer.push_back(rule);
	m_Lock.releaseWriteLock();
}

void RulesList::erase(Rule* rule)
{
	m_Lock.writeLock();
	vector<Rule*>::iterator iter = find(m_RulesContainer.begin(), m_RulesContainer.end(), rule);

	if (iter != m_RulesContainer.end()) {
		m_RulesContainer.erase(iter);
		;
	}

	m_Lock.releaseWriteLock();
}

Rule* RulesList::get_by_ID(const IdentifierType id)
{
	Rule* result = NULL;
	m_Lock.readLock();
	for (vector<Rule*>::iterator iter = m_RulesContainer.begin(); iter != m_RulesContainer.end(); iter++) {
		if (id == (*iter)->getIdentifier()) {
			result = (*iter);
			break;
		}
	}
	m_Lock.releaseReadLock();
	return result;
}

bool RulesList::hasActiveRule()
{
	//	return !empty();
	bool result = false;
	m_Lock.readLock();
	for (vector<Rule*>::iterator iter = m_RulesContainer.begin(); iter != m_RulesContainer.end(); iter++) {
		if ((*iter)->isActive()) {
			result = true;
			break;
		}
	}
	m_Lock.releaseReadLock();
	return result;
}
}
