////////////////////////////////////////////////////////////////////////////////
/// @brief palo string area result storage
///
/// @file
///
/// The contents of this file are subject to the Jedox AG Palo license. You
/// may not use this file except in compliance with the license. You may obtain
/// a copy of the License at
///
/// <a href="http://www.palo.com/license.txt">
///   http://www.palo.com/license.txt
/// </a>
///
/// Software distributed under the license is distributed on an "AS IS" basis,
/// WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the license
/// for the specific language governing rights and limitations under the
/// license.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_AREA_STRING_RESULT_STORAGE_H
#define OLAP_AREA_STRING_RESULT_STORAGE_H 1

#include "palo.h"

#include <vector>
#include <set>

#include "Olap/AreaResultStorage.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief palo string area result storage
///
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS AreaStringResultStorage : public AreaResultStorage {

public:
	AreaStringResultStorage(const vector<map<Element*, uint32_t> >& hashMapping) :
		AreaResultStorage(hashMapping)
	{
		storage = new string[areaSize];
	}

	~AreaStringResultStorage()
	{
		delete[] storage;
	}

public:
	void setValue(const IdentifiersType* path, const string& value, IdentifierType idRule)
	{
		size_t pos = getPos(path);

		string * val = storage + pos;
		uint16_t * stat = status + pos;

		(*val) = value;

		if (idRule != Rule::NO_RULE) {
			(*stat) = RuleStatus | idRule;
		} else {
			(*stat) = BaseStatus;
		}
	}

	string* getValue(const IdentifiersType* path, IdentifierType *idRule, unsigned int *errorNumber, bool *found)
	{
		size_t pos = getPos(path);

		string* result = storage + pos;
		uint16_t * stat = status + pos;

		if (IsErrorStatus(*stat)) {
			(*found) = false;
			(*idRule) = 0;
			(*errorNumber) = getErrorType(stat);
		} else if (IsEmptyStatus(*stat)) {
			(*found) = false;
			(*idRule) = 0;
			(*errorNumber) = 0;
		} else if (IsRuleStatus(*stat)) {
			(*found) = true;
			(*idRule) = (*stat) & RuleNumberMask;
			(*errorNumber) = 0;
		}
		// is OK
		else {
			(*found) = true;
			(*idRule) = 0;
			(*errorNumber) = 0;
		}

		return result;
	}

protected:
	string * storage;
};

}

#endif
