/****************************************************************************
 **
 *W  Engine.h
 */

#ifndef ENGINE_H
#define ENGINE_H

#include "Collections/abattoir.h"
#include "Collections/list.h"
#include "Thread/WriteLocker.h"
#include "Cube.h"
#include "Database.h"

namespace palo {

/****************************************************************************
 **

 *defines rule error behaviour. affects results for division by zero
 */
extern bool ignoreRuleError;

/****************************************************************************
 **

 *T  Offset  . . . . . . . . . . . . . . . . . .  type of indices into buffers
 */
typedef uint32_t Offset;

/****************************************************************************
 **
 *T  Value . . . . . . . . . . . . . . . . . . . . type of the value of a cell
 **
 */
typedef double Value;

/****************************************************************************
 **
 *T  Status  . . . . . . . . . . . . . . . . . .  type of the status of a cell
 **
 */
typedef uint16_t Status;

#define ErrorStatus     0x8000
#define BaseStatus      0x4000
#define RuleStatus      0x2000
#define StetStatus      0x1000

#define RuleNumberMask	0x0FFF

#define IsEmptyStatus(s)                (((s) & 0xF000) == 0x0000)
#define IsErrorStatus(s)                (((s) & ErrorStatus) == ErrorStatus)
#define IsBaseStatus(s)                 (((s) & BaseStatus) == BaseStatus)
#define IsRuleStatus(s)                 (((s) & RuleStatus) == RuleStatus)

#define ErrorDatabaseNotFound           0x0040
#define ErrorCubeNotFound               0x0080
#define ErrorElementNotFound            0x00C0
#define ErrorInvalidCoordinates         0x0100
#define ErrorCircularRule		        0x0200
#define ErrorDivisionByZero		        0x0400

/****************************************************************************
 **

 *T  Environment . . .  type of environment to carry global information around
 **
 **  I wonder whether we really need this.
 */
typedef struct StructEnvironment {
	User * auser;
	FILE * file;
	uint32_t tei;
} Environment;

/****************************************************************************
 **

 *T  EDatabase . . . . . . . . . . . . . . . . . . . . . .  type of a database
 **
 */
typedef struct StructDatabase {
	uint32_t dummy;
} EDatabase;

/****************************************************************************
 **

 *T  EElement  . . . . . . . . . . . . . . type of an element, DO WE NEED THIS
 **
 **  element->dimension
 **  element->type N, S, C
 **  element->id
 **  element->name
 **  element->level/depth/height (oder was auch immer ;-)
 **  element->parents
 **  element->kids
 **  element->leafs
 **
 **  DO WE REALLY NEED THIS AT ALL
 */

/****************************************************************************
 **
 *T  EElementId  . . . . . . . . . . . . . . . . . . . . .  type of element ID
 **
 **  'ElementId' is the type of an element identifier.
 */
typedef uint32_t EElementId;

/****************************************************************************
 **
 *T  EDimension  . . . . . . . . . . . . . . . . . . . . . type of a dimension
 **
 **  dimension->database
 */
typedef struct StructDimension {
	/* EDatabase *         database; */
	uint32_t size;
	uint32_t nrCons;
	EElementId * conss;
	uint32_t nrBase;
	EElementId * bases;
	char *type;
	uint32_t maximal;
	Dimension * adimension;
} EDimension;

/****************************************************************************
 **
 *T  EBorder . . . . . .  type of the border of an area, subset of a dimension
 **
 */
typedef struct StructBorder {

	EDimension * dimension;

	uint32_t size; /* nrCons + nrBase                 */

	uint32_t nrCons;
	EElementId * conss; /* empty if nrCons == 0 or all     */

	uint32_t nrBase;
	EElementId * bases; /* empty if nrBase == 0 or all     */

} EBorder;

/****************************************************************************
 **
 *T  EDimMap . . . . . . . . . . . . type of a map from a dimension to offsets
 */
typedef struct StructEidOffWgt {
	EElementId eid;
	Offset off;
	Value wgt;
} EidOffWgt;

typedef struct StructDimMap {
	EElementId max;
	EBorder * source;
	EidOffWgt * raws;
	EidOffWgt * * eows;
} EDimMap;

/****************************************************************************
 **

 *T  EPath . . . . . . . . . . . . . . . . . . . . . . . . . . .  type of path
 **
 */

#define MAX_DIMENSIONS      256

typedef EElementId EPath[MAX_DIMENSIONS];

/****************************************************************************
 **
 *T  ECube . . . . . . . . . . . . . . . . . . . . . . . . . .  type of a cube
 **
 */
typedef struct StructCube {
	/* EDatabase *         database; */
	uint32_t nrDimensions;
	EDimension * dimensions[MAX_DIMENSIONS];
	uint32_t nrRules;
	TypeList rules_c;
	TypeList rules_n;
	TypeList rules_nm;
	TypeList rules_nu;
	Cube * acube;
} ECube;

/****************************************************************************
 **
 *T  EArea . . . . . . . . . . . . . . . . . . . . . . . . . . .  type of area
 **
 */
typedef struct StructArea {
	ECube * cube;
	EBorder * borders[MAX_DIMENSIONS];
	StructArea * next;
} EArea;

/****************************************************************************
 **
 *T  ECubeMap  . . . . . . type of the mapping from a nums-area to a cons-area
 **
 */
typedef struct StructCubeMap {
	EArea * source;
	EDimMap * dimmaps[MAX_DIMENSIONS];
} ECubeMap;

/****************************************************************************
 **


 *T  ERule . . . . . . . . . . . . . . . . . . . . . . . . . .  type of a rule
 **
 */
typedef uint16_t Bytecode;

typedef struct StructRule {

	ECube * cube;

	uint16_t nr_rule;

	EArea * dest_area;

	uint8_t is_base;
	uint8_t is_cons;

	uint8_t marker_flag; /* 1: has a marker                 */

	uint8_t ubm_flag; /* 1: has a uniq bijective marker  */
	EPath ubm_mask; /*[i] == 1: this dim is restricted */
	EPath ubm_dest;
	EPath ubm_dest_is_base;
	EPath ubm_source;
	EPath ubm_source_is_base;

	Bytecode * bytecode;
	Value * dbl_consts;
	string * str_consts;
	Value * dbl_stack;
	string * str_stack;
	EElementId * * copy_mask;
	EElementId * * copy_source;

	uint32_t gc_bc_nr;
	uint32_t gc_bc_max;
	uint32_t gc_dbl_const_nr;
	uint32_t gc_dbl_const_max;
	uint32_t gc_str_const_nr;
	uint32_t gc_str_const_max;
	uint32_t gc_dbl_stack_nr;
	uint32_t gc_dbl_stack_max;
	uint32_t gc_str_stack_nr;
	uint32_t gc_str_stack_max;
	uint32_t gc_copy_nr;

	Rule * arule;

	StructRule * next_c;
	StructRule * next_n;
	StructRule * next_nx;
} ERule;

static int bula = 0;

class Recursion_Stack : private std::vector<std::pair<Rule *, std::vector<EElementId> > > {
	static const unsigned int MAX_STACK_SIZE = 50 * 1024;

public:
	void push(ERule* rule, EElementId* path)
	{
		//TODO: implement here a real dependency graph

		bula++;

		uint32_t nrDims = rule->cube->nrDimensions;

		for (reverse_iterator i = rbegin(); i != rend(); i++) {
			if (i->first != rule->arule)
				continue;

			bool match = true;
			for (uint32_t j = 0; j < nrDims; j++) {
				if (i->second[j] != path[j]) {
					match = false;
					break;
				}
			}

			if (!match)
				continue;

			clear();

			Logger::warning << "recursion in rule found" << endl;
			throw ErrorException(ErrorException::ERROR_RULE_HAS_CIRCULAR_REF, "cube=" + StringUtils::convertToString(rule->cube->acube->getIdentifier()) + "&rule=" + StringUtils::convertToString(rule->nr_rule));

		}

		std::vector<EElementId> crt(nrDims);

		for (uint32_t j = 0; j < nrDims; j++) {
			crt[j] = path[j];
		}

		push_back(std::pair<Rule *, std::vector<EElementId> >(rule->arule, crt));

		if (MAX_STACK_SIZE < size()) {
			throw ErrorException(ErrorException::ERROR_INTERNAL, "Stack Overflow while evaluating: cube=" + StringUtils::convertToString(rule->cube->acube->getIdentifier()) + "&rule=" + StringUtils::convertToString(rule->nr_rule));
		}

	}

	void pop()
	{
		pop_back();
	}
};

class MarkerRecursion_Stack : private std::vector<EArea *> {

public:
	void push(EArea *area, ERule* rule);
	void pop()
	{
		pop_back();
	}
};

/****************************************************************************
 **
 *
 *memory allocation and stack corruption check
 */
class EMemoryContext {
private:
	static int m_alloc_count;
	std::set<void*> m_live_pointers;
	size_t m_last_mem_alloc;
	Mutex m_mutex;
	SimpleCache::query_cache_type m_querycache;
	bool m_query_cache_full;
	size_t m_querycache_writes;
	bool rules;
public:
	Recursion_Stack m_recursion_stack;
	MarkerRecursion_Stack m_marker_recursion_stack;

	EMemoryContext() :
		m_last_mem_alloc(0),m_query_cache_full(false),m_querycache_writes(0),rules(true)
	{
	}
	~EMemoryContext();

	bool readQueryCache(const Cube *acube, const IdentifiersType &cellkey, Value &val, Status &stat);
	bool readQueryCache(const Cube *acube, const IdentifiersType &cellkey, Cube::CellValueType &cellValue, bool* found);
	void writeQueryCache(const Cube *acube, const IdentifiersType &cellkey, const double val, const uint16_t stat);
	void writeQueryCache(const Cube *acube, const EPath pathBegin, const EPath pathEnd, const double val, const uint16_t stat);

	void cleanup()
	{
		WriteLocker lock(&m_mutex);
		if (0 != m_live_pointers.size()) {
			Logger::error << "found orphaned memory" << endl;
		}
		for (std::set<void*>::iterator i = m_live_pointers.begin(); i != m_live_pointers.end(); i++) {
			free(*i);
		}
	}

	void push(void* data, size_t size)
	{
		WriteLocker lock(&m_mutex);

		m_live_pointers.insert(data);
		m_last_mem_alloc = size;
		m_alloc_count++;
	}

	bool pop(void* data)
	{
		WriteLocker lock(&m_mutex);

		m_alloc_count--;
		std::set<void*>::iterator item = m_live_pointers.find(data);
		if (m_live_pointers.end() == item)
			return false;
		m_live_pointers.erase(item);
		return true;
	}

	void noRules() {rules = false;}
	bool calcRules() {return rules;}
};

//typedef std::vector< std::pair< uint16_t, EElementId* > >  EStack;

/****************************************************************************
 **
 *F  NewEntryRule(rule,cube) . . . . . . . . .  allocate a new rule upon entry
 **
 */
void EmitForceTypeCode(ERule * erule, uint8_t have, uint8_t want);

void EmitNopCode(ERule * erule);

void EmitHaltCode(ERule * erule);

void EmitLdConstDblCode(ERule * erule, double dbl, EMemoryContext* mem_context);

void EmitLdConstStrCode(ERule * erule, string str);

void EmitSourceCode(ERule * erule, vector<IdentifierType> * elementIDs, vector<bool> * isRestricted, bool marker, uint8_t want, EMemoryContext* mem_context);

void EmitVariableStrCode(ERule * erule, uint8_t dim);

void EmitOp2DblCode(ERule * erule, string name, EMemoryContext* mem_context);

void EmitOp1DblCode(ERule * erule, string name, EMemoryContext* mem_context);

void EmitFuncCode(ERule * erule, string name);

void EmitOp2ComparisonDblCode(ERule * erule, string name);

void EmitOp2ComparisonStrCode(ERule * erule, string name);

void EmitCallDataDblCode(ERule * erule, uint8_t num);

void EmitCallDataStrCode(ERule * erule, uint8_t num);

void EmitIfCondCode(ERule * erule, uint32_t * end_of_cond, uint32_t * end_of_if);

void EmitIfIfCode(ERule * erule, uint32_t * end_of_cond, uint32_t * end_of_if);

void EmitIfElseCode(ERule * erule, uint32_t * end_of_cond, uint32_t * end_of_if);

void EmitContinue(ERule * erule);

void EmitStet(ERule * erule);

void EmitAggrCode(ERule * erule, string name, uint8_t num);

void EmitIsError(ERule * erule, uint8_t num);

ERule * NewEntryRule(Rule * rule, ECube * cube, EMemoryContext* mem_context, bool register_mem = true);

/****************************************************************************
 **

 *T  EBuffer . . . . . . . . . type of the buffer to hold the computed results
 **
 **  'EBuffer' is the type of buffer that holds the results of a computation.
 **
 **  '<buf>->values' is a vector of doubles that holds the values.
 **  '<buf>->status' is a vector if ints that holds the metainformation
 **  (empty, cons, rule-<nr>, error).
 */
typedef struct StructBuffer {
	Value * values;
	Status * status;
} EBuffer;

/****************************************************************************
 **

 *T  EAction . . . . . . . . . . . . . . . . . . .  abstract type of an action
 **
 **  'Action' is the abstract data type for an action.
 */
typedef void (* ApplyAction)(void * action, EPath path, Value value, Status status, Environment * env, EMemoryContext* mem_context);

typedef struct StructAction {
	ApplyAction apply_;
} EAction;

/****************************************************************************
 **
 *T  RuleSrcFillAction . type of action that applies a rule and fills a buffer
 **
 **  'RuleSrcFillAction' is the type of actions that first evaluate a rule and
 **  then write the result into a result buffer.
 **
 **  Note that rule, map, and buf do not belong to the action, so they must
 **  not be deallocated when the action is deallocated.
 **
 **  Maybe 'RuleSrcFillActions' are never dynamically allocated (only as
 **  automatic variables in FillRuleBuffer and the like).
 */
typedef struct StructRuleSrcFillAction {
	EAction action;
	ERule * rule;
	ECubeMap * map;
	EBuffer * buf;
	/* Bytecode            bytecode; */
	/* Constants           constants; */
} RuleSrcFillAction;

/****************************************************************************
 **
 *T  RuleDstFillAction . type of action that applies a rule and fills a buffer
 **
 **  'RuleDstFillAction' is the type of actions that first evaluate a rule and
 **  then write the result into a result buffer.
 **
 **  Note that rule, map, and buf do not belong to the action, so they must
 **  not be deallocated when the action is deallocated.
 **
 **  Maybe 'RuleDstFillActions' are never dynamically allocated (only as
 **  automatic variables in FillRuleBuffer and the like).
 */
typedef struct StructRuleDstFillAction {
	EAction action;
	ERule * rule;
	ECubeMap * map;
	EBuffer * buf;
	/* Bytecode            bytecode; */
	/* Constants           constants; */
} RuleDstFillAction;

/****************************************************************************
 **
 *T  RuleSrcChainAction  type of action that applies a rule and chains another
 **
 */
typedef struct StructRuleSrcChainAction {
	EAction action;
	ERule * rule;
	EAction * chain;
	/* Bytecode            bytecode; */
	/* Constants           constants; */
} RuleSrcChainAction;

/****************************************************************************
 **
 *T  RuleDstChainAction  type of action that applies a rule and chains another
 **
 */
typedef struct StructRuleDstChainAction {
	EAction action;
	ERule * rule;
	EAction * chain;
	/* Bytecode            bytecode; */
	/* Constants           constants; */
} RuleDstChainAction;

/****************************************************************************
 **
 *F  NewEntryCube(cube,user) . . . . . . . . .  allocate a new cube upon entry
 **
 */
ECube * NewEntryCube(Cube * acube, const User * auser, EMemoryContext* mem_context);

/****************************************************************************
 **

 *F  IsPathInArea(area,path) . . . . . . . .  test wheter a path is in an area
 **
 */
uint8_t IsPathInArea(EArea * area, EPath path);

/****************************************************************************
 **
 *F  IsBasePathInArea(area,path) . . . . test wheter a base path is in an area
 **
 */
uint8_t IsBasePathInArea(EArea * area, EPath path);

/****************************************************************************
 **
 *F  SrcPathRule(src,rule,dst) . . . . map a destination path to a source path
 **
 */
void SrcPathRule(EPath srcPath, ERule * rule, EPath dstPath);

/****************************************************************************
 **
 *F  NewPathArea(path) . . . . . . . . . . create a singleton area from a path
 **
 */
EArea * NewPathArea(ECube * cube, EPath path, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  NewBuffer(size) . . . . . . . . . . . . . . . . . . allocate a new buffer
 **
 **  'NewBuffer'  allocates a  new  buffer  and returns  the  pointer to  this
 **  buffer.  The  vectors 'values' and 'status'  in the buffer  have room for
 **  <size> entries.
 */
EBuffer * NewBuffer(Offset size, EMemoryContext* mem_context);

/****************************************************************************
 **

 *F  FreeBuffer(buf) . . . . . . . . . . . . . . . . . . . deallocate a buffer
 **
 **  'FreeBuffer' deallocates the buffer <buf> again.
 **
 */
void FreeBuffer(EBuffer * buf, EMemoryContext* mem_context);

/****************************************************************************
 **

 *F  FreeArea(area)  . . . . . . . . . . . . . . . . . . .  deallocate an area
 */
void FreeArea(EArea * area, EMemoryContext* mem_context);

/****************************************************************************
 **

 *F  GetValueNoRule()  . . . . . . . . . . . . . . . . . . .  computes value
 */
void GetValueNoRule(Value * ptrValue, Status * ptrStatus, ECube * cube, EPath path, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **

 *F  FillAreaBuffer(buf,dst,env) . .  fill a buffer with the values of an area
 **
 */
void FillAreaBuffer(EBuffer * buf, EArea * dst, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  FillRuleBuffer(buf,map,dst,rule,env)  . .  fill a buffer with rule values
 **
 */
void FillRuleBuffer(EBuffer * buf, ECubeMap * map, EArea * dst, ERule * rule, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  FillBaseBuffer(buf,map,dst,env) . . . . .  fill a buffer with base values
 **
 */
void FillBaseBuffer(EBuffer *buf, ECubeMap *map, EArea *dst, Environment *env, EMemoryContext *mem_context, bool useMarkers);

/****************************************************************************
 **

 *F  EnumAreaAction(act,dst,env) . . . enumerate an area and execute an action
 **
 **  'EnumAreaAction' enumerates the nonzero values in the area <dst> and for
 **  each executes the action <act>.  The environment <env> is passed as final
 **  argument to <act>.
 */
void EnumAreaAction(EAction * act, EArea * dst, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  EnumConsAction(action,dst,env)  . . . . . . .  enumerate over cons values
 **
 **  'EnumConsAction'  calls  the function  <dest_func>  for all  consolidated
 **  cells in the area <dest_area> passing <dest_env> as third argument.  Note
 **  that  it simply  ignores any  non consolidated  cells in  the destination
 **  area.
 */
void EnumConsAction(EAction * action, EArea * area, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  EnumRuleAction(action,dst,rule,env) . . . . .  enumerate over rule values
 **
 */
void EnumRuleAction(EAction * dstAct, EArea * dst, ERule * rule, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  EnumBaseAction(action,dst,env)  . . . . . . .  enumerate over base values
 **
 **  run over entire base and apply function where appropriate
 */
void EnumBaseAction(EAction * act, EArea * dst, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  EnumBaseMarkerAction(action,dst,env)  . . . .  enumerate over base values
 **
 **  run over entire base and apply function where appropriate
 */
void EnumBaseMarkerAction(EAction * act, EArea * dst, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  EnumBaseZeroAction(action,dst,env)  . . . . .  enumerate over base values
 **
 **  run over entire base and apply function where appropriate
 */
void EnumBaseZeroAction(EAction * act, EArea * dst, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  EngineCell(ptrValue,ptrStatus,apath,acube,auser)  . . entry to the engine
 **
 */
uint8_t EngineCellHasRule(Value * ptrValue, Status * ptrStatus, IdentifierType * apath, Cube * acube, User * auser, EMemoryContext* mem_context);

void EngineCell(Value * ptrValue, Status * ptrStatus, IdentifiersType * apath, Cube * acube, User * auser);

uint8_t EngineAreaHasRule(vector<vector<Element*> > * aarea, Cube * acube, User * auser, EMemoryContext* mem_context);

void FreeCube(ECube * cube, EMemoryContext* mem_context);

/****************************************************************************
 **
 *F  Engine(storage,area,cube,user)  . . . . . . . . . . . entry to the engine
 **
 */
void Engine(HashAreaStorage * astorage, vector<vector<Element*> > * aarea, Cube * acube, User * auser, EMemoryContext* mem_context);

}

#endif
