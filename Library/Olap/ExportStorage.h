////////////////////////////////////////////////////////////////////////////////
/// @brief palo export storage
///
/// @file
///
/// The contents of this file are subject to the Jedox AG Palo license. You
/// may not use this file except in compliance with the license. You may obtain
/// a copy of the License at
///
/// <a href="http://www.palo.com/license.txt">
///   http://www.palo.com/license.txt
/// </a>
///
/// Software distributed under the license is distributed on an "AS IS" basis,
/// WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the license
/// for the specific language governing rights and limitations under the
/// license.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_EXPORT_STORAGE_H
#define OLAP_EXPORT_STORAGE_H 1

#include "palo.h"

#include <set>

#include "Exceptions/ErrorException.h"

#include "Logger/Logger.h"

#include "Olap/CellPath.h"
#include "Olap/Cube.h"
#include "Olap/CubeIndex.h"
#include "Olap/ExportPage.h"
#include "Olap/Element.h"
#include "Olap/StringIterator.h"

namespace palo {
class Condition;

////////////////////////////////////////////////////////////////////////////////
/// @brief palo export storage
///
///
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS ExportStorage {

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief Creates a filled export storage
	////////////////////////////////////////////////////////////////////////////////

	ExportStorage(const vector<Dimension*>* dimensions, vector<IdentifiersType>* area, size_t blocksize);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief Remove a export storage
	////////////////////////////////////////////////////////////////////////////////

	~ExportStorage();

public:
	////////////////////////////////////////////////////////////////////////////////
	/// @brief removes a value
	////////////////////////////////////////////////////////////////////////////////

	template<typename PATH>
	void deleteCell(const PATH path)
	{
		fillKeyBuffer(tmpKeyBuffer, path);

		ExportPage::element_t element = index->lookupKey(tmpKeyBuffer);

		if (element == 0) {
			return;
		}

		// delete old value
		delete *(Cube::CellValueType**)element;

		page->removeElement(element);
		numberElements--;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief copies the value ("valueSize" bytes) to the cube storage
	////////////////////////////////////////////////////////////////////////////////

	template<typename PATH>
	void setCellValue(const PATH path, CubePage::value_t value)
	{
		CubePage::key_t keyBuffer = tmpElementBuffer + valueSize;
		memcpy(tmpElementBuffer, value, valueSize);

		fillKeyBuffer(keyBuffer, path);

		CubePage::element_t element = index->lookupKey(keyBuffer);

		if (element == 0) {
			addNewCell(tmpElementBuffer);
		} else {
			// delete old value
			delete *(Cube::CellValueType**)element;

			memcpy(element, tmpElementBuffer, elementSize);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns a pointer to a cell value
	///
	/// The method returns "0" for an undefined value
	////////////////////////////////////////////////////////////////////////////////

	template<typename PATH>
	ExportPage::element_t getCellValue(const PATH path)
	{
		fillKeyBuffer(tmpKeyBuffer, path);

		return index->lookupKey(tmpKeyBuffer);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief removes empty cells
	////////////////////////////////////////////////////////////////////////////////
	void trim_empty()
	{
		size_t i = 0;
		while (i < size()) {
			if (UNDEFINED == getCell(i)->type) {
				removeCell(i);
			} else {
				i++;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief number of used rows
	////////////////////////////////////////////////////////////////////////////////

	size_t size() const
	{
		return (numberElements > blocksize) ? blocksize : numberElements;
	}
	;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief memory size of a value in byte
	////////////////////////////////////////////////////////////////////////////////

	size_t getValueSize() const
	{
		return valueSize;
	}
	;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief adds a double value to an element
	///
	/// creates the element if it is missing
	////////////////////////////////////////////////////////////////////////////////

	void addDoubleValue(uint8_t * keyBuffer, double value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief adds a string value
	///
	/// creates the element if it is missing
	////////////////////////////////////////////////////////////////////////////////

	void addStringValue(uint8_t * keyBuffer, string& value);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the number of elements
	///
	///
	////////////////////////////////////////////////////////////////////////////////

	size_t getNumberElements() const
	{
		return numberElements;
	}
	;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets a new start path
	///
	/// Begin export after the given path
	/// returns true if it was possible to find a new start path
	////////////////////////////////////////////////////////////////////////////////

	bool setStartPath(const IdentifiersType* path);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the first path
	////////////////////////////////////////////////////////////////////////////////

	IdentifiersType* getFirstPath()
	{
		return &firstPath;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the last path
	////////////////////////////////////////////////////////////////////////////////

	IdentifiersType* getLastPath()
	{
		return &lastPath;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sets a new area
	////////////////////////////////////////////////////////////////////////////////

	void setArea(vector<IdentifiersType>* area);

	bool hasBlocksizeElements()
	{
		return blocksize == numberElements;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the position of the last checked element of the last loop
	////////////////////////////////////////////////////////////////////////////////

	size_t getLastCheckedElement()
	{
		return lastCheckedElement;
	}

	bool isGreaterThanLast(uint8_t * keyBuffer)
	{
		if (0 == numberElements)
			return true;
		return !page->isLess(keyBuffer - valueSize, page->getLastElement());
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns a value
	////////////////////////////////////////////////////////////////////////////////

	Cube::CellValueType* getCell(size_t pos);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns a value and fills a path vector
	////////////////////////////////////////////////////////////////////////////////

	Cube::CellValueType* getCell(size_t pos, IdentifiersType* path);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief removes a cell
	////////////////////////////////////////////////////////////////////////////////

	void removeCell(size_t pos);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns true if a partial path is between first and last path
	////////////////////////////////////////////////////////////////////////////////

	bool isInStorageArea(IdentifierType id1);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns true if a partial path is between first and last path
	////////////////////////////////////////////////////////////////////////////////

	bool isInStorageArea(IdentifierType id1, IdentifierType id2);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns true if a path between first and last path
	///
	/// sets the number of the first wrong dimension in errorPos
	////////////////////////////////////////////////////////////////////////////////

	bool isInStorageArea(ExportPage::key_t keyBuffer, size_t& errorPos);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns number of elements in area (used for the progress bar)
	////////////////////////////////////////////////////////////////////////////////

	double getAreaSize();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns position of last element (used for the progress bar)
	////////////////////////////////////////////////////////////////////////////////

	double getPosNumber();

	void print();

	const size_t getBlocksize() const
	{
		return blocksize;
	}

private:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief fills path
	////////////////////////////////////////////////////////////////////////////////

	void fillPath(const ExportPage::element_t row, IdentifiersType* path)
	{
		const IdentifierType * buffer = (const IdentifierType*)(row + valueSize);
		IdentifiersType::iterator pathIter = path->begin();

		for (; pathIter != path->end(); pathIter++, buffer++) {
			*pathIter = *buffer;
		}
	}

	void fillKeyBuffer(ExportPage::key_t keyBuffer, const IdentifiersType * path)
	{
		IdentifierType * buffer = (IdentifierType*)keyBuffer;
		IdentifiersType::const_iterator pathIter = path->begin();

		for (; pathIter != path->end(); pathIter++) {
			*buffer++ = (IdentifierType)(*pathIter);
		}
	}

	void fillKeyBuffer(ExportPage::key_t keyBuffer, const PathType * path)
	{
		IdentifierType * buffer = (IdentifierType*)keyBuffer;
		PathType::const_iterator pathIter = path->begin();

		for (; pathIter != path->end(); pathIter++) {
			*buffer++ = (IdentifierType)(*pathIter)->getIdentifier();
		}
	}

	void fillKeyBuffer(ExportPage::key_t keyBuffer, const PathWeightType * path)
	{
		IdentifierType * buffer = (IdentifierType*)keyBuffer;
		PathType::const_iterator pathIter = path->first.begin();

		for (; pathIter != path->first.end(); pathIter++) {
			*buffer++ = (IdentifierType)(*pathIter)->getIdentifier();
		}
	}

	void fillKeyBuffer(ExportPage::key_t keyBuffer, ExportPage::key_t path)
	{
		memcpy(keyBuffer, path, keySize);
	}

	void fillKeyBuffer(ExportPage::key_t keyBuffer, const CellPath * path)
	{
		fillKeyBuffer(keyBuffer, path->getPathIdentifier());
	}

	bool isInStorageArea(ExportPage::key_t keyBuffer);

	void updateLastPath(ExportPage::key_t keyBuffer);

	void advanceToPath(ExportPage::key_t keyBuffer);

	void addNewCell(uint8_t * elementBuffer);

private:
	////////////////////////////////////////////////////////////////////////////////
	/// @brief total number of elements
	////////////////////////////////////////////////////////////////////////////////

	size_t numberElements;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief blocksize
	////////////////////////////////////////////////////////////////////////////////

	size_t blocksize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief maximum possible value for dimension identifier
	////////////////////////////////////////////////////////////////////////////////

	vector<size_t> maxima;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief the used page
	////////////////////////////////////////////////////////////////////////////////

	ExportPage * page;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief memory size needed to store a path
	////////////////////////////////////////////////////////////////////////////////

	size_t keySize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief memory size needed to store a value
	////////////////////////////////////////////////////////////////////////////////

	size_t valueSize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief memory size needed to store the path and the value
	////////////////////////////////////////////////////////////////////////////////

	size_t elementSize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief Pointer to a cube index object
	////////////////////////////////////////////////////////////////////////////////

	CubeIndex * index;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief temporary buffer
	////////////////////////////////////////////////////////////////////////////////

	uint8_t * tmpKeyBuffer;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief temporary buffer
	////////////////////////////////////////////////////////////////////////////////

	uint8_t * tmpElementBuffer;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief area
	////////////////////////////////////////////////////////////////////////////////

	vector<IdentifiersType>* area;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief first path of the storage
	////////////////////////////////////////////////////////////////////////////////

	IdentifiersType firstPath;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief last path of the storage
	////////////////////////////////////////////////////////////////////////////////

	IdentifiersType lastPath;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief last checked element of the last loop
	////////////////////////////////////////////////////////////////////////////////

	size_t lastCheckedElement;

	size_t areaLength;

	IdentifiersType crt_path;

	IdentifierType crt_raw_path[256];

	Cube* originator_cube;

	RulesList* rules;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief area
	////////////////////////////////////////////////////////////////////////////////

	vector<set<IdentifierType> > areaSets;

	vector<IdentifiersType::iterator> end_iterators;
	vector<IdentifiersType::iterator> begin_iterators;
	vector<IdentifiersType::iterator> crt_iterators;

	void advanceIterators();
	void advanceIterators(const IdentifiersType& target);
	void advanceIterators(IdentifierType* target);
	bool findConsolidationBelow(IdentifiersType& consPath, IntervalVectorSet<IdentifierType>& consolidationSet, const IdentifiersType& maxPath);

	bool isInArea(const IdentifiersType& path)
	{
		for (size_t i = 0; i < areaLength; i++) {
			if ((areaSets[i]).end() == (areaSets[i]).find(path[i]))
				return false;
		}
		return true;
	}

	bool compare_crt_path_with_path(IdentifierType* target_path)
	{
		if (NULL == target_path)
			return false;
		for (size_t i = areaLength; i > 0; i--) {
			if (crt_path[i - 1] != target_path[i - 1])
				return false;
		}
		return true;
	}

	bool path_smaller_eg_path(const IdentifiersType& left, const IdentifierType* right)
	{
		for (size_t i = 0; i < areaLength; i++) {
			if (left[i] < right[i])
				return true;
			if (left[i] > right[i])
				return false;
		}

		return true; //equal
	}

	bool path_smaller_eg_path(const IdentifiersType& left, const IdentifiersType& right)
	{
		for (size_t i = 0; i < areaLength; i++) {
			if (left[i] < right[i])
				return true;
			if (left[i] > right[i])
				return false;
		}

		return true; //equal
	}
public:
	bool is_crt_path_smaller_or_equalthan(vector<IdentifierType>& path)
	{
		for (size_t i = 0; i < areaLength; i++) {
			if (crt_path[i] < path[i])
				return true;
			if (crt_path[i] > path[i])
				return false;
		}

		return true; //equal
	}

	void setRulesList(RulesList* a_rules)
	{
		rules = a_rules;
	}

	void setOriginatorCube(Cube* originator_cube)
	{
		this->originator_cube = originator_cube;
	}

	void advancePath(const vector<Rule*>& basicRules, IntervalVectorSet<IdentifierType>& consolidationSet, StringIterator& stringsSet, Condition* condition, IdentifierType* target_path, bool addEmpty, bool addConsolidation, EMemoryContext* mem_context, uint32_t cellType);

	void advancePath(IntervalVectorSet<IdentifierType>& consolidationSet, StringIterator& stringsSet, Condition* condition, IdentifierType* target_path, bool addEmpty, bool addConsolidation, EMemoryContext* mem_context, uint32_t cellType);

	bool testEndPath()
	{
		IdentifierType* target_path = (IdentifierType*)alloca( sizeof( IdentifierType ) * areaLength );

		for (size_t i = 0; i < areaLength; i++) {
			target_path[i] = area->at(i).back();
		}

		bool result = compare_crt_path_with_path(target_path);
		if (result) {
			result = this->getCellValue(&crt_path) != NULL;
		}

		return result;
	}

	static bool findIndirectRuleMatch(Cube* cube, const vector<Rule*>& rules, IdentifierType* path, Cube::CellValueType& cellValue, EMemoryContext* mem_context);

};

}

#endif
