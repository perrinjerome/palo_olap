////////////////////////////////////////////////////////////////////////////////
/// @brief palo normal dimension
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_NORMAL_DIMENSION_H
#define OLAP_NORMAL_DIMENSION_H 1

#include "palo.h"

#include "Olap/BasicDimension.h"
#include "Olap/AttributesDimension.h"
#include "Olap/AttributedDimension.h"

#include "Thread/ReadWriteLock.h"
#include "Thread/ReadLocker.h"
#include "Thread/UpgradeLocker.h"

namespace palo {

class AttributesCube;
class RightsCube;

////////////////////////////////////////////////////////////////////////////////
/// @brief normal OLAP dimension
///
/// An OLAP dimension is an ordered list of elements
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS NormalDimension : public BasicDimension, public AttributedDimension {
public:
	static const uint32_t DIMENSION_TYPE = 1;

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief creates new dimension with given identifier
	////////////////////////////////////////////////////////////////////////////////

	NormalDimension(IdentifierType identifier, const string& name, Database* database) :
		BasicDimension(identifier, name, database), AttributedDimension()
	{
		rightsCube = 0;
	}

public:
	void notifyAddDimension();

	void beforeRemoveDimension();

	void notifyRenameDimension(const string& oldName);

	AttributesDimension* getAttributesDimension();

	AttributesCube* getAttributesCube();

	RightsCube* getRightsCube();

	vector<Element *> getElements(User *user, IdentifierType level = NO_IDENTIFIER);
	vector<Element *> getBaseElements(User *user);

	Element* findElement(IdentifierType elementIdentifier, User* user);
	Element * findElementByName(const string& name, User* user);
	Element * findElementByPosition(PositionType position, User* user);

public:
	void saveDimensionType(FileWriter* file);

public:
	ItemType getType()
	{
		return NORMAL;
	}

private:
	RightsCube* rightsCube;
	Mutex rightsCubeLock;

	vector<Element *> filterHideElements(vector<Element *> &allElements, User *user);

protected:
	bool hasStringAttributes();
};

}

#endif
