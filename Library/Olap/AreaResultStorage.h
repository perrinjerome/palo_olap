////////////////////////////////////////////////////////////////////////////////
/// @brief palo area result storage
///
/// @file
///
/// The contents of this file are subject to the Jedox AG Palo license. You
/// may not use this file except in compliance with the license. You may obtain
/// a copy of the License at
///
/// <a href="http://www.palo.com/license.txt">
///   http://www.palo.com/license.txt
/// </a>
///
/// Software distributed under the license is distributed on an "AS IS" basis,
/// WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the license
/// for the specific language governing rights and limitations under the
/// license.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_AREA_RESULT_STORAGE_H
#define OLAP_AREA_RESULT_STORAGE_H 1

#include "palo.h"

#include <vector>
#include <map>
#include <string>

#include "Logger/Logger.h"
#include "Exceptions/ErrorException.h"

namespace palo {
class Element;
class Cube;

////////////////////////////////////////////////////////////////////////////////
/// @brief palo area result storage
///
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS AreaResultStorage {

public:
	static bool computeAreaParameters(Cube* cube, vector<vector<Element*> >& area, vector<map<Element*, uint32_t> >& hashMapping, vector<vector<vector<pair<uint32_t, double> > > >& numericMapping, vector<uint32_t>& hashSteps, vector<uint32_t>& lengths);

	static bool computeAreaParameters(Cube* cube, vector<vector<Element*> >& area, vector<map<Element*, uint32_t> >& hashMapping);

public:
	AreaResultStorage(const vector<map<Element*, uint32_t> >& hashMapping);

	~AreaResultStorage()
	{
		delete[] status;
	}

public:

	static unsigned int getErrorType(uint16_t * stat);

	static uint16_t setErrorCode(ErrorException::ErrorType errorType);

	void setErrorValue(const IdentifiersType* path, ErrorException::ErrorType errorType);

	bool containsPath(const IdentifiersType* path);

	const vector<map<IdentifierType, uint32_t> >* getMapping()
	{
		return &idMapping;
	}

	size_t getPos(const IdentifiersType* path)
	{
		size_t pos = 0;

		for (size_t i = 0; i < length; i++) {
			map<IdentifierType, uint32_t>& m = idMapping[i];
			pos += m[path->at(i)];
		}
		return pos;
	}

	uint16_t * getStatus()
	{
		return status;
	}

protected:
	size_t areaSize;
	uint16_t * status;
	size_t length;
	vector<map<Element*, uint32_t> > hashMapping;
	vector<map<IdentifierType, uint32_t> > idMapping;

};

}

#endif
