////////////////////////////////////////////////////////////////////////////////
/// @brief palo rollback storage
///
/// @file
///
/// The contents of this file are subject to the Jedox AG Palo license. You
/// may not use this file except in compliance with the license. You may obtain
/// a copy of the License at
///
/// <a href="http://www.palo.com/license.txt">
///   http://www.palo.com/license.txt
/// </a>
///
/// Software distributed under the license is distributed on an "AS IS" basis,
/// WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the license
/// for the specific language governing rights and limitations under the
/// license.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_ROLLBACK_STORAGE_H
#define OLAP_ROLLBACK_STORAGE_H 1

#include "palo.h"

#include <set>

#include "Exceptions/ErrorException.h"

#include "Logger/Logger.h"

#include "Olap/CellPath.h"
#include "Olap/Cube.h"
#include "Olap/RollbackPage.h"
#include "Olap/Dimension.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief palo rollback storage
///
/// A rollback storage uses a rollback page
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS RollbackStorage {

public:

	static void setMaximumMemoryRollbackSize(size_t maximum)
	{
		maximumMemoryRollbackSize = maximum;
	}

	static void setMaximumFileRollbackSize(size_t maximum)
	{
		maximumFileRollbackSize = maximum;
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief Creates a filled cache storage
	////////////////////////////////////////////////////////////////////////////////

	RollbackStorage(const vector<Dimension*>* dimensions, FileName* cubeFileName, IdentifierType id);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief Remove a cache storage
	////////////////////////////////////////////////////////////////////////////////

	~RollbackStorage();

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief get number of rollback steps
	////////////////////////////////////////////////////////////////////////////////

	size_t getNumberSteps()
	{
		return step2pos.size();
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief add a rollback step
	////////////////////////////////////////////////////////////////////////////////
	void addRollbackStep();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief rollback steps
	/// this removes the related values from the storage
	////////////////////////////////////////////////////////////////////////////////
	void rollback(Cube* cube, size_t numSteps, User* user);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief add a value to the rollback storage
	////////////////////////////////////////////////////////////////////////////////
	void addCellValue(const IdentifiersType* path, double * value);
	void addCellValue(const IdentifiersType* path, char * * value);
	void addCellValue(RollbackPage::buffer_t element);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief checks if the rollback storage can hold num more values
	////////////////////////////////////////////////////////////////////////////////

	bool hasCapacity(double num);

public:
	void print();

private:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief get a value at position "pos" of the rollback storage
	////////////////////////////////////////////////////////////////////////////////

	Cube::CellValueType* getCellValue(RollbackPage* page, IdentifiersType* path, size_t pos);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief fills path
	////////////////////////////////////////////////////////////////////////////////

	void fillPath(const RollbackPage::element_t row, IdentifiersType* path)
	{
		const IdentifierType * buffer = (const IdentifierType*)(row + valueSize);
		IdentifiersType::iterator pathIter = path->begin();

		for (; pathIter != path->end(); pathIter++, buffer++) {
			*pathIter = *buffer;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief fill key buffer with path
	////////////////////////////////////////////////////////////////////////////////

	void fillKeyBuffer(RollbackPage::key_t keyBuffer, const IdentifiersType * path)
	{
		IdentifierType * buffer = (IdentifierType*)keyBuffer;
		IdentifiersType::const_iterator pathIter = path->begin();

		for (; pathIter != path->end(); pathIter++) {
			*buffer++ = (IdentifierType)(*pathIter);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief check if the current page can hold one more element
	/// this method saves full pages to file and creates new pages
	////////////////////////////////////////////////////////////////////////////////

	void checkCurrentPage();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief creates a file name for a rollback page
	////////////////////////////////////////////////////////////////////////////////

	FileName computePageFileName(size_t identifier);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief save a rollback page to file
	////////////////////////////////////////////////////////////////////////////////

	void savePageToFile(RollbackPage* page, size_t identifier);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief create a new rollback page and fill the page from file
	////////////////////////////////////////////////////////////////////////////////

	RollbackPage* loadPageFromFile(size_t identifier);

private:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief total number of elements
	////////////////////////////////////////////////////////////////////////////////

	size_t numberElements;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief maximum possible value for dimension identifier
	////////////////////////////////////////////////////////////////////////////////

	vector<size_t> maxima;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief the used page
	////////////////////////////////////////////////////////////////////////////////

	RollbackPage * currentPage;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief memory size needed to store a path
	////////////////////////////////////////////////////////////////////////////////

	size_t keySize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief memory size needed to store a value
	////////////////////////////////////////////////////////////////////////////////

	size_t valueSize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief memory size needed to store the path and the value
	////////////////////////////////////////////////////////////////////////////////

	size_t elementSize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief temporary buffer
	////////////////////////////////////////////////////////////////////////////////

	uint8_t * tmpKeyBuffer;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief temporary buffer
	////////////////////////////////////////////////////////////////////////////////

	uint8_t * tmpElementBuffer;

	vector<pair<size_t, size_t> > step2pos;

	size_t numPages;

	FileName* fileName;

	size_t sizeSavedPages;

private:
	static size_t maximumFileRollbackSize;
	static size_t maximumMemoryRollbackSize;

};

}

#endif
