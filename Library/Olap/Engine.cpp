////////////////////////////////////////////////////////////////////////////////
/// @brief palo engine
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Martin Schoenert, triagens GmbH, Cologne, Germany
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
///
/// *W  Engine.cpp
///
////////////////////////////////////////////////////////////////////////////////

#include "palo.h"

#include "Collections/abattoir.h"
#include "Collections/list.h"

#include "Olap/Cube.h"
#include "Olap/CubeStorage.h"
#include "Olap/CubeIndex.h"
#include "Olap/CellPath.h"
#include "Olap/HashAreaStorage.h"
#include "Logger/Logger.h"
#include "Parser/Node.h"
#include "Parser/RuleNode.h"

#include "Olap/Engine.h"
#include "VirtualMachine/BytecodeGenerator.h"
#include "VirtualMachine/VirtualMachine.h"

#include <iostream>
#include <math.h>
#include <time.h>

/* #define TRACE_ENGINE */
#ifdef TRACE_ENGINE
#include <sys/time.h>
#ifdef RUSAGE_THREAD
#define TRACE_INIT_TIME struct rusage hr_beg, hr_end; getrusage( RUSAGE_THREAD, &hr_beg ); \
                        struct timeval hr_real;
#define TRACE_TIME (getrusage( RUSAGE_THREAD, &hr_end ), \
    (((double)(hr_end.ru_utime.tv_sec) - (double)(hr_beg.ru_utime.tv_sec)) \
    + ((double)(hr_end.ru_utime.tv_usec) - (double)(hr_beg.ru_utime.tv_usec)) / 1000000))
#else
#define TRACE_INIT_TIME struct rusage hr_beg, hr_end; getrusage( RUSAGE_SELF, &hr_beg ); \
                        struct timeval hr_real;
#define TRACE_TIME (getrusage( RUSAGE_SELF, &hr_end ), \
    (((double)(hr_end.ru_utime.tv_sec) - (double)(hr_beg.ru_utime.tv_sec)) \
    + ((double)(hr_end.ru_utime.tv_usec) - (double)(hr_beg.ru_utime.tv_usec)) / 1000000))
#endif
#define TRACE_REAL (gettimeofday( &hr_real, 0 ), (double)(hr_real.tv_sec) + (double)(hr_real.tv_usec) / 1000000)
#define TRACE_PRINTF(x) fprintf x
#else
#define TRACE_INIT_TIME
#define TRACE_TIME
#define TRACE_REAL
#define TRACE_PRINTF(x)
#endif

/* #define DEBUG_ENGINE */
#ifdef DEBUG_ENGINE
#define DEBUG_PRINTF(x) printf x
#else
#define DEBUG_PRINTF(x)
#endif

namespace palo {
/****************************************************************************
 **
 */
void * ecalloc(size_t n, size_t s, EMemoryContext* mem_context, bool register_mem = true)
{
	void * p;
	p = calloc(n, s);
	if (p == 0) {
		Logger::trace << "ecalloc cannot allocate memory (" << n << "," << s << ")" << endl;

		throw ErrorException(ErrorException::ERROR_OUT_OF_MEMORY, "tried to allocate " + StringUtils::convertToString((uint32_t)n * s) + " Bytes ");
		//exit( 1 );
	}
	if (register_mem) {
		mem_context->push(p, n * s);
	}
	return p;
}

void efree(void * data, EMemoryContext* mem_context)
{
	mem_context->pop(data);
	free(data);
}

int EMemoryContext::m_alloc_count;

bool EMemoryContext::readQueryCache(const Cube *acube, const IdentifiersType &cellkey, Value &val, Status &stat)
{
	if (!(size_t)Cube::getCacheBarrier()) {
		return false;
	}

	bool found = false;

	SimpleCache::query_cache_key qk(acube->getDatabase()->getIdentifier(), acube->getIdentifier());
	SimpleCache::query_cache_iterator qci = m_querycache.find(qk);
	if (qci != m_querycache.end()) {
		SimpleCache::const_cache_type_iterator qcci = qci->second.find(cellkey);
		if (qcci != qci->second.end()) {
			val = qcci->second.first;
			stat = qcci->second.second;
			found = true;
		}
	}
	return found;
}

bool EMemoryContext::readQueryCache(const Cube *acube, const IdentifiersType &cellkey, Cube::CellValueType &cellValue, bool* found)
{
	if (!(size_t)Cube::getCacheBarrier()) {
		return false;
	}

	Value val;
	Status stat;
	bool result;

	result = readQueryCache(acube, cellkey, val, stat);
	if (result) {
		cellValue.doubleValue = val;
		cellValue.type = NUMERIC;
		if (SimpleCache::NOT_FOUND == stat) {
			cellValue.rule = Rule::NO_RULE;
			(*found) = false;
		} else {
			cellValue.rule = SimpleCache::ruleFromStat(stat);
			(*found) = true;
		}
	}
	return result;
}

void EMemoryContext::writeQueryCache(const Cube *acube, const IdentifiersType &cellkey, const double val, const uint16_t stat)
{
	if (!(size_t)Cube::getCacheBarrier()) {
		return;
	}
	if (!m_query_cache_full) {
		SimpleCache::query_cache_key qk(acube->getDatabase()->getIdentifier(), acube->getIdentifier());
		m_querycache[qk][cellkey] = SimpleCache::cache_value_type(val, stat);
		if (!(m_querycache_writes++ % 1000)) {
			size_t total_size = 0;

			// check total size of query cache
			for (SimpleCache::query_cache_type::iterator qci = m_querycache.begin(); qci != m_querycache.end(); qci++) {
				total_size += qci->second.size();
			}
			if (total_size > 1000000) {
				Logger::debug << "Per query cache full!" << endl;
				m_query_cache_full = true;
			}
		}
	}
}

void EMemoryContext::writeQueryCache(const Cube *acube, const EPath pathBegin, const EPath pathEnd, const double val, const uint16_t stat)
{
	if (!(size_t)Cube::getCacheBarrier()) {
		return;
	}
	writeQueryCache(acube, IdentifiersType(pathBegin, pathEnd), val, stat);
}

EMemoryContext::~EMemoryContext()
{
	if (Logger::isDebug() && m_querycache.size()) {
		stringstream ss;
		uint32_t totalcells = 0;
		ss << "Query Cache: ";
		for (SimpleCache::query_cache_iterator qci = m_querycache.begin(); qci != m_querycache.end(); qci++) {
			totalcells += (uint32_t)qci->second.size();
			ss << qci->second.size() << "[" << qci->first.first << "," << qci->first.second << "];";
		}
		ss << "Total: " << totalcells << endl;
		;
		Logger::debug << ss.str();
	}
}

uint32_t NrCallocBases = 0;
uint32_t NrCallocConss = 0;
uint32_t NrCallocEArea = 0;
uint32_t NrCallocEBorder = 0;
uint32_t NrCallocEBuffer = 0;
uint32_t NrCallocECube = 0;
uint32_t NrCallocECubeMap = 0;
uint32_t NrCallocEDimension = 0;
uint32_t NrCallocEDimMap = 0;
uint32_t NrCallocEids = 0;
uint32_t NrCallocEnvironment = 0;
uint32_t NrCallocEows = 0;
uint32_t NrCallocERule = 0;
uint32_t NrCallocIsCons = 0;
uint32_t NrCallocRaws = 0;
uint32_t NrCallocStatus = 0;
uint32_t NrCallocValues = 0;

/****************************************************************************
 **

 *F  FreeBorder(border)  . . . . . . . . . . . . . . . . . deallocate a border
 **
 */
void FreeBorder(EBorder * border, EMemoryContext* mem_context)
{
	if (border) {
		if (border->nrCons < border->dimension->nrCons && border->conss != 0) {
			efree(border->conss, mem_context);
			NrCallocConss--;
		}
		if (border->nrBase < border->dimension->nrBase && border->bases != 0) {
			efree(border->bases, mem_context);
			NrCallocBases--;
		}
		efree(border, mem_context);
		NrCallocEBorder--;
	}
}

/****************************************************************************
 **
 *F  NewEmptyBorder()  . . . . . . . .  . . . . . . . .  allocate a new border
 **
 */
EBorder * NewEmptyBorder(EDimension * dimension, EMemoryContext* mem_context)
{
	EBorder * border;

	border = (EBorder*)ecalloc(1, sizeof(EBorder), mem_context, false); //todo Validate a container
	NrCallocEBorder++;

	border->dimension = dimension;
	border->size = 0;
	border->nrCons = 0;
	border->conss = 0;
	border->nrBase = 0;
	border->bases = 0;

	return border;
}

/****************************************************************************
 **
 *F  NewCopyBorder(border) . . . . . . . . . . . . allocate a copy of a border
 **
 */
EBorder * NewCopyBorder(EBorder * border, EMemoryContext* mem_context)
{
	EBorder * result;
	EDimension * dim;
	uint32_t i;

	dim = border->dimension;
	result = NewEmptyBorder(dim, mem_context);

	result->nrCons = border->nrCons;
	if (0 < border->nrCons && border->nrCons < border->dimension->nrCons) {
		result->conss = (EElementId*)ecalloc(result->nrCons, sizeof(EElementId), mem_context);
		NrCallocConss++;
		for (i = 0; i < result->nrCons; i++) {
			result->conss[i] = border->conss[i];
		}
	} else if (border->nrCons == border->dimension->nrCons) {
		result->conss = border->dimension->conss;
	}

	result->nrBase = border->nrBase;
	if (0 < border->nrBase && border->nrBase < border->dimension->nrBase) {
		result->bases = (EElementId*)ecalloc(result->nrBase, sizeof(EElementId), mem_context);
		NrCallocBases++;
		for (i = 0; i < result->nrBase; i++) {
			result->bases[i] = border->bases[i];
		}
	} else if (border->nrBase == border->dimension->nrBase) {
		result->bases = border->dimension->bases;
	}

	result->size = result->nrCons + result->nrBase;

	return result;
}

/****************************************************************************
 **
 *F  NewConsBorder(border) . . . . . . . . . . . . allocate a copy of a border
 **
 */
EBorder * NewConsBorder(EBorder * border, EMemoryContext* mem_context)
{
	EBorder * result;
	EDimension * dim;
	uint32_t i;

	dim = border->dimension;
	result = NewEmptyBorder(dim, mem_context);

	result->nrCons = border->nrCons;
	if (0 < border->nrCons && border->nrCons < border->dimension->nrCons) {
		result->conss = (EElementId*)ecalloc(result->nrCons, sizeof(EElementId), mem_context);
		NrCallocConss++;
		for (i = 0; i < result->nrCons; i++) {
			result->conss[i] = border->conss[i];
		}
	} else if (border->nrCons == border->dimension->nrCons) {
		result->conss = border->dimension->conss;
	}

	result->size = result->nrCons + result->nrBase;

	return result;
}

/****************************************************************************
 **
 *F  NewBaseBorder(border) . . . . . . . . . . . . allocate a copy of a border
 **
 */
EBorder * NewBaseBorder(EBorder * border, EMemoryContext* mem_context)
{
	EBorder * result;
	EDimension * dim;
	uint32_t i;

	dim = border->dimension;
	result = NewEmptyBorder(dim, mem_context);

	result->nrBase = border->nrBase;
	if (0 < border->nrBase && border->nrBase < border->dimension->nrBase) {
		result->bases = (EElementId*)ecalloc(result->nrBase, sizeof(EElementId), mem_context);
		NrCallocBases++;
		for (i = 0; i < result->nrBase; i++) {
			result->bases[i] = border->bases[i];
		}
	} else if (border->nrBase == border->dimension->nrBase) {
		result->bases = border->dimension->bases;
	}

	result->size = result->nrCons + result->nrBase;

	return result;
}

/****************************************************************************
 **
 *F  NewIntersectionBorder(border1,border2)  . . . . allocate the intersection C
 **
 */
uint32_t IsEmptyIntersectionBorder(EBorder * border1, EBorder * border2)
{
	uint32_t cons;
	uint32_t base;

	/* none * any || any * none */
	if (border1->nrCons == 0 || border2->nrCons == 0) {
		cons = 0;
	}

	/* one * one */
	else if (border1->nrCons == 1 && border2->nrCons == 1) {
		cons = (border1->conss[0] == border2->conss[0]);
	}

	/* some * some */
	else {
		cons = 1;
	}

	/* none * any || any * none */
	if (border1->nrBase == 0 || border2->nrBase == 0) {
		base = 0;
	}

	/* one * one */
	else if (border1->nrBase == 1 && border2->nrBase == 1) {
		base = (border1->bases[0] == border2->bases[0]);
	}

	/* some * some */
	else {
		base = 1;
	}

	return ((cons == 0) && (base == 0));
}

void MarkerRecursion_Stack::push(EArea *area, ERule* rule)
{
	uint32_t nrDims = area->cube->nrDimensions;

	for (reverse_iterator i = rbegin(); i != rend(); i++) {
		// test intersection of areas on stack with current one

		bool match = true;
		for (uint32_t j = 0; j < nrDims; j++) {
			if (IsEmptyIntersectionBorder(area->borders[j], (*i)->borders[j])) {
				match = false;
				break;
			}
		}

		if (!match) {
			continue;
		}
		clear();

		Logger::warning << "recursion in rule found" << endl;
		throw ErrorException(ErrorException::ERROR_RULE_HAS_CIRCULAR_REF, "cube=" + StringUtils::convertToString(area->cube->acube->getIdentifier()) + "&rule=" + StringUtils::convertToString(rule->nr_rule));
	}
	push_back(area);
}

EBorder * NewIntersectionBorder(EBorder * border1, EBorder * border2, EMemoryContext* mem_context)
{
	EBorder * border;
	EDimension * dim;
	uint32_t nr;
	EElementId * e1, *f1, *e2, *f2, *e;
	uint32_t i;

	dim = border1->dimension;
	border = NewEmptyBorder(dim, mem_context);

	/* none * any || any & none */
	if (border1->nrCons == 0 || border2->nrCons == 0) {
		border->nrCons = 0;
		border->conss = 0;
	}

	/* some * some */
	else if (border1->nrCons < dim->nrCons && border2->nrCons < dim->nrCons) {
		e1 = border1->conss;
		f1 = border1->conss + border1->nrCons;
		e2 = border2->conss;
		f2 = border2->conss + border2->nrCons;
		nr = 0;
		while (e1 < f1 && e2 < f2) {
			if (*e1 < *e2) {
				e1++;
			} else if (*e1 > *e2) {
				e2++;
			} else {
				nr++;
				e1++;
				e2++;
			}
		}
		if (0 < nr) {
			border->nrCons = nr;
			border->conss = (EElementId*)ecalloc(border->nrCons, sizeof(EElementId), mem_context);
			NrCallocConss++;
			e1 = border1->conss;
			f1 = border1->conss + border1->nrCons;
			e2 = border2->conss;
			f2 = border2->conss + border2->nrCons;
			e = border->conss;
			while (e1 < f1 && e2 < f2) {
				if (*e1 < *e2) {
					e1++;
				} else if (*e1 > *e2) {
					e2++;
				} else {
					*e++ = *e1++;
					e2++;
				}
			}
		} else {
			border->nrCons = 0;
			border->conss = 0;
		}
	}

	/* some * all */
	else if (border1->nrCons < dim->nrCons) {
		border->nrCons = border1->nrCons;
		border->conss = (EElementId*)ecalloc(border->nrCons, sizeof(EElementId), mem_context);
		NrCallocConss++;
		for (i = 0; i < border->nrCons; i++) {
			border->conss[i] = border1->conss[i];
		}
	}

	/* all * some */
	else if (border2->nrCons < dim->nrCons) {
		border->nrCons = border2->nrCons;
		border->conss = (EElementId*)ecalloc(border->nrCons, sizeof(EElementId), mem_context);
		NrCallocConss++;
		for (i = 0; i < border->nrCons; i++) {
			border->conss[i] = border2->conss[i];
		}
	}

	/* all * all */
	else {
		border->nrCons = dim->nrCons;
		border->conss = dim->conss;
	}

	/* none * any || any * none */
	if (border1->nrBase == 0 || border2->nrBase == 0) {
		border->nrBase = 0;
		border->bases = 0;
	}

	/* some * some */
	else if (border1->nrBase < dim->nrBase && border2->nrBase < dim->nrBase) {
		e1 = border1->bases;
		f1 = border1->bases + border1->nrBase;
		e2 = border2->bases;
		f2 = border2->bases + border2->nrBase;
		nr = 0;
		while (e1 < f1 && e2 < f2) {
			if (*e1 < *e2) {
				e1++;
			} else if (*e1 > *e2) {
				e2++;
			} else {
				nr++;
				e1++;
				e2++;
			}
		}
		if (0 < nr) {
			border->nrBase = nr;
			border->bases = (EElementId*)ecalloc(border->nrBase, sizeof(EElementId), mem_context);
			NrCallocBases++;
			e1 = border1->bases;
			f1 = border1->bases + border1->nrBase;
			e2 = border2->bases;
			f2 = border2->bases + border2->nrBase;
			e = border->bases;
			while (e1 < f1 && e2 < f2) {
				if (*e1 < *e2) {
					e1++;
				} else if (*e1 > *e2) {
					e2++;
				} else {
					*e++ = *e1++;
					e2++;
				}
			}
		} else {
			border->nrBase = 0;
			border->bases = 0;
		}
	}

	/* some * all */
	else if (border1->nrBase < dim->nrBase) {
		border->nrBase = border1->nrBase;
		border->bases = (EElementId*)ecalloc(border->nrBase, sizeof(EElementId), mem_context);
		NrCallocBases++;
		for (i = 0; i < border->nrBase; i++) {
			border->bases[i] = border1->bases[i];
		}
	}

	/* all * some */
	else if (border2->nrBase < dim->nrBase) {
		border->nrBase = border2->nrBase;
		border->bases = (EElementId*)ecalloc(border->nrBase, sizeof(EElementId), mem_context);
		NrCallocBases++;
		for (i = 0; i < border->nrBase; i++) {
			border->bases[i] = border2->bases[i];
		}
	}

	/* all * all */
	else {
		border->nrBase = dim->nrBase;
		border->bases = dim->bases;
	}

	border->size = border->nrCons + border->nrBase;

	return border;
}

/****************************************************************************
 **
 *F  NewComplementBorder(border1,border2)  . . . . . . allocate the complement C
 **
 */
EBorder * NewComplementBorder(EBorder * border1, EBorder * border2, EMemoryContext* mem_context)
{
	EBorder * border;
	EDimension * dim;
	uint32_t nr;
	EElementId * e1, *f1, *e2, *f2, *e;
	uint32_t i;

	dim = border1->dimension;
	border = NewEmptyBorder(dim, mem_context);

	/* none - any || any - all */
	if (border1->nrCons == 0 || border2->nrCons == dim->nrCons) {
		border->nrCons = 0;
		border->conss = 0;
	}

	/* some - some */
	else if (border1->nrCons < dim->nrCons && 0 < border2->nrCons) {
		e1 = border1->conss;
		f1 = border1->conss + border1->nrCons;
		e2 = border2->conss;
		f2 = border2->conss + border2->nrCons;
		nr = 0;
		while (e1 < f1 && e2 < f2) {
			if (*e1 < *e2) {
				nr++;
				e1++;
			} else if (*e1 > *e2) {
				e2++;
			} else {
				e1++;
				e2++;
			}
		}
		nr += (uint32_t)(f1 - e1);
		if (0 < nr) {
			border->nrCons = nr;
			border->conss = (EElementId*)ecalloc(border->nrCons, sizeof(EElementId), mem_context);
			NrCallocConss++;
			e1 = border1->conss;
			f1 = border1->conss + border1->nrCons;
			e2 = border2->conss;
			f2 = border2->conss + border2->nrCons;
			e = border->conss;
			while (e1 < f1 && e2 < f2) {
				if (*e1 < *e2) {
					*e++ = *e1++;
				} else if (*e1 > *e2) {
					e2++;
				} else {
					e1++;
					e2++;
				}
			}
			while (e1 < f1) {
				*e++ = *e1++;
			}
		} else {
			border->nrCons = 0;
			border->conss = 0;
		}
	}

	/* some - none */
	else if (border1->nrCons < dim->nrCons) {
		border->nrCons = border1->nrCons;
		border->conss = (EElementId*)ecalloc(border->nrCons, sizeof(EElementId), mem_context);
		NrCallocConss++;
		for (i = 0; i < border->nrCons; i++) {
			border->conss[i] = border1->conss[i];
		}
	}

	/* all - some */
	else if (0 < border2->nrCons) {
		e1 = dim->conss;
		f1 = dim->conss + dim->nrCons;
		e2 = border2->conss;
		f2 = border2->conss + border2->nrCons;
		nr = 0;
		while (e1 < f1 && e2 < f2) {
			if (*e1 < *e2) {
				nr++;
				e1++;
			} else if (*e1 > *e2) {
				e2++;
			} else {
				e1++;
				e2++;
			}
		}
		nr += (uint32_t)(f1 - e1);
		if (0 < nr) {
			border->nrCons = nr;
			border->conss = (EElementId*)ecalloc(nr, sizeof(EElementId), mem_context);
			NrCallocConss++;
			e1 = border1->conss;
			f1 = border1->conss + border1->nrCons;
			e2 = border2->conss;
			f2 = border2->conss + border2->nrCons;
			e = border->conss;
			while (e1 < f1 && e2 < f2) {
				if (*e1 < *e2) {
					*e++ = *e1++;
				} else if (*e1 > *e2) {
					e2++;
				} else {
					e1++;
					e2++;
				}
			}
			while (e1 < f1) {
				*e++ = *e1++;
			}
		} else {
			border->nrCons = 0;
			border->conss = 0;
		}
	}

	/* all - none */
	else {
		border->nrCons = dim->nrCons;
		border->conss = dim->conss;
	}

	/* none - any || any - all */
	if (border1->nrBase == 0 || border2->nrBase == dim->nrBase) {
		border->nrBase = 0;
		border->bases = 0;
	}

	/* some - some */
	else if (border1->nrBase < dim->nrBase && 0 < border2->nrBase) {
		e1 = border1->bases;
		f1 = border1->bases + border1->nrBase;
		e2 = border2->bases;
		f2 = border2->bases + border2->nrBase;
		nr = 0;
		while (e1 < f1 && e2 < f2) {
			if (*e1 < *e2) {
				nr++;
				e1++;
			} else if (*e1 > *e2) {
				e2++;
			} else {
				e1++;
				e2++;
			}
		}
		nr += (uint32_t)(f1 - e1);
		if (0 < nr) {
			border->nrBase = nr;
			border->bases = (EElementId*)ecalloc(border->nrBase, sizeof(EElementId), mem_context);
			NrCallocBases++;
			e1 = border1->bases;
			f1 = border1->bases + border1->nrBase;
			e2 = border2->bases;
			f2 = border2->bases + border2->nrBase;
			e = border->bases;
			while (e1 < f1 && e2 < f2) {
				if (*e1 < *e2) {
					*e++ = *e1++;
				} else if (*e1 > *e2) {
					e2++;
				} else {
					e1++;
					e2++;
				}
			}
			while (e1 < f1) {
				*e++ = *e1++;
			}
		} else {
			border->nrBase = 0;
			border->bases = 0;
		}
	}

	/* some - none */
	else if (border1->nrBase < dim->nrBase) {
		border->nrBase = border1->nrBase;
		border->bases = (EElementId*)ecalloc(border->nrBase, sizeof(EElementId), mem_context);
		NrCallocBases++;
		for (i = 0; i < border->nrBase; i++) {
			border->bases[i] = border1->bases[i];
		}
	}

	/* all - some */
	else if (0 < border2->nrBase) {
		e1 = dim->bases;
		f1 = dim->bases + dim->nrBase;
		e2 = border2->bases;
		f2 = border2->bases + border2->nrBase;
		nr = 0;
		while (e1 < f1 && e2 < f2) {
			if (*e1 < *e2) {
				nr++;
				e1++;
			} else if (*e1 > *e2) {
				e2++;
			} else {
				e1++;
				e2++;
			}
		}
		nr += (uint32_t)(f1 - e1);
		if (0 < nr) {
			border->nrBase = nr;
			border->bases = (EElementId*)ecalloc(nr, sizeof(EElementId), mem_context);
			NrCallocBases++;
			e1 = border1->bases;
			f1 = border1->bases + border1->nrBase;
			e2 = border2->bases;
			f2 = border2->bases + border2->nrBase;
			e = border->bases;
			while (e1 < f1 && e2 < f2) {
				if (*e1 < *e2) {
					*e++ = *e1++;
				} else if (*e1 > *e2) {
					e2++;
				} else {
					e1++;
					e2++;
				}
			}
			while (e1 < f1) {
				*e++ = *e1++;
			}
		} else {
			border->nrBase = 0;
			border->bases = 0;
		}
	}

	/* all - none */
	else {
		border->nrBase = dim->nrBase;
		border->bases = dim->bases;
	}

	border->size = border->nrCons + border->nrBase;

	return border;
}

/****************************************************************************
 **

 *F  FreeDimMap(dimmap)  . . . . . . . . . . . . .  deallocate a dimension map
 */
void FreeDimMap(EDimMap * dimmap, EMemoryContext* mem_context)
{
	if (dimmap) {
		if (dimmap->source) {
			FreeBorder(dimmap->source, mem_context);
		}
		if (dimmap->raws) {
			efree(dimmap->raws, mem_context);
			NrCallocRaws--;
		}
		if (dimmap->eows) {
			efree(dimmap->eows, mem_context);
			NrCallocEows--;
		}
		efree(dimmap, mem_context);
		NrCallocEDimMap--;
	}
}

/****************************************************************************
 **
 *F  NewDimMap(border,factor)  . . . . . . . . .  allocate a new dimension map
 **
 */
#define SORT_NAME           SortDimMap
#define SORT_TYPE           EidOffWgt
#define SORT_LESS(x,y)      ((x)->eid < (y)->eid)
#define SORT_COPY(x,y)      ((x)->eid = (y)->eid, (x)->off = (y)->off, (x)->wgt = (y)->wgt)
#define SORT_SWAP(x,y,z)    (SORT_COPY(z,x), SORT_COPY(x,y), SORT_COPY(y,z))

#define SORT_COFF           20

void SORT_NAME(SORT_TYPE * beg, SORT_TYPE * end)
{
	SORT_TYPE * b;
	SORT_TYPE * e;
	SORT_TYPE * p;
	SORT_TYPE t;

	if (end - beg < 2) {
		return;
	}

	if (end - beg < SORT_COFF) {
		for (b = beg + 1; b < end; b++) {
			if (SORT_LESS(b,b-1)) {
				SORT_COPY( &t, b );
				SORT_COPY( b, b-1 );
				for (e = b - 2; beg <= e && SORT_LESS(&t,e); e--) {
					SORT_COPY( e+1, e );
				}
				SORT_COPY( e+1, &t );
			}
		}
		return;
	}

	b = beg + (end - beg) / 2;
	e = end - 1;
	if (SORT_LESS(b,e)) {
		SORT_SWAP( b, e, &t );
	}
	if (SORT_LESS(beg,e)) {
		SORT_SWAP( beg, e, &t );
	} else if (SORT_LESS(b,beg)) {
		SORT_SWAP( b, beg, &t );
	}

	p = beg;
	b = beg;
	e = end;
	while (b < e) {
		do {
			e--;
		} while (b < e && !SORT_LESS(e,p));
		do {
			b++;
		} while (b < e && !SORT_LESS(p,b));
		if (b < e) {
			SORT_SWAP( b, e, &t );
		}
	}
	SORT_SWAP( beg, e, &t );

	SORT_NAME(beg, e);
	SORT_NAME(e + 1, end);

}

#undef SORT_NAME
#undef SORT_TYPE
#undef SORT_LESS
#undef SORT_COPY
#undef SORT_SWAP
#undef SORT_COFF

EDimMap * NewDimMap(EBorder * dst, Value factor, EMemoryContext* mem_context)
{
	EDimMap * dimmap;
	EBorder * border;
	EidOffWgt * raws;
	EidOffWgt * raw;
	Offset nrRaw;
	EidOffWgt * * eows;
	/* EidOffWgt * *       eow; */
	Offset nrBase;
	Offset off;
	Offset max;
	uint32_t i, k;
	Dimension * adimension = dst->dimension->adimension;

	/* find out how many raws we need */
	nrRaw = 0;
	for (i = 0; i < dst->nrCons; i++) {
		Element * celm = adimension->lookupElement((IdentifierType)dst->conss[i]);
		nrRaw += (Offset)celm->getNumBaseElements(adimension);
	}
	nrRaw += dst->nrBase;
	nrRaw++;

	/* allocate the raws */
	raws = (EidOffWgt*)ecalloc((nrRaw + 1), sizeof(EidOffWgt), mem_context);
	NrCallocRaws++;
	raw = raws;
	off = 0;

	/* copy the base elements for the cons elements */
	for (i = 0; i < dst->nrCons; i++) {
		Element * celm = adimension->lookupElement((IdentifierType)dst->conss[i]);
		const map<IdentifierType, double> * bmap = celm->getBaseElements(adimension);
		map<IdentifierType, double>::const_iterator biter = bmap->begin();
		while (biter != bmap->end()) {
			raw->eid = biter->first;
			raw->off = off;
			raw->wgt = biter->second;
			raw++;
			biter++;
		}
		off += (Offset)factor;
	}

	/* copy the base elements */
	for (i = 0; i < dst->nrBase; i++) {
		raw->eid = dst->bases[i];
		raw->off = off;
		raw->wgt = 1.0;
		raw++;
		off += (Offset)factor;
	}

	/* add a rear-guard */
	max = dst->dimension->maximal;
	raw->eid = max + 1;
	raw->off = 0;
	raw->wgt = 0.0;
	raw++;

	/* sort the raws */
	SortDimMap(raws, raws + nrRaw);

	/* allocate (and initialize) the eows */
	eows = (EidOffWgt**)ecalloc((max + 2), sizeof(EidOffWgt*), mem_context);
	NrCallocEows++;

	/* copy for each element id the first raw pointed into the rows */
	for (raw = raws + nrRaw - 1; raw >= raws; raw--) {
		eows[raw->eid] = raw;
	}

	/* allocate a new border */
	border = NewEmptyBorder(dst->dimension, mem_context);

	/* find out how many base elements we have */
	nrBase = 0;
	for (raw = raws; raw < raws + nrRaw - 1; raw++) {
		if (raw->eid != (raw + 1)->eid) {
			nrBase++;
		}
	}
	/*
	 for ( eow = eows; eow < eows+(max+1); eow++ ) {
	 if ( *eow ) {
	 nrBase++;
	 }
	 }
	 */
	border->nrBase = nrBase;
	border->size = border->nrBase + border->nrCons;

	/* copy the base elements into the border */
	if (0 < nrBase && nrBase < dst->dimension->nrBase) {
		border->bases = (EElementId*)ecalloc(nrBase, sizeof(EElementId), mem_context);
		NrCallocBases++;
		k = 0;
		for (raw = raws; raw < raws + nrRaw - 1; raw++) {
			if (raw->eid != (raw + 1)->eid) {
				border->bases[k] = raw->eid;
				k++;
			}
		}
		/*
		 for ( i = 0; i <= max; i++ ) {
		 if ( eows[i] ) {
		 border->bases[k] = i;
		 k++;
		 }
		 }
		 */
	} else if (nrBase == dst->dimension->nrBase) {
		border->bases = dst->dimension->bases;
	}

	/* allocate and fill the dimmap */
	dimmap = (EDimMap*)ecalloc(1, sizeof(EDimMap), mem_context);
	NrCallocEDimMap++;
	dimmap->max = max;
	dimmap->source = border;
	dimmap->raws = raws;
	dimmap->eows = eows;

	return dimmap;
}

/****************************************************************************
 **

 *F  FreeArea(area)  . . . . . . . . . . . . . . . . . . .  deallocate an area
 */
void FreeArea(EArea * area, EMemoryContext* mem_context)
{
	uint32_t i;
	if (area) {
		for (i = 0; i < area->cube->nrDimensions; i++) {
			if (area->borders[i]) {
				FreeBorder(area->borders[i], mem_context);
				area->borders[i] = 0;
			}
		}
		efree(area, mem_context);
		NrCallocEArea--;
	}
}

void DebugArea(EArea * area, Environment * env)
{
	EBorder * border;
	uint32_t i, j;
	for (i = 0; i < area->cube->nrDimensions; i++) {
		border = area->borders[i];
		if (0 < border->nrCons && border->nrCons == border->dimension->nrCons) {
			fprintf(env->file, "*");
		} else if (0 < border->nrCons) {
			for (j = 0; j < border->nrCons - 1; j++) {
				fprintf(env->file, "%d:", border->conss[j]);
			}
			fprintf(env->file, "%d", border->conss[border->nrCons - 1]);
		}
		fprintf(env->file, ";");
		if (0 < border->nrBase && border->nrBase == border->dimension->nrBase) {
			fprintf(env->file, "*");
		} else if (0 < border->nrBase) {
			for (j = 0; j < border->nrBase - 1; j++) {
				fprintf(env->file, "%d:", border->bases[j]);
			}
			fprintf(env->file, "%d", border->bases[border->nrBase - 1]);
		}
		if (border->size != border->nrBase + border->nrCons) {
			fprintf(env->file, "[size-wrong]");
		}
		fprintf(env->file, " | ");
	}
	fprintf(env->file, "\n");
}

/****************************************************************************
 **
 *F  NewEmptyArea(cube)  . . . . . . . . allocate a new area (without borders)
 */
EArea * NewEmptyArea(ECube * cube, EMemoryContext* mem_context)
{
	EArea * area;
	uint32_t i;

	area = (EArea*)ecalloc(1, sizeof(EArea), mem_context, false); //TODO: validate a cotainer
	NrCallocEArea++;
	area->cube = cube;
	for (i = 0; i < MAX_DIMENSIONS; i++) {
		area->borders[i] = 0;
	}
	area->next = 0;
	return area;
}

/****************************************************************************
 **
 *F  NewCopyArea(area) . . . . . . . . . . . . . .  allocate a copy of an area
 **
 */
EArea * NewCopyArea(EArea * area, EMemoryContext* mem_context)
{
	EArea * result;
	uint32_t dim;

	result = NewEmptyArea(area->cube, mem_context);
	for (dim = 0; dim < area->cube->nrDimensions; dim++) {
		if (area->borders[dim]) {
			result->borders[dim] = NewCopyBorder(area->borders[dim], mem_context);
		} else {
			result->borders[dim] = 0;
		}
	}

	return result;
}

/****************************************************************************
 **
 *F  NewPathArea(path) . . . . . . . . . . create a singleton area from a path
 **
 */
EArea * NewPathArea(ECube * cube, EPath path, EMemoryContext* mem_context)
{
	EArea * area;
	uint32_t d;

	area = NewEmptyArea(cube, mem_context);
	for (d = 0; d < cube->nrDimensions; d++) {
		area->borders[d] = NewEmptyBorder(cube->dimensions[d], mem_context);
		if (cube->dimensions[d]->type[path[d]] == 'C') {
			area->borders[d]->nrCons = 1;
			if (1 < cube->dimensions[d]->nrCons) {
				area->borders[d]->conss = (EElementId*)ecalloc(1, sizeof(EElementId), mem_context);
				area->borders[d]->conss[0] = path[d];
			} else {
				area->borders[d]->conss = cube->dimensions[d]->conss;
			}
		} else {
			area->borders[d]->nrBase = 1;
			if (1 < cube->dimensions[d]->nrBase) {
				area->borders[d]->bases = (EElementId*)ecalloc(1, sizeof(EElementId), mem_context);
				area->borders[d]->bases[0] = path[d];
			} else {
				area->borders[d]->bases = cube->dimensions[d]->bases;
			}
		}
		area->borders[d]->size = area->borders[d]->nrCons + area->borders[d]->nrBase;
	}

	return area;

}

void DebugPath(EPath path, ECube * cube, Environment * env)
{
	uint32_t d;
	for (d = 0; d < cube->nrDimensions; d++) {
		fprintf(env->file, "%d,", path[d]);
	}
	fprintf(env->file, "\n");
}

/****************************************************************************
 **
 *F  IsEmptyArea(area) . . . . . . . . . . . . . . .  test if an area is empty
 */
uint32_t IsEmptyArea(EArea * area)
{
	uint32_t i;

	for (i = 0; i < area->cube->nrDimensions; i++) {
		if (area->borders[i]->size == 0) {
			return 1;
		}
	}

	return 0;
}

/****************************************************************************
 **
 *F  NewIntersectionArea(area1,are2) . . .  allocate intersection of two areas
 **
 **  area = NewIntersectionArea(area1,area2)
 **  as always this must create a new area
 **  since we will deallocate it
 */
uint32_t IsEmptyIntersectionArea(EArea * area1, EArea * area2)
{
	uint32_t d;

	for (d = 0; d < area1->cube->nrDimensions; d++) {
		if (IsEmptyIntersectionBorder(area1->borders[d], area2->borders[d])) {
			return 1;
		}
	}

	return 0;
}

EArea * NewIntersectionArea(EArea * area1, EArea * area2, EMemoryContext* mem_context)
{
	EArea * area;
	uint32_t d;

	for (d = 0; d < area1->cube->nrDimensions; d++) {
		if (IsEmptyIntersectionBorder(area1->borders[d], area2->borders[d])) {
			return 0;
		}
	}

	area = NewEmptyArea(area1->cube, mem_context);

	for (d = 0; d < area->cube->nrDimensions; d++) {
		area->borders[d] = NewIntersectionBorder(area1->borders[d], area2->borders[d], mem_context);
	}

	return area;
}

/****************************************************************************
 **
 *F  AppendComplementArea(list,area1,area2)  . . . . . complement of two areas
 **
 */
void AppendComplementArea(TypeList * areas, EArea * area1, EArea * area2, EMemoryContext* mem_context)
{
	EArea * area;
	uint32_t d;
	uint32_t i;

	/* the complement may be the union of several areas                    */
	for (d = 0; d < area1->cube->nrDimensions; d++) {

		/* if the complement in the dimension d is not empty               */
		if (area2->borders[d]->size < area1->borders[d]->size) {
			area = NewEmptyArea(area1->cube, mem_context);
			for (i = 0; i < d; i++) {
				area->borders[i] = NewCopyBorder(area2->borders[i], mem_context);
			}
			area->borders[d] = NewComplementBorder(area1->borders[d], area2->borders[d], mem_context);
			for (i = d + 1; i < area1->cube->nrDimensions; i++) {
				area->borders[i] = NewCopyBorder(area1->borders[i], mem_context);
			}
			AppendItemList( areas, area );
		}

	}

}

/****************************************************************************
 **

 *F  IsPathInArea(area,path) . . . . . . . .  test wheter a path is in an area
 **
 */
uint8_t IsPathInArea(EArea * area, EPath path)
{
	EElementId e;
	uint32_t d;
	uint32_t i;

	for (d = 0; d < area->cube->nrDimensions; d++) {
		e = path[d];
		if (area->borders[d]->dimension->type[e] == 'C') {
			if (area->borders[d]->nrCons == 0) {
				return 0;
			} else if (area->borders[d]->nrCons == 1) {
				if (e != area->borders[d]->conss[0]) {
					return 0;
				}
			}
		} else {
			if (area->borders[d]->nrBase == 0) {
				return 0;
			} else if (area->borders[d]->nrBase == 1) {
				if (e != area->borders[d]->bases[0]) {
					return 0;
				}
			}
		}
	}

	for (d = 0; d < area->cube->nrDimensions; d++) {
		e = path[d];
		if (area->borders[d]->dimension->type[e] == 'C') {
			if (area->borders[d]->nrCons < area->borders[d]->dimension->nrCons) {
				for (i = 0; i < area->borders[d]->nrCons; i++) {
					if (e == area->borders[d]->conss[i]) {
						break;
					}
				}
				if (i == area->borders[d]->nrCons) {
					return 0;
				}
			}
		} else {
			if (area->borders[d]->nrBase < area->borders[d]->dimension->nrBase) {
				for (i = 0; i < area->borders[d]->nrBase; i++) {
					if (e == area->borders[d]->bases[i]) {
						break;
					}
				}
				if (i == area->borders[d]->nrBase) {
					return 0;
				}
			}
		}
	}
	return 1;

}

/****************************************************************************
 **
 *F  IsBasePathInArea(area,path) . . . . test wheter a base path is in an area
 **
 */
uint8_t IsBasePathInArea(EArea * area, EPath path)
{
	EElementId e;
	uint32_t d;
	uint32_t i;

	for (d = 0; d < area->cube->nrDimensions; d++) {
		e = path[d];
		if (area->borders[d]->nrBase == 0) {
			return 0;
		} else if (area->borders[d]->nrBase == 1) {
			if (e != area->borders[d]->bases[0]) {
				return 0;
			}
		}
	}

	for (d = 0; d < area->cube->nrDimensions; d++) {
		e = path[d];
		if (area->borders[d]->nrBase < area->borders[d]->dimension->nrBase) {
			for (i = 0; i < area->borders[d]->nrBase; i++) {
				if (e == area->borders[d]->bases[i]) {
					break;
				}
			}
			if (i == area->borders[d]->nrBase) {
				return 0;
			}
		}
	}
	return 1;

}

/****************************************************************************
 **

 *F  FreeCubeMap(cubemap)  . . . . . . . . . . . . . . . deallocate a cube map
 **
 */
void FreeCubeMap(ECubeMap * map, EMemoryContext* mem_context)
{
	uint32_t d;

	if (map) {
		for (d = 0; d < map->source->cube->nrDimensions; d++) {
			if (map->dimmaps[d]) {
				FreeDimMap(map->dimmaps[d], mem_context);
				map->dimmaps[d] = 0;
			}
		}
		if (map->source) {
			FreeArea(map->source, mem_context);
			map->source = 0;
		}
		efree(map, mem_context);
		NrCallocECubeMap--;
	}

}

/****************************************************************************
 **
 *F  NewCubeMap(dst)   . . . . . . . . . . . . . . allocate a new cube mapping
 **
 */
ECubeMap * NewCubeMap(EArea * dst, EMemoryContext* mem_context)
{
	ECubeMap * map;
	Offset fac;
	uint32_t d;

	map = (ECubeMap*)ecalloc(1, sizeof(ECubeMap), mem_context);
	NrCallocECubeMap++;
	map->source = NewEmptyArea(dst->cube, mem_context);

	fac = 1;
	for (d = 0; d < dst->cube->nrDimensions; d++) {
		map->dimmaps[d] = NewDimMap(dst->borders[d], fac, mem_context);
		map->source->borders[d] = map->dimmaps[d]->source;
		map->dimmaps[d]->source = 0;
		fac = fac * dst->borders[d]->size;
	}

	return map;
}

/****************************************************************************
 **

 *F  NewSourceAreaRule(rule,area)  . . . allocate the source area under a rule
 **
 **  NewSourceAreaRule computes the source area of the area <area> under the
 **  rule <rule>.
 */
EArea * NewSourceAreaRule(ERule * rule, EArea * dst, EMemoryContext* mem_context)
{
	EArea * src;
	EBorder * border;
	uint8_t dim;

	src = NewEmptyArea(dst->cube, mem_context);
	for (dim = 0; dim < dst->cube->nrDimensions; dim++) {
		if (rule->ubm_mask[dim]) {
			/* if we come here dst->borders[dim] must be { ubm_dest[dim] } */
			/* this we replace by { ubm_source[dim] }                      */
			border = NewEmptyBorder(dst->borders[dim]->dimension, mem_context);
			/* but we must find out, whether it is base or consolidated    */
			if (rule->ubm_source_is_base[dim]) {
				border->nrBase = 1;
				border->bases = (EElementId*)ecalloc(border->nrBase, sizeof(EElementId), mem_context);
				NrCallocBases++;
				border->bases[0] = rule->ubm_source[dim];
			} else {
				border->nrCons = 1;
				border->conss = (EElementId*)ecalloc(border->nrCons, sizeof(EElementId), mem_context);
				NrCallocConss++;
				border->conss[0] = rule->ubm_source[dim];
			}
			border->size = border->nrBase + border->nrCons;
			src->borders[dim] = border;
		} else {
			src->borders[dim] = NewCopyBorder(dst->borders[dim], mem_context);
		}
	}

	return src;
}

/****************************************************************************
 **
 *F  DstPathRule(dst,rule,src) . . . . map a source path to a destination path
 **
 **  'DstPathRule' maps  the source path  <src> to the destination  path <dst>
 **  for the rule <rule>.  The rule must  be such, that it has only one marker
 **  and a bijection between source and destination area under that marker.
 */
void DstPathRule(EPath dstPath, ERule * rule, EPath srcPath)
{
	uint8_t dim;
	for (dim = 0; dim < rule->cube->nrDimensions; dim++) {
		if (rule->ubm_mask[dim]) {
			dstPath[dim] = rule->ubm_dest[dim];
		} else {
			dstPath[dim] = srcPath[dim];
		}
	}
}

/****************************************************************************
 **
 *F  SrcPathRule(src,rule,dst) . . . . map a destination path to a source path
 **
 */
void SrcPathRule(EPath srcPath, ERule * rule, EPath dstPath)
{
	uint8_t dim;
	for (dim = 0; dim < rule->cube->nrDimensions; dim++) {
		if (rule->ubm_mask[dim]) {
			srcPath[dim] = rule->ubm_source[dim];
		} else {
			srcPath[dim] = dstPath[dim];
		}
	}
}

/****************************************************************************
 **
 *F  ComputeRule(ptrValue,ptrStatus,path,rule,srcValue,srcStatus,env) . . . ..
 **
 */
#define NOP                                       0
#define HALT                                      1

#define PUSH_DBL                                 10
#define PULL_DBL                                 11
#define SWAP_DBL                                 12
#define PUSH_STR                                 13
#define PULL_STR                                 14
#define SWAP_STR                                 15

#define DBL_2_STR                                20
#define STR_2_DBL                                21

#define LD_CONST_DBL                             30
#define LD_CONST_STR                             31

#define LD_SRC_HIT_DBL                           40
/* #define LD_SRC_HIT_STR */
/* this does not exist, because you cannot marker on strings */
#define LD_SRC_DBL                               41
#define LD_SRC_STR                               42

#define LD_VAR_STR                               50

#define OP2_SUM_DBL                              60
#define OP2_DIFF_DBL                             61
#define OP2_PROD_DBL                             62
#define OP2_QUO_DBL                              63

#define OP2_EQ_DBL                               70
#define OP2_NE_DBL                               71
#define OP2_LT_DBL                               72
#define OP2_LE_DBL                               73

#define OP2_EQ_STR                               80
#define OP2_NE_STR                               81
#define OP2_LT_STR                               82
#define OP2_LE_STR                               83

#define CALL_GEN_DBL                             90
#define CALL_GEN_STR                             91

#define CALL_DATA_DBL                            100
#define CALL_DATA_STR                            101

#define JUMP                                     110
#define JUMP_IF_NOT                              111

#define CONTINUE                                 120
#define STET                                     121

#define OP2_LOG_DBL                              130
#define OP2_MOD_DBL                              131
#define OP2_POWER_DBL                            132
#define OP2_QUOTIENT_DBL                         133
#define OP2_RANDBETWEEN_DBL                      134
#define OP2_ROUND_DBL                            135

#define OP1_ABS_DBL                              140
#define OP1_ACOS_DBL                             141
#define OP1_ASIN_DBL                             142
#define OP1_ATAN_DBL                             143
#define OP1_CEILING_DBL                          144
#define OP1_COS_DBL                              145
#define OP1_EVEN_DBL                             146
#define OP1_EXP_DBL                              147
#define OP1_FACT_DBL                             148
#define OP1_FLOOR_DBL                            149
#define OP1_INT_DBL                              150
#define OP1_LN_DBL                               151
#define OP1_LOG10_DBL                            152
#define OP1_ODD_DBL                              153
#define OP1_SIGN_DBL                             154
#define OP1_SIN_DBL                              155
#define OP1_SQRT_DBL                             156
#define OP1_TAN_DBL                              157
#define OP1_TRUNC_DBL                            158
#define OP1_WEEKDAY_DBL                          159

#define FUNC_CHAR                                160
#define FUNC_CLEAN                               161
#define FUNC_CODE                                162
#define FUNC_CONCATENATE                         163
#define FUNC_DATE                                164
#define FUNC_DATEFORMAT                          165
#define FUNC_DATEVALUE                           166
#define FUNC_EXACT                               167
#define FUNC_LEFT                                168
#define FUNC_LEN                                 169
#define FUNC_LOWER                               170
#define FUNC_MID                                 171
#define FUNC_NOW                                 172
#define FUNC_PI                                  173
#define FUNC_PROPER                              174
#define FUNC_RAND                                175
#define FUNC_REPLACE                             176
#define FUNC_REPT                                177
#define FUNC_RIGHT                               178
#define FUNC_SEARCH                              179
#define FUNC_STR                                 180
#define FUNC_SUBSTITUTE                          181
#define FUNC_TRIM                                182
#define FUNC_UPPER                               183
#define FUNC_VALUE                               184

#define AGGR_SUM                                 190
#define AGGR_PROD                                191
#define AGGR_MIN                                 192
#define AGGR_MAX                                 193
#define AGGR_COUNT                               194
#define AGGR_FIRST                               195
#define AGGR_LAST                                196
#define AGGR_AVG                                 197

#define PALO_CUBEDIMENSION                       200
#define PALO_ECHILD                              201
#define PALO_ECHILDCOUNT                         202
#define PALO_ECOUNT                              203
#define PALO_EFIRST                              204
#define PALO_EINDENT                             205
#define PALO_EINDEX                              206
#define PALO_EISCHILD                            207
#define PALO_ELEVEL                              208
#define PALO_ENAME                               209
#define PALO_ENEXT                               210
#define PALO_EPARENT                             211
#define PALO_EPARENTCOUNT                        212
#define PALO_EPREV                               213
#define PALO_ESIBLING                            214
#define PALO_ETOPLEVEL                           215
#define PALO_ETYPE                               216
#define PALO_EWEIGHT                             217

#define FUNC_ISERROR_DBL                         220
#define FUNC_ISERROR_STR                         221

void GetValue(Value * ptrValue, Status * ptrStatus, ECube * cube, EPath path, Environment * env, EMemoryContext* mem_context);

void GetValueNoRule(Value * ptrValue, Status * ptrStatus, ECube * cube, EPath path, Environment * env, EMemoryContext* mem_context);

/****************************************************************************
 **

 *F  FreeBuffer(buf) . . . . . . . . . . . . . . . . . . . deallocate a buffer
 **
 **  'FreeBuffer' deallocates the buffer <buf> again.
 **
 */
void FreeBuffer(EBuffer * buf, EMemoryContext* mem_context)
{

	if (buf) {
		if (buf->values) {
			efree(buf->values, mem_context);
			NrCallocValues--;
		}
		if (buf->status) {
			efree(buf->status, mem_context);
			NrCallocStatus--;
		}
		efree(buf, mem_context);
		NrCallocEBuffer--;
	}
}

/****************************************************************************
 **
 *F  NewBuffer(size) . . . . . . . . . . . . . . . . . . allocate a new buffer
 **
 **  'NewBuffer'  allocates a  new  buffer  and returns  the  pointer to  this
 **  buffer.  The  vectors 'values' and 'status'  in the buffer  have room for
 **  <size> entries.
 */
EBuffer * NewBuffer(Offset size, EMemoryContext* mem_context)
{
	EBuffer * buf;

	buf = (EBuffer*)ecalloc(1, sizeof(EBuffer), mem_context);
	NrCallocEBuffer++;
	buf->values = (Value*)ecalloc(size, sizeof(Value), mem_context);
	NrCallocValues++;
	buf->status = (Status*)ecalloc(size, sizeof(Status), mem_context);
	NrCallocStatus++;

	return buf;
}

/****************************************************************************
 **
 *F  FillBuffer(buf,map,path,value,status) . . . . write a value into a buffer
 **
 */
void FillBuffer(EBuffer * buf, ECubeMap * map, EPath path, Value value, Status status)
{
	Offset off;
	Value wgt;
	uint8_t mlt;
	EidOffWgt * eows[MAX_DIMENSIONS];
	EidOffWgt * eow;
	uint32_t d;
	uint32_t nrDims;

	nrDims = map->source->cube->nrDimensions;

	/* map the destination path to an offset and a weight in the buffer    */
	off = 0;
	wgt = 1.0;
	mlt = 0;
	for (d = 0; d < nrDims; d++) {
		// TODO: simple fix preventing accees violation
		// search real solution for invalid paths
		// see bug #4204
		if (path[d] > map->dimmaps[d]->max)
			continue;
		eow = map->dimmaps[d]->eows[path[d]];
		if (eow) {
			off += eow->off;
			wgt *= eow->wgt;
			mlt = mlt || (eow->eid == (eow + 1)->eid);
		}
	}

	/* handle easy case (unique mapping)                                   */
	if (!mlt) {
		if (!IsErrorStatus(status)) {
			buf->values[off] += wgt * value;
			buf->status[off] |= status;
		}
		//#ifdef PALO_HMMM_DO_NOT_IGNORE_ERRORS
		else {
			buf->values[off] = value;
			buf->status[off] = status;
		}
		//#endif
	}

	/* handle difficult case (multiple mapping)                            */
	else {
		for (d = 0; d < nrDims; d++) {
			eows[d] = map->dimmaps[d]->eows[path[d]];
		}
		mlt = 1;
		while (mlt) {
			off = 0;
			wgt = 1.0;
			for (d = 0; d < nrDims; d++) {
				off += eows[d]->off;
				wgt *= eows[d]->wgt;
			}
			if (!IsErrorStatus(status)) {
				buf->values[off] += wgt * value;
				buf->status[off] |= status;
			}
			//#ifdef PALO_HMMM_DO_NOT_IGNORE_ERRORS
			else {
				buf->values[off] = value;
				buf->status[off] = status;
			}
			//#endif
			mlt = 0;
			for (d = 0; d < nrDims; d++) {
				if (eows[d]->eid == (eows[d] + 1)->eid) {
					eows[d]++;
					mlt = 1;
					break;
				} else {
					eows[d] = map->dimmaps[d]->eows[path[d]];
				}
			}
		}
	}

}

/****************************************************************************
 **

 *F  ApplyAction(act,path,value,status,env)  . . .  apply an action to a value
 **
 **  'ApplyAction' this is what you do with an action.
 */
#define ApplyAction(action,path,value,status,env, context ) \
    (*(action)->apply_)( (action), (path), (value), (status), (env), (context) )

/****************************************************************************
 **
 *F  InitRuleSrcFillAction(act,rule,map,buf)  . . . . . . initialize an action
 **
 **  'InitRuleSrcFillAction' initializes the action <act>.
 */
void ApplyRuleSrcFillAction(RuleSrcFillAction * act, EPath srcPath, Value srcValue, Status srcStatus, Environment * env, EMemoryContext* mem_context)
{
	EPath path;
	Value value;
	Status status;

	/* compute the destination path from the source path                   */
	DstPathRule(path, act->rule, srcPath);

	/* apply the rule                                                      */
	virtual_machine machine(env, mem_context, &value, &status);
	machine.compute(path, act->rule, srcValue, srcStatus, 1);
	//    ComputeRule( &value, &status, path, act->rule, srcValue, srcStatus, env, 1, &stack, mem_context );
	/* and write the result into the buffer                                */
	FillBuffer(act->buf, act->map, path, value, status);

}

void InitRuleSrcFillAction(RuleSrcFillAction * action, ERule * rule, ECubeMap * map, EBuffer * buf)
{
	action->action.apply_ = (ApplyAction)&ApplyRuleSrcFillAction;
	action->rule = rule;
	action->map = map;
	action->buf = buf;
	/* action->bytecode = ... */
	/* action->constants = ... */
}

/****************************************************************************
 **
 *F  InitRuleDstFillAction(act,rule,map,buf)  . . . . . . initialize an action
 **
 **  'InitRuleDstFillAction' initializes the action <act>.
 */
void ApplyRuleDstFillAction(RuleDstFillAction * act, EPath dstPath, Value dstValue, Status dstStatus, Environment * env, EMemoryContext* mem_context)
{
	Value value;
	Status status;

	try {
		/* apply the rule                                                      */
		virtual_machine machine(env, mem_context, &value, &status);
		machine.compute(dstPath, act->rule, dstValue, dstStatus, 1);

		//        ComputeRule( &value, &status, dstPath, act->rule, dstValue, dstStatus, env, 1, &stack, mem_context );
	} catch (ErrorException& e) {
		if ((e.getErrorType() == ErrorException::ERROR_RULE_HAS_CIRCULAR_REF) || (e.getErrorType() == ErrorException::ERROR_INTERNAL)) {
			//mask
			value = act->rule->nr_rule;
			status = ErrorStatus + ErrorCircularRule;
		} else {
			throw e;
		}
	}
	/* and write the result into the buffer                                */
	FillBuffer(act->buf, act->map, dstPath, value, status);

}

void InitRuleDstFillAction(RuleDstFillAction * action, ERule * rule, ECubeMap * map, EBuffer * buf)
{
	action->action.apply_ = (ApplyAction)&ApplyRuleDstFillAction;
	action->rule = rule;
	action->map = map;
	action->buf = buf;
	/* action->bytecode = ... */
	/* action->constants = ... */
}

/****************************************************************************
 **
 *F  InitRuleSrcChainAction(act,rule,chain)  . . . . . .  initialize an action
 **
 */
void ApplyRuleSrcChainAction(RuleSrcChainAction * act, EPath srcPath, Value srcValue, Status srcStatus, Environment * env, EMemoryContext* mem_context)
{
	EPath path;
	Value value;
	Status status;

	/* compute the destination path from the source path                   */
	DstPathRule(path, act->rule, srcPath);

	/* apply the rule                                                      */

	virtual_machine machine(env, mem_context, &value, &status);
	machine.compute(path, act->rule, srcValue, srcStatus, 1);

	//    ComputeRule( &value, &status, path, act->rule, srcValue, srcStatus, env, 1, &stack, mem_context );

	/* and chain                                                           */
	ApplyAction( act->chain, path, value, status, env, mem_context );

}

void InitRuleSrcChainAction(RuleSrcChainAction * action, ERule * rule, EAction * chain)
{
	action->action.apply_ = (ApplyAction)&ApplyRuleSrcChainAction;
	action->rule = rule;
	action->chain = chain;
	/* action->bytecode = ... */
	/* action->constants = ... */
}

/****************************************************************************
 **
 *F  InitRuleDstChainAction(act,rule,chain)  . . . . . .  initialize an action
 **
 */
void ApplyRuleDstChainAction(RuleDstChainAction * act, EPath dstPath, Value dstValue, Status dstStatus, Environment * env, EMemoryContext* mem_context)
{
	Value value;
	Status status;

	/* apply the rule                                                      */

	virtual_machine machine(env, mem_context, &value, &status);
	machine.compute(dstPath, act->rule, dstValue, dstStatus, 1);

	//    ComputeRule( &value, &status, dstPath, act->rule, dstValue, dstStatus, env, 1, &stack, mem_context );
	/* and chain                                                           */
	ApplyAction( act->chain, dstPath, value, status, env, mem_context );

}

void InitRuleDstChainAction(RuleDstChainAction * action, ERule * rule, EAction * chain)
{
	action->action.apply_ = (ApplyAction)&ApplyRuleDstChainAction;
	action->rule = rule;
	action->chain = chain;
	/* action->bytecode = ... */
	/* action->constants = ... */
}

/****************************************************************************
 **

 *F  FillAreaBuffer(buf,dst,env) . .  fill a buffer with the values of an area
 **
 */
void FillAreaBuffer(EBuffer * buf, EArea * dst, Environment * env, EMemoryContext* mem_context)
{
	ECubeMap * map;
	TypeList areas;
	TypeList nexts;
	ERule * rule;
	EArea * src;
	EArea * itr;
	TRACE_INIT_TIME

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s FillAreaBuffer ",++env->tei," "));
	DebugArea( dst, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "FillAreaBuffer" << " called" << endl;
		/* printf("FillAreaBuffer  ");  DebugArea( dst, env ); */
	}

	/* find the mapping of the cells that must be consolidated into dst    */
	map = NewCubeMap(dst, mem_context);
	src = map->source;

	/* we keep a list of areas that must still be consolidated into dst    */
	/* initially this consist of the entire source area                    */
	InitList( &areas, offsetof( struct StructArea, next ), -1 );
	AppendItemList( &areas, src );

	if (mem_context->calcRules()) {
		/* loop over the rules                                                 */
		for (rule = (ERule*)FirstItemList(&(dst->cube->rules_n)); rule != 0; rule = (ERule*)NextItemList(&(dst->cube->rules_n),rule)) {

			/* prepare the list of remaining areas for the next round          */
			InitList( &nexts, offsetof( struct StructArea, next ), -1 );

			/* work off the list of areas                                      */
			while (0 < NrItemsList( &areas )) {
				src = (EArea*)FirstItemList( &areas );
				RemoveItemList(&areas, src);

				/* compute the intersection, where the rule can be applied     */
				itr = NewIntersectionArea(src, rule->dest_area, mem_context);

				/* if the intersection is nonempty                             */
				if (itr && !IsEmptyArea(itr)) {

					/* with <rule> compute <intersection>, then apply <action> */
					FillRuleBuffer(buf, map, itr, rule, env, mem_context);

					/* in the next round we need to work off the complement    */
					AppendComplementArea(&nexts, src, itr, mem_context);
					if (src != map->source) {
						FreeArea(src, mem_context);
					}
				} else { // if the intersection is empty
					/* keep the area for the next round                        */
					AppendItemList( &nexts, src );
				}

				/* deallocate the intersection                                 */
				if (itr) {
					FreeArea(itr, mem_context);
				}
			}

			/* now use the new list of remaining areas for the next rule       */
			/* here it might be worthwhile to try to join areas together       */
			CopyList(&areas, &nexts);
		}
	}

	/* the remaining cells are not computed (have no rule)                 */
	/* here it might be worthwhile to try and join areas together          */
	while (0 < NrItemsList( &areas )) {
		src = (EArea*)FirstItemList( &areas );
		RemoveItemList(&areas, src);
		FillBaseBuffer(buf, map, src, env, mem_context, false);
		if (src != map->source) {
			FreeArea(src, mem_context);
		}
	}

	/* deallocate the mapping                                              */
	FreeCubeMap(map, mem_context);

	if (Logger::isTrace()) {
		Logger::trace << "FillAreaBuffer" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s FillAreaBuffer returns %4.3f\n",env->tei--," ",TRACE_TIME));

}

bool IsEmptyIntersectionForRule(ERule *rule, vector<set<IdentifierType> > &area)
{
	uint32_t nrDim = (uint32_t)area.size();
	EArea *destArea = rule->dest_area;

	for (uint32_t i = 0; i < nrDim; i++) {
		bool isEmptyCons = false;
		bool isEmptyBase = false;
		uint32_t j;

		for (j = 0; j < destArea->borders[i]->nrCons; j++) {
			if (area[i].find(destArea->borders[i]->conss[j]) != area[i].end()) {
				break;
			}
		}
		if (j == destArea->borders[i]->nrCons) {
			isEmptyCons = true;
		}
		if (isEmptyCons) {
			for (j = 0; j < destArea->borders[i]->nrBase; j++) {
				if (area[i].find(destArea->borders[i]->bases[j]) != area[i].end()) {
					break;
				}
			}
			if (j == destArea->borders[i]->nrBase) {
				isEmptyBase = true;
			}
		}
		if (isEmptyCons && isEmptyBase) {
			return true;
		}
	}
	return false;
}

bool IsRuleForArea(EArea *area)
{
	ECube *cube = area->cube;
	vector<set<IdentifierType> > srcArea; //elements from area
	vector<set<IdentifierType> > baseArea; //base descendants of elements from area

	bool isCons = false;
	for (uint32_t i = 0; i < cube->nrDimensions; i++) {
		if (area->borders[i]->nrCons) {
			isCons = true;
			srcArea.resize(cube->nrDimensions);
			break;
		}
	}
	baseArea.resize(cube->nrDimensions);

	for (uint32_t i = 0; i < cube->nrDimensions; i++) {
		Dimension *dim = cube->dimensions[i]->adimension;

		for (uint32_t j = 0; j < area->borders[i]->nrCons; j++) {
			Element *elem = dim->lookupElement(area->borders[i]->conss[j]);
			if (elem) {
				if (isCons) {
					srcArea[i].insert(area->borders[i]->conss[j]);
				}

				const map<IdentifierType, double> *desc = elem->getBaseElements(dim);
				for (map<IdentifierType, double>::const_iterator it = desc->begin(); it != desc->end(); it++) {
					baseArea[i].insert(it->first);
				}
			}
		}
		for (uint32_t j = 0; j < area->borders[i]->nrBase; j++) {
			if (isCons) {
				srcArea[i].insert(area->borders[i]->bases[j]);
			}
			baseArea[i].insert(area->borders[i]->bases[j]);
		}
	}

	bool isRule = false;
	if (isCons) {
		for (ERule *r = (ERule *)FirstItemList(&(cube->rules_c)); r != 0; r = (ERule*)NextItemList(&(cube->rules_c), r)) {
			isRule = !IsEmptyIntersectionForRule(r, srcArea);
			if (isRule) {
				break;
			}
		}
	}
	if (!isRule) {
		for (ERule *r = (ERule *)FirstItemList(&(cube->rules_n)); r != 0; r = (ERule*)NextItemList(&(cube->rules_n), r)) {
			isRule = !IsEmptyIntersectionForRule(r, baseArea);
			if (isRule) {
				break;
			}
		}
	}
	return isRule;
}

bool IsBigArea(EArea *dst)
{
	uint32_t nrDims = dst->cube->nrDimensions;
	uint32_t size = 1;

	for (uint32_t dim = 0; dim < nrDims && size < 100; dim++) {
		size *= dst->borders[dim]->nrBase;
	}
	return size >= 100; // this magical constant is used in FillBaseBuffer
}

/****************************************************************************
 **
 *F  FillRuleBuffer(buf,map,dst,rule,env)  . .  fill a buffer with rule values
 **
 */
void FillRuleBuffer(EBuffer * buf, ECubeMap * map, EArea * dst, ERule * rule, Environment * env, EMemoryContext* mem_context)
{
	EArea * src;
	RuleSrcFillAction srcAct;
	RuleDstFillAction dstAct;
	TRACE_INIT_TIME

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s FillRuleBuffer %d ",++env->tei," ",rule->nr_rule));
	DebugArea( dst, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "FillRuleBuffer" << " called" << endl;
	}

	/* uniq bijective marker */
	if (rule->ubm_flag) {
		src = NewSourceAreaRule(rule, dst, mem_context);

		/* if there is no rule for cells in src then the optimized FillBaseBuffer can be used */
		bool useMarkers = IsBigArea(dst) && !IsRuleForArea(src);
		FillBaseBuffer(buf, map, dst, env, mem_context, useMarkers);

		if (!useMarkers) {
			InitRuleSrcFillAction(&srcAct, rule, map, buf);
			EnumAreaAction((EAction*)&srcAct, src, env, mem_context);
			FreeArea(src, mem_context);
		}
	}

	/* marker but not uniq bijection */
	else if (rule->marker_flag) {
		FillBaseBuffer(buf, map, dst, env, mem_context, false);
		InitRuleDstFillAction(&dstAct, rule, map, buf);
		EnumBaseMarkerAction((EAction*)&dstAct, dst, env, mem_context);
	}

	/* no marker */
	else {
		InitRuleDstFillAction(&dstAct, rule, map, buf);
		EnumBaseZeroAction((EAction*)&dstAct, dst, env, mem_context);
	}

	if (Logger::isTrace()) {
		Logger::trace << "FillRuleBuffer" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s FillRuleBuffer returns %4.3f\n",env->tei--," ",TRACE_TIME));

}

/****************************************************************************
 **
 *F  FillBaseBuffer(buf,map,dst,env) . . . . .  fill a buffer with base values X
 **
 */
#define IS_MARKERED(row)    (((*(uint32_t*)(((char*)(row)) + val_size + key_size)) & 0x80000000) != 0)

void FillBaseBuffer(EBuffer *buf, ECubeMap *map, EArea *dst, Environment *env, EMemoryContext* mem_context, bool useMarkers)
{
	EPath path;
	Offset idxs[MAX_DIMENSIONS];
	uint32_t size;
	uint32_t dim;
	uint32_t nrDims;
	uint32_t isDone;
	Value * row;
	uint32_t i0, i1;
	size_t val_size = dst->cube->acube->getStorageDouble()->getValueSize();
	size_t key_size = dst->cube->acube->getStorageDouble()->getKeySize();
	TRACE_INIT_TIME

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s FillBaseBuffer ",++env->tei," "));
	DebugArea( dst, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "FillBaseBuffer" << " called" << endl;
	}

	nrDims = map->source->cube->nrDimensions;

	/* test if the area has more than 100 cells in it                      */
	size = 1;
	for (dim = 0; dim < nrDims && size < 100; dim++) {
		size *= dst->borders[dim]->nrBase;
	}

	/* handle single elements                                              */
	if (size == 1) {
		for (dim = 0; dim < nrDims; dim++) {
			path[dim] = dst->borders[dim]->bases[0];
		}
		row = (Value*)dst->cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
		if (row && !IS_MARKERED(row)) {
			FillBuffer(buf, map, path, *row, BaseStatus);
		}
	}

	/* handle very small areas by enumerating all the cells                */
	//TODO RRA:: see below, it's the only working way
	else if (size < 100) {
		isDone = 0;
		for (dim = 0; dim < nrDims; dim++) {
			if (0 < dst->borders[dim]->nrBase) {
				idxs[dim] = 0;
				path[dim] = dst->borders[dim]->bases[idxs[dim]];
			} else {
				isDone = 1;
				break;
			}
		}
		while (!isDone) {
			row = (Value*)dst->cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
			if (row && !IS_MARKERED(row)) {
				FillBuffer(buf, map, path, *row, BaseStatus);
			}
			isDone = 1;
			for (dim = 0; dim < nrDims; dim++) {
				if (idxs[dim] + 1 < dst->borders[dim]->nrBase) {
					idxs[dim]++;
					path[dim] = dst->borders[dim]->bases[idxs[dim]];
					isDone = 0;
					break;
				} else {
					idxs[dim] = 0;
					path[dim] = dst->borders[dim]->bases[idxs[dim]];
				}
			}
		}

	}
	// TODO RRA:: Make it really work!
	/* handle large areas                                                  */
	else {
		EidOffWgt * * eowss[MAX_DIMENSIONS];
		EidOffWgt * * eows;
		EElementId e;
		uint8_t mlt;
		mlt = 0;
		for (i0 = 0; i0 < dst->cube->nrDimensions; i0++) {
			eows = (EidOffWgt**)ecalloc(map->dimmaps[i0]->max + 2, sizeof(EidOffWgt*), mem_context);
			for (i1 = 0; i1 < dst->borders[i0]->nrBase; i1++) {
				e = dst->borders[i0]->bases[i1];
				eows[e] = map->dimmaps[i0]->eows[e];
				mlt = mlt || ((eows[e]) && (eows[e]->eid == (eows[e] + 1)->eid));
			}
			eowss[i0] = eows;
		}
		if (Logger::isTrace()) {
			Logger::trace << "mapping is multimapping " << (int)mlt << endl;
		}

		if (!mlt) {
			// -jj-
			uint32_t i0max = dst->cube->nrDimensions <= 1 ? 1 : dst->borders[0]->nrBase;
			uint32_t i1max = dst->cube->nrDimensions <= 2 ? 1 : dst->borders[1]->nrBase;

			for (i0 = 0; i0 < i0max; i0++) {
				for (i1 = 0; i1 < i1max; i1++) {
					CubePage * page = dst->cube->acube->getStorageDouble()->lookupCubePage((IdentifierType)(dst->borders[0]->bases[i0]), dst->cube->nrDimensions > 2 ? (IdentifierType)(dst->borders[1]->bases[i1]) : 0);
					if (page == 0) {
						continue;
					}

					page->sortL();

					size_t row_size = page->getRowSize();
					CubePage::buffer_t page_beg = page->begin();
					CubePage::buffer_t page_end = page->end();
					CubePage::buffer_t row; // a row
					IdentifierType * path; // the path of that row
					IdentifierType p; // one dimension element
					uint32_t hash; // the hash of the path
					double wgt; // the weighting for the path
					/* XXX hash and wgt could be precomputed for the first two dimensions */
					// EDimMap *                        m;  // a mapping of one dimension
					int32_t i; // loop variable
					EidOffWgt * x;

					// loop over all rows in the page
					row = page_beg;
					while (row < page_end) {

						// TODO: skip markered elements
						// is sometimes faster, but I fear it is often slower
						// see bug#4783
						if (!useMarkers && IS_MARKERED(row)) {
							row += row_size;
							continue;
						}

						// compute the hash and the weight for that path
						// break if one path component could not be mapped
						path = (IdentifierType*)(row + val_size);
						hash = 0;
						wgt = 1.0;
						p = path[nrDims - 1];
						for (i = nrDims - 1; 0 <= i; i--) {
							p = path[i];
							x = eowss[i][p];
							if (x == 0) {
								break;
							}
							hash += x->off;
							wgt *= x->wgt;
						}

						// if the source value shall be consolidated, add it to the storage
						if (i < 0) {
							double val;
							Status status;

							if (!IS_MARKERED(row)) {
								val = *(double*)row;
								/* printf("added %f*%f at %d\n",val,wgt,hash); */
								buf->values[hash] += val * wgt;
								buf->status[hash] |= BaseStatus;
							} else {
								if (useMarkers) {
									GetValue(&val, &status, dst->cube, path, env, mem_context);
									if (status != ErrorStatus) {
										buf->values[hash] += val * wgt;
									}
									buf->status[hash] |= status;
								}
							}
							row += row_size;
						} else { // otherwise skip over all rows with the same offending element
							do {
								row += row_size;
								if (page_end <= row)
									break;
								path = (IdentifierType*)(row + val_size);
							} while (path[i] == p);
						}
					}
				}
			}
		} else {
			uint32_t i0max = nrDims <= 1 ? 1 : dst->borders[0]->nrBase; // -jj-
			uint32_t i1max = nrDims <= 2 ? 1 : dst->borders[1]->nrBase; // -jj-

			for (i0 = 0; i0 < i0max; i0++) {
				for (i1 = 0; i1 < i1max; i1++) {
					CubePage * page = dst->cube->acube->getStorageDouble()->lookupCubePage((IdentifierType)(dst->borders[0]->bases[i0]), nrDims > 2 ? (IdentifierType)(dst->borders[1]->bases[i1]) : 0);
					if (page == 0) {
						continue;
					}

					if (Logger::isTrace()) {
						Logger::trace << "page " << dst->borders[0]->bases[i0] << " " << dst->borders[1]->bases[i1] << endl;
					}

					page->sortL();

					size_t row_size = page->getRowSize();
					CubePage::buffer_t page_beg = page->begin();
					CubePage::buffer_t page_end = page->end();
					CubePage::buffer_t row; // a row
					IdentifierType * path; // the path of that row
					IdentifierType p; // one dimension element
					uint32_t hash; // the hash of the path
					double wgt; // the weighting for the path
					/* XXX hash and wgt could be precomputed for the first two dimensions */
					// EDimMap *                        m;  // a mapping of one dimension
					int32_t i; // loop variable
					EidOffWgt * xs[MAX_DIMENSIONS];
					uint32_t j;
					uint8_t inc;

					// loop over all rows in the page
					row = page_beg;
					while (row < page_end) {

						// compute the hash and the weight for that path
						// break if one path component could not be mapped
						path = (IdentifierType*)(row + val_size);
						hash = 0;
						wgt = 1.0;
						p = path[nrDims - 1];
						for (i = nrDims - 1; 0 <= i; i--) {
							p = path[i];
							xs[i] = eowss[i][p];
							if (xs[i] == 0) {
								break;
							}
							hash += xs[i]->off;
							wgt *= xs[i]->wgt;
						}

						// if the source value shall be consolidated, add it to the storage
						if (i < 0) {
							if (!IS_MARKERED(row) || useMarkers) {
								inc = 0;
								while (!inc) {
									double val;
									Status status;

									if (!IS_MARKERED(row)) {
										val = *(double*)row;
										buf->values[hash] += val * wgt;
										buf->status[hash] |= BaseStatus;
									} else {
										GetValue(&val, &status, dst->cube, path, env, mem_context);
										if (status != ErrorStatus) {
											buf->values[hash] += val * wgt;
										}
										buf->status[hash] |= status;
									}
									hash = 0;
									wgt = 1.0;
									inc = 1;
									for (j = 0; j < nrDims; j++) {
										if (inc && (xs[j] + 1)->eid == path[j]) {
											xs[j]++;
											inc = 0;
										} else if (inc) {
											xs[j] = eowss[j][path[j]];
										}
										hash += xs[j]->off;
										wgt *= xs[j]->wgt;
									}
								}
							}
							row += row_size;
						}

						// otherwise skip over all rows with the same offending element
						else {
							do {
								row += row_size;
								if (page_end <= row)
									break;
								path = (IdentifierType*)(row + val_size);
							} while (path[i] == p);
						}

					}

				}
			}

		}

		for (i0 = 0; i0 < dst->cube->nrDimensions; i0++) {
			efree(eowss[i0], mem_context);
		}

	}
	// end TODO

	if (Logger::isTrace()) {
		Logger::trace << "FillBaseBuffer" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s FillBaseBuffer returns %4.3f\n",env->tei--," ",TRACE_TIME));

}

/****************************************************************************
 **

 *F  EnumAreaAction(act,dst,env) . . . enumerate an area and execute an action
 **
 **  'EnumAreaAction' enumerates the nonzero values in the area <dst> and for
 **  each executes the action <act>.  The environment <env> is passed as final
 **  argument to <act>.
 */
void EnumAreaAction(EAction * act, EArea * dst, Environment * env, EMemoryContext* mem_context)
{
	TypeList areas;
	TypeList nexts;
	ERule * rule;
	EArea * src;
	EArea * itr;
	uint32_t dim;
	TRACE_INIT_TIME

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s EnumAreaAction ",++env->tei," "));
	DebugArea( dst, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "EnumAreaAction" << " called" << endl;
		/* printf("EnumAreaAction");  DebugArea( dst, env ); */
	}

	/* the consolidation zone consists of several areas                    */
	src = NewCopyArea(dst, mem_context);
	for (dim = 0; dim < dst->cube->nrDimensions; dim++) {
		if (dst->borders[dim]->nrCons != 0) {
			FreeBorder(src->borders[dim], mem_context);
			src->borders[dim] = NewConsBorder(dst->borders[dim], mem_context);
			EnumConsAction(act, src, env, mem_context);
		}
		if (dst->borders[dim]->nrBase != 0) {
			FreeBorder(src->borders[dim], mem_context);
			src->borders[dim] = NewBaseBorder(dst->borders[dim], mem_context);
		} else {
			FreeArea(src, mem_context);
			TRACE_PRINTF((env->file,"%*s EnumAreaAction magically returns %4.3f\n",env->tei--," ",TRACE_TIME));
			return; /* this is really magical ;-) */
		}
	}

	/* magically now src is the subarea of dst containing the base cells   */
	/* we keep a list of areas that must still be consolidated into dst    */
	/* initially this consist of the entire source area                    */
	InitList( &areas, offsetof( struct StructArea, next ), -1 );
	AppendItemList( &areas, src );

	/* loop over the rules                                                 */
	for (rule = (ERule*)FirstItemList(&(dst->cube->rules_n)); rule != 0; rule = (ERule*)NextItemList(&(dst->cube->rules_n),rule)) {

		/* prepare the list of remaining areas for the next round          */
		InitList( &nexts, offsetof( struct StructArea, next ), -1 );

		/* work off the list of areas                                      */
		while (0 < NrItemsList( &areas )) {
			src = (EArea*)FirstItemList( &areas );
			RemoveItemList(&areas, src);

			/* compute the intersection, where the rule can be applied     */
			itr = NewIntersectionArea(src, rule->dest_area, mem_context);

			/* if the intersection is nonempty                             */
			if (itr && !IsEmptyArea(itr)) {

				/* with <rule> compute <intersection>, then apply <action> */
				EnumRuleAction(act, itr, rule, env, mem_context);

				/* in the next round we need to work off the complement    */
				AppendComplementArea(&nexts, src, itr, mem_context);
				FreeArea(src, mem_context);

			} else { // if the intersection is empty

				/* keep the area for the next round                        */
				AppendItemList( &nexts, src );
			}

			/* deallocate the intersection                                 */
			if (itr) {
				FreeArea(itr, mem_context);
			}
		}

		/* now use the new list of remaining areas for the next rule       */
		/* here it might be worthwhile to try to join areas together       */
		CopyList(&areas, &nexts);
	}

	/* the remaining cells are not computed (have no rule)                 */
	/* here it might be worthwhile to try and join areas together          */
	while (0 < NrItemsList( &areas )) {
		src = (EArea*)FirstItemList( &areas );
		RemoveItemList(&areas, src);
		EnumBaseAction(act, src, env, mem_context);
		FreeArea(src, mem_context);
	}

	if (Logger::isTrace()) {
		Logger::trace << "EnumAreaAction" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s EnumAreaAction returns %4.3f\n",env->tei--," ",TRACE_TIME));

}

/****************************************************************************
 **
 *F  EnumConsAction(action,dst,env)  . . . . . . .  enumerate over cons values
 **
 **  'EnumConsAction'  calls  the function  <dest_func>  for all  consolidated
 **  cells in the area <dest_area> passing <dest_env> as third argument.  Note
 **  that  it simply  ignores any  non consolidated  cells in  the destination
 **  area.
 */

void EnumConsAction(EAction * action, EArea * area, Environment * env, EMemoryContext* mem_context)
{
	EBuffer * dst;
	uint32_t size;
	double dsize;
	uint32_t sizes[MAX_DIMENSIONS];
	EElementId * eids[MAX_DIMENSIONS];
	/* Offset              idxs [ MAX_DIMENSIONS ]; */
	EPath path;
	uint32_t dim;
	EElementId * e;
	uint32_t i;
	Offset pos;
	uint32_t count;
	TRACE_INIT_TIME

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s EnumConsAction %9.3f",++env->tei," ",TRACE_REAL));
	DebugArea( area, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "EnumConsAction" << " called" << endl;
		/* printf("EnumConsAction");  DebugArea( area, env ); */
	}

	/* copy the element ids for easier access (this should go away)        */
	dsize = 1;
	for (dim = 0; dim < area->cube->nrDimensions; dim++) {
		sizes[dim] = area->borders[dim]->size;
		dsize = dsize * sizes[dim];
		if (dsize > UINT_MAX) {
			throw ErrorException(ErrorException::ERROR_INTERNAL, "Too many cells in the consolidation area: cube=" + StringUtils::convertToString(area->cube->acube->getIdentifier()));
		}
		eids[dim] = (EElementId*)ecalloc(sizes[dim], sizeof(EElementId), mem_context);
		NrCallocEids++;
		e = eids[dim];
		/* diese unterscheidung ist ueberfluessig, weil ich immer border->conss habe XXX */
		if (area->borders[dim]->nrCons < area->borders[dim]->dimension->nrCons) {
			for (i = 0; i < area->borders[dim]->nrCons; i++) {
				*e++ = area->borders[dim]->conss[i];
			}
		} else {
			for (i = 0; i < area->borders[dim]->nrCons; i++) {
				*e++ = area->borders[dim]->dimension->conss[i];
			}
		}
		if (area->borders[dim]->nrBase < area->borders[dim]->dimension->nrBase) {
			for (i = 0; i < area->borders[dim]->nrBase; i++) {
				*e++ = area->borders[dim]->bases[i];
			}
		} else {
			for (i = 0; i < area->borders[dim]->nrBase; i++) {
				*e++ = area->borders[dim]->dimension->bases[i];
			}
		}
	}

	/* allocate the new buffer                                             */
	size = (uint32_t)dsize;
	dst = NewBuffer(size, mem_context);

	/*                                                                     */
	FillAreaBuffer(dst, area, env, mem_context);

	/* and enumerate the elements                                          */
	/*
	 for ( dim = 0; dim < area->cube->nrDimensions; dim++ ) {
	 idxs[ dim ] = 0;
	 path[ dim ] = eids[ dim ][ idxs[ dim ] ];
	 }
	 */
	count = 0;
	for (pos = 0; pos < size; pos++) {
		if (dst->values[pos] != 0) {
			count++;
			i = pos;
			for (dim = 0; dim < area->cube->nrDimensions; dim++) {
				path[dim] = eids[dim][i % sizes[dim]];
				i = i / sizes[dim];
			}
			ApplyAction( action, path, dst->values[pos], 0, env, mem_context );
		}
		/* it may be a good idea to compute the path only if needed */
		/*
		 if ( pos < size-1 ) {
		 for ( dim = 0; idxs[dim] == sizes[dim]-1; dim++ ) {
		 idxs[dim] = 0;
		 path[dim] = eids[dim][idxs[dim]];
		 }
		 idxs[dim]++;
		 path[dim] = eids[dim][idxs[dim]];
		 }
		 */
	}

	/* free the stuff                                                      */
	FreeBuffer(dst, mem_context);
	for (dim = 0; dim < area->cube->nrDimensions; dim++) {
		efree(eids[dim], mem_context);
		NrCallocEids--;
	}

	if (Logger::isTrace()) {
		Logger::trace << "EnumConsAction" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s EnumConsAction returns %d %d %4.3f %9.3f\n",env->tei--," ",size,count,TRACE_TIME,TRACE_REAL));

}

/****************************************************************************
 **
 *F  EnumRuleAction(action,dst,rule,env) . . . . .  enumerate over rule values
 **
 */
void EnumRuleAction(EAction * act, EArea * dst, ERule * rule, Environment * env, EMemoryContext* mem_context)
{
	EArea * src;
	RuleSrcChainAction srcAct;
	RuleDstChainAction dstAct;
	TRACE_INIT_TIME

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s EnumRuleAction %d ",++env->tei," ",rule->nr_rule));
	DebugArea( dst, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "EnumRuleAction" << " called" << endl;
		/* printf("EnumRuleAction %d ",rule->nr_rule);  DebugArea( dst, env ); */
	}

	/* uniq bijective marker */
	if (rule->ubm_flag) {
		EnumBaseAction(act, dst, env, mem_context);

		src = NewSourceAreaRule(rule, dst, mem_context);

		mem_context->m_marker_recursion_stack.push(src, rule); //-jj-

		InitRuleSrcChainAction(&srcAct, rule, act);
		EnumAreaAction((EAction*)&srcAct, src, env, mem_context);

		mem_context->m_marker_recursion_stack.pop(); //-jj-
		FreeArea(src, mem_context);
	}

	/* marker but not uniq bijection */
	else if (rule->marker_flag) {
		EnumBaseAction(act, dst, env, mem_context);
		InitRuleDstChainAction(&dstAct, rule, act);
		EnumBaseMarkerAction((EAction*)&dstAct, dst, env, mem_context);
	}

	/* no marker */
	else {
		InitRuleDstChainAction(&dstAct, rule, act);
		EnumBaseZeroAction((EAction*)&dstAct, dst, env, mem_context);
	}

	if (Logger::isTrace()) {
		Logger::trace << "EnumRuleAction" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s EnumRuleAction returns %4.3f\n",env->tei--," ",TRACE_TIME));

}

/****************************************************************************
 **
 *F  EnumBaseAction(action,dst,env)  . . . . . . .  enumerate over base values
 **
 **  run over entire base and apply function where appropriate
 */
uint32_t NrEnumBaseAction1 = 0;
uint32_t NrEnumBaseAction100 = 0;
uint32_t NrEnumBaseActionLarge = 0;

void EnumBaseAction(EAction * act, EArea * dst, Environment * env, EMemoryContext* mem_context)
{
	EPath path;
	Offset idxs[MAX_DIMENSIONS];
	uint32_t size;
	uint32_t dim;
	uint32_t nrDims;
	uint32_t isDone;
	Value * row;
	uint32_t i0, i1;
	size_t val_size = dst->cube->acube->getStorageDouble()->getValueSize();
	size_t key_size = dst->cube->acube->getStorageDouble()->getKeySize();
	TRACE_INIT_TIME

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s EnumBaseAction ",++env->tei," "));
	DebugArea( dst, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "EnumBaseAction" << " called" << endl;
		/* printf("EnumBaseAction");  DebugArea( dst, env ); */
	}

	nrDims = dst->cube->nrDimensions;

	/* test if the area has more than 100 cells in it                      */
	size = 1;
	for (dim = 0; dim < nrDims && size < 100; dim++) {
		size *= dst->borders[dim]->nrBase;
	}

	/* handle single elements                                              */
	if (size == 1) {
		NrEnumBaseAction1++;
		for (dim = 0; dim < nrDims; dim++) {
			path[dim] = dst->borders[dim]->bases[0];
		}
		row = (Value*)dst->cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
		if (row && !IS_MARKERED(row)) {
			ApplyAction( act, path, *row, BaseStatus, env, mem_context );
		}
	}

	/* handle very small areas by enumerating all the cells                */
	else if (size < 100) {
		NrEnumBaseAction100++;
		isDone = 0;
		for (dim = 0; dim < nrDims; dim++) {
			if (0 < dst->borders[dim]->nrBase) {
				idxs[dim] = 0;
				path[dim] = dst->borders[dim]->bases[idxs[dim]];
			} else {
				isDone = 1;
				break;
			}
		}
		while (!isDone) {
			row = (Value*)dst->cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
			if (row && !IS_MARKERED(row)) {
				ApplyAction( act, path, *row, BaseStatus, env, mem_context );
			}
			isDone = 1;
			for (dim = 0; dim < nrDims; dim++) {
				if (idxs[dim] + 1 < dst->borders[dim]->nrBase) {
					idxs[dim]++;
					path[dim] = dst->borders[dim]->bases[idxs[dim]];
					isDone = 0;
					break;
				} else {
					idxs[dim] = 0;
					path[dim] = dst->borders[dim]->bases[idxs[dim]];
				}
			}
		}
	} else { // handle large areas
		NrEnumBaseActionLarge++;
		uint32_t i0max = nrDims <= 1 ? 1 : dst->borders[0]->nrBase; // -jj-
		uint32_t i1max = nrDims <= 2 ? 1 : dst->borders[1]->nrBase; // -jj-

		for (i0 = 0; i0 < i0max; i0++) {
			for (i1 = 0; i1 < i1max; i1++) {
				CubePage * page = dst->cube->acube->getStorageDouble()->lookupCubePage((IdentifierType)(dst->borders[0]->bases[i0]), nrDims > 2 ? (IdentifierType)(dst->borders[1]->bases[i1]) : 0);

				if (page == 0) {
					continue;
				}

				page->sortL();

				size_t row_size = page->getRowSize();
				CubePage::buffer_t page_beg = page->begin();
				CubePage::buffer_t page_end = page->end();
				CubePage::buffer_t row; // a row
				double val; // the value of that row
				IdentifierType * path; // the path of that row
				IdentifierType p; // one dimension element
				int32_t i; // loop variable
				uint32_t j;

				// loop over all rows in the page
				row = page_beg;
				while (row < page_end) {

					// compute the hash and the weight for that path
					// break if one path component could not be mapped
					path = (IdentifierType*)(row + val_size);
					p = path[1];
					for (i = nrDims - 1; 1 < i; i--) {
						p = path[i];
						if (dst->borders[i]->nrBase < dst->borders[i]->dimension->nrBase) {
							for (j = 0; j < dst->borders[i]->nrBase; j++) {
								if (p == dst->borders[i]->bases[j]) {
									break;
								}
							}
							if (j == dst->borders[i]->nrBase) {
								break;
							}
						}
					}

					// if the source value shall be consolidated, add it to the storage
					if (i == 1) {
						if (!IS_MARKERED(row)) {
							val = *(double*)row;
							ApplyAction( act, (EElementId*)(row + val_size), val, BaseStatus, env, mem_context );
						}
						row += row_size;
					}

					// otherwise skip over all rows with the same offending element
					else {
						do {
							row += row_size;
							if (page_end <= row)
								break;
							path = (IdentifierType*)(row + val_size);
						} while (path[i] == p);
					}

				}

			}
		}
	}

	if (Logger::isTrace()) {
		Logger::trace << "EnumBaseAction" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s EnumBaseAction returns %4.3f\n",env->tei--," ",TRACE_TIME));

}

/****************************************************************************
 **
 *F  EnumBaseMarkerAction(action,dst,env)  . . . .  enumerate over base values
 **
 **  run over entire base and apply function where appropriate
 */
void EnumBaseMarkerAction(EAction * act, EArea * dst, Environment * env, EMemoryContext* mem_context)
{
	CubePage::element_t row;
	IdentifierType * pathr;
	IdentifierType p;
	EPath path;
	Offset idxs[MAX_DIMENSIONS];
	uint32_t size;
	uint32_t dim;
	uint32_t nrDims;
	uint32_t isDone;
	uint32_t i0, i1;
	size_t val_size = dst->cube->acube->getStorageDouble()->getValueSize();
	size_t key_size = dst->cube->acube->getStorageDouble()->getKeySize();
	TRACE_INIT_TIME

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s EnumBaseMarkerAction ",++env->tei," "));
	DebugArea( dst, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "EnumBaseMarkerAction" << " called" << endl;
		/* printf("EnumBaseMarkerAction");  DebugArea( dst, env ); */
	}

	nrDims = dst->cube->nrDimensions;

	/* test if the area has more than 100 cells in it                      */
	size = 1;
	for (dim = 0; dim < nrDims && size < 100; dim++) {
		size *= dst->borders[dim]->nrBase;
	}

	/* handle single elements                                              */
	if (size == 1) {
		NrEnumBaseAction1++;
		for (dim = 0; dim < nrDims; dim++) {
			path[dim] = dst->borders[dim]->bases[0];
		}
		row = dst->cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
		// go only for valid path
		if (row && IS_MARKERED(row)) {
			ApplyAction( act, (IdentifierType*)(row + val_size), *(Value*)row, BaseStatus, env, mem_context );
		}
	}

	/* handle very small areas by enumerating all the cells                */
	else if (size < 100) {
		NrEnumBaseAction100++;
		isDone = 0;
		for (dim = 0; dim < nrDims; dim++) {
			if (0 < dst->borders[dim]->nrBase) {
				idxs[dim] = 0;
				path[dim] = dst->borders[dim]->bases[idxs[dim]];
			} else {
				isDone = 1;
				break;
			}
		}
		while (!isDone) {
			row = dst->cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
			// go only for valid path
			if (row && IS_MARKERED (row)) {
				ApplyAction( act, (IdentifierType*)(row + val_size), *(Value*)row, BaseStatus, env, mem_context );
			}

			isDone = 1;
			for (dim = 0; dim < nrDims; dim++) {
				if (idxs[dim] + 1 < dst->borders[dim]->nrBase) {
					idxs[dim]++;
					path[dim] = dst->borders[dim]->bases[idxs[dim]];
					isDone = 0;
					break;
				} else {
					idxs[dim] = 0;
					path[dim] = dst->borders[dim]->bases[idxs[dim]];
				}
			}
		}

	} else { /* handle large areas */
		NrEnumBaseActionLarge++;

		// -jj-
		uint32_t i0max = nrDims <= 1 ? 1 : dst->borders[0]->nrBase;
		uint32_t i1max = nrDims <= 2 ? 1 : dst->borders[1]->nrBase;

		for (i0 = 0; i0 < i0max; i0++) {

			for (i1 = 0; i1 < i1max; i1++) {
				CubePage * page = dst->cube->acube->getStorageDouble()->lookupCubePage((IdentifierType)(dst->borders[0]->bases[i0]), dst->cube->nrDimensions > 2 ? (IdentifierType)(dst->borders[1]->bases[i1]) : 0 );

				if (page == 0) {
					continue;
				}

				page->sortL();

				size_t row_size = page->getRowSize();
				CubePage::buffer_t page_beg = page->begin();
				CubePage::buffer_t page_end = page->end();
				int32_t i; // loop variable
				uint32_t j;

				// loop over all rows in the page
				row = page_beg;
				while (row < page_end) {

					// compute the hash and the weight for that path
					// break if one path component could not be mapped
					pathr = (IdentifierType*)(row + val_size);
					p = pathr[0];
					for (i = nrDims - 1; 1 < i; i--) {
						p = pathr[i];
						if (dst->borders[i]->nrBase < dst->borders[i]->dimension->nrBase) {
							for (j = 0; j < dst->borders[i]->nrBase; j++) {
								if (p == dst->borders[i]->bases[j]) {
									break;
								}
							}
							if (j == dst->borders[i]->nrBase) {
								break;
							}
						}
					}

					// if the source value shall be consolidated, add it to the storage
					if (i == 1) {
						if (((*(uint32_t*)(row + val_size + key_size)) & 0x80000000) != 0) {
							ApplyAction( act, pathr, *(Value*)row, BaseStatus, env, mem_context );
						}
						row += row_size;
					} else { // otherwise skip over all rows with the same offending element
						do {
							row += row_size;
							if (page_end <= row)
								break;
							pathr = (IdentifierType*)(row + val_size);
						} while (pathr[i] == p);
					}

				}

			}
		}
	}

	if (Logger::isTrace()) {
		Logger::trace << "EnumBaseMarkerAction" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s EnumBaseMarkerAction returns %4.3f\n",env->tei--," ",TRACE_TIME));

}

/****************************************************************************
 **
 *F  EnumBaseZeroAction(action,dst,env)  . . . . .  enumerate over base values
 **
 **  run over entire base and apply function where appropriate
 */
void EnumBaseZeroAction(EAction * act, EArea * dst, Environment * env, EMemoryContext* mem_context)
{
	CubePage::element_t row;
	EPath path;
	Offset idxs[MAX_DIMENSIONS];
	uint32_t dim;
	uint32_t nrDims;
	uint32_t isDone;
	size_t val_size = dst->cube->acube->getStorageDouble()->getValueSize();
	size_t key_size = dst->cube->acube->getStorageDouble()->getKeySize();
	TRACE_INIT_TIME

	/* XXX das muss in der Lage sein auch mit Cons Zellen umzugehen */

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s EnumBaseZeroAction ",++env->tei," "));
	DebugArea( dst, env );
	TRACE_PRINTF((env->file,"\n"));
#endif

	if (Logger::isTrace()) {
		Logger::trace << "EnumBaseZeroAction" << " called" << endl;
		/* printf("EnumBaseZeroAction");  DebugArea( dst, env ); */
	}

	nrDims = dst->cube->nrDimensions;

	isDone = 0;
	for (dim = 0; dim < nrDims; dim++) {
		if (0 < dst->borders[dim]->nrBase) {
			idxs[dim] = 0;
			path[dim] = dst->borders[dim]->bases[idxs[dim]];
		} else {
			isDone = 1;
			break;
		}
	}
	//this is the point of failure. check the areas
	while (!isDone) {
		row = dst->cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
		// go only for valid path
		if (row && IS_MARKERED(row)) {
			ApplyAction( act, path, *(Value*)row, BaseStatus, env, mem_context );
		} else {
			ApplyAction( act, path, 0, BaseStatus, env, mem_context );
		}
		isDone = 1;
		for (dim = 0; dim < nrDims; dim++) {
			if (idxs[dim] + 1 < dst->borders[dim]->nrBase) {
				idxs[dim]++;
				path[dim] = dst->borders[dim]->bases[idxs[dim]];
				isDone = 0;
				break;
			} else {
				idxs[dim] = 0;
				path[dim] = dst->borders[dim]->bases[idxs[dim]];
			}
		}
	}

	if (Logger::isTrace()) {
		Logger::trace << "EnumBaseZeroAction" << " done" << endl;
	}

	TRACE_PRINTF((env->file,"%*s EnumBaseZeroAction returns %4.3f\n",env->tei--," ",TRACE_TIME));

}

/****************************************************************************
 **

 *F  GetValue(ptrValue,ptrStatus,cube,path,env)
 */
void GetValue(Value * ptrValue, Status * ptrStatus, ECube * cube, EPath path, Environment * env, EMemoryContext* mem_context)
{
	ERule * rule;
	EPath srcPath;
	Value srcValue;
	Status srcStatus;
	Value * row;
	EArea * area;
	EBuffer * buf;
	uint32_t d;
	TRACE_INIT_TIME

	TRACE_PRINTF((env->file,"%*s GetValue\n",++env->tei," "));

	/* check if consolidated                                               */
	for (d = 0; d < cube->nrDimensions; d++) {
		if (cube->dimensions[d]->type[path[d]] == 'C') {
			break;
		}
	}

	/* cell is consolidated                                                */
	if (d < cube->nrDimensions) {

		/* loop over the rules                                             */
		for (rule = (ERule*)FirstItemList(&(cube->rules_c)); rule != 0; rule = (ERule*)NextItemList(&(cube->rules_c),rule)) {
			if (IsPathInArea(rule->dest_area, path)) {
				/* rules that apply to cons cells should never have marker */
				/* even if the code does have a marker the following code  */
				/* will not crash, but simply return 0 for the marker      */
				virtual_machine machine(env, mem_context, ptrValue, ptrStatus);
				machine.compute(path, rule, 0, 0, 0);
				//                ComputeRule( ptrValue, ptrStatus, path, rule, 0, 0, env, 0, stack, mem_context );
				TRACE_PRINTF((env->file,"%*s GetValue returns after applying rule %d %4.3f\n",env->tei--," ",rule->nr_rule,TRACE_TIME));
				return;
			}
		}

		/* if no applicable rule is found, treat this as an area           */
		area = NewPathArea(cube, path, mem_context);
		buf = NewBuffer(1, mem_context);
		FillAreaBuffer(buf, area, env, mem_context);
		*ptrValue = buf->values[0];
		*ptrStatus = buf->status[0];
		FreeBuffer(buf, mem_context);
		FreeArea(area, mem_context);

	}

	/* cell is base                                                        */
	else {

		/* loop over the rules                                             */
		for (rule = (ERule*)FirstItemList(&(cube->rules_n)); rule != 0; rule = (ERule*)NextItemList(&(cube->rules_n),rule)) {
			if (IsBasePathInArea(rule->dest_area, path)) {
				if (rule->ubm_flag) {
					SrcPathRule(srcPath, rule, path);
					GetValue(&srcValue, &srcStatus, cube, srcPath, env, mem_context);
					virtual_machine machine(env, mem_context, ptrValue, ptrStatus);
					machine.compute(path, rule, srcValue, srcStatus, 1);
					//                    ComputeRule( ptrValue, ptrStatus, path, rule, srcValue, srcStatus, env, 1, stack, mem_context );
					TRACE_PRINTF((env->file,"%*s GetValue returns after applying rule %d %4.3f\n",env->tei--," ",rule->nr_rule,TRACE_TIME));
					return;
				} else {
					virtual_machine machine(env, mem_context, ptrValue, ptrStatus);
					machine.compute(path, rule, 0, 0, 1);
					//                    ComputeRule( ptrValue, ptrStatus, path, rule, 0, 0, env, 1, stack, mem_context );
					TRACE_PRINTF((env->file,"%*s GetValue returns after applying rule %d %4.3f\n",env->tei--," ",rule->nr_rule,TRACE_TIME));
					return;
				}
			}
		}

		/* if no applicable rule is found, try a base lookup               */
		row = (Value*)cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
		if (row) {
			*ptrValue = *row;
			*ptrStatus = BaseStatus;
		} else {
			*ptrValue = 0.0;
			*ptrStatus = 0;
		}

	}

	TRACE_PRINTF((env->file,"%*s GetValue returns %f %4.3f\n",env->tei--," ",*ptrValue,TRACE_TIME));

}

void GetValueNoRule(Value * ptrValue, Status * ptrStatus, ECube * cube, EPath path, Environment * env, EMemoryContext* mem_context)
{
	Value * row;
	EArea * area;
	EBuffer * buf;
	uint32_t d;
	TRACE_INIT_TIME

	TRACE_PRINTF((env->file,"%*s GetValueNoRule\n",++env->tei," "));

	/* check if consolidated                                               */
	for (d = 0; d < cube->nrDimensions; d++) {
		if (cube->dimensions[d]->type[path[d]] == 'C') {
			break;
		}
	}

	/* cell is consolidated                                                */
	if (d < cube->nrDimensions) {

		/* if no applicable rule is found, treat this as an area           */
		area = NewPathArea(cube, path, mem_context);
		buf = NewBuffer(1, mem_context);
		FillAreaBuffer(buf, area, env, mem_context);
		*ptrValue = buf->values[0];
		*ptrStatus = buf->status[0];
		FreeBuffer(buf, mem_context);
		FreeArea(area, mem_context);

	}

	/* cell is base                                                        */
	else {

		/* if no applicable rule is found, try a base lookup               */
		row = (Value*)cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
		if (row) {
			*ptrValue = *row;
			*ptrStatus = BaseStatus;
		} else {
			*ptrValue = 0.0;
			*ptrStatus = 0;
		}

	}

	TRACE_PRINTF((env->file,"%*s GetValueNoRule returns %f %4.3f\n",env->tei--," ",*ptrValue,TRACE_TIME));

}

/****************************************************************************
 **

 *F  NewEntryEnvironment(user) . . . . . . . . . .  allocate a new environment
 **
 */
Environment * NewEntryEnvironment(User * auser, EMemoryContext* mem_context)
{
	Environment * result;
	result = (Environment*)ecalloc(1, sizeof(Environment), mem_context);
	NrCallocEnvironment++;
	result->auser = auser;
#ifdef TRACE_ENGINE
	char filename [64];
	sprintf( filename, "/tmp/palog_%u.log", (uint32_t)rand() );
	result->file = fopen( filename, "w" );
	result->tei = 0;
#endif
	return result;
}

/****************************************************************************
 **
 *F  FreeEnvironment(env)  . . . . . . . . . . . . . deallocate an environment
 */
void FreeEnvironment(Environment * env, EMemoryContext* mem_context)
{
	if (env) {
#ifdef TRACE_ENGINE
		fclose( env->file );
#endif
		efree(env, mem_context);
		NrCallocEnvironment--;
	}
}

/****************************************************************************
 **
 *F  NewEntryDimension(dimension)  . . . . allocate a new dimension upon entry
 **
 */
EDimension * NewEntryDimension(Dimension * adimension, EMemoryContext* mem_context, bool register_mem = true)
{
	EDimension * result;
	IdentifierType max;
	Offset cntCons;
	Offset cntBase;
	uint32_t i;

	result = (EDimension*)ecalloc(1, sizeof(EDimension), mem_context, register_mem);
	NrCallocEDimension++;
	result->adimension = adimension;
	max = adimension->getMaximalIdentifier();
	result->maximal = max;

	cntCons = 0;
	cntBase = 0;
	for (i = 0; i <= max; i++) {
		Element * celm = adimension->lookupElement((IdentifierType)i);
		if (celm) {
			if (celm->getElementType() == CONSOLIDATED) {
				cntCons++;
			} else if (celm->getElementType() == NUMERIC || celm->getElementType() == STRING) {
				cntBase++;
			}

		}
	}

	result->nrCons = cntCons;
	result->nrBase = cntBase;
	result->size = result->nrCons + result->nrBase;

	if (result->nrCons != 0) {
		result->conss = (EElementId*)ecalloc(cntCons, sizeof(EElementId), mem_context, register_mem);
		NrCallocConss++;
	}
	if (result->nrBase != 0) {
		result->bases = (EElementId*)ecalloc(cntBase, sizeof(EElementId), mem_context, register_mem);
		NrCallocBases++;
	}
	result->type = (char *)ecalloc((max + 1), sizeof(uint8_t), mem_context, register_mem);
	NrCallocIsCons++;

	cntCons = 0;
	cntBase = 0;
	for (i = 0; i <= max; i++) {
		Element * celm = adimension->lookupElement((IdentifierType)i);
		if (celm) {
			if (celm->getElementType() == CONSOLIDATED) {
				result->conss[cntCons] = i;
				cntCons++;
				result->type[i] = 'C';
			} else if (celm->getElementType() == NUMERIC) {
				result->bases[cntBase] = i;
				cntBase++;
				result->type[i] = 'N';
			} else if (celm->getElementType() == STRING) {
				result->type[i] = 'S';
			}
		}
	}

	return result;
}

void FreeDimension(EDimension * dimension, EMemoryContext* mem_context)
{
	if (dimension) {
		if (dimension->conss) {
			efree(dimension->conss, mem_context);
			NrCallocConss--;
		}
		if (dimension->bases) {
			efree(dimension->bases, mem_context);
			NrCallocConss--;
		}
		if (dimension->type) {
			efree(dimension->type, mem_context);
			NrCallocIsCons--;
		}
		efree(dimension, mem_context);
		NrCallocEDimension--;
	}
}

/****************************************************************************
 **
 *F  NewEntryBorder(dimension,border)  . . . . .  allocate a border upon entry
 **
 */
#define SORT_NAME           SortEidList
#define SORT_TYPE           EElementId
#define SORT_LESS(x,y)      (*(x) < *(y))
#define SORT_COPY(x,y)      (*(x) = *(y))
#define SORT_SWAP(x,y,z)    (SORT_COPY(z,x), SORT_COPY(x,y), SORT_COPY(y,z))

#define SORT_COFF           20

void SORT_NAME(SORT_TYPE * beg, SORT_TYPE * end)
{
	SORT_TYPE * b;
	SORT_TYPE * e;
	SORT_TYPE * p;
	SORT_TYPE t;

	if (end - beg < 2) {
		return;
	}

	if (end - beg < SORT_COFF) {
		for (b = beg + 1; b < end; b++) {
			if (SORT_LESS(b,b-1)) {
				SORT_COPY( &t, b );
				SORT_COPY( b, b-1 );
				for (e = b - 2; beg <= e && SORT_LESS(&t,e); e--) {
					SORT_COPY( e+1, e );
				}
				SORT_COPY( e+1, &t );
			}
		}
		return;
	}

	b = beg + (end - beg) / 2;
	e = end - 1;
	if (SORT_LESS(b,e)) {
		SORT_SWAP( b, e, &t );
	}
	if (SORT_LESS(beg,e)) {
		SORT_SWAP( beg, e, &t );
	} else if (SORT_LESS(b,beg)) {
		SORT_SWAP( b, beg, &t );
	}

	p = beg;
	b = beg;
	e = end;
	while (b < e) {
		do {
			e--;
		} while (b < e && !SORT_LESS(e,p));
		do {
			b++;
		} while (b < e && !SORT_LESS(p,b));
		if (b < e) {
			SORT_SWAP( b, e, &t );
		}
	}
	SORT_SWAP( beg, e, &t );

	SORT_NAME(beg, e);
	SORT_NAME(e + 1, end);

}

#undef SORT_NAME
#undef SORT_TYPE
#undef SORT_LESS
#undef SORT_COPY
#undef SORT_SWAP
#undef SORT_COFF

EBorder * NewEntryBorder(vector<Element*> * aborder, EDimension * dimension, EMemoryContext* mem_context)
{
	EBorder * result;
	uint32_t nrCons;
	uint32_t nrBase;
	uint32_t i;
	IdentifierType eid;

	nrCons = 0;
	nrBase = 0;
	for (vector<Element*>::iterator p = aborder->begin(); p < aborder->end(); p++) {
		eid = (*p)->getIdentifier();
		if (dimension->type[eid] == 'C') {
			nrCons++;
		} else {
			nrBase++;
		}
	}

	result = (EBorder*)ecalloc(1, sizeof(EBorder), mem_context);
	NrCallocEBorder++;
	result->dimension = dimension;

	result->nrCons = nrCons;
	if (0 < nrCons && nrCons < dimension->nrCons) {
		result->conss = (EElementId*)ecalloc(nrCons, sizeof(EElementId), mem_context);
		NrCallocConss++;
		i = 0;
		for (vector<Element*>::iterator p = aborder->begin(); p < aborder->end(); p++) {
			eid = (*p)->getIdentifier();
			if (dimension->type[eid] == 'C') {
				result->conss[i] = eid;
				i++;
			}
		}
		SortEidList(result->conss, result->conss + nrCons);
	} else if (nrCons == dimension->nrCons) {
		result->conss = dimension->conss;
	}

	result->nrBase = nrBase;
	if (0 < nrBase && nrBase < dimension->nrBase) {
		result->bases = (EElementId*)ecalloc(nrBase, sizeof(EElementId), mem_context);
		NrCallocBases++;
		i = 0;
		for (vector<Element*>::iterator p = aborder->begin(); p < aborder->end(); p++) {
			eid = (*p)->getIdentifier();
			if (dimension->type[eid] != 'C') {
				result->bases[i] = eid;
				i++;
			}
		}
		SortEidList(result->bases, result->bases + nrBase);
	} else if (nrBase == dimension->nrBase) {
		result->bases = dimension->bases;
	}

	result->size = result->nrCons + result->nrBase;

	return result;
}

/****************************************************************************
 **
 *F  NewEntryCube(cube,user) . . . . . . . . .  allocate a new cube upon entry
 **
 */
ECube * NewEntryCube(Cube * acube, const User * auser, EMemoryContext* mem_context)
{
	ECube * result;
	ERule * rule;
	uint32_t i;

	WriteLocker wl(&(acube->ecubeLock));

	result = acube->getEngineCube();
	if (result) {
		return result;
	}

	//memory allocated here is reffered on the Cube.cpp level. It does not qualify for autodestruction
	result = (ECube*)ecalloc(1, sizeof(ECube), mem_context, false);
	NrCallocECube++;
	result->acube = acube;

	result->nrDimensions = (uint32_t)acube->getDimensions()->size();
	for (i = 0; i < result->nrDimensions; i++) {
		result->dimensions[i] = NewEntryDimension(acube->getDimensions()->at(i), mem_context, false);
	}

	InitList( &(result->rules_c), offsetof( struct StructRule, next_c ), -1 );
	InitList( &(result->rules_n), offsetof( struct StructRule, next_n ), -1 );
	InitList( &(result->rules_nm), offsetof( struct StructRule, next_nx ), -1 );
	InitList( &(result->rules_nu), offsetof( struct StructRule, next_nx ), -1 );
	result->nrRules = (uint32_t)acube->getRules(const_cast<User*> (auser)).size();
	for (i = 0; i < result->nrRules; i++) {
		rule = NewEntryRule(acube->getRules(const_cast<User*> (auser)).at(i), result, mem_context, false);
		if (rule) {
			if (rule->is_cons) {
				AppendItemList( &(result->rules_c), rule );
			}
			if (rule->is_base) {
				AppendItemList( &(result->rules_n), rule );
				if (rule->marker_flag) {
					AppendItemList( &(result->rules_nm), rule );
				} else {
					AppendItemList( &(result->rules_nu), rule );
				}
			}
		}
	}

	acube->setEngineCube(result);

	return result;
}

void FreeRule(ERule * rule, EMemoryContext* mem_context);

void FreeCube(ECube * cube, EMemoryContext* mem_context)
{
	ERule * rule;
	uint32_t i;
	if (cube) {
		for (i = 0; i < cube->nrDimensions; i++) {
			if (cube->dimensions[i]) {
				FreeDimension(cube->dimensions[i], mem_context);
			}
		}
		rule = (ERule*)FirstItemList(&(cube->rules_c));
		while (rule) {
			RemoveItemList(&(cube->rules_c), rule);
			if (!rule->is_base) {
				FreeRule(rule, mem_context);
			}
			rule = (ERule*)FirstItemList(&(cube->rules_c));
		}
		rule = (ERule*)FirstItemList(&(cube->rules_n));
		while (rule) {
			RemoveItemList(&(cube->rules_n), rule);
			FreeRule(rule, mem_context);
			rule = (ERule*)FirstItemList(&(cube->rules_n));
		}
		efree(cube, mem_context);
		NrCallocECube--;
	}
}

/****************************************************************************
 **
 *F  NewEntryArea(cube,area) . . . . . . . . . . . allocate an area upon entry
 **
 */
EArea * NewEntryArea(vector<vector<Element*> > * aarea, ECube * cube, EMemoryContext* mem_context)
{
	EArea * result;
	uint32_t i;

	result = (EArea*)ecalloc(1, sizeof(EArea), mem_context);
	NrCallocEArea++;
	result->cube = cube;

	for (i = 0; i < cube->nrDimensions; i++) {
		result->borders[i] = NewEntryBorder(&((*aarea)[i]), cube->dimensions[i], mem_context);
	}

	result->next = 0;

	return result;
}

ERule * NewEntryRule(Rule * arule, ECube * cube, EMemoryContext* mem_context, bool register_mem)
{
	ERule * result;

	if (!arule->isActive()) {
		return 0;
	}

	result = (ERule*)ecalloc(1, sizeof(ERule), mem_context, register_mem);
	NrCallocERule++;
	result->arule = arule;
	result->nr_rule = arule->getIdentifier();
	if (arule->getRuleOption() == RuleNode::NONE) {
		result->is_base = 1;
		result->is_cons = 1;
	} else if (arule->getRuleOption() == RuleNode::CONSOLIDATION) {
		result->is_base = 0;
		result->is_cons = 1;
	} else if (arule->getRuleOption() == RuleNode::BASE) {
		result->is_base = 1;
		result->is_cons = 0;
	}

	DEBUG_PRINTF(("generating code for rule %d\n",result->nr_rule));

	result->cube = cube;

	/* allocate bytecode */
	result->gc_bc_nr = 0;
	result->gc_bc_max = 25600;
	result->bytecode = (Bytecode*)ecalloc(result->gc_bc_max, sizeof(Bytecode), mem_context, register_mem);

	/* constants */
	result->gc_dbl_const_nr = 0;
	result->gc_dbl_const_max = 16;
	result->gc_str_const_nr = 0;
	result->gc_str_const_max = 16;
	result->dbl_consts = (Value*)ecalloc(result->gc_dbl_const_max, sizeof(Value), mem_context, register_mem);
	result->str_consts = new string[result->gc_str_const_max];

	/* stack pointer */
	result->gc_dbl_stack_nr = 0;
	result->gc_dbl_stack_max = 0;
	result->gc_str_stack_nr = 0;
	result->gc_str_stack_max = 0;
	/* stack space is allocated after code generation */

	/* copy mask */
	result->gc_copy_nr = 0;
	result->copy_mask = (EElementId**)ecalloc(256, sizeof(EElementId*), mem_context, register_mem);
	result->copy_source = (EElementId**)ecalloc(256, sizeof(EElementId*), mem_context, register_mem);

	bytecode_generator generator(*result);

	/* a rules has no marker if the conversion failed                      */
	/* (from the format in the rule node to the format in the cube struct) */
	/* this happens if the left hand side has a unspecified dimension      */
	/* that does not appear as variable in PALO.MARKER()                   */
	cube->acube->checkNewMarkerRules();
	if (!result->arule->hasMarkers()) {
		result->marker_flag = 0;
	} else {
		result->marker_flag = 1;
	}

	/* rules with a marker are automatically N-Rules */
	if (result->marker_flag) {
		result->is_base = 1;
		result->is_cons = 0;
	}

	arule->genCode(generator);

	/* allocate after code generation */
	result->dbl_stack = (Value*)ecalloc(result->gc_dbl_stack_max + 2, sizeof(Value), mem_context, register_mem);
	result->str_stack = new string[result->gc_str_stack_max + 2]; //TODO: will leak here

#ifdef DEBUG_ENGINE
	/* printf("area for rule %d: ",result->nr_rule); */
	/* DebugArea( result->dest_area, env ); */
	/* printf("\n"); */
#endif

	return result;
}

void FreeRule(ERule * rule, EMemoryContext* mem_context)
{
	if (rule) {
		if (rule->bytecode) {
			efree(rule->bytecode, mem_context);
		}
		if (rule->dbl_consts) {
			efree(rule->dbl_consts, mem_context);
		}
		if (rule->str_consts) {
			delete[] rule->str_consts;
		}
		if (rule->copy_mask) {
			efree(rule->copy_mask, mem_context);
		}
		if (rule->copy_source) {
			efree(rule->copy_source, mem_context);
		}
		if (rule->dbl_stack) {
			efree(rule->dbl_stack, mem_context);
		}
		if (rule->str_stack) {
			delete[] rule->str_stack;
		}
		efree(rule, mem_context);
		NrCallocERule--;
	}
}

/****************************************************************************
 **

 *F  EngineCell(ptrValue,ptrStatus,apath,acube,auser)  . . entry to the engine
 **
 */
uint8_t EngineCellHasRule(Value * ptrValue, Status * ptrStatus, IdentifierType * apath, Cube * acube, User * auser, EMemoryContext* mem_context)
{
	ECube * cube;
	EElementId * path;
	Environment * env;
	ERule * rule;
	uint32_t d;
	clock_t clo;
	TRACE_INIT_TIME

	env = NewEntryEnvironment(auser, mem_context);

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s EngineCellHasRule %9.3f ",++env->tei," ",TRACE_REAL));
	DebugPath(apath,NewEntryCube(acube,auser),env);
#endif

	clo = clock();
	if (Logger::isTrace()) {
		Logger::trace << "EngineCellHasRule" << " called" << endl;
		/* DebugPath(apath,NewEntryCube(acube,auser),env); */
	}

	/* convert the area to the engine format                               */
	cube = NewEntryCube(acube, auser, mem_context);
	path = apath;

	/* check if consolidated                                               */
	for (d = 0; d < cube->nrDimensions; d++) {
		if (cube->dimensions[d]->type[path[d]] == 'C') {
			break;
		}
	}

	/* cell is consolidated                                                */
	if (d < cube->nrDimensions) {

		/* loop over the rules                                             */
		for (rule = (ERule*)FirstItemList(&(cube->rules_c)); rule != 0; rule = (ERule*)NextItemList(&(cube->rules_c),rule)) {
			if (IsPathInArea(rule->dest_area, path)) {
				/* rules that apply to cons cells should never have marker */
				/* even if the code does have a marker the following code  */
				/* will not crash, but simply return 0 for the marker      */
				Logger::trace << "compute with rule " << rule->nr_rule << endl;
				try {
					virtual_machine machine(env, mem_context, ptrValue, ptrStatus);
					machine.compute(path, rule, 0, 0, 0);

					//                    ComputeRule( ptrValue, ptrStatus, path, rule, 0, 0, env, 0, &stack, mem_context );
					Logger::trace << "EngineCellHasRule" << " done 1 " << (clock() - clo) << endl;
				} catch (ErrorException& e) {
					if ((e.getErrorType() == ErrorException::ERROR_RULE_HAS_CIRCULAR_REF) || (e.getErrorType() == ErrorException::ERROR_INTERNAL)) {
						//mask
						*ptrValue = rule->nr_rule;
						*ptrStatus = ErrorStatus + ErrorCircularRule;
					} else {
						throw e;
					}
				} TRACE_PRINTF((env->file,"%*s EngineCellHasRule returns 1 %4.3f %9.3f\n",env->tei--," ",TRACE_TIME,TRACE_REAL));
				FreeEnvironment(env, mem_context);
				return 1;
			}
		}

		/* no rule found, compute later                                    */
		Logger::trace << "EngineCellHasRule" << " done 0 " << (clock() - clo) << endl;
		TRACE_PRINTF((env->file,"%*s EngineCellHasRule returns 0 %4.3f %9.3f\n",env->tei--," ",TRACE_TIME,TRACE_REAL));
		FreeEnvironment(env, mem_context);
		return 0;

	}

	/* cell is base                                                        */
	else {

		/* not consolidated, compute later                                 */
		Logger::trace << "EngineCellHasRule" << " done 0 " << (clock() - clo) << endl;
		TRACE_PRINTF((env->file,"%*s EngineCellHasRule returns 0 %4.3f %9.3f\n",env->tei--," ",TRACE_TIME,TRACE_REAL));
		FreeEnvironment(env, mem_context);
		return 0;

	}

}

void EngineCell(Value * ptrValue, Status * ptrStatus, IdentifierType * apath, Cube * acube, User * auser, EMemoryContext* mem_context)
{
	ECube * cube;
	EElementId * path;
	Environment * env;
	ERule * rule;
	EPath srcPath;
	Value srcValue;
	Status srcStatus;
	Value * row;
	EArea * area;
	EBuffer * buf;
	uint32_t d;

	/* convert the area to the engine format                               */
	cube = NewEntryCube(acube, auser, mem_context);
	path = apath;
	env = NewEntryEnvironment(auser, mem_context);

	/* check if consolidated                                               */
	for (d = 0; d < cube->nrDimensions; d++) {
		if (cube->dimensions[d]->type[path[d]] == 'C') {
			break;
		}
	}

	/* cell is consolidated                                                */
	if (d < cube->nrDimensions) {

		/* loop over the rules                                             */
		for (rule = (ERule*)FirstItemList(&(cube->rules_c)); rule != 0; rule = (ERule*)NextItemList(&(cube->rules_c),rule)) {
			if (IsPathInArea(rule->dest_area, path)) {
				/* rules that apply to cons cells should never have marker */
				/* even if the code does have a marker the following code  */
				/* will not crash, but simply return 0 for the marker      */
				virtual_machine machine(env, mem_context, ptrValue, ptrStatus);
				machine.compute(path, rule, 0, 0, 0);

				//                ComputeRule( ptrValue, ptrStatus, path, rule, 0, 0, env, 0, &stack, mem_context );
				FreeEnvironment(env, mem_context);
				return;
			}
		}

		/* if no applicable rule is found, treat this as an area           */
		area = NewPathArea(cube, path, mem_context);
		buf = NewBuffer(1, mem_context);
		FillAreaBuffer(buf, area, env, mem_context);
		*ptrValue = buf->values[0];
		*ptrStatus = buf->status[0];
		FreeBuffer(buf, mem_context);
		FreeArea(area, mem_context);

	}

	/* cell is base                                                        */
	else {

		/* loop over the rules                                             */
		for (rule = (ERule*)FirstItemList(&(cube->rules_n)); rule != 0; rule = (ERule*)NextItemList(&(cube->rules_n),rule)) {
			if (IsBasePathInArea(rule->dest_area, path)) {
				if (rule->ubm_flag) {
					SrcPathRule(srcPath, rule, path);
					GetValue(&srcValue, &srcStatus, cube, srcPath, env, mem_context);

					virtual_machine machine(env, mem_context, ptrValue, ptrStatus);
					machine.compute(path, rule, srcValue, srcStatus, 1);

					//                    ComputeRule( ptrValue, ptrStatus, path, rule, srcValue, srcStatus, env, 1, &stack, mem_context );
					FreeEnvironment(env, mem_context);
					return;
				} else {
					virtual_machine machine(env, mem_context, ptrValue, ptrStatus);
					machine.compute(path, rule, 0, 0, 1);

					//                    ComputeRule( ptrValue, ptrStatus, path, rule, 0, 0, env, 1, &stack, mem_context );
					FreeEnvironment(env, mem_context);
					return;
				}
			}
		}

		/* if no applicable rule is found, try a base lookup               */
		row = (Value*)cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path);
		if (row) {
			*ptrValue = *row;
			*ptrStatus = BaseStatus;
		} else {
			*ptrValue = 0.0;
			*ptrStatus = 0;
		}

	}

	FreeEnvironment(env, mem_context);

}

uint8_t EngineAreaHasRule(vector<vector<Element*> > * aarea, Cube * acube, User * auser, EMemoryContext* mem_context)
{
	ECube * cube;
	EArea * area;
	ERule * rule;

	cube = NewEntryCube(acube, auser, mem_context);
	area = NewEntryArea(aarea, cube, mem_context);

	/* loop over the rules                                             */
	for (rule = (ERule*)FirstItemList(&(cube->rules_c)); rule != 0; rule = (ERule*)NextItemList(&(cube->rules_c),rule)) {
		if (!IsEmptyIntersectionArea(rule->dest_area, area)) {
			FreeArea(area, mem_context);
			return 1;
		}
	}

	FreeArea(area, mem_context);
	return 0;
}

/****************************************************************************
 **
 *F  Engine(storage,area,cube,user)  . . . . . . . . . . . entry to the engine
 **
 */
void Engine(HashAreaStorage * astorage, vector<vector<Element*> > * aarea, Cube * acube, User * auser, EMemoryContext* mem_context)
{
	ECube * cube;
	EArea * area;
	Environment * env;
	EBuffer * dst;
	uint32_t size;
	uint32_t sizes[MAX_DIMENSIONS];
	EElementId * eids[MAX_DIMENSIONS];
	Offset idxs[MAX_DIMENSIONS];
	EPath path;
	uint32_t dim;
	EElementId * e;
	uint32_t i;
	Offset pos;
	clock_t clo;

	uint32_t nrCallocBases = NrCallocBases;
	uint32_t nrCallocConss = NrCallocConss;
	uint32_t nrCallocEArea = NrCallocEArea;
	uint32_t nrCallocEBorder = NrCallocEBorder;
	uint32_t nrCallocEBuffer = NrCallocEBuffer;
	uint32_t nrCallocECube = NrCallocECube;
	uint32_t nrCallocECubeMap = NrCallocECubeMap;
	uint32_t nrCallocEDimension = NrCallocEDimension;
	uint32_t nrCallocEDimMap = NrCallocEDimMap;
	uint32_t nrCallocEids = NrCallocEids;
	uint32_t nrCallocEnvironment = NrCallocEnvironment;
	uint32_t nrCallocEows = NrCallocEows;
	uint32_t nrCallocERule = NrCallocERule;
	uint32_t nrCallocIsCons = NrCallocIsCons;
	uint32_t nrCallocRaws = NrCallocRaws;
	uint32_t nrCallocStatus = NrCallocStatus;
	uint32_t nrCallocValues = NrCallocValues;
	TRACE_INIT_TIME

	env = NewEntryEnvironment(auser, mem_context);

#ifdef TRACE_ENGINE
	TRACE_PRINTF((env->file,"%*s Engine ",++env->tei," "));
	TRACE_PRINTF((env->file,"%9.3f\n",TRACE_REAL));
#endif

	clo = clock();
	if (Logger::isTrace()) {
		Logger::trace << "Engine" << " called" << endl;
	}

	/* convert the area to the engine format                               */
	cube = NewEntryCube(acube, auser, mem_context);
	area = NewEntryArea(aarea, cube, mem_context);

	/* copy the element ids for easier access (this should go away)        */
	size = 1;
	for (dim = 0; dim < area->cube->nrDimensions; dim++) {
		sizes[dim] = area->borders[dim]->size;
		size = size * sizes[dim];
		eids[dim] = (EElementId*)ecalloc(sizes[dim], sizeof(EElementId), mem_context);
		NrCallocEids++;
		e = eids[dim];
		for (i = 0; i < area->borders[dim]->nrCons; i++) {
			*e++ = area->borders[dim]->conss[i];
		}
		for (i = 0; i < area->borders[dim]->nrBase; i++) {
			*e++ = area->borders[dim]->bases[i];
		}
	}

	if (Logger::isTrace()) {
		Logger::trace << "Engine" << " size is " << size << endl;
	}

	/* allocate the new buffer                                             */
	dst = NewBuffer(size, mem_context);

	/*                                                                     */
	FillAreaBuffer(dst, area, env, mem_context);

	/* and enumerate the elements                                          */
	for (dim = 0; dim < area->cube->nrDimensions; dim++) {
		idxs[dim] = 0;
		path[dim] = eids[dim][idxs[dim]];
	}
	for (pos = 0; pos < size; pos++) {
		if (dst->values[pos] != 0 || IsRuleStatus( dst->status[pos] )) {
			astorage->setValue(path, dst->values[pos], dst->status[pos]);
		}
		/* it may be a good idea to compute the path only if needed */
		if (pos < size - 1) {
			for (dim = 0; idxs[dim] == sizes[dim] - 1; dim++) {
				idxs[dim] = 0;
				path[dim] = eids[dim][idxs[dim]];
			}
			idxs[dim]++;
			path[dim] = eids[dim][idxs[dim]];
		}
	}

	/* free the stuff                                                      */
	FreeBuffer(dst, mem_context);
	for (dim = 0; dim < area->cube->nrDimensions; dim++) {
		efree(eids[dim], mem_context);
		NrCallocEids--;
	}
	FreeArea(area, mem_context);

	if (Logger::isTrace()) {
		if (nrCallocBases < NrCallocBases) {
			Logger::trace << "loose nrCallocBases " << NrCallocBases - nrCallocBases << endl;
		}
		if (nrCallocBases > NrCallocBases) {
			Logger::trace << "gain nrCallocBases " << nrCallocBases - NrCallocBases << endl;
		}
		if (nrCallocConss < NrCallocConss) {
			Logger::trace << "loose nrCallocConss " << NrCallocConss - nrCallocConss << endl;
		}
		if (nrCallocConss > NrCallocConss) {
			Logger::trace << "gain nrCallocConss " << nrCallocConss - NrCallocConss << endl;
		}
		if (nrCallocEArea < NrCallocEArea) {
			Logger::trace << "loose nrCallocEArea " << NrCallocEArea - nrCallocEArea << endl;
		}
		if (nrCallocEArea > NrCallocEArea) {
			Logger::trace << "gain nrCallocEArea " << nrCallocEArea - NrCallocEArea << endl;
		}
		if (nrCallocEBorder < NrCallocEBorder) {
			Logger::trace << "loose nrCallocEBorder " << NrCallocEBorder - nrCallocEBorder << endl;
		}
		if (nrCallocEBorder > NrCallocEBorder) {
			Logger::trace << "gain nrCallocEBorder " << nrCallocEBorder - NrCallocEBorder << endl;
		}
		if (nrCallocEBuffer < NrCallocEBuffer) {
			Logger::trace << "loose nrCallocEBuffer " << NrCallocEBuffer - nrCallocEBuffer << endl;
		}
		if (nrCallocEBuffer > NrCallocEBuffer) {
			Logger::trace << "gain nrCallocEBuffer " << nrCallocEBuffer - NrCallocEBuffer << endl;
		}
		if (nrCallocECube < NrCallocECube) {
			Logger::trace << "loose nrCallocECube " << NrCallocECube - nrCallocECube << endl;
		}
		if (nrCallocECube > NrCallocECube) {
			Logger::trace << "gain nrCallocECube " << nrCallocECube - NrCallocECube << endl;
		}
		if (nrCallocECubeMap < NrCallocECubeMap) {
			Logger::trace << "loose nrCallocECubeMap " << NrCallocECubeMap - nrCallocECubeMap << endl;
		}
		if (nrCallocECubeMap > NrCallocECubeMap) {
			Logger::trace << "gain nrCallocECubeMap " << nrCallocECubeMap - NrCallocECubeMap << endl;
		}
		if (nrCallocEDimension < NrCallocEDimension) {
			Logger::trace << "loose nrCallocEDimension " << NrCallocEDimension - nrCallocEDimension << endl;
		}
		if (nrCallocEDimension > NrCallocEDimension) {
			Logger::trace << "gain nrCallocEDimension " << nrCallocEDimension - NrCallocEDimension << endl;
		}
		if (nrCallocEDimMap < NrCallocEDimMap) {
			Logger::trace << "loose nrCallocEDimMap " << NrCallocEDimMap - nrCallocEDimMap << endl;
		}
		if (nrCallocEDimMap > NrCallocEDimMap) {
			Logger::trace << "gain nrCallocEDimMap " << nrCallocEDimMap - NrCallocEDimMap << endl;
		}
		if (nrCallocEids < NrCallocEids) {
			Logger::trace << "loose nrCallocEids " << NrCallocEids - nrCallocEids << endl;
		}
		if (nrCallocEids > NrCallocEids) {
			Logger::trace << "gain nrCallocEids " << nrCallocEids - NrCallocEids << endl;
		}
		if (nrCallocEnvironment < NrCallocEnvironment) {
			Logger::trace << "loose nrCallocEnvironment " << NrCallocEnvironment - nrCallocEnvironment << endl;
		}
		if (nrCallocEnvironment > NrCallocEnvironment) {
			Logger::trace << "gain nrCallocEnvironment " << nrCallocEnvironment - NrCallocEnvironment << endl;
		}
		if (nrCallocEows < NrCallocEows) {
			Logger::trace << "loose nrCallocEows " << NrCallocEows - nrCallocEows << endl;
		}
		if (nrCallocEows > NrCallocEows) {
			Logger::trace << "gain nrCallocEows " << nrCallocEows - NrCallocEows << endl;
		}
		if (nrCallocERule < NrCallocERule) {
			Logger::trace << "loose nrCallocERule " << NrCallocERule - nrCallocERule << endl;
		}
		if (nrCallocERule > NrCallocERule) {
			Logger::trace << "gain nrCallocERule " << nrCallocERule - NrCallocERule << endl;
		}
		if (nrCallocIsCons < NrCallocIsCons) {
			Logger::trace << "loose nrCallocIsCons " << NrCallocIsCons - nrCallocIsCons << endl;
		}
		if (nrCallocIsCons > NrCallocIsCons) {
			Logger::trace << "gain nrCallocIsCons " << nrCallocIsCons - NrCallocIsCons << endl;
		}
		if (nrCallocRaws < NrCallocRaws) {
			Logger::trace << "loose nrCallocRaws " << NrCallocRaws - nrCallocRaws << endl;
		}
		if (nrCallocRaws > NrCallocRaws) {
			Logger::trace << "gain nrCallocRaws " << nrCallocRaws - NrCallocRaws << endl;
		}
		if (nrCallocStatus < NrCallocStatus) {
			Logger::trace << "loose nrCallocStatus " << NrCallocStatus - nrCallocStatus << endl;
		}
		if (nrCallocStatus > NrCallocStatus) {
			Logger::trace << "gain nrCallocStatus " << nrCallocStatus - NrCallocStatus << endl;
		}
		if (nrCallocValues < NrCallocValues) {
			Logger::trace << "loose nrCallocValues " << NrCallocValues - nrCallocValues << endl;
		}
		if (nrCallocValues > NrCallocValues) {
			Logger::trace << "gain nrCallocValues " << nrCallocValues - NrCallocValues << endl;
		}

		Logger::trace << "Engine" << " done " << (clock() - clo) << endl;
		Logger::trace << "NrEnumBaseAction " << NrEnumBaseAction1 << " " << NrEnumBaseAction100 << " " << NrEnumBaseActionLarge << endl;
	}

	TRACE_PRINTF((env->file,"%*s Engine returns %4.3f %9.3f\n",env->tei--," ",TRACE_TIME,TRACE_REAL));

	FreeEnvironment(env, mem_context);
}

}

/****************************************************************************
 **

 *E  Engine.c  . . . . . . . . . . . . . . . . . . . . . . . . . . . ends here
 **
 */
