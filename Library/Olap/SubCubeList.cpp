////////////////////////////////////////////////////////////////////////////////
/// @brief palo subcube list
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Ladislav Morva, qBicon s.r.o., Prague, Czech Republic
////////////////////////////////////////////////////////////////////////////////

#include "palo.h"
#include "Olap/SubCubeList.h"

namespace palo {

SubCubeList::SubCubeList()
{
	reset();
}

SubCubeList::~SubCubeList()
{
}

RSubCube SubCubeList::next()
{
	list<RSubCube>::iterator it = iter;
	iter++; 
	return *it;
}

void SubCubeList::addCell(const IdentifiersType *cell)
{
	bool merge = false;
	for (list<RSubCube>::iterator it = subCubes.begin(); it != subCubes.end(); it++) {
		merge = isJoinable(*it, cell);
		if (merge) {
			break;
		}
	}
	//create a new subCube from cell;
	RSubCube cube = createSubCube(cell);
	//add it to the list of subCubes
	subCubes.push_front(cube);
	if (merge) {
		//recursively merge cubes;
		mergeSubCubes(subCubes, subCubes.begin());
	}
}

bool SubCubeList::isJoinable(RSubCube cube, const IdentifiersType *cell)
{
	size_t diff = 0;
	size_t i = 0;
	for (vector<RSetID>::iterator it = cube->begin(); it != cube->end(); i++, it++) {
		if ((*it)->size() > 1) {
			diff++;
		} else {
			if (*(*it)->begin() != (*cell)[i]) {
				diff++;
			}
		}
		if (diff > 1) {
			return false; // there are at least two differences
		}
	}
	return true;
}

bool SubCubeList::isJoinable(RSubCube cube1, RSubCube cube2)
{
	size_t diff = 0;
	vector<RSetID>::iterator it1;
	vector<RSetID>::iterator it2;
	for (it1 = cube1->begin(), it2 = cube2->begin(); it1 != cube1->end(); it1++, it2++) {
		if (**it1 != **it2) {
			diff++;
		}
		if (diff > 1) {
			return false; // there are at least two differences
		}
	}
	return true;
}

RSubCube SubCubeList::createSubCube(const IdentifiersType *cell)
{
	RSubCube cube(new vector<RSetID>(cell->size()));

	size_t size = cell->size();
	for (size_t i = 0; i < size; i++) {
		RSetID set(new SetID);
		set->insert((*cell)[i]);
		(*cube)[i] = set;
	}
	return cube;
}

RSubCube SubCubeList::joinSubCubes(const RSubCube cube1, const RSubCube cube2)
{
	RSubCube newCube(new vector<RSetID>(cube1->size()));

	vector<RSetID>::iterator it1;
	vector<RSetID>::iterator it2;
	size_t i = 0;
	for (it1 = cube1->begin(), it2 = cube2->begin(); it1 != cube1->end(); i++, it1++, it2++) {
		if (**it1 == **it2) {
			(*newCube)[i] = *it1;
		} else {
			//insert IDs from the shorter set to the copy of the longer set
			SetID &shortSet = (*it1)->size() < (*it2)->size() ? **it1 : **it2;
			SetID &longSet = (*it1)->size() < (*it2)->size() ? **it2 : **it1;
			RSetID newSet(new SetID(longSet)); //copy constructor

			for (SetID::iterator sit = shortSet.begin(); sit != shortSet.end(); sit++) {
				newSet->insert(*sit);
			}
			(*newCube)[i] = newSet;
		}
	}
	return newCube;
}

void SubCubeList::mergeSubCubes(list<RSubCube> &subCubes, list<RSubCube>::iterator sit)
{
	if (subCubes.size() < 2) {
		return;
	}
	for (list<RSubCube>::iterator lit = subCubes.begin(); lit != subCubes.end(); lit++) {
		if (lit != sit) {
			if (isJoinable(*lit, *sit)) {
				subCubes.push_front(joinSubCubes(*lit, *sit));
				subCubes.erase(sit);
				subCubes.erase(lit);
				mergeSubCubes(subCubes, subCubes.begin());
				break;
			}
		}
	}
}

}
