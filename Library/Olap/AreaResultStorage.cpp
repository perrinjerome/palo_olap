////////////////////////////////////////////////////////////////////////////////
/// @brief palo area result storage
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "Olap/AreaResultStorage.h"

#include "Logger/Logger.h"
#include "InputOutput/Statistics.h"

#include "Olap/CellPath.h"
#include "Olap/Cube.h"
#include "Olap/CubeIndex.h"
#include "Olap/Dimension.h"
#include "Olap/Engine.h"
#include "Olap/Rule.h"

#include <iostream>

namespace palo {

bool AreaResultStorage::computeAreaParameters(Cube* cube, vector<vector<Element*> >& area, vector<map<Element*, uint32_t> >& hashMapping, vector<vector<vector<pair<uint32_t, double> > > >& numericMapping, vector<uint32_t>& hashSteps, vector<uint32_t>& lengths)
{

	const vector<Dimension*> * dimensions = cube->getDimensions();
	size_t numDims = dimensions->size();

	hashMapping.resize(numDims);
	numericMapping.resize(numDims);
	hashSteps.resize(numDims);
	lengths.resize(numDims);

	uint32_t step = 1;
	for (size_t i = 0; i < numDims; i++) {

		// compute hashMapping
		// mapping from IdentifierType -> position in AreaResultStorage
		map<Element*, uint32_t>& dimMapping = hashMapping[i];
		uint32_t id = 0;

		for (vector<Element*>::const_iterator ii = area[i].begin(); ii != area[i].end(); ii++) {
			dimMapping.insert(make_pair(*ii, id));
			id += step;
		}

		hashSteps[i] = (uint32_t)step;
		lengths[i] = (uint32_t)dimMapping.size();
		step *= (uint32_t)dimMapping.size();

		// compute numericMapping
		// mapping from base element IdentifierType to consolidation position in AreaResultStorage
		Dimension* dimension = dimensions->at(i);

		vector<vector<pair<uint32_t, double> > >& dimElements = numericMapping[i];
		dimElements.resize(dimension->getMaximalIdentifier() + 1);

		map<Element*, uint32_t>::iterator elementIter = dimMapping.begin();
		for (; elementIter != dimMapping.end(); elementIter++) {
			uint32_t position = elementIter->second;
			Element* parent = elementIter->first;

			const map<IdentifierType, double>* childWeightMap = parent->getBaseElements(dimension);
			map<IdentifierType, double>::const_iterator childWeightIter = childWeightMap->begin();
			for (; childWeightIter != childWeightMap->end(); childWeightIter++) {
				dimElements[childWeightIter->first].push_back(make_pair(position, childWeightIter->second));
			}

		}

	}

	return (step > 0);
}

bool AreaResultStorage::computeAreaParameters(Cube* cube, vector<vector<Element*> >& area, vector<map<Element*, uint32_t> >& hashMapping)
{

	const vector<Dimension*> * dimensions = cube->getDimensions();
	size_t numDims = dimensions->size();

	hashMapping.resize(numDims);

	uint32_t step = 1;
	for (size_t i = 0; i < numDims; i++) {

		// compute hashMapping
		// mapping from IdentifierType -> position in AreaResultStorage
		map<Element*, uint32_t>& dimMapping = hashMapping[i];
		uint32_t id = 0;

		for (vector<Element*>::const_iterator ii = area[i].begin(); ii != area[i].end(); ii++) {
			dimMapping.insert(make_pair(*ii, id));
			id += step;
		}

		step *= (uint32_t)dimMapping.size();
	}

	return (step > 0);
}

AreaResultStorage::AreaResultStorage(const vector<map<Element*, uint32_t> >& hashMapping) :
	hashMapping(hashMapping)
{

	// compute the number of elements
	areaSize = 1;

	idMapping.resize(hashMapping.size());
	size_t pos = 0;

	for (vector<map<Element*, uint32_t> >::const_iterator i = hashMapping.begin(); i != hashMapping.end(); i++) {
		areaSize *= (*i).size();

		map<IdentifierType, uint32_t>& iMap = idMapping.at(pos++);
		for (map<Element*, uint32_t>::const_iterator j = i->begin(); j != i->end(); j++) {
			iMap[j->first->getIdentifier()] = j->second;
		}
	}

	status = new uint16_t[areaSize];
	memset(status, 0, sizeof(uint16_t) * areaSize);

	length = idMapping.size();
}

unsigned int AreaResultStorage::getErrorType(uint16_t * stat)
{
	uint16_t errorCode = ((*stat) & 0x0FC0);

	// used error codes
	switch (errorCode) {
	case (ErrorDatabaseNotFound):
		return ErrorException::ERROR_DATABASE_NOT_FOUND;
	case (ErrorCubeNotFound):
		return ErrorException::ERROR_CUBE_NOT_FOUND;
	case (ErrorElementNotFound):
		return ErrorException::ERROR_ELEMENT_NOT_FOUND;
	case (ErrorInvalidCoordinates):
		return ErrorException::ERROR_INVALID_COORDINATES;
	case (ErrorCircularRule):
		return ErrorException::ERROR_RULE_HAS_CIRCULAR_REF;
	case (ErrorDivisionByZero):
		return ErrorException::ERROR_RULE_DIVISION_BY_ZERO;
	default:
		return 0;
	}
}

uint16_t AreaResultStorage::setErrorCode(ErrorException::ErrorType errorType)
{
	// used error codes
	switch (errorType) {
	case (ErrorException::ERROR_DATABASE_NOT_FOUND):
		return ErrorDatabaseNotFound;
	case (ErrorException::ERROR_CUBE_NOT_FOUND):
		return ErrorCubeNotFound;
	case (ErrorException::ERROR_ELEMENT_NOT_FOUND):
		return ErrorElementNotFound;
	case (ErrorException::ERROR_INVALID_COORDINATES):
		return ErrorInvalidCoordinates;
	case (ErrorException::ERROR_RULE_HAS_CIRCULAR_REF):
		return ErrorCircularRule;
	case (ErrorException::ERROR_RULE_DIVISION_BY_ZERO):
		return ErrorDivisionByZero;
	default:
		return ErrorDatabaseNotFound;
	}
}

void AreaResultStorage::setErrorValue(const IdentifiersType* path, ErrorException::ErrorType errorType)
{
	size_t pos = getPos(path);

	uint16_t * stat = status + pos;

	(*stat) = ErrorStatus | setErrorCode(errorType);
}

bool AreaResultStorage::containsPath(const IdentifiersType* path)
{
	for (size_t i = 0; i < length; i++) {
		map<IdentifierType, uint32_t>::iterator find = idMapping[i].find(path->at(i));
		if (find == idMapping[i].end()) {
			return false;
		}
	}
	return true;
}

}
