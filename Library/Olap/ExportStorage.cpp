////////////////////////////////////////////////////////////////////////////////
/// @brief palo export storage
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "Olap/ExportStorage.h"

#include "InputOutput/Condition.h"
#include "Logger/Logger.h"
#include "InputOutput/Statistics.h"

#include "Olap/CellPath.h"
#include "Olap/CubeIndex.h"
#include "Olap/Dimension.h"
#include "Olap/Rule.h"
#include "Olap/CubeStorage.h"

#include <iostream>

namespace palo {

ExportStorage::ExportStorage(const vector<Dimension*>* dimensions, vector<IdentifiersType>* area, size_t blocksize) :
	numberElements(0), blocksize(blocksize)
{

	valueSize = sizeof(Cube::CellValueType*);
	maxima.clear();

	size_t totalNumberBits = 0;

	for (vector<Dimension*>::const_iterator i = dimensions->begin(); i != dimensions->end(); i++) {
		uint32_t bitsDimension = 32;
		uint32_t maximum = ~0;

		maxima.push_back(maximum);
		totalNumberBits += bitsDimension;
	}

	keySize = ((totalNumberBits + 31) / 32) * 4; // normalise to 32 bit, convert to byte
	elementSize = keySize + valueSize;

	//Logger::trace << "creating new ExportStorage: key size = " << keySize
	//              << ", value size = " <<  valueSize << endl;

	tmpKeyBuffer = new uint8_t[keySize];
	tmpElementBuffer = new uint8_t[elementSize];

	// generate index for base elements
	index = new CubeIndex(keySize, valueSize);

	try {
		page = new ExportPage(index, keySize, valueSize, blocksize);
	} catch (ErrorException e) {
		delete index;
		delete tmpKeyBuffer;
		delete tmpElementBuffer;
		if (blocksize > 10000 && e.getErrorType() == ErrorException::ERROR_OUT_OF_MEMORY) {
			throw ErrorException(ErrorException::ERROR_OUT_OF_MEMORY, "reduce the blocksize");
		}
		throw e;
	}

	firstPath.resize(dimensions->size());
	lastPath.resize(dimensions->size());
	setArea(area);

	lastCheckedElement = 0;

	for (vector<IdentifiersType>::iterator i = area->begin(); i != area->end(); i++) {
		set<IdentifierType> s;
		for (IdentifiersType::iterator j = i->begin(); j != i->end(); j++) {
			s.insert(*j);
		}
		areaSets.push_back(s);
	}

}

ExportStorage::~ExportStorage()
{
	delete[] tmpKeyBuffer;
	delete[] tmpElementBuffer;

	size_t size;
	ExportPage::element_t const * array = index->getArray(size);

	for (size_t i = 0; i < size; array++) {
		Cube::CellValueType ** ptr = (Cube::CellValueType**)(*array);
		if (ptr != 0) {
			delete *ptr;
			i++;
		}
	}

	delete page;
	delete index;
}

void ExportStorage::setArea(vector<IdentifiersType>* area)
{
	this->area = area;

	areaLength = area->size();

	for (size_t i = 0; i < firstPath.size(); i++) {
		if (!area->at(i).empty()) {
			firstPath[i] = *(area->at(i).begin());
			lastPath[i] = *(--(area->at(i).end()));
		}
	}

	end_iterators.clear();
	begin_iterators.clear();
	crt_iterators.clear();

	crt_path.clear();

	size_t idx = 0;
	for (vector<IdentifiersType>::iterator i = area->begin(), e = area->end(); i != e; i++) {
		end_iterators.push_back(i->end());
		begin_iterators.push_back(i->begin());
		crt_iterators.push_back(i->begin());
		crt_path.push_back(*(i->begin()));
		crt_raw_path[idx++] = *(i->begin());
	}
}

void ExportStorage::print()
{
	ExportPage::buffer_t ptr = page->begin();
	ExportPage::buffer_t end = page->end();

	cout << "blocksize = " << blocksize << endl;
	cout << "numberElements = " << numberElements << endl;

	cout << "first path <";
	for (IdentifiersType::iterator i = firstPath.begin(); i != firstPath.end(); i++) {
		cout << " " << *i;
	}
	cout << " >" << endl;

	cout << "last path <";
	for (IdentifiersType::iterator i = lastPath.begin(); i != lastPath.end(); i++) {
		cout << " " << *i;
	}
	cout << " >" << endl;

	for (; ptr < end; ptr = page->next(ptr)) {

		IdentifiersType path(maxima.size());
		fillPath(ptr, &path);

		cout << "Element <";
		for (IdentifiersType::iterator i = path.begin(); i != path.end(); i++) {
			cout << " " << *i;
		}
		cout << " > = ";

		Cube::CellValueType* * value = (Cube::CellValueType* *)ptr;
		if (value) {
			switch ((*value)->type) {
			case (NUMERIC):
				cout << (*value)->doubleValue << endl;
				break;
			case (STRING):
				cout << (*value)->charValue << endl;
				break;
			default:
				cout << "unknown" << endl;
			}
		} else {
			cout << "not found" << endl;
		}
	}

}

void ExportStorage::addDoubleValue(uint8_t * keyBuffer, double value)
{
	if (hasBlocksizeElements())
		return;

	Cube::CellValueType* *v = (Cube::CellValueType* *)index->lookupKey(keyBuffer);

	if (v) {
		(*v)->type = NUMERIC;
		(*v)->doubleValue += value;
	} else {
		Cube::CellValueType* newValue = new Cube::CellValueType();
		newValue->type = NUMERIC;
		newValue->doubleValue = value;

		memcpy(tmpElementBuffer, (ExportPage::value_t)&newValue, valueSize);

		CubePage::key_t tmpKeyBuffer = tmpElementBuffer + valueSize;
		memcpy(tmpKeyBuffer, keyBuffer, keySize);

		addNewCell(tmpElementBuffer);
	}
}

void ExportStorage::addStringValue(uint8_t * keyBuffer, string& value)
{
	if (hasBlocksizeElements())
		return;

	Cube::CellValueType* *v = (Cube::CellValueType* *)index->lookupKey(keyBuffer);

	if (v) {
		(*v)->type = STRING;
		(*v)->charValue = value;
	} else {
		Cube::CellValueType* newValue = new Cube::CellValueType();
		newValue->type = STRING;
		newValue->charValue = value;

		memcpy(tmpElementBuffer, (ExportPage::value_t)&newValue, valueSize);

		CubePage::key_t tmpKeyBuffer = tmpElementBuffer + valueSize;
		memcpy(tmpKeyBuffer, keyBuffer, keySize);

		addNewCell(tmpElementBuffer);
	}
}

Cube::CellValueType* ExportStorage::getCell(size_t pos)
{
	ExportPage::buffer_t ptr = page->getCell(pos);
	if (ptr) {
		return *(Cube::CellValueType* *)ptr;
	} else {
		Logger::error << "ExportStorage::getCell internal error: value not found" << endl;
		return 0;
	}
}

Cube::CellValueType* ExportStorage::getCell(size_t pos, IdentifiersType* path)
{
	ExportPage::buffer_t ptr = page->getCell(pos);
	if (ptr) {
		fillPath(ptr, path);
		return *(Cube::CellValueType* *)ptr;
	} else {
		Logger::error << "ExportStorage::getCell internal error: value not found" << endl;
		return 0;
	}
}

void ExportStorage::removeCell(size_t pos)
{
	ExportPage::buffer_t element = page->getCell(pos);
	if (element) {
		// delete old value
		delete *(Cube::CellValueType**)element;

		page->removeElement(element);
		numberElements--;
	} else {
		Logger::error << "ExportStorage::removeCell internal error: value not found" << endl;
	}
}

bool ExportStorage::setStartPath(const IdentifiersType* path)
{
	size_t max = area->size();

	vector<IdentifiersType::iterator> iv(area->size());

	// wrong number of elements
	if (path->size() != iv.size()) {
		return false;
	}

	// is the given cell path an element of the area?
	for (size_t i = 0; i < max; i++) {
		iv[i] = find(area->at(i).begin(), area->at(i).end(), path->at(i));

		// path not found in area
		if (iv[i] == area->at(i).end()) {
			return false;
		}
	}

	IdentifiersType newPath(*path);

	size_t pos = max - 1;

	// set the crt iterators to point on the first valid path
	for (size_t i = 0; i < path->size(); i++) {
		crt_iterators[i] = begin_iterators[i];
		while (*(crt_iterators[i]) != path->at(i)) {
			crt_iterators[i]++;
		}

		crt_path[i] = *(crt_iterators[i]);
		crt_raw_path[i] = *(crt_iterators[i]);
	}

	advanceIterators();

	while (pos >= 0 && pos < path->size()) {
		iv[pos]++;

		if (iv[pos] != area->at(pos).end()) {
			newPath[pos] = *(iv[pos]);
			firstPath = newPath;

			return true;
		}

		// error in new start path!
		if (pos == 0) {
			return false;
		}

		newPath[pos] = *(area->at(pos).begin());

		// next dimension
		pos--;
	}

	return false;
}

void ExportStorage::advanceIterators()
{
	for (int i = (int)areaLength - 1; i >= 0; i--) {
		crt_iterators[i]++;

		if (crt_iterators[i] == end_iterators[i]) {
			crt_iterators[i] = begin_iterators[i];
			crt_path[i] = *(crt_iterators[i]);
			crt_raw_path[i] = crt_path[i];
		} else {
			crt_path[i] = *(crt_iterators[i]);
			crt_raw_path[i] = crt_path[i];
			break;
		}
	}
}

void ExportStorage::advanceIterators(const IdentifiersType& target)
{
	//don't call this with invalid paths
	for (size_t i = 0; i < areaLength; i++) {
		crt_iterators[i] = begin_iterators[i];
		while (*(crt_iterators[i]) != target[i]) {
			crt_iterators[i]++;
		}
		crt_path[i] = *(crt_iterators[i]);
	}
}

void ExportStorage::advanceIterators(IdentifierType* target)
{
	//don't call this with invalid paths
	for (size_t i = 0; i < areaLength; i++) {
		crt_iterators[i] = begin_iterators[i];
		while (*(crt_iterators[i]) != target[i]) {
			crt_iterators[i]++;
		}
		crt_path[i] = *(crt_iterators[i]);
	}
}

bool ExportStorage::isInStorageArea(ExportPage::key_t keyBuffer)
{
	IdentifierType * buffer = (IdentifierType*)keyBuffer;
	size_t max = firstPath.size();

	for (size_t i = 0; i < max; i++) {
		set<IdentifierType>::iterator find = (areaSets[i]).find(*buffer++);

		if (find == (areaSets[i]).end()) {
			return false;
		}
	}

	buffer = (IdentifierType*)keyBuffer;
	IdentifiersType::const_iterator firstPathIter = firstPath.begin();

	for (; firstPathIter != firstPath.end(); firstPathIter++) {
		if (*buffer < *firstPathIter) {
			return false;
		} else if (*buffer++ > *firstPathIter) {
			break;
		}
	}

	buffer = (IdentifierType*)keyBuffer;
	IdentifiersType::const_iterator lastPathIter = lastPath.begin();
	for (; lastPathIter != lastPath.end(); lastPathIter++) {
		if (*buffer < *lastPathIter) {
			break;
		} else if (*buffer++ > *lastPathIter) {
			return false;
		}
	}

	return true;

}

bool ExportStorage::isInStorageArea(ExportPage::key_t keyBuffer, size_t& errorPos)
{
	IdentifierType * buffer = (IdentifierType*)keyBuffer;
	size_t max = firstPath.size();

	for (size_t i = 0; i < max; i++) {
		set<IdentifierType>::iterator find = (areaSets[i]).find(*buffer++);

		if (find == (areaSets[i]).end()) {
			errorPos = i;
			return false;
		}
	}

	buffer = (IdentifierType*)keyBuffer;

	for (size_t i = 0; i < max; i++) {
		if (*buffer < firstPath[i]) {
			errorPos = i;
			return false;
		} else if (*buffer++ > firstPath[i]) {
			break;
		}
	}

	buffer = (IdentifierType*)keyBuffer;
	for (size_t i = 0; i < max; i++) {
		if (*buffer < lastPath[i]) {
			break;
		} else if (*buffer++ > lastPath[i]) {
			errorPos = i;
			return false;
		}
	}

	return true;
}

bool ExportStorage::isInStorageArea(IdentifierType id1)
{
	set<IdentifierType>::iterator find = (areaSets[0]).find(id1);

	if (find == (areaSets[0]).end()) {
		return false;
	}

	if (firstPath.size() > 0) {
		if (id1 < firstPath[0] || id1 > lastPath[0])
			return false;
	}

	return true;
}

bool ExportStorage::isInStorageArea(IdentifierType id1, IdentifierType id2)
{

	set<IdentifierType>::iterator find = (areaSets[0]).find(id1);
	if (find == (areaSets[0]).end()) {
		return false;
	}

	find = (areaSets[1]).find(id2);
	if (find == (areaSets[1]).end()) {
		return false;
	}

	if (firstPath.size() > 1) {
		if (id1 < firstPath[0] || id1 > lastPath[0])
			return false;

		if (id1 == firstPath[0] && id2 < firstPath[1])
			return false;

		if (id1 == lastPath[0] && id2 > lastPath[1])
			return false;
	}

	return true;
}

void ExportStorage::updateLastPath(ExportPage::key_t keyBuffer)
{
	IdentifierType * buffer = (IdentifierType*)keyBuffer;

	for (size_t i = 0; i < lastPath.size(); i++) {
		lastPath[i] = *(buffer++);
	}
}

void ExportStorage::addNewCell(uint8_t * elementBuffer)
{
	if (page->isLess(page->getLastElement(), elementBuffer)) {
		updateLastPath((ExportPage::key_t)elementBuffer + valueSize);
	}

	page->addElement(elementBuffer);
	numberElements++;
}

double ExportStorage::getAreaSize()
{
	double result = 1.0;
	size_t max = area->size();

	for (size_t i = 0; i < max; i++) {
		result *= area->at(i).size();
	}

	return result;
}

double ExportStorage::getPosNumber()
{
	if (numberElements == 0 || numberElements < blocksize) {
		return getAreaSize();
	}

	size_t pos = size() - 1;
	IdentifiersType path(area->size());

	// sets the path
	getCell(pos, &path);

	double result = 1.0;
	vector<double> m(path.size());

	size_t max = area->size();

	for (size_t i = max; i > 0; i--) {
		m[i - 1] = result;
		result *= area->at(i - 1).size();
	}

	result = 1.0;

	for (size_t i = 0; i < max; i++) {

		for (size_t j = 0; j < area->at(i).size(); j++) {
			if (path[i] == area->at(i)[j]) {
				result += j * m[i];
			}
		}
	}

	return result;
}

// get a direct match for an enterprise rule
static inline bool findDirectRuleMatch(Cube* cube, const vector<Rule*>& rules, IdentifierType* path, Cube::CellValueType& cellValue, EMemoryContext* mem_context)
{

	set<pair<Rule*, IdentifiersType> > ruleHistory;

	for (vector<Rule*>::const_iterator iter = rules.begin(); iter != rules.end(); iter++) {
		Rule* rule = *iter;

		if (rule == 0 || !rule->isActive())
			continue;

		if (!rule->within(path))
			continue;

		if (rule->isRestrictedRule() && !rule->withinRestricted(path)) {
			Logger::trace << "encountered STET restriction" << endl;
			break;
		}

		bool skipAllRules = false;
		bool skipRule = false;

		CellPath cellPath(cube, path);

		Cube::CellValueType value;
		if (rule->hasMarkers()) {
			if (cellPath.getPathType() == CONSOLIDATED) {
				//no markers for consolidations
				skipRule = true;
			} else {
				bool found;
				value = cube->getCellValueNew(&cellPath, &found, &ruleHistory, mem_context);
				skipRule = !found;
			}
		} else {
			value = rule->getValue(&cellPath, &skipRule, &skipAllRules, mem_context);
		}
		if (skipAllRules)
			break;
		if (skipRule)
			continue; //try next rule

		cellValue.type = value.type;
		cellValue.rule = value.rule;

		if (value.type == NUMERIC) {
			cellValue.doubleValue = value.doubleValue;
		} else {
			cellValue.charValue.assign(value.charValue);
		}

		return true;
	}
	return false;
}

// get a direct match for an enterprise rule
inline bool ExportStorage::findIndirectRuleMatch(Cube* cube, const vector<Rule*>& rules, IdentifierType* path, Cube::CellValueType& cellValue, EMemoryContext* mem_context)
{

	// We split the rules into the following categories:
	// - linear rules applicable to the path
	// - all other rules applicable to the path
	// - all applicable rules in the correct order
	// Keep in mind, that we already dealt with a direct matches. We will not try
	// again here. A direct match must have resulted in a STET because we reached
	// this function.

	int normalRules = 0;
	int linearRules = 0;
	int allRules = 0;
	int restrictedRules = 0;
	int markerRules = 0;

	Rule* linearRule = 0;

	for (vector<Rule*>::const_iterator iter = rules.begin(); iter != rules.end(); iter++) {
		Rule* rule = *iter;

		if (rule == 0 || !rule->isActive())
			continue;

		// no need to check a direct match again
		if (!rule->within(path) && rule->contains(path)) {
			if (rule->getRuleOption() == RuleNode::BASE && rule->isLinearRule() && rule->withinArea(path)) {
				linearRules++;
				linearRule = rule;
			} else {
				normalRules++;
			}

			allRules++;

			if (rule->isRestrictedRule() && !rule->containsRestricted(path)) {
				restrictedRules++;
			}

			if (rule->hasMarkers()) {
				markerRules++;
			}
		}
	}

	// no match at all, then return
	if (allRules == 0 || markerRules > 0)
		return false;

	// use linear evaluation
	bool useLinear = false;

	// if we have only one match of a linear rule, try to use it
	if (normalRules == 0 && linearRules == 1)
		useLinear = true;

	// if we have only one match and this is outside of a restriction, stop
	if (allRules == 1 && restrictedRules == 1) {
		Logger::trace << "encountered STET restriction in consolidation" << endl;
		return false;
	}

	// compute value
	Cube::CellValueType value;
	CellPath cellPath(cube, path);

	bool found = true;

	if (useLinear) {
		Logger::debug << "using linear rule to optimize access" << endl;

		bool skipAllRules;
		bool skipRule;

		value = linearRule->getValue(&cellPath, &skipRule, &skipAllRules, mem_context);

		cellValue = value;
	} else {
		set<pair<Rule*, IdentifiersType> > ruleHistory;
		value = cube->getConsolidatedRuleValue(&cellPath, &found, &ruleHistory, mem_context);

		if (found)
			cellValue = value;
	}

	return found;
}

void ExportStorage::advancePath(const vector<Rule*>& basicRules, IntervalVectorSet<IdentifierType>& consolidationSet, StringIterator& stringsSet, Condition* condition, IdentifierType* target_path, bool addEmpty, bool addConsolidation, EMemoryContext* mem_context, uint32_t cellType)
{
	bool skiponce = 0 == numberElements;

	set<pair<Rule*, IdentifiersType> > ruleHistory;
	if (0 == numberElements)
		skiponce = true;

	if (NULL == target_path) {
		target_path = (IdentifierType*)alloca( sizeof( IdentifierType ) * areaLength );
		for (size_t i = 0; i < areaLength; i++) {
			target_path[i] = area->at(i).back();
		}
	}
	Cube::CellValueType value;
	size_t ks = originator_cube->getStorageString()->getKeySize();
	uint8_t * keyBuffer = new uint8_t[ks];

	if (stringsSet.isValid()) {
		IdentifiersType next_string = *stringsSet;

		while (stringsSet.isValid() && path_smaller_eg_path(next_string, crt_path)) {
			++stringsSet;
			next_string = *stringsSet;
		}
	}

	while (!compare_crt_path_with_path(target_path) || skiponce) {
		if (hasBlocksizeElements()) {
			return;
		}

		if (!skiponce) {
			advanceIterators();
		}

		skiponce = false;

		value.charValue.clear();
		bool addValue = findDirectRuleMatch(originator_cube, basicRules, crt_raw_path, value, mem_context);

		if (!addValue) {
			if (consolidationSet.contains(crt_path)) {
				addValue = findIndirectRuleMatch(originator_cube, basicRules, crt_raw_path, value, mem_context);
			}

			if (!addValue && addConsolidation) {
				//compute consolidation
				bool found = false;
				CellPath cellPath(originator_cube, &crt_path);
				value = originator_cube->getCellValueNew(&cellPath, value, &found, &ruleHistory, mem_context);// getCellValue(&cellPath, &found, NULL, NULL, &ruleHistory, mem_context);
				if (cellType == 1 && cellPath.getPathType() == STRING) {
					addValue = false;
				} else {
					addValue = found;
				}
			}

		}

		if (!addValue) {

			IdentifierType * buffer = (IdentifierType*)keyBuffer;
			IdentifiersType::const_iterator pathIter = crt_path.begin();

			for (; pathIter != crt_path.end(); pathIter++) {
				*buffer++ = (IdentifierType)(*pathIter);
			}

			CubePage::element_t el = NULL;
			if (cellType == 0 || cellType == 2) {
				el = originator_cube->getStorageString()->getCellValueEngine(keyBuffer);
			}

			if (NULL != el) {
				bool found = false;
				CellPath cellPath(originator_cube, &crt_path);
				value = originator_cube->getCellValueNew(&cellPath, value, &found, &ruleHistory, mem_context);
				value.type = STRING;
				addValue = found;
			} else {
				if (addEmpty) {
					CellPath cellPath(originator_cube, &crt_path);

					switch (cellPath.getPathType()) {
					case NUMERIC:
						if (cellType == 2) {
							addValue = false;
						} else {
							value.doubleValue = 0;
							value.type = NUMERIC;
							addValue = true;
						}
						break;
					case STRING:
						if (cellType == 1) {
							addValue = false;
						} else {
							value.charValue = "";
							value.type = STRING;
							addValue = true;
						}
						break;
					case CONSOLIDATED:
						if (!addConsolidation || cellType == 2) {
							addValue = false;
						} else {
							value.doubleValue = 0;
							value.type = NUMERIC;
							addValue = addEmpty;
						}
						break;
					default:
						if (cellType == 2) {
							addValue = false;
						} else {
							value.doubleValue = 0;
							value.type = NUMERIC;
							addValue = true;
						}
						break;
					}
				}
			}
		}

		if (addValue && condition) {
			addValue = condition->check(value);
		}

		if (addValue) {
			Cube::CellValueType * ptr = new Cube::CellValueType();
			ptr->type = value.type;
			ptr->doubleValue = value.doubleValue;
			ptr->charValue = value.charValue;
			ptr->rule = value.rule;

			setCellValue(&crt_path, (CubePage::value_t)&ptr);
		}
	}
	delete[] keyBuffer;
}

void ExportStorage::advancePath(IntervalVectorSet<IdentifierType>& consolidationSet, StringIterator& stringsSet, Condition* condition, IdentifierType* target_path, bool addEmpty, bool addConsolidation, EMemoryContext* mem_context, uint32_t cellType)
{
	set<pair<Rule*, IdentifiersType> > ruleHistory;

	bool skiponce = 0 == numberElements;

	if (NULL == target_path) {
		target_path = (IdentifierType*)alloca( sizeof( IdentifierType ) * areaLength );
		for (size_t i = 0; i < areaLength; i++) {
			target_path[i] = area->at(i).back();
		}
	}

	Cube::CellValueType value;

	if (stringsSet.isValid()) {
		IdentifiersType next_string = *stringsSet;

		while (stringsSet.isValid() && path_smaller_eg_path(next_string, crt_path)) {
			++stringsSet;
			next_string = *stringsSet;
		}
		stringsSet.skipNext();
	}

	while (!compare_crt_path_with_path(target_path) || skiponce) {
		if (hasBlocksizeElements()) {
			return;
		}
		if (!skiponce) {
			if (addEmpty || addConsolidation) {
				advanceIterators();
			} else {
				do {
					++stringsSet;
				} while (stringsSet.isValid() && !isInArea(*stringsSet) && path_smaller_eg_path(*stringsSet, target_path));

				//TODO:
				// if ( findConsolidationBelow... )
				// {
				//     cange path for cons
				// }

				if (!stringsSet.isValid()) {
					advanceIterators(target_path);
					return;
				}

				if (!path_smaller_eg_path(*stringsSet, target_path)) {
					stringsSet.previous();
					advanceIterators(target_path);
					return;
				}

				advanceIterators(*stringsSet);
			}
		}

		skiponce = false;

		value.charValue.clear();
		bool addValue = false;

		if (addConsolidation) {

			if (consolidationSet.contains(crt_path)) {
				//compute consolidation
				bool found = false;
				CellPath cellPath(originator_cube, &crt_path);
				value = originator_cube->getCellValueNew(&cellPath, value, &found, &ruleHistory, mem_context);
				if (cellType == 1 && cellPath.getPathType() == STRING) {
					addValue = false;
				} else {
					addValue = found;
				}
			}
		}

		if (!addValue) {
			CellPath cellPath(originator_cube, &crt_path);
			bool found = false;
			switch (cellPath.getPathType()) {
			case NUMERIC:
				if (cellType == 2) {
					addValue = false;
				} else {
					value.doubleValue = 0;
					value.type = NUMERIC;
					addValue = addEmpty;
				}
				break;
			case STRING:
				if (cellType == 1) {
					addValue = false;
				} else {
					value = originator_cube->getCellValueNew(&cellPath, value, &found, &ruleHistory, mem_context);
					if ("" == value.charValue) {
						value.type = STRING;
						addValue = addEmpty;
					} else {
						addValue = true;
					}
				}
				break;
			case CONSOLIDATED:
				if (!addConsolidation || cellType == 2) {
					addValue = false;
				} else {
					value.doubleValue = 0;
					value.type = NUMERIC;
					addValue = addEmpty;
				}
				break;
			default:
				if (cellType == 2) {
					addValue = false;
				} else {
					value.doubleValue = 0;
					value.type = NUMERIC;
					addValue = addEmpty;
				}
				break;
			}
		}

		if (addValue && condition) {
			addValue = condition->check(value);
		}

		if (addValue) {
			Cube::CellValueType * ptr = new Cube::CellValueType();
			ptr->type = value.type;
			ptr->doubleValue = value.doubleValue;
			ptr->charValue = value.charValue;
			ptr->rule = value.rule;

			setCellValue(&crt_path, (CubePage::value_t)&ptr);
		}
	}
}

bool ExportStorage::findConsolidationBelow(IdentifiersType& consPath, IntervalVectorSet<IdentifierType>& consolidationSet, const IdentifiersType& maxPath)
{
	vector<IdentifierType> start;

	//set start position
	for (size_t i = 0; i < areaLength; i++) {
		start[i] = (IdentifierType)(crt_iterators[i] - begin_iterators[i]);
	}

	do {
		bool is_consolidated = false;
		bool increment = true;

		//next path in area
		for (int i = (int)areaLength - 1; i >= 0; i--) {
			if (increment)
				(start[i])++;

			if (start[i] == area[i].size()) {
				start[i] = 0;
				if (0 == i)
					return false; //passed the end of area
			} else {
				increment = false;
			}
			consPath[i] = (*area)[i][start[i]];

			if (consolidationSet[i].contains(consPath[i])) {
				is_consolidated = true;
			}
		}
		if (is_consolidated && path_smaller_eg_path(consPath, maxPath))
			return true;
	} while (path_smaller_eg_path(consPath, maxPath));

	return false;

}

}
