////////////////////////////////////////////////////////////////////////////////
/// @brief palo hash storage
///
/// @file
///
/// The contents of this file are subject to the Jedox AG Palo license. You
/// may not use this file except in compliance with the license. You may obtain
/// a copy of the License at
///
/// <a href="http://www.palo.com/license.txt">
///   http://www.palo.com/license.txt
/// </a>
///
/// Software distributed under the license is distributed on an "AS IS" basis,
/// WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the license
/// for the specific language governing rights and limitations under the
/// license.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_HASH_AREA_STORAGE_H
#define OLAP_HASH_AREA_STORAGE_H 1

#include "palo.h"

#include <math.h>

#if defined(_MSC_VER)
#include <float.h>
#include <limits>
#endif

#include "Olap/AreaStorage.h"
#include "Olap/AreaResultStorage.h"
#include "Olap/Engine.h"
#include "Olap/Rule.h"

#include "Exceptions/ErrorException.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief palo hash area storage
///
/// A storage that is used to hold a specific sub-cube. The access to the
/// elements is done via a precomputed hash function
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS HashAreaStorage {

public:
	HashAreaStorage(Cube* cube, const vector<map<Element*, uint32_t> >& hashMapping) :
		hashMapping(hashMapping)
	{

		// compute the number of elements
		areaSize = 1;

		idMapping.resize(hashMapping.size());
		size_t pos = 0;

		for (vector<map<Element*, uint32_t> >::const_iterator i = hashMapping.begin(); i != hashMapping.end(); i++, pos++) {
			areaSize *= (*i).size();

			const vector<Dimension*>* dims = cube->getDimensions();
			vector<uint32_t>& dimMapping = idMapping.at(pos);

			size_t max = dims->at(pos)->getMaximalIdentifier() + 1;
			dimMapping.resize(max);

			for (map<Element*, uint32_t>::const_iterator j = i->begin(); j != i->end(); j++) {
				dimMapping[j->first->getIdentifier()] = j->second;
			}
		}

		if (Logger::isTrace()) {
			Logger::trace << "hash area size " << areaSize << endl;
		}

		storage = new double[areaSize];
		status = new uint16_t[areaSize];

		memset(storage, 0, sizeof(double) * areaSize);
		memset(status, 0, sizeof(uint16_t) * areaSize);

		length = hashMapping.size();
	}

	~HashAreaStorage()
	{
		delete[] storage;
		delete[] status;
	}

public:
	double* getArea()
	{
		return storage;
	}

	double* getStorage()
	{
		return storage;
	}

	uint16_t * getStatus()
	{
		return status;
	}

	void fillAreaStorage(AreaStorage* area, vector<map<uint32_t, vector<pair<IdentifierType, double> > > >& reverseMapping, vector<uint32_t>& hashSteps, vector<uint32_t>& lengths)
	{

		vector<uint32_t> numList(length, 0);
		vector<uint32_t> hashList(length, 0);
		IdentifiersType path(length, 0);
		double * s = storage;

		for (size_t i = 0; i < areaSize; i++, s++) {
			if (!isnan(*s))
				fillAreaStorageRecursive(area, path, reverseMapping, hashList, 0, *s);
			// increment path
			size_t pos = 0;

			while (pos < length) {
				numList[pos]++;

				if (numList[pos] == lengths[pos]) {
					numList[pos] = 0;
					hashList[pos] = 0;
					pos++;
				} else {
					hashList[pos] = hashSteps[pos] * numList[pos];
					break;
				}
			}
		}
	}

	size_t getPos(const IdentifiersType* path)
	{
		size_t pos = 0;

		for (size_t i = 0; i < length; i++) {
			pos += idMapping[i][path->at(i)];
		}
		return pos;
	}

	IdentifierType getRule(uint16_t * stat)
	{
		return (*stat) & RuleNumberMask;
	}

	double* getValue(const IdentifiersType* path, IdentifierType *idRule, unsigned int *errorNumber, bool *found)
	{
		size_t pos = getPos(path);
		return getPosValue(pos, idRule, errorNumber, found);
	}

	double* getPosValue(size_t pos, IdentifierType *idRule, unsigned int *errorNumber, bool *found)
	{
		double* result = storage + pos;
		uint16_t* stat = status + pos;

		if (IsErrorStatus(*stat)) {
			(*found) = false;
			(*idRule) = (IdentifierType)*result;
			(*errorNumber) = AreaResultStorage::getErrorType(stat);
		} else if (IsEmptyStatus(*stat)) {
			(*found) = false;
			(*idRule) = Rule::NO_RULE;
			(*errorNumber) = 0;
		} else if (IsRuleStatus(*stat)) {
			(*found) = true;
			(*idRule) = getRule(stat);
			(*errorNumber) = 0;
		}
		// isOK
		else {
			(*found) = true;
			(*idRule) = Rule::NO_RULE;
			(*errorNumber) = 0;
		}
		return result;
	}

	void setValue(IdentifierType * path, double evalue, uint16_t estatus)
	{
		size_t pos = 0;

		for (size_t i = 0; i < length; i++) {
			pos += idMapping[i][path[i]];
		}

		*(storage + pos) = evalue;
		*(status + pos) = estatus;
	}

	void setValue(const IdentifiersType* path, double value, uint16_t stat)
	{
		size_t pos = getPos(path);
		*(storage + pos) = value;
		*(status + pos) = stat;
	}

	bool getValue(const IdentifiersType& path, double& value, uint16_t& stat)
	{
		size_t pos = getPos(&path);
		value = *(storage + pos);
		stat = *(status + pos);
		return !IsEmptyStatus( stat );
	}
	/*
	 void copy (HashAreaStorage* dest,
	 vector<uint32_t>& hashSteps,
	 vector<uint32_t>& lengths) {

	 // build reverse mapping for source storage
	 vector< map<uint32_t, IdentifierType> > reverseMapping(length);
	 for (size_t i = 0; i < length; i++) {
	 map<IdentifierType, uint32_t>& m = idMapping[i];
	 for (map<IdentifierType, uint32_t>::iterator ii = m.begin(); ii != m.end(); ii++) {
	 #if defined(_MSC_VER)
	 reverseMapping[i][ii->second] = ii->first;
	 #else
	 reverseMapping[i].insert(make_pair(ii->second, ii->first));
	 #endif
	 }
	 }

	 // destination
	 double * destStorage = dest->getStorage();
	 uint16_t * destStatus = dest->getStatus();

	 // source
	 vector<uint32_t> numList(length, 0);
	 vector<uint32_t> hashList(length, 0);
	 IdentifiersType path(length, 0);
	 double * s = storage;
	 uint16_t * stat = status;

	 for (size_t i = 0;  i < areaSize;  i++, s++, stat++) {
	 for (size_t row = 0; row < length; row++) {
	 path[row] = reverseMapping[row][hashList[row]];
	 }

	 size_t destPos = dest->getPos(&path);
	 *(destStorage + destPos) = *s;
	 *(destStatus  + destPos) = *stat;

	 // increment path
	 size_t pos = 0;

	 while (pos < length) {
	 numList[pos]++;

	 if (numList[pos] == lengths[pos]) {
	 numList[pos] = 0;
	 hashList[pos] = 0;
	 pos++;
	 }
	 else {
	 hashList[pos] = hashSteps[pos] * numList[pos];
	 break;
	 }
	 }
	 }
	 }
	 */
	void computeConsolidations(vector<vector<Element*> >& numericAreaElements, vector<vector<vector<pair<IdentifierType, double> > > >& consIdentifiers)
	{

		int length = (int)numericAreaElements.size();
		vector<size_t> combinations(length);
		bool eval = true;
		IdentifiersType path(length);
		vector<vector<pair<IdentifierType, double> > *> consMap(length);

		for (int i = 0; i < length;) {

			if (eval) {
				bool isCon = false;

				// construct path and mapping to children
				for (int j = 0; j < length; j++) {
					path[j] = numericAreaElements[j][combinations[j]]->getIdentifier();
					consMap[j] = &(consIdentifiers[j][combinations[j]]);

					if (consMap[j]->size() > 0) {
						isCon = true;
					}

				}
				if (isCon) {
					// compute value of consolidated path
					computeConsolidation(path, consMap);
				}
			}

			size_t position = combinations[i];
			const vector<Element*>& dim = numericAreaElements[i];

			if (position + 1 < dim.size()) {
				combinations[i] = (int)position + 1;
				i = 0;
				eval = true;
			} else {
				i++;

				for (int k = 0; k < i; k++) {
					combinations[k] = 0;
				}

				eval = false;
			}
		}

	}

private:
	void fillAreaStorageRecursive(AreaStorage* area, IdentifiersType& path, vector<map<uint32_t, vector<pair<IdentifierType, double> > > >& reverseMapping, vector<uint32_t>& hashList, size_t pos, double value)
	{
		if (pos == length - 1) {
			vector<pair<IdentifierType, double> >& l = reverseMapping[pos][hashList[pos]];

			for (vector<pair<IdentifierType, double> >::iterator i = l.begin(); i != l.end(); i++) {
				path[pos] = i->first;

				// for (size_t j = 0;  j < length;  j++) {
				//   cout << path[j] << " ";
				// }
				//
				// cout << " = " << (value * i->second) << endl;

				area->addDoubleValue(&path, value * i->second);
			}
		} else {
			vector<pair<IdentifierType, double> >& l = reverseMapping[pos][hashList[pos]];

			for (vector<pair<IdentifierType, double> >::iterator i = l.begin(); i != l.end(); i++) {
				path[pos] = i->first;

				fillAreaStorageRecursive(area, path, reverseMapping, hashList, pos + 1, value * i->second);
			}
		}
	}

private:
	void computeConsolidation(IdentifiersType& path, vector<vector<pair<IdentifierType, double> > *>& consMap);
	void computeConsolidationRecursive(double* consValue, uint16_t* consStatus, vector<vector<pair<IdentifierType, double> > *>& consMap, IdentifiersType& source, size_t pos, size_t length, double weight, size_t index);

private:
	size_t areaSize;
	double * storage;
	uint16_t * status;
	size_t length;
	vector<map<Element*, uint32_t> > hashMapping;
	vector<vector<uint32_t> > idMapping;
};

}

#endif
