////////////////////////////////////////////////////////////////////////////////
/// @brief palo export page
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef OLAP_EXPORT_PAGE_H
#define OLAP_EXPORT_PAGE_H 1

#include "palo.h"

#include "Olap/CubeIndex.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief palo cube page
///
/// A cube page stores the cell data into a fixed memory size.
/// Each cube page is divided into rows of a given size.
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS ExportPage {

public:
	typedef uint8_t * buffer_t;
	typedef uint8_t * element_t;
	typedef uint8_t * key_t;
	typedef uint8_t * value_t;

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief creates an empty export storage
	////////////////////////////////////////////////////////////////////////////////

	ExportPage(CubeIndex* index, size_t keySize, size_t valueSize, size_t numValues);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief delete export storage
	////////////////////////////////////////////////////////////////////////////////

	~ExportPage();

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief adds an entry to cube
	////////////////////////////////////////////////////////////////////////////////

	element_t addElement(element_t);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief removes an entry from the page
	////////////////////////////////////////////////////////////////////////////////

	element_t removeElement(element_t);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief removes last entry from the page
	////////////////////////////////////////////////////////////////////////////////

	element_t removeLastElement();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief removes last entry from the page
	////////////////////////////////////////////////////////////////////////////////

	element_t getLastElement();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns a pointer to the beginning
	////////////////////////////////////////////////////////////////////////////////

	buffer_t begin()
	{
		return buffer;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns a pointer to the end (first non-used entry)
	////////////////////////////////////////////////////////////////////////////////

	buffer_t end()
	{
		return buffer + usedElements * rowSize;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the used row size
	////////////////////////////////////////////////////////////////////////////////

	size_t getRowSize()
	{
		return rowSize;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the row after ptr
	////////////////////////////////////////////////////////////////////////////////

	buffer_t next(buffer_t ptr)
	{
		return ptr + rowSize;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief returns the a row by position
	////////////////////////////////////////////////////////////////////////////////

	buffer_t getCell(size_t pos)
	{
		if (pos < usedElements) {
			return buffer + pos * rowSize;
		}
		return 0;
	}

public:
	void sort();
	inline bool isLess(element_t left, element_t right) const
	{
		if (NULL == left)
			return false;
		IdentifierType * leftElement = (IdentifierType*)(left + valueSize);

		IdentifierType * rightElement = (IdentifierType*)(right + valueSize);

		IdentifierType * rightEnd = (IdentifierType*)(right + valueSize + keySize);

		for (; leftElement < rightEnd; leftElement++, rightElement++) {
			if (*leftElement < *rightElement) {
				return true;
			} else if (*leftElement > *rightElement) {
				return false;
			}
		}

		return false;
	}

private:
	void saveElement(size_t src);

	void restoreElement(size_t dst);

	void copyElement(size_t dst, size_t src);

	bool isLessElement(size_t right);

private:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief cube index for resize
	////////////////////////////////////////////////////////////////////////////////

	CubeIndex * index;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief sorted flag
	////////////////////////////////////////////////////////////////////////////////

	bool sorted;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief size of the key
	////////////////////////////////////////////////////////////////////////////////

	size_t keySize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief size of the value
	////////////////////////////////////////////////////////////////////////////////

	size_t valueSize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief size of an entry
	///
	/// Aligned element size.
	////////////////////////////////////////////////////////////////////////////////

	size_t rowSize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief total size of the page
	////////////////////////////////////////////////////////////////////////////////

	size_t totalSize;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief used number of rows per page
	////////////////////////////////////////////////////////////////////////////////

	size_t usedElements;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief number of rows per page
	////////////////////////////////////////////////////////////////////////////////

	size_t numberElements;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief pointer to the page memory
	////////////////////////////////////////////////////////////////////////////////

	buffer_t buffer;

	////////////////////////////////////////////////////////////////////////////////
	/// @brief temporary buffer for one row
	////////////////////////////////////////////////////////////////////////////////

	element_t tmpBuffer;
};

}

#endif
