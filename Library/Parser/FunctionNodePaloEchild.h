////////////////////////////////////////////////////////////////////////////////
/// @brief parser function node palo echild
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PARSER_FUNCTION_NODE_PALO_ECHILD_H
#define PARSER_FUNCTION_NODE_PALO_ECHILD_H 1

#include "palo.h"

#include <string>
#include "Parser/FunctionNodePalo.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief parser function node palo echild
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS FunctionNodePaloEchild : public FunctionNodePalo {

public:
	static FunctionNode* createNode(const string& name, vector<Node*> *params)
	{
		return new FunctionNodePaloEchild(name, params);
	}

public:
	FunctionNodePaloEchild() :
		FunctionNodePalo()
	{
	}

	FunctionNodePaloEchild(const string& name, vector<Node*> *params) :
		FunctionNodePalo(name, params)
	{
	}

	Node * clone()
	{
		FunctionNodePaloEchild * cloned = new FunctionNodePaloEchild(name, cloneParameters());
		cloned->valid = this->valid;
		cloned->server = this->server;
		cloned->database = this->database;
		return cloned;
	}

public:
	ValueType getValueType()
	{
		return Node::NODE_STRING;
	}

	bool validate(Server* server, Database* database, Cube* cube, Node* destination, string& error)
	{
		return validateParameter(server, database, cube, destination, error, 3, 1);
	}

	RuleValueType getValue(CellPath* cellPath, bool* isCachable, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context)
	{
		RuleValueType result;
		result.type = Node::NODE_STRING;

		if (valid) {
			Database* database = getDatabase(server, params->at(0), cellPath, isCachable, ruleHistory, mem_context);
			Dimension* dimension = getDimension(database, params->at(1), cellPath, isCachable, ruleHistory, mem_context);
			Element* element = getElement(dimension, params->at(2), cellPath, isCachable, ruleHistory, mem_context);

			RuleValueType value = params->at(3)->getValue(cellPath, isCachable, ruleHistory, mem_context);

			if (element && value.type == Node::NODE_NUMERIC) {
				const ElementsWeightType childrenList = dimension->getChildren(element);
				int offset = (int)value.doubleValue;

				if (offset > 0 && (size_t)offset <= childrenList.size()) {
					Element* child = childrenList.at(offset - 1).first;
					result.stringValue = child->getName();

					return result;
				}
			}
		}

		result.stringValue = "";

		return result;
	}

	bool genCode(bytecode_generator& generator, uint8_t want)
	{
		if (!params->at(3)->genCode(generator, Node::NODE_NUMERIC))
			return false;
		if (!params->at(2)->genCode(generator, Node::NODE_STRING))
			return false;
		if (!params->at(1)->genCode(generator, Node::NODE_STRING))
			return false;
		if (!params->at(0)->genCode(generator, Node::NODE_STRING))
			return false;
		if (!generator.EmitFuncCode("palo.echild"))
			return false;
		return generator.EmitForceTypeCode(Node::NODE_STRING, want);
	}

};

}
#endif
