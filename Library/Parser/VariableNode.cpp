////////////////////////////////////////////////////////////////////////////////
/// @brief parser variable node
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "Parser/VariableNode.h"

#include "Collections/StringUtils.h"

namespace palo {
VariableNode::VariableNode(string *value) :
	value(value)
{
	valid = false;
	num = -1;
	dimension = 0;
}

VariableNode::~VariableNode()
{
	delete value;
}

Node * VariableNode::clone()
{
	VariableNode * cloned = new VariableNode(new string(*value));
	cloned->valid = this->valid;
	cloned->num = this->num;
	cloned->dimension = this->dimension;
	return cloned;
}

bool VariableNode::validate(Server* server, Database* database, Cube* cube, Node* destination, string& error)
{
	if (database == 0 || cube == 0) {
		dimension = 0;
		valid = true;
		return valid;
	}

	dimension = database->lookupDimensionByName(*value);

	if (!dimension) {
		error = "dimension '" + *value + "' not found";
		valid = false;
		return valid;
	}

	const vector<Dimension*>* dimensions = cube->getDimensions();
	int pos = 0;

	for (vector<Dimension*>::const_iterator i = dimensions->begin(); i != dimensions->end(); i++, pos++) {
		if ((*i) == dimension) {
			num = pos;
			valid = true;
			return valid;
		}
	}

	error = "dimension '" + *value + "' is not member of cube '" + cube->getName() + "'";
	valid = false;
	return valid;
}

Node::RuleValueType VariableNode::getValue(CellPath* cellPath, bool* usesOtherDatabase, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context)
{
	RuleValueType result;
	result.type = Node::NODE_STRING;

	if (num >= 0) {
		const PathType * elements = cellPath->getPathElements();
		Element* element = elements->at(num);
		result.stringValue = element->getName();
	} else {
		result.stringValue = "";
	}

	return result;
}

void VariableNode::appendXmlRepresentation(StringBuffer* sb, int ident, bool)
{
	identXML(sb, ident);
	sb->appendText("<variable>");
	sb->appendText(StringUtils::escapeXml(*value));
	sb->appendText("</variable>");
	sb->appendEol();
}

void VariableNode::appendRepresentation(StringBuffer* sb, Cube* cube) const
{
	sb->appendChar('!');
	sb->appendText(StringUtils::escapeStringSingle(*value));
}

uint32_t VariableNode::guessType(uint32_t level)
{
	Logger::trace << "guessType " << "level " << level << "node " << "VariableNode " << " type " << "none" << endl;
	return Node::NODE_UNKNOWN_VALUE;
}

bool VariableNode::genCode(bytecode_generator& generator, uint8_t want)
{
	if (!generator.EmitVariableStrCode(num))
		return false;
	return generator.EmitForceTypeCode(Node::NODE_STRING, want);
}

}
