////////////////////////////////////////////////////////////////////////////////
/// @brief parser function node palo data
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PARSER_FUNCTION_NODE_PALO_DATA_H
#define PARSER_FUNCTION_NODE_PALO_DATA_H 1

#include "palo.h"

#include <iostream>

#include "Parser/FunctionNodePalo.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief parser function node palo data
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS FunctionNodePaloData : public FunctionNodePalo {

public:
	static FunctionNode* createNode(const string& name, vector<Node*> *params)
	{
		// printf("FunctionNodePaloData created\n");
		return new FunctionNodePaloData(name, params);
	}

public:
	FunctionNodePaloData() :
		FunctionNodePalo()
	{
	}

	FunctionNodePaloData(const string& name, vector<Node*> *params) :
		FunctionNodePalo(name, params)
	{
	}

	Node * clone()
	{
		FunctionNodePaloData * cloned = new FunctionNodePaloData(name, cloneParameters());
		cloned->valid = this->valid;
		cloned->server = this->server;
		cloned->database = this->database;
		return cloned;
	}

public:
	ValueType getValueType()
	{
		return Node::NODE_UNKNOWN_VALUE;
	}

	bool validate(Server* server, Database* database, Cube* cube, Node* destination, string& error)
	{

		// has three parameters
		if (!params || params->size() < 3) {
			error = "function '" + name + "' needs more than two parameters";
			return valid = false;
		}

		for (size_t i = 0; i < params->size(); i++) {
			Node* param = params->at(i);
			/*
			 // if we have a string cube param diff. from current
			 // then swap it
			 if ( 1 == i && param->getNodeType() == Node::NODE_STRING_NODE)
			 {
			 std::string value = dynamic_cast<StringNode*>(param)->getStringValue();

			 if ( value != cube->getName() )
			 {
			 palo::Cube* c = database->lookupCubeByName(value);
			 if ( c )
			 {
			 cube = c;
			 }
			 }
			 }
			 */
			// validate parameter
			if (!param->validate(server, database, cube, destination, error)) {
				return valid = false;
			}

			// check data type left
			if (param->getValueType() != Node::NODE_STRING && param->getValueType() != Node::NODE_UNKNOWN_VALUE) {
				error = "parameter of function '" + name + "' has wrong data type";
				return valid = false;
			}
		}

		this->server = server;

		return valid = true;
	}

	RuleValueType getValue(CellPath* cellPath, bool* isCachable, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context)
	{
		RuleValueType result;

		if (valid) {
			Database* database = getDatabase(server, params->at(0), cellPath, isCachable, ruleHistory, mem_context);

			if (database == 0) {
				throw ParameterException(ErrorException::ERROR_DATABASE_NOT_FOUND, "database unknown", "database", params->at(0)->getValue(cellPath, isCachable, ruleHistory, mem_context).stringValue);
			}

			Cube* cube = getCube(database, params->at(1), cellPath, isCachable, ruleHistory, mem_context);

			if (cube == 0) {
				throw ParameterException(ErrorException::ERROR_CUBE_NOT_FOUND, "cube unknown", "database", params->at(1)->getValue(cellPath, isCachable, ruleHistory, mem_context).stringValue);
			}

			const vector<Dimension*>* dimensions = cube->getDimensions();

			if (dimensions->size() != params->size() - 2) {
				throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", "number of coordinates", (int)(params->size() - 2));
			}

			PathType path(dimensions->size());

			vector<Dimension*>::const_iterator dim = dimensions->begin();
			vector<Node*>::iterator node = params->begin() + 2;
			PathType::iterator pIter = path.begin();

			for (; dim != dimensions->end(); dim++, node++, pIter++) {
				Element* element = getElement(*dim, *node, cellPath, isCachable, ruleHistory, mem_context);

				if (!element) {
					RuleValueType value = (*node)->getValue(cellPath, isCachable, ruleHistory, mem_context);

					if (value.type == Node::NODE_STRING) {
						throw ParameterException(ErrorException::ERROR_ELEMENT_NOT_FOUND, "element of dimension not found", "element", value.stringValue);
					} else {
						throw ParameterException(ErrorException::ERROR_ELEMENT_NOT_FOUND, "decimal value as element name is not allowed", "element", "decimal value");
					}

				}

				*pIter = element;
			}

			CellPath cp(cube, &path);
			bool found;
			Cube::CellValueType value = cube->getCellValueNew(&cp, &found, ruleHistory, mem_context);

			if (found) {
				if (value.type == STRING) {
					result.type = Node::NODE_STRING;
					result.stringValue = value.charValue;
				} else {
					result.type = Node::NODE_NUMERIC;
					result.doubleValue = value.doubleValue;
				}
			} else {
				if (cp.getPathType() == STRING) {
					result.type = Node::NODE_STRING;
					result.stringValue = "";
				} else {
					result.type = Node::NODE_NUMERIC;
					result.doubleValue = 0.0;
				}
			}

			return result;
		}

		result.type = Node::NODE_NUMERIC;
		result.doubleValue = 0.0;

		return result;
	}

	bool genCode(bytecode_generator& generator, uint8_t want)
	{
		int32_t i;
		for (i = (int32_t)params->size() - 1; 0 <= i; i--) {
			if (!params->at(i)->genCode(generator, Node::NODE_STRING))
				return false;
		}
		if (want == Node::NODE_NUMERIC) {
			if (!generator.EmitCallDataDblCode((uint8_t)params->size()))
				return false;
		} else {
			if (!generator.EmitCallDataStrCode((uint8_t)params->size()))
				return false;
		}

		return true;
	}

};

}
#endif
