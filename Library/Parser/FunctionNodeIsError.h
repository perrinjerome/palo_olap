////////////////////////////////////////////////////////////////////////////////
/// @brief
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// Developed by Marko Stijak, Banja Luka on behalf of Jedox AG.
/// Copyright and exclusive worldwide exploitation right has
/// Jedox AG, Freiburg.
///
/// @author Marko Stijak, Banja Luka, Bosnia and Herzegovina
////////////////////////////////////////////////////////////////////////////////


#ifndef PARSER_FUNCTION_NODE_ISERROR_H
#define PARSER_FUNCTION_NODE_ISERROR_H 1

#include "palo.h"

#include "Parser/FunctionNode.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief parser function node str
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS FunctionNodeIsError : public FunctionNode {

public:
	static FunctionNode* createNode(const string& name, vector<Node*> *params)
	{
		return new FunctionNodeIsError(name, params);
	}

public:
	FunctionNodeIsError() :
		FunctionNode()
	{
	}
	;
	FunctionNodeIsError(const string& name, vector<Node*> *params) :
		FunctionNode(name, params)
	{
	}
	;

public:

	ValueType getValueType()
	{
		return Node::NODE_NUMERIC;
	}

	Node * clone()
	{
		FunctionNodeIsError * cloned = new FunctionNodeIsError(name, cloneParameters());
		cloned->valid = this->valid;
		return cloned;
	}

	bool validate(Server* server, Database* database, Cube* cube, Node* destination, string& error)
	{

		// add has two parameters
		if (!params || params->size() != 1) {
			error = "function '" + name + "' needs exactly one parameter";
			return valid = false;
		}

		p1 = params->at(0);

		// validate parameters
		if (!p1->validate(server, database, cube, destination, error)) {
			return valid = false;
		}

		return valid = true;
	}

	RuleValueType getValue(CellPath* cellPath, bool* isCachable, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context)
	{

		RuleValueType result;
		result.type = Node::NODE_NUMERIC;

		if (valid) {
			try {
				RuleValueType v1 = p1->getValue(cellPath, isCachable, ruleHistory, mem_context);
				if (v1.type == Node::NODE_STRING || v1.type == Node::NODE_NUMERIC) {
					result.doubleValue = 0;
					return result;
				}
			} catch (...) {

			}
		}

		result.doubleValue = 1;
		return result;
	}

	bool genCode(bytecode_generator& generator, uint8_t want)
	{
		if (p1->getValueType() == Node::NODE_NUMERIC || p1->getValueType() == Node::NODE_UNKNOWN_VALUE) {
			if (!p1->genCode(generator, Node::NODE_NUMERIC))
				return false;
			if (!generator.EmitIsError(1))
				return false;
		} else {
			if (!p1->genCode(generator, Node::NODE_STRING))
				return false;
			if (!generator.EmitIsError(0))
				return false;
		}
		return generator.EmitForceTypeCode(Node::NODE_NUMERIC, want);
	}

protected:
	Node* p1;
};

}
#endif
