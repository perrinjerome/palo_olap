////////////////////////////////////////////////////////////////////////////////
/// @brief parser function node palo marker
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PARSER_FUNCTION_NODE_PALO_MARKER_H
#define PARSER_FUNCTION_NODE_PALO_MARKER_H 1

#include "palo.h"

#include <string>

#include "Parser/FunctionNodePalo.h"
#include "Parser/StringNode.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief parser function node palo marker
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS FunctionNodePaloMarker : public FunctionNodePalo {

public:
	static FunctionNode* createNode(const string& name, vector<Node*> *params)
	{
		return new FunctionNodePaloMarker(name, params);
	}

public:
	FunctionNodePaloMarker() :
		FunctionNodePalo()
	{
	}

	FunctionNodePaloMarker(const string& name, vector<Node*> *params) :
		FunctionNodePalo(name, params)
	{
	}

	Node * clone()
	{
		FunctionNodePaloMarker * cloned = new FunctionNodePaloMarker(name, cloneParameters());
		cloned->valid = this->valid;
		cloned->server = this->server;
		cloned->database = this->database;
		return cloned;
	}

public:
	NodeType getNodeType() const
	{
		return NODE_FUNCTION_PALO_MARKER;
	}

	ValueType getValueType()
	{
		return Node::NODE_UNKNOWN_VALUE;
	}

	const string& getDatabaseName() const
	{
		return databaseName;
	}

	const string& getCubeName() const
	{
		return cubeName;
	}

	const vector<Node*>& getPath() const
	{
		return path;
	}

	bool validate(Server* server, Database* database, Cube* cube, Node* destination, string& error)
	{

		// has three parameters
		if (!params || params->size() < 3) {
			error = "function '" + name + "' needs more than two parameters";
			return valid = false;
		}

		for (size_t i = 0; i < params->size(); i++) {
			Node* param = params->at(i);

			// validate parameter
			if (!param->validate(server, database, cube, destination, error)) {
				return valid = false;
			}

			// check data type left
			if (param->getValueType() != Node::NODE_STRING && param->getValueType() != Node::NODE_UNKNOWN_VALUE) {
				error = "parameter of function '" + name + "' has wrong data type";
				return valid = false;
			}

			// check paramater type, should be constant string or variable
			if (i < 2) {
				if (param->getNodeType() != Node::NODE_STRING_NODE) {
					error = "database or cube paramater of function '" + name + "' must be a constant";
					return valid = false;
				}

				StringNode * s = dynamic_cast<StringNode*> (param);

				if (i == 0) {
					databaseName = s->getStringValue();
				} else if (i == 1) {
					cubeName = s->getStringValue();
				}
			} else {
				if (param->getNodeType() != Node::NODE_STRING_NODE && param->getNodeType() != Node::NODE_VARIABLE_NODE) {
					error = "parameter of function '" + name + "' must be a constant or a variable";
					return valid = false;
				}

				path.push_back(param);
			}
		}

		this->server = server;

		return valid = true;
	}

	RuleValueType getValue(CellPath* cellPath, bool* isCachable, set<pair<Rule*, IdentifiersType> >* ruleHistory, EMemoryContext* mem_context)
	{
		RuleValueType result;

		if (valid) {
			Database* database = getDatabase(server, params->at(0), cellPath, isCachable, ruleHistory, mem_context);

			if (database == 0) {
				throw ParameterException(ErrorException::ERROR_DATABASE_NOT_FOUND, "database unknown", "database", params->at(0)->getValue(cellPath, isCachable, ruleHistory, mem_context).stringValue);
			}

			Cube* cube = getCube(database, params->at(1), cellPath, isCachable, ruleHistory, mem_context);

			if (cube == 0) {
				throw ParameterException(ErrorException::ERROR_CUBE_NOT_FOUND, "cube unknown", "database", params->at(1)->getValue(cellPath, isCachable, ruleHistory, mem_context).stringValue);
			}

			const vector<Dimension*>* dimensions = cube->getDimensions();

			if (dimensions->size() != params->size() - 2) {
				throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", "number of coordinates", (int)(params->size() - 2));
			}

			PathType path;

			vector<Dimension*>::const_iterator dim = dimensions->begin();
			vector<Node*>::iterator node = params->begin() + 2;

			for (; dim != dimensions->end(); dim++, node++) {
				Element* element = getElement(*dim, *node, cellPath, isCachable, ruleHistory, mem_context);

				if (!element) {
					RuleValueType value = (*node)->getValue(cellPath, isCachable, ruleHistory, mem_context);
					string name = "decimal value";

					if (value.type == Node::NODE_STRING) {
						name = value.stringValue;
					}

					throw ParameterException(ErrorException::ERROR_ELEMENT_NOT_FOUND, "element of dimension not found", "element", name);
				}

				path.push_back(element);
			}

			CellPath cp(cube, &path);
			bool found;
			Cube::CellValueType value = cube->getCellValueNew(&cp, &found, ruleHistory, mem_context);

			if (found) {
				if (value.type == STRING) {
					result.type = Node::NODE_STRING;
					result.stringValue = value.charValue;
				} else {
					result.type = Node::NODE_NUMERIC;
					result.doubleValue = value.doubleValue;
				}
			} else {
				if (cp.getPathType() == STRING) {
					result.type = Node::NODE_STRING;
					result.stringValue = "";
				} else {
					result.type = Node::NODE_NUMERIC;
					result.doubleValue = 0.0;
				}
			}

			return result;
		}

		result.type = Node::NODE_NUMERIC;
		result.doubleValue = 0.0;

		return result;
	}

	bool genCode(bytecode_generator& generator, uint8_t want)
	{
		int32_t i;
		for (i = (int32_t)params->size() - 1; 0 <= i; i--) {
			if (!params->at(i)->genCode(generator, Node::NODE_STRING))
				return false;
		}
		if (want == Node::NODE_NUMERIC) {
			if (!generator.EmitCallDataDblCode((uint8_t)params->size()))
				return false;
		} else {
			if (!generator.EmitCallDataDblCode((uint8_t)params->size()))
				return false;
		}
		return true;
	}

	void collectMarkers(vector<Node*>& markers)
	{
		markers.push_back(this);
	}

private:
	string databaseName;
	string cubeName;
	vector<Node*> path;
};

}
#endif
