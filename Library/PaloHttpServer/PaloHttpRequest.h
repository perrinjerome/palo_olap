////////////////////////////////////////////////////////////////////////////////
/// @brief palo http request
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_HTTP_SERVER_PALO_HTTP_REQUEST_H
#define PALO_HTTP_SERVER_PALO_HTTP_REQUEST_H 1

#include "palo.h"

#include "HttpServer/HttpRequest.h"

namespace palo {
class PaloJobRequest;

////////////////////////////////////////////////////////////////////////////////
/// @brief http request
///
/// The http server reads the request string from the client and converts it
/// into an instance of this class. An http request object provides methods to
/// inspect the header and parameter fields.
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS PaloHttpRequest : public HttpRequest {

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief palo http request
	////////////////////////////////////////////////////////////////////////////////

	PaloHttpRequest(const string& url, HttpRequestHandler*);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief destructor
	////////////////////////////////////////////////////////////////////////////////

	~PaloHttpRequest();

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief extracts the header fields of the request
	///
	/// @warning this might alter the contents of the string buffer
	////////////////////////////////////////////////////////////////////////////////

	void extractHeader(char* begin, char* end);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief extracts the body of the request
	///
	/// @warning this might alter the contents of the string buffer
	////////////////////////////////////////////////////////////////////////////////

	void extractBody(char* begin, char* end);

	////////////////////////////////////////////////////////////////////////////////
	/// @brief releases the job request
	////////////////////////////////////////////////////////////////////////////////

	PaloJobRequest* releasePaloJobRequest()
	{
		PaloJobRequest* job = paloJobRequest;
		paloJobRequest = 0;
		return job;
	}

private:
	void setKeyValues(char* begin, char* end);
	void setKeyValue(char * keyStart, char * keyPtr, char * valueStart, char * valuePtr);

	void fillToken(uint32_t*& token, char* begin, char* end);

	void fillIdentifier(IdentifierType& identifier, char* begin, char* end);
	void fillSession(IdentifierType& identifier, char* begin, char* end);
	void fillBoolean(bool& flag, char* begin, char* end);
	void fillString(string*& text, char* begin, char* end);
	void fillUint(uint32_t& identifier, char* begin, char* end);
	void fillVectorIdentifier(vector<IdentifierType>*& identifiers, char* begin, char* end);
	void fillVectorUint(vector<uint32_t>*& ints, char* begin, char* end);
	void fillVectorDouble(vector<double>*& doubles, char* begin, char* end);
	void fillVectorString(vector<string>*& strings, char* begin, char* end, char separator);
	void fillVectorStringQuote(vector<string>*& strings, char* begin, char* end, char separator);
	void fillVectorVectorIdentifier(vector<vector<IdentifierType> >*& identifiers, char* begin, char* end, char first, char second);
	void fillVectorVectorString(vector<vector<string> >*& strings, char* begin, char* end, char first, char second);
	void fillVectorVectorStringQuote(vector<vector<string> >*& strings, char* valueStart, char* valueEnd, char first, char second);
	void fillVectorVectorDouble(vector<vector<double> >*& doubles, char* begin, char* end, char first, char second);

private:
	PaloJobRequest * paloJobRequest;
};

}

#endif
