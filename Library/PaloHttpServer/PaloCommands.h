/* C++ code produced by gperf version 3.0.1 */
/* Command-line: gperf -CGD -N PaloValue -K option -L C++ -t PaloCommands.gperf > PaloCommands.h  */
/* Computed positions: -k'1,8,$' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gnu-gperf@gnu.org>."
#endif

#line 1 "PaloCommands.gperf"

#include "palo.h"

#include "PaloHttpServer/PaloRequestHandler.h"
#line 6 "PaloCommands.gperf"
struct CommandOption {
  const char * option;
  int code;
};

#define TOTAL_KEYWORDS 71
#define MIN_WORD_LENGTH 3
#define MAX_WORD_LENGTH 19
#define MIN_HASH_VALUE 5
#define MAX_HASH_VALUE 154
/* maximum key range = 150, duplicates = 0 */

class Perfect_Hash
{
private:
  static inline unsigned int hash (const char *str, unsigned int len);
public:
  static const struct CommandOption *PaloValue (const char *str, unsigned int len);
};

inline unsigned int
Perfect_Hash::hash (register const char *str, register unsigned int len)
{
  static const unsigned char asso_values[] =
    {
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155,  70, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155,  30,  65,  95, 155,
      155, 155, 155, 155, 155, 155, 155,   0, 155, 155,
      155, 155, 155,  10, 155, 155,   5, 155,   5, 155,
      155, 155, 155, 155, 155, 155, 155,  60,  15,  45,
       30,  15, 125, 155,   0,   5, 155,  70,  30,   0,
        0,  20,   5, 155,  50,   0,  15,  65,  10,   0,
      155,  40,  60, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155
    };
  register int hval = len;

  switch (hval)
    {
      default:
        hval += asso_values[(unsigned char)str[7]];
      /*FALLTHROUGH*/
      case 7:
      case 6:
      case 5:
      case 4:
      case 3:
      case 2:
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval + asso_values[(unsigned char)str[len - 1]];
}

static const struct CommandOption wordlist[] =
  {
#line 56 "PaloCommands.gperf"
    {"steps", palo::PaloRequestHandler::CMD_NUM_STEPS},
#line 69 "PaloCommands.gperf"
    {"splash", palo::PaloRequestHandler::CMD_SPLASH},
#line 74 "PaloCommands.gperf"
    {"weights", palo::PaloRequestHandler::CMD_WEIGHTS},
#line 37 "PaloCommands.gperf"
    {"path", palo::PaloRequestHandler::CMD_ID_PATH},
#line 38 "PaloCommands.gperf"
    {"paths", palo::PaloRequestHandler::CMD_ID_PATHS},
#line 64 "PaloCommands.gperf"
    {"show_system", palo::PaloRequestHandler::CMD_SHOW_SYSTEM},
#line 58 "PaloCommands.gperf"
    {"position", palo::PaloRequestHandler::CMD_POSITION},
#line 47 "PaloCommands.gperf"
    {"name_dimension", palo::PaloRequestHandler::CMD_NAME_DIMENSION},
#line 48 "PaloCommands.gperf"
    {"name_dimensions", palo::PaloRequestHandler::CMD_NAME_DIMENSIONS},
#line 73 "PaloCommands.gperf"
    {"values", palo::PaloRequestHandler::CMD_VALUES},
#line 44 "PaloCommands.gperf"
    {"name_children", palo::PaloRequestHandler::CMD_NAME_CHILDREN},
#line 35 "PaloCommands.gperf"
    {"mode", palo::PaloRequestHandler::CMD_ID_MODE},
#line 42 "PaloCommands.gperf"
    {"types", palo::PaloRequestHandler::CMD_ID_TYPES},
#line 68 "PaloCommands.gperf"
    {"source", palo::PaloRequestHandler::CMD_SOURCE},
#line 32 "PaloCommands.gperf"
    {"elements", palo::PaloRequestHandler::CMD_ID_ELEMENTS},
#line 51 "PaloCommands.gperf"
    {"name_path", palo::PaloRequestHandler::CMD_NAME_PATH},
#line 52 "PaloCommands.gperf"
    {"name_paths", palo::PaloRequestHandler::CMD_NAME_PATHS},
#line 36 "PaloCommands.gperf"
    {"parent", palo::PaloRequestHandler::CMD_ID_PARENT},
#line 50 "PaloCommands.gperf"
    {"name_elements", palo::PaloRequestHandler::CMD_NAME_ELEMENTS},
#line 76 "PaloCommands.gperf"
    {"X-PALO-SV", palo::PaloRequestHandler::CMD_X_PALO_SERVER},
#line 72 "PaloCommands.gperf"
    {"value", palo::PaloRequestHandler::CMD_VALUE},
#line 39 "PaloCommands.gperf"
    {"path_to", palo::PaloRequestHandler::CMD_ID_PATH_TO},
#line 59 "PaloCommands.gperf"
    {"sid", palo::PaloRequestHandler::CMD_SID},
#line 41 "PaloCommands.gperf"
    {"type", palo::PaloRequestHandler::CMD_ID_TYPE},
#line 22 "PaloCommands.gperf"
    {"event", palo::PaloRequestHandler::CMD_EVENT},
#line 31 "PaloCommands.gperf"
    {"element", palo::PaloRequestHandler::CMD_ID_ELEMENT},
#line 55 "PaloCommands.gperf"
    {"new_name", palo::PaloRequestHandler::CMD_NEW_NAME},
#line 45 "PaloCommands.gperf"
    {"name_cube", palo::PaloRequestHandler::CMD_NAME_CUBE},
#line 63 "PaloCommands.gperf"
    {"show_rules", palo::PaloRequestHandler::CMD_SHOW_RULES},
#line 49 "PaloCommands.gperf"
    {"name_element", palo::PaloRequestHandler::CMD_NAME_ELEMENT},
#line 46 "PaloCommands.gperf"
    {"name_database", palo::PaloRequestHandler::CMD_NAME_DATABASE},
#line 60 "PaloCommands.gperf"
    {"show_attribute", palo::PaloRequestHandler::CMD_SHOW_ATTRIBUTE},
#line 19 "PaloCommands.gperf"
    {"definition", palo::PaloRequestHandler::CMD_DEFINITION},
#line 53 "PaloCommands.gperf"
    {"name_path_to", palo::PaloRequestHandler::CMD_NAME_PATH_TO},
#line 65 "PaloCommands.gperf"
    {"show_user_info", palo::PaloRequestHandler::CMD_SHOW_USER_INFO},
#line 33 "PaloCommands.gperf"
    {"limit", palo::PaloRequestHandler::CMD_ID_LIMIT},
#line 26 "PaloCommands.gperf"
    {"children", palo::PaloRequestHandler::CMD_ID_CHILDREN},
#line 62 "PaloCommands.gperf"
    {"show_rule", palo::PaloRequestHandler::CMD_SHOW_RULE},
#line 67 "PaloCommands.gperf"
    {"skip_empty", palo::PaloRequestHandler::CMD_SKIP_EMPTY},
#line 29 "PaloCommands.gperf"
    {"dimension", palo::PaloRequestHandler::CMD_ID_DIMENSION},
#line 30 "PaloCommands.gperf"
    {"dimensions", palo::PaloRequestHandler::CMD_ID_DIMENSIONS},
#line 27 "PaloCommands.gperf"
    {"cube", palo::PaloRequestHandler::CMD_ID_CUBE},
#line 20 "PaloCommands.gperf"
    {"extern_password", palo::PaloRequestHandler::CMD_EXTERN_PASSWORD},
#line 12 "PaloCommands.gperf"
    {"action", palo::PaloRequestHandler::CMD_ACTION,},
#line 16 "PaloCommands.gperf"
    {"comment", palo::PaloRequestHandler::CMD_COMMENT},
#line 28 "PaloCommands.gperf"
    {"database", palo::PaloRequestHandler::CMD_ID_DATABASE},
#line 40 "PaloCommands.gperf"
    {"rule", palo::PaloRequestHandler::CMD_ID_RULE},
#line 57 "PaloCommands.gperf"
    {"password", palo::PaloRequestHandler::CMD_PASSWORD},
#line 18 "PaloCommands.gperf"
    {"condition", palo::PaloRequestHandler::CMD_CONDITION},
#line 75 "PaloCommands.gperf"
    {"show_lock_info", palo::PaloRequestHandler::CMD_SHOW_LOCK_INFO},
#line 17 "PaloCommands.gperf"
    {"complete", palo::PaloRequestHandler::CMD_COMPLETE},
#line 43 "PaloCommands.gperf"
    {"name_area", palo::PaloRequestHandler::CMD_NAME_AREA},
#line 71 "PaloCommands.gperf"
    {"use_rules", palo::PaloRequestHandler::CMD_USE_RULES},
#line 61 "PaloCommands.gperf"
    {"show_normal", palo::PaloRequestHandler::CMD_SHOW_NORMAL},
#line 13 "PaloCommands.gperf"
    {"add", palo::PaloRequestHandler::CMD_ADD},
#line 14 "PaloCommands.gperf"
    {"base_only", palo::PaloRequestHandler::CMD_BASE_ONLY},
#line 11 "PaloCommands.gperf"
    {"activate", palo::PaloRequestHandler::CMD_ACTIVATE},
#line 15 "PaloCommands.gperf"
    {"blocksize", palo::PaloRequestHandler::CMD_BLOCKSIZE},
#line 34 "PaloCommands.gperf"
    {"lock", palo::PaloRequestHandler::CMD_ID_LOCK},
#line 79 "PaloCommands.gperf"
    {"X-PALO-CB", palo::PaloRequestHandler::CMD_X_PALO_CUBE},
#line 78 "PaloCommands.gperf"
    {"X-PALO-DIM", palo::PaloRequestHandler::CMD_X_PALO_DIMENSION},
#line 21 "PaloCommands.gperf"
    {"external_identifier", palo::PaloRequestHandler::CMD_EXTERNAL_IDENTIFIER},
#line 54 "PaloCommands.gperf"
    {"user", palo::PaloRequestHandler::CMD_NAME_USER},
#line 25 "PaloCommands.gperf"
    {"area", palo::PaloRequestHandler::CMD_ID_AREA},
#line 70 "PaloCommands.gperf"
    {"use_identifier", palo::PaloRequestHandler::CMD_USE_IDENTIFIER},
#line 23 "PaloCommands.gperf"
    {"event_processor", palo::PaloRequestHandler::CMD_EVENT_PROCESSOR},
#line 24 "PaloCommands.gperf"
    {"functions", palo::PaloRequestHandler::CMD_FUNCTIONS},
#line 77 "PaloCommands.gperf"
    {"X-PALO-DB", palo::PaloRequestHandler::CMD_X_PALO_DATABASE},
#line 80 "PaloCommands.gperf"
    {"X-PALO-CC", palo::PaloRequestHandler::CMD_X_PALO_CUBE_CLIENT_CACHE},
#line 81 "PaloCommands.gperf"
    {"Content-Length", palo::PaloRequestHandler::CMD_CONTENT_LENGTH},
#line 66 "PaloCommands.gperf"
    {"show_info", palo::PaloRequestHandler::CMD_SHOW_INFO}
  };

static const signed char lookup[] =
  {
    -1, -1, -1, -1, -1,  0,  1,  2, -1,  3,  4,  5, -1,  6,
     7,  8,  9, -1, 10, 11, 12, 13, -1, 14, 15, 16, 17, -1,
    18, 19, 20, -1, 21, 22, 23, 24, -1, 25, 26, 27, 28, -1,
    29, 30, 31, 32, -1, 33, -1, 34, 35, -1, -1, 36, 37, 38,
    -1, -1, -1, 39, 40, -1, -1, -1, 41, 42, 43, 44, 45, 46,
    -1, -1, -1, 47, 48, -1, -1, -1, -1, 49, -1, -1, -1, 50,
    51, -1, -1, -1, -1, 52, -1, 53, -1, 54, 55, -1, -1, -1,
    56, 57, -1, -1, -1, -1, 58, -1, -1, -1, -1, 59, 60, -1,
    -1, -1, 61, -1, -1, -1, -1, 62, -1, -1, -1, -1, 63, -1,
    -1, -1, -1, 64, 65, -1, -1, -1, 66, -1, -1, -1, -1, 67,
    -1, -1, -1, -1, 68, -1, -1, -1, -1, 69, -1, -1, -1, -1,
    70
  };

const struct CommandOption *
Perfect_Hash::PaloValue (register const char *str, register unsigned int len)
{
  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register int index = lookup[key];

          if (index >= 0)
            {
              register const char *s = wordlist[index].option;

              if (*str == *s && !strcmp (str + 1, s + 1))
                return &wordlist[index];
            }
        }
    }
  return 0;
}
