////////////////////////////////////////////////////////////////////////////////
/// @brief palo engine
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_bi_suite.txt">
///   http://www.jedox.com/license_palo_bi_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Developed by triagens GmbH, Koeln on behalf of Jedox AG. Intellectual
/// property rights has triagens GmbH, Koeln. Exclusive worldwide
/// exploitation right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Martin Schoenert, triagens GmbH, Cologne, Germany
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
///
/// @author Radu Ialovoi started this file with code migrated from  Engine.cpp
/// @author Jiri Junek
///
////////////////////////////////////////////////////////////////////////////////

#include "VirtualMachine.h"
#include "BytecodeGenerator.h"
#include "Olap/CubeStorage.h"
#include "Olap/Rule.h"

namespace palo {
void virtual_machine::compute(EPath path, ERule* rule, Value defValue, Status defStatus, uint8_t is_base)
{
	pc = rule->bytecode;

	Bytecode crt_pc;
	Bytecode ret_pc = bytecode_generator::INVALID;
	while (1) {
		if (bytecode_generator::INVALID == ret_pc) {
			crt_pc = *pc++;
		} else {
			crt_pc = ret_pc;
			ret_pc = bytecode_generator::INVALID;
		}

		switch (crt_pc) {

		case bytecode_generator::NOP:
			break;

		case bytecode_generator::HALT: {
			*ptrValue = dbl_0;
			*ptrStatus = RuleStatus | (rule->nr_rule & RuleNumberMask);
			m_mem_context->writeQueryCache(rule->arule->getCube(), &path[0], &path[rule->cube->nrDimensions], dbl_0, *ptrStatus);
			RETURN()
		}

		case bytecode_generator::FUNC_ISERROR_DBL:
			dbl_0 = *--sp_dbl;
			*sp_dbl++ = dbl_0;
			dbl_0 = 0.0;
			break;

		case bytecode_generator::FUNC_ISERROR_STR:
			str_0 = *--sp_str;
			*sp_dbl++ = dbl_0;
			dbl_0 = 0.0;
			break;

		case bytecode_generator::PUSH_DBL:
			*sp_dbl++ = dbl_0;
			break;

		case bytecode_generator::PULL_DBL:
			dbl_0 = *--sp_dbl;
			break;

		case bytecode_generator::SWAP_DBL:
			dbl_t = *(sp_dbl - 1);
			*(sp_dbl - 1) = dbl_0;
			dbl_0 = dbl_t;
			break;

		case bytecode_generator::PUSH_STR:
			*sp_str++ = str_0;
			break;

		case bytecode_generator::PULL_STR:
			str_0 = *--sp_str;
			break;

		case bytecode_generator::SWAP_STR:
			str_t = *(sp_str - 1);
			*(sp_str - 1) = str_0;
			str_0 = str_t;
			break;

		case bytecode_generator::DBL_2_STR:
			sprintf(str_buf, "%f", dbl_0);
			dbl_0 = *--sp_dbl;
			*sp_str++ = str_0;
			str_0 = str_buf;
			break;

		case bytecode_generator::STR_2_DBL:
			*sp_dbl++ = dbl_0;
			dbl_0 = strtod(str_0.c_str(), &p);
			str_0 = *--sp_str;
			break;

		case bytecode_generator::LD_CONST_DBL:
			*sp_dbl++ = dbl_0;
			dbl_0 = rule->dbl_consts[*pc++];
			break;

		case bytecode_generator::LD_CONST_STR:
			*sp_str++ = str_0;
			str_0 = rule->str_consts[*pc++];
			break;

		case bytecode_generator::LD_SRC_HIT_DBL:
			if (m_stack.is_empty()) {
				i = *pc++;
				if (IsErrorStatus(defStatus)) {
					HANDLE_ISERROR_DBL( 0, 0 )
					*ptrValue = 0.0;
					*ptrStatus = defStatus;
					RETURN()
				}
				*sp_dbl++ = dbl_0;
				dbl_0 = defValue;
				break;
			} else {
				// else - evaluate LD_SRC_HIT_DBL as normal LD_SRC_DBL
			}
		case bytecode_generator::LD_SRC_DBL:
			i = *pc++;
			for (j = 0; j < rule->cube->nrDimensions; j++) {
				path_t[j] = (rule->copy_mask[i][j] ? rule->copy_source[i][j] : path[j]);
			}

			if (m_mem_context->readQueryCache(rule->arule->getCube(), IdentifiersType(&path_t[0], &path_t[rule->cube->nrDimensions]), dbl_t, status_t)) {
				*sp_dbl++ = dbl_0;
				dbl_0 = dbl_t;
				break;
			}
			{
				bool new_call = false;

				if (is_consolidation(path_t, *rule->cube)) {

					/* loop over the rules                                             */
					for (ERule * l_rule = (ERule*)FirstItemList(&(rule->cube->rules_c)); l_rule != 0; l_rule = (ERule*)NextItemList(&(rule->cube->rules_c),l_rule)) {
						if (IsPathInArea(l_rule->dest_area, path_t)) {
							/* rules that apply to cons cells should never have marker */
							/* even if the code does have a marker the following code  */
							/* will not crash, but simply return 0 for the marker      */

							PUSH( bytecode_generator::RET_LD_SRC_DBL )

							rule = l_rule;
							for (j = 0; j < rule->cube->nrDimensions; j++) {
								path[j] = path_t[j];
							}
							pc = rule->bytecode;
							defValue = 0;
							defStatus = 0;
							is_base = 0;
							new_call = true;
							break;

						}
					}
					if (new_call) {
						continue;
					}

					/* if no applicable rule is found, treat this as an area           */
					EArea *area = NewPathArea(rule->cube, path_t, m_mem_context);
					EBuffer *buf = NewBuffer(1, m_mem_context);
					FillAreaBuffer(buf, area, m_env, m_mem_context);
					dbl_t = buf->values[0];
					status_t = buf->status[0];
					FreeBuffer(buf, m_mem_context);
					FreeArea(area, m_mem_context);

					m_mem_context->writeQueryCache(rule->arule->getCube(), &path_t[0], &path_t[rule->cube->nrDimensions], dbl_t, status_t);
				} else { // cell is base
					// loop over the rules
					for (ERule * l_rule = (ERule*)FirstItemList(&(rule->cube->rules_n)); l_rule != 0; l_rule = (ERule*)NextItemList(&(rule->cube->rules_n),l_rule)) {
						if (IsBasePathInArea(l_rule->dest_area, path_t)) {
							if (l_rule->ubm_flag) {
								PUSH( bytecode_generator::RET_LD_SRC_DBL )

								rule = l_rule;
								for (j = 0; j < rule->cube->nrDimensions; j++) {
									path[j] = path_t[j];
								}
								pc = rule->bytecode;

								defValue = 0;	//TODO: markersin VM: -jj- better to return nothing than random values 
								defStatus = 0;
								is_base = 1;
								new_call = true;
								break;
							} else {
								PUSH( bytecode_generator::RET_LD_SRC_DBL )

								rule = l_rule;
								for (j = 0; j < rule->cube->nrDimensions; j++) {
									path[j] = path_t[j];
								}
								pc = rule->bytecode;
								defValue = 0;
								defStatus = 0;
								is_base = 1;
								new_call = true;
								break;
							}
						}
					}
					if (new_call) {
						continue;
					}
					// if no applicable rule is found, try a base lookup
					Value *row = (Value*)rule->cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path_t);
					if (row) {
						dbl_t = *row;
						status_t = BaseStatus;
					} else {
						dbl_t = 0.0;
						status_t = 0;
					}
				}
			}

			if (IsErrorStatus(status_t)) {
				HANDLE_ISERROR_DBL( 0, 0 )
				*ptrValue = 0.0;
				*ptrStatus = status_t;
				RETURN()
			}
			*sp_dbl++ = dbl_0;
			dbl_0 = dbl_t;
			break;

		case bytecode_generator::RET_LD_SRC_DBL:
			sp_dbl++;
			dbl_0 = *ptrValue;
			break;

		case bytecode_generator::LD_SRC_STR: {
			i = *pc++;
			for (j = 0; j < rule->cube->nrDimensions; j++) {
				path_t[j] = (rule->copy_mask[i][j] ? rule->copy_source[i][j] : path[j]);
			}
			CellPath apath(rule->cube->acube, (IdentifierType*)&path_t);
			bool found;

			// check rule recursion
			m_mem_context->m_recursion_stack.push(rule, path);
			Cube::CellValueType avalue = rule->cube->acube->getCellValueNew(&apath, &found, 0, m_mem_context);
			m_mem_context->m_recursion_stack.pop();

			if (found && avalue.type == STRING) {
				*sp_str++ = str_0;
				str_0 = avalue.charValue;
			} else {
				*sp_str++ = str_0;
				str_0 = "";
			}
		}
			break;

		case bytecode_generator::LD_VAR_STR:
			i = *pc++;
			*sp_str++ = str_0;
			str_0 = rule->cube->acube->getDimensions()->at(i)->lookupElement((IdentifierType)path[i])->getName();
			break;

		case bytecode_generator::OP2_SUM_DBL:
			dbl_0 = *--sp_dbl + dbl_0;
			break;

		case bytecode_generator::OP2_DIFF_DBL:
			dbl_0 = *--sp_dbl - dbl_0;
			break;

		case bytecode_generator::OP2_PROD_DBL:
			dbl_0 = *--sp_dbl * dbl_0;
			break;

		case bytecode_generator::OP2_QUO_DBL:
			if (0 == dbl_0) {
				if (ignoreRuleError) {
					dbl_0 = 0;
				} else {
					HANDLE_ISERROR_DBL( 0, 0 )
					*ptrValue = *--sp_dbl / dbl_0;
					*ptrStatus = ErrorStatus | ErrorDivisionByZero;
					RETURN()
				}
			} else {
				dbl_0 = *--sp_dbl / dbl_0;
			}
			break;

		case bytecode_generator::OP2_EQ_DBL:
			dbl_0 = (*--sp_dbl == dbl_0 ? 1.0 : 0.0);
			break;

		case bytecode_generator::OP2_NE_DBL:
			dbl_0 = (*--sp_dbl != dbl_0 ? 1.0 : 0.0);
			break;

		case bytecode_generator::OP2_LT_DBL:
			dbl_0 = (*--sp_dbl < dbl_0 ? 1.0 : 0.0);
			break;

		case bytecode_generator::OP2_LE_DBL:
			dbl_0 = (*--sp_dbl <= dbl_0 ? 1.0 : 0.0);
			break;

		case bytecode_generator::OP2_EQ_STR:
			*sp_dbl++ = dbl_0;
			dbl_0 = (*--sp_str == str_0 ? 1.0 : 0.0);
			str_0 = *--sp_str;
			break;

		case bytecode_generator::OP2_NE_STR:
			*sp_dbl++ = dbl_0;
			dbl_0 = (*--sp_str != str_0 ? 1.0 : 0.0);
			str_0 = *--sp_str;
			break;

		case bytecode_generator::OP2_LT_STR:
			*sp_dbl++ = dbl_0;
			dbl_0 = (*--sp_str < str_0 ? 1.0 : 0.0);
			str_0 = *--sp_str;
			break;

		case bytecode_generator::OP2_LE_STR:
			*sp_dbl++ = dbl_0;
			dbl_0 = (*--sp_str <= str_0 ? 1.0 : 0.0);
			str_0 = *--sp_str;
			break;

		case bytecode_generator::CALL_GEN_DBL:
			break;

		case bytecode_generator::CALL_GEN_STR:
			break;

		case bytecode_generator::CALL_DATA_DBL: {
			i = *pc++;
			Server * asrv = rule->cube->acube->getDatabase()->getServer();
			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, i )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;
			Cube * acube = adb->findCubeByName(str_0, 0);
			if (!acube) {
				HANDLE_ISERROR_DBL( 0, i-1 )
				*ptrValue = rule->nr_rule;
				*ptrStatus = ErrorStatus | ErrorCubeNotFound;
				RETURN()
			}
			str_0 = *--sp_str;
			if (i - 2 != acube->getDimensions()->size()) {
				HANDLE_ISERROR_DBL( 0, i-2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			bool bError = false;
			for (j = 0; j < i - 2; j++) {
				Dimension * adim = acube->getDimensions()->at(j);
				Element * aelm = adim->lookupElementByName(str_0);
				if (!aelm) {
					HANDLE_ISERROR_DBL( 0, i-2-j )
					*ptrValue = 0.0;
					*ptrStatus = ErrorStatus | ErrorElementNotFound;
					bError = true;
					break;
				}
				path_t[j] = aelm->getIdentifier();
				str_0 = *--sp_str;
			}
			if (bError) {
				RETURN()
			}
			ECube * cube = NewEntryCube(acube, m_user, m_mem_context);

			if (m_mem_context->readQueryCache(acube, IdentifiersType(&path_t[0], &path_t[cube->nrDimensions]), dbl_t, status_t)) {
				*sp_dbl++ = dbl_0;
				dbl_0 = dbl_t;
				break;
			}
			{
				bool new_call = false;

				if (is_consolidation(path_t, *cube)) {

					/* loop over the rules                                             */
					for (ERule *l_rule = (ERule*)FirstItemList(&(cube->rules_c)); l_rule != 0; l_rule = (ERule*)NextItemList(&(cube->rules_c),l_rule)) {
						if (IsPathInArea(l_rule->dest_area, path_t)) {
							/* rules that apply to cons cells should never have marker */
							/* even if the code does have a marker the following code  */
							/* will not crash, but simply return 0 for the marker      */
							//compute( path_t, l_rule, 0, 0, 0 );
							//RETURN()//function return from GetValue

							PUSH( bytecode_generator::RET_CALL_DATA_DBL )

							*sp_dbl++ = dbl_0;
							rule = l_rule;
							for (j = 0; j < rule->cube->nrDimensions; j++) {
								path[j] = path_t[j];
							}
							pc = rule->bytecode;
							defValue = 0;
							defStatus = 0;
							is_base = 0;

							new_call = true;
							break;
						}
					}
					if (new_call) {
						continue;
					}

					/* if no applicable rule is found, treat this as an area           */
					EArea *area = NewPathArea(cube, path_t, m_mem_context);
					EBuffer *buf = NewBuffer(1, m_mem_context);
					FillAreaBuffer(buf, area, m_env, m_mem_context);
					dbl_t = buf->values[0];
					status_t = buf->status[0];
					FreeBuffer(buf, m_mem_context);
					FreeArea(area, m_mem_context);

					m_mem_context->writeQueryCache(acube, &path_t[0], &path_t[rule->cube->nrDimensions], dbl_t, status_t);
				} else { // cell is base
					// loop over the rules
					for (ERule *l_rule = (ERule*)FirstItemList(&(cube->rules_n)); l_rule != 0; l_rule = (ERule*)NextItemList(&(cube->rules_n),l_rule)) {
						if (IsBasePathInArea(l_rule->dest_area, path_t)) {
							if (l_rule->ubm_flag) {
								PUSH( bytecode_generator::RET_CALL_DATA_DBL )

								*sp_dbl++ = dbl_0;
								rule = l_rule;
								for (j = 0; j < rule->cube->nrDimensions; j++) {
									path[j] = path_t[j];
								}
								pc = rule->bytecode;
								is_base = 1;

								new_call = true;
								break;
							} else {
								PUSH( bytecode_generator::RET_CALL_DATA_DBL )

								*sp_dbl++ = dbl_0;
								rule = l_rule;
								for (j = 0; j < rule->cube->nrDimensions; j++) {
									path[j] = path_t[j];
								}
								pc = rule->bytecode;
								defValue = 0;
								defStatus = 0;
								is_base = 1;
								new_call = true;
								break;
							}
						}
					}
					if (new_call) {
						continue;
					}
					// if no applicable rule is found, try a base lookup
					Value *row = (Value*)cube->acube->getStorageDouble()->getCellValueEngine((uint8_t*)path_t);
					if (row) {
						dbl_t = *row;
						status_t = BaseStatus;
					} else {
						dbl_t = 0.0;
						status_t = 0;
					}
				}
			}
			if (IsErrorStatus(status_t)) {
				HANDLE_ISERROR_DBL( 0, 0 )
				*ptrValue = 0.0;
				*ptrStatus = status_t;
				RETURN()
			}

			*sp_dbl++ = dbl_0;
			dbl_0 = dbl_t;
		}
			break;

		case bytecode_generator::RET_CALL_DATA_DBL:
			sp_dbl++;
			dbl_0 = *ptrValue;
			break;

		case bytecode_generator::CALL_DATA_STR: {
			i = *pc++;
			Server * asrv = rule->cube->acube->getDatabase()->getServer();
			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 0, i )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;
			Cube * acube = adb->findCubeByName(str_0, 0);
			if (!acube) {
				HANDLE_ISERROR_STR( 0, i-1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorCubeNotFound;
				RETURN()
			}
			str_0 = *--sp_str;
			if (i - 2 != acube->getDimensions()->size()) {
				HANDLE_ISERROR_STR( 0, i-2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			bool bError = false;
			for (j = 0; j < i - 2; j++) {
				Dimension * adim = acube->getDimensions()->at(j);
				Element * aelm = adim->lookupElementByName(str_0);
				if (!aelm) {
					HANDLE_ISERROR_STR( 0, i-2-j )
					*ptrValue = 0.0;
					*ptrStatus = ErrorStatus | ErrorElementNotFound;
					bError = true;
					break;
				}
				path_t[j] = aelm->getIdentifier();
				str_0 = *--sp_str;
			}
			if (bError) {
				RETURN()
			}
			ECube * cube = NewEntryCube(acube, m_user, m_mem_context);
			bool rulecalculated = false;
			if (is_string(path_t, *cube)) {
				// loop over the rules
				for (ERule *l_rule = (ERule*)FirstItemList(&(cube->rules_n)); l_rule != 0; l_rule = (ERule*)NextItemList(&(cube->rules_n),l_rule)) {
					if (IsPathInArea(l_rule->dest_area, path_t)) {
						CellPath apath(acube, (IdentifierType*)&path_t);
						bool found;

						// check rule recursion
						m_mem_context->m_recursion_stack.push(rule, path);
						Cube::CellValueType avalue = acube->getCellValueNew(&apath, &found, 0, m_mem_context);
						m_mem_context->m_recursion_stack.pop();
						rulecalculated = true;

						if (found && avalue.type == STRING) {
							*sp_str++ = str_0;
							str_0 = avalue.charValue;
						} else {
							*sp_str++ = str_0;
							str_0 = "";
						}
					}
				}
				if (!rulecalculated) {
					// if no applicable rule is found, try a base lookup
					char ** value = (char **)cube->acube->getStorageString()->getCellValueEngine((uint8_t*)path_t);

					if (value) {
						*sp_str++ = str_0;
						str_0 = *value;
					} else {
						*sp_str++ = str_0;
						str_0 = "";
					}
				}
			} else {
				*sp_str++ = str_0;
				str_0 = "";
			}
		}
			break;

		case bytecode_generator::JUMP:
			i = *pc++;
			pc += i;
			break;

		case bytecode_generator::JUMP_IF_NOT:
			i = *pc++;
			if (dbl_0 == 0.0) {
				dbl_0 = *--sp_dbl;
				pc += i;
			} else {
				dbl_0 = *--sp_dbl;
			}
			break;

		case bytecode_generator::CONTINUE:
			/* XXX or could we need to call a C-Rule here too ? */
		{
			ERule *rule2 = rule;
			if (is_base) {
				/* HOW DO I DECIDE WHERE TO CONTINUE ??? */
				do {
					rule2 = (ERule*)NextItemList( &(rule->cube->rules_n), rule2 );
				} while (rule2 && !IsPathInArea(rule2->dest_area, path));
			} else {
				do {
					rule2 = (ERule*)NextItemList( &(rule->cube->rules_c), rule2 );
				} while (rule2 && !IsPathInArea(rule2->dest_area, path));
			}
			if (rule2) {
				//compute( path, rule2, defValue, defStatus, is_base );
				//RETURN()
				rule = rule2;
				pc = rule->bytecode;
				break;
			} else {
				GetValueNoRule(ptrValue, ptrStatus, rule->cube, path, m_env, m_mem_context);
				RETURN()
			}
		}
			break;

		case bytecode_generator::STET:
			*sp_dbl = dbl_0;
			GetValueNoRule(ptrValue, ptrStatus, rule->cube, path, m_env, m_mem_context);
			RETURN()

		case bytecode_generator::OP2_LOG_DBL:
			dbl_t = *--sp_dbl;
			if (0 < dbl_t && 0 < dbl_0) {
				dbl_0 = log(dbl_t) / log(dbl_0);
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP2_MOD_DBL:
			dbl_t = *--sp_dbl;
			if (dbl_0 != 0.0) {
				dbl_0 = dbl_t - trunc(dbl_t / dbl_0) * dbl_0;
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP2_POWER_DBL:
			dbl_t = *--sp_dbl;
			dbl_0 = pow(dbl_t, dbl_0);
			break;

		case bytecode_generator::OP2_QUOTIENT_DBL:
			dbl_t = *--sp_dbl;
			if (dbl_0 != 0.0) {
				dbl_0 = trunc(dbl_t / dbl_0);
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP2_RANDBETWEEN_DBL:
			dbl_t = *--sp_dbl;
			if (dbl_t < dbl_0) {
				dbl_0 = dbl_t + ((double)rand()) / double(RAND_MAX) * (dbl_0 - dbl_t);
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP2_ROUND_DBL:
			dbl_t = *--sp_dbl;
			if (0 < dbl_0) {
				dbl_0 = pow((double)10, int(dbl_0));
				dbl_0 = round(dbl_t * dbl_0) / dbl_0;
			} else if (dbl_0 < 0) {
				dbl_0 = pow((double)10, -int(dbl_0));
				dbl_0 = round(dbl_t / dbl_0) * dbl_0;
			} else {
				dbl_0 = round(dbl_t);
			}
			break;

		case bytecode_generator::OP1_ABS_DBL:
			dbl_0 = (0 <= dbl_0 ? dbl_0 : -dbl_0);
			break;

		case bytecode_generator::OP1_ACOS_DBL:
			if (-1.0 <= dbl_0 && dbl_0 <= 1.0) {
				dbl_0 = acos(dbl_0);
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP1_ASIN_DBL:
			if (-1.0 <= dbl_0 && dbl_0 <= 1.0) {
				dbl_0 = asin(dbl_0);
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP1_ATAN_DBL:
			dbl_0 = atan(dbl_0);
			break;

		case bytecode_generator::OP1_CEILING_DBL:
			dbl_0 = ceil(dbl_0);
			break;

		case bytecode_generator::OP1_COS_DBL:
			dbl_0 = cos(dbl_0);
			break;

		case bytecode_generator::OP1_EVEN_DBL:
			dbl_0 = round(dbl_0 / 2) * 2;
			break;

		case bytecode_generator::OP1_EXP_DBL:
			dbl_0 = exp(dbl_0);
			break;

		case bytecode_generator::OP1_FACT_DBL:
			i = (int)dbl_0;
			dbl_0 = 1.0;
			for (j = 2; j <= i; j++) {
				dbl_0 *= j;
			}
			break;

		case bytecode_generator::OP1_FLOOR_DBL:
			dbl_0 = floor(dbl_0);
			break;

		case bytecode_generator::OP1_INT_DBL:
			dbl_0 = floor(dbl_0);
			break;

		case bytecode_generator::OP1_LN_DBL:
			if (0.0 < dbl_0) {
				dbl_0 = log(dbl_0);
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP1_LOG10_DBL:
			if (0.0 < dbl_0) {
				dbl_0 = log10(dbl_0);
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP1_ODD_DBL:
			dbl_0 = round((dbl_0 - 1) / 2) * 2 + 1;
			break;

		case bytecode_generator::OP1_SIGN_DBL:
			if (0 < dbl_0) {
				dbl_0 = 1.0;
			} else if (dbl_0 < 0) {
				dbl_0 = -1.0;
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP1_SIN_DBL:
			dbl_0 = sin(dbl_0);
			break;

		case bytecode_generator::OP1_SQRT_DBL:
			if (0.0 <= dbl_0) {
				dbl_0 = sqrt(dbl_0);
			} else {
				dbl_0 = 0.0;
			}
			break;

		case bytecode_generator::OP1_TAN_DBL:
			dbl_0 = tan(dbl_0);
			break;

		case bytecode_generator::OP1_TRUNC_DBL:
			dbl_0 = floor(dbl_0);
			break;

		case bytecode_generator::OP1_WEEKDAY_DBL: {
			time_t tt = (time_t)dbl_0;
			struct tm* t = gmtime(&tt);
			if (t) {
				dbl_0 = (double)t->tm_wday + 1.0;
			} else {
				dbl_0 = 0.0;
			}
		}
			break;

		case bytecode_generator::FUNC_CHAR: /* S <- D */
		{
			string str_c(1, (char)dbl_0);
			dbl_0 = *--sp_dbl;
			*sp_str = str_0;
			str_0 = str_c;
		}
			break;

		case bytecode_generator::FUNC_CLEAN: /* S <- S */
		{
			size_t len = str_0.size();
			char * str_c = new char[len + 1];
			const char * p = str_0.c_str();
			char * q = str_c;
			for (i = 0; i < len; i++) {
				if (' ' <= *p) {
					*q++ = *p;
				}
				p++;
			}
			*q = '\0';
			str_0 = str_c;
			delete[] str_c;
		}
			break;

		case bytecode_generator::FUNC_CODE: /* D <- S */
			*sp_dbl++ = dbl_0;
			dbl_0 = (double)str_0[0];
			str_0 = *--sp_str;
			break;

		case bytecode_generator::FUNC_CONCATENATE: /* S <- S,S */
			str_0 = *--sp_str + str_0;
			break;

		case bytecode_generator::FUNC_DATE: /* D <- D,D,D */
			t.tm_mday = (int)dbl_0;
			t.tm_mon = (int)*--sp_dbl - 1;
			t.tm_year = (int)*--sp_dbl - 1900;
			t.tm_hour = 12;
			t.tm_min = 0;
			t.tm_sec = 0;
			t.tm_wday = 0;
			t.tm_yday = 0;
			t.tm_isdst = -1;
			dbl_0 = (double)(mktime(&t) / 86400 * 86400);
			break;

		case bytecode_generator::FUNC_DATEFORMAT: /* S <- D,S */
			break;

		case bytecode_generator::FUNC_DATEVALUE: /* D <- S */
			if (str_0.length() > 6) {
				t.tm_mday = StringToInt(str_0.substr(3, 2));
				t.tm_mon = StringToInt(str_0.substr(0, 2)) - 1;
				t.tm_year = StringToInt(str_0.substr(6, 4));
				if (t.tm_year >= 1970 && t.tm_year < 2038) {
					t.tm_year -= 1900;
				} else if (t.tm_year >= 0 && t.tm_year < 100) {
					t.tm_year += 100;
				} else {
					dbl_0 = 0;
					break;
				}
				t.tm_hour = 12;
				t.tm_min = 0;
				t.tm_sec = 0;
				t.tm_wday = 0;
				t.tm_yday = 0;
				t.tm_isdst = -1;
				time_t dtm = mktime(&t);
				dbl_0 = (double)(dtm / 86400 * 86400);
			} else {
				dbl_0 = 0;
			}
			break;

		case bytecode_generator::FUNC_EXACT: /* D <- S,S */
			*sp_dbl++ = dbl_0;
			dbl_0 = (*--sp_str == str_0 ? 1.0 : 0.0);
			str_0 = *--sp_str;
			break;

		case bytecode_generator::FUNC_LEFT: /* S <- S,D */
			str_0 = str_0.substr(0, (int)dbl_0);
			dbl_0 = *--sp_dbl;
			break;

		case bytecode_generator::FUNC_LEN: /* D <- S */
			*sp_dbl++ = dbl_0;
			dbl_0 = (double)(str_0.length());
			str_0 = *--sp_str;
			break;

		case bytecode_generator::FUNC_LOWER: /* S <- S */
			str_0 = StringUtils::tolower(str_0);
			break;

		case bytecode_generator::FUNC_MID: /* S <- S,D,D */
			dbl_t = *--sp_dbl; /* pos */
			if (0 < (int)dbl_t && (unsigned int)dbl_t < str_0.length() && 0 < (int)dbl_0) {
				str_0 = str_0.substr(((int)dbl_t) - 1, (int)dbl_0);
			} else {
				str_0 = "";
			}
			dbl_0 = *--sp_dbl;
			break;

		case bytecode_generator::FUNC_NOW: /* D <- */
			*sp_dbl++ = dbl_0;
			dbl_0 = (double)time(0);
			break;

		case bytecode_generator::FUNC_PI: /* D <- */
			*sp_dbl++ = dbl_0;
			dbl_0 = M_PI;
			break;

		case bytecode_generator::FUNC_PROPER: /* S <- S */
			str_0 = StringUtils::capitalization(str_0);
			break;

		case bytecode_generator::FUNC_RAND: /* D <- */
			*sp_dbl++ = dbl_0;
			dbl_0 = ((double)rand()) / ((double)RAND_MAX);
			break;

		case bytecode_generator::FUNC_REPLACE: /* S <- S,D,D,S */
			dbl_t = *--sp_dbl; /* pos */
			str_t = *--sp_str; /* str */
			if (0 <= ((int)dbl_t) && ((unsigned int)dbl_t) < str_t.length() && 0 <= ((int)dbl_0)) {
				if (((unsigned int)dbl_t) + ((unsigned int)dbl_0) <= str_t.length()) {
					str_0 = str_t.substr(0, ((int)dbl_t)) + str_0 + str_t.substr(((int)dbl_t) + ((int)dbl_0));
				} else {
					str_0 = str_t.substr(0, ((int)dbl_t)) + str_0;
				}
			} else {
				str_0 = "";
			}
			dbl_0 = *--sp_dbl;
			break;

		case bytecode_generator::FUNC_REPT: /* S <- S,D */
			str_t = str_0;
			str_0 = "";
			for (i = 0; i < ((unsigned int)dbl_0); i++) {
				str_0 += str_t;
			}
			dbl_0 = *--sp_dbl;
			break;

		case bytecode_generator::FUNC_RIGHT: /* S <- S,D */
			if (((unsigned int)dbl_0) < str_0.length()) {
				str_0 = str_0.substr(str_0.length() - ((int)dbl_0), ((int)dbl_0));
			}
			dbl_0 = *--sp_dbl;
			break;

		case bytecode_generator::FUNC_SEARCH: /* D <- S,S */
			str_t = *--sp_str;
			*sp_dbl++ = dbl_0;
			dbl_0 = (StringUtils::simpleSearch(str_0, str_t) ? 1.0 : 0.0);
			break;

		case bytecode_generator::FUNC_STR: /* S <- D */
			j = (uint32_t)dbl_0;
			dbl_0 = *--sp_dbl; /* digits */
			if (j < 0) {
				j = 0;
			}
			if (200 < j) {
				j = 200;
			}
			i = (uint32_t)dbl_0;
			dbl_0 = *--sp_dbl; /* length */
			if (i < 0) {
				i = 0;
			}
			if (255 < i) {
				i = 255;
			}
			s = 1;
			if (dbl_0 < 0.0) {
				s = -1;
				dbl_0 = -dbl_0;
			}
			dbl_t = 0.5;
			for (k = 0; k < j; k++) {
				dbl_t = dbl_t / 10;
			}
			dbl_0 = dbl_0 + dbl_t;
			k = 0;
			if (0 < j) {
				dbl_t = dbl_0 - floor(dbl_0);
				p = str_buf + sizeof(str_buf) - j - 2;
				*p++ = '.';
				k++;
				while (k <= j) {
					*p++ = '0' + (uint32_t)(10 * dbl_t);
					k++;
					dbl_t = 10 * dbl_t - (uint32_t)(10 * dbl_t);
				}
			}
			dbl_0 = floor(dbl_0);
			p = str_buf + sizeof(str_buf) - k - 1;
			*p = 0;
			do {
				*--p = '0' + (uint32_t)(10 * (dbl_0 / 10 - floor(dbl_0 / 10)) + 0.5);
				k++;
				dbl_0 = floor(dbl_0 / 10);
			} while (0.0 < dbl_0);
			if (s == -1) {
				*--p = '-';
				k++;
			}
			while (k < i) {
				*--p = ' ';
				k++;
			}
			dbl_0 = *--sp_dbl;
			*sp_str++ = str_0;
			str_0 = p;
			break;

		case bytecode_generator::FUNC_SUBSTITUTE: /* S <- S,S,S */
			str_s = *--sp_str; /* repl */
			str_t = *--sp_str; /* str */
			if (str_s != "") {
				size_t i = 0;
				while ((i = str_t.find(str_s, i)) != string::npos) {
					str_t = str_t.substr(0, i) + str_0 + str_t.substr(i + str_s.size());
					i += str_0.size();
				}
			}
			str_0 = str_t;
			break;

		case bytecode_generator::FUNC_TRIM: /* S <- S */
		{
			size_t i = str_0.find_first_not_of(" \t\n\r");
			size_t j = str_0.find_last_not_of(" \t\n\r");
			if (i != std::string::npos) {
				str_0 = str_0.substr(i, j - i + 1);
			} else {
				str_0 = "";
			}
			break;
		}

		case bytecode_generator::FUNC_UPPER: /* S <- S */
			str_0 = StringUtils::toupper(str_0);
			break;

		case bytecode_generator::FUNC_VALUE: /* D <- S */
			*sp_dbl++ = dbl_0;
			dbl_0 = strtod(str_0.c_str(), &p);
			if (p && *p != 0) {
				dbl_0 = 0;
			}
			str_0 = *--sp_str;
			break;

		case bytecode_generator::AGGR_SUM: /* D <- D* */
			i = *pc++;
			for (j = 1; j < i; j++) {
				--sp_dbl;
				dbl_0 += *sp_dbl;
			}
			break;

		case bytecode_generator::AGGR_PROD: /* D <- D* */
			i = *pc++;
			for (j = 1; j < i; j++) {
				--sp_dbl;
				dbl_0 *= *sp_dbl;
			}
			break;

		case bytecode_generator::AGGR_MIN: /* D <- D* */
			i = *pc++;
			for (j = 1; j < i; j++) {
				--sp_dbl;
				if (*sp_dbl < dbl_0) {
					dbl_0 = *sp_dbl;
				}
			}
			break;

		case bytecode_generator::AGGR_MAX: /* D <- D* */
			i = *pc++;
			for (j = 1; j < i; j++) {
				--sp_dbl;
				if (dbl_0 < *sp_dbl) {
					dbl_0 = *sp_dbl;
				}
			}
			break;

		case bytecode_generator::AGGR_COUNT: /* D <- D* */
			i = *pc++;
			for (j = 1; j < i; j++) {
				--sp_dbl;
			}
			dbl_0 = (Value)i;
			break;

		case bytecode_generator::AGGR_FIRST: /* D <- D* */
			i = *pc++;
			for (j = 1; j < i; j++) {
				--sp_dbl;
				dbl_0 = *sp_dbl;
			}
			break;

		case bytecode_generator::AGGR_LAST: /* D <- D* */
			i = *pc++;
			for (j = 1; j < i; j++) {
				--sp_dbl;
			}
			break;

		case bytecode_generator::AGGR_AVG: /* D <- D* */
			i = *pc++;
			for (j = 1; j < i; j++) {
				--sp_dbl;
				dbl_0 += *sp_dbl;
			}
			dbl_0 = dbl_0 / i;
			break;

		case bytecode_generator::PALO_CUBEDIMENSION: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 1, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Cube * acube = adb->findCubeByName(str_0, 0);
			if (!acube) {
				HANDLE_ISERROR_STR( 1, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorCubeNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			i = (int)dbl_0;
			dbl_0 = *--sp_dbl;

			*sp_str++ = str_0;
			str_0 = "";
			if (0 < i && i <= acube->getDimensions()->size()) {
				str_0 = acube->getDimensions()->at(i - 1)->getName();
			}

		}
			break;

		case bytecode_generator::PALO_ECHILD: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 1, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_STR( 1, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_STR( 1, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			i = (int)dbl_0;
			dbl_0 = *--sp_dbl;

			*sp_str++ = str_0;
			str_0 = "";
			if (0 < i && i <= adim->getChildren(aelm).size()) {
				str_0 = adim->getChildren(aelm).at(i - 1).first->getName();
			}

		}
			break;

		case bytecode_generator::PALO_ECHILDCOUNT: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_dbl++ = dbl_0;
			dbl_0 = (double)(adim->getChildren(aelm).size());

		}
			break;

		case bytecode_generator::PALO_ECOUNT: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_dbl++ = dbl_0;
			dbl_0 = (double)(adim->sizeElements());

		}
			break;

		case bytecode_generator::PALO_EFIRST: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_STR( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_str++ = str_0;
			str_0 = "";
			if (0 < adim->sizeElements()) {
				str_0 = adim->getElements(0)[0]->getName();
			}

		}
			break;

		case bytecode_generator::PALO_EINDENT: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_dbl++ = dbl_0;
			dbl_0 = (double)(aelm->getIndent(adim));

		}
			break;

		case bytecode_generator::PALO_EINDEX: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				//*ptrStatus = ErrorStatus | ErrorElementNotFound;
				//RETURN()
				break;
			}
			str_0 = *--sp_str;

			vector<Element*> aelms = adim->getElements(0);
			vector<Element*>::iterator aelmsi = find(aelms.begin(), aelms.end(), aelm);

			*sp_dbl++ = dbl_0;
			dbl_0 = (double)((aelmsi - aelms.begin()) + 1);

		}
			break;

		case bytecode_generator::PALO_EISCHILD: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 4 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm2 = adim->lookupElementByName(str_0);
			if (!aelm2) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_dbl++ = dbl_0;
			dbl_0 = 0.0;
			const ElementsWeightType aelms = adim->getChildren(aelm);
			for (ElementsWeightType::const_iterator aelmsi = aelms.begin(); aelmsi != aelms.end(); aelmsi++) {
				if ((*aelmsi).first == aelm2) {
					dbl_0 = 1.0;
					break;
				}
			}

		}
			break;

		case bytecode_generator::PALO_ELEVEL: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_dbl++ = dbl_0;
			dbl_0 = (double)(aelm->getLevel(adim));

		}
			break;

		case bytecode_generator::PALO_ENAME: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 1, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_STR( 1, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			i = (0 <= dbl_0 ? (uint32_t)(dbl_0 + 0.5) : (uint32_t)(dbl_0 - 0.5));
			dbl_0 = *--sp_dbl;

			*sp_str++ = str_0;
			str_0 = "";
			if (0 < i && i <= adim->sizeElements()) {
				str_0 = adim->getElements(0)[i - 1]->getName();
			}

		}
			break;

		case bytecode_generator::PALO_ENEXT: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_STR( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_STR( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			vector<Element*> aelms = adim->getElements(0);
			vector<Element*>::iterator aelmsi = find(aelms.begin(), aelms.end(), aelm);

			*sp_str++ = str_0;
			str_0 = "";
			if (aelmsi != aelms.end()) {
				aelmsi++;
				if (aelmsi != aelms.end()) {
					str_0 = (*aelmsi)->getName();
				}
			}

		}
			break;

		case bytecode_generator::PALO_EPARENT: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 1, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_STR( 1, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_STR( 1, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			i = (int)dbl_0;
			dbl_0 = *--sp_dbl;

			*sp_str++ = str_0;
			str_0 = "";
			if (0 < i && i <= adim->getParents(aelm)->size()) {
				str_0 = adim->getParents(aelm)->at(i - 1)->getName();
			}

		}
			break;

		case bytecode_generator::PALO_EPARENTCOUNT: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_dbl++ = dbl_0;
			dbl_0 = (double)(adim->getParents(aelm)->size());

		}
			break;

		case bytecode_generator::PALO_EPREV: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_STR( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_STR( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			vector<Element*> aelms = adim->getElements(0);
			vector<Element*>::iterator aelmsi = find(aelms.begin(), aelms.end(), aelm);

			*sp_str++ = str_0;
			str_0 = "";
			if ((aelmsi != aelms.end()) && (aelmsi != aelms.begin())) {
				aelmsi--;
				if (aelmsi >= aelms.begin()) {
					str_0 = (*aelmsi)->getName();
				}
			}

		}
			break;

		case bytecode_generator::PALO_ESIBLING:
			break;

		case bytecode_generator::PALO_ETOPLEVEL: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_dbl++ = dbl_0;
			dbl_0 = (double)(adim->getLevel());

		}
			break;

		case bytecode_generator::PALO_ETYPE: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_STR( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_STR( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_STR( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_str++ = str_0;
			str_0 = "";
			if (aelm->getElementType() == CONSOLIDATED) {
				str_0 = "consolidated";
			} else if (aelm->getElementType() == NUMERIC) {
				str_0 = "numeric";
			} else if (aelm->getElementType() == STRING) {
				str_0 = "string";
			}

		}
			break;

		case bytecode_generator::PALO_EWEIGHT: {
			Server * asrv = rule->cube->acube->getDatabase()->getServer();

			Database * adb = asrv->lookupDatabaseByName(str_0);
			if (!adb) {
				HANDLE_ISERROR_DBL( 0, 4 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorDatabaseNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Dimension * adim = adb->findDimensionByName(str_0, 0);
			if (!adim) {
				HANDLE_ISERROR_DBL( 0, 3 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorInvalidCoordinates;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm = adim->lookupElementByName(str_0);
			if (!aelm) {
				HANDLE_ISERROR_DBL( 0, 2 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			Element * aelm2 = adim->lookupElementByName(str_0);
			if (!aelm2) {
				HANDLE_ISERROR_DBL( 0, 1 )
				*ptrValue = 0.0;
				*ptrStatus = ErrorStatus | ErrorElementNotFound;
				RETURN()
			}
			str_0 = *--sp_str;

			*sp_dbl++ = dbl_0;
			dbl_0 = 0.0;
			const ElementsWeightType aelms = adim->getChildren(aelm);
			for (ElementsWeightType::const_iterator aelmsi = aelms.begin(); aelmsi != aelms.end(); aelmsi++) {
				if ((*aelmsi).first == aelm2) {
					dbl_0 = (*aelmsi).second;
					break;
				}
			}

		}
			break;
		}
	}
}
}
