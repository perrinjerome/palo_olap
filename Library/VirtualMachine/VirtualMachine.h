////////////////////////////////////////////////////////////////////////////////
/// @brief palo engine
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_bi_suite.txt">
///   http://www.jedox.com/license_palo_bi_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Developed by triagens GmbH, Koeln on behalf of Jedox AG. Intellectual
/// property rights has triagens GmbH, Koeln. Exclusive worldwide
/// exploitation right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Martin Schoenert, triagens GmbH, Cologne, Germany
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
///
/// @author Radu Ialovoi started this file with code migrated from  Engine.h/cpp
///
////////////////////////////////////////////////////////////////////////////////

#ifndef VIRTUAL_MACHINE_H
#define VIRTUAL_MACHINE_H

#include "palo.h"
#include "Parser/Node.h"
#include "Olap/Cube.h"

#include <stack>

namespace palo {
class virtual_machine {
private:

	class preallocated_stacks {
	public:
		typedef std::pair<Value*, std::string*> preallocated_stack_type;
	private:
		static const size_t INITIAL_PREALLOCATED_SIZE = 10;
		static const int32_t STACK_BUFF_SIZE = 64000;
		std::vector<preallocated_stack_type> stacks;
		std::vector<bool> marks;
		Mutex lock;

		size_t size;
	public:

		const preallocated_stack_type& get(size_t& handle)
		{
			WriteLocker w(&lock);
			size_t enter_size = marks.size();
			handle = enter_size;
			for (size_t i = 0; i < enter_size; i++) {
				if (!marks[i]) {
					marks[i] = true;
					handle = i;
					break;
				}
			}

			if (handle == enter_size) {
				stacks.push_back(preallocated_stack_type(new Value[STACK_BUFF_SIZE], new string[STACK_BUFF_SIZE]));
				marks.push_back(true);
			}

			return stacks[handle];
		}

		void release(size_t& handle)
		{
			WriteLocker w(&lock);
			marks[handle] = false;
		}

		preallocated_stacks() :
			size(INITIAL_PREALLOCATED_SIZE)
		{
			for (size_t i = 0; i < INITIAL_PREALLOCATED_SIZE; i++) {
				stacks.push_back(preallocated_stack_type(new Value[STACK_BUFF_SIZE], new string[STACK_BUFF_SIZE]));
				marks.push_back(false);
			}
		}

		~preallocated_stacks()
		{
			for (size_t i = 0; i < stacks.size(); i++) {
				std::pair<Value*, std::string*>& crt = stacks[i];
				delete[] crt.first;
				delete[] crt.second;
			}
		}

		static preallocated_stacks& get_instance()
		{
			static preallocated_stacks instance;
			return instance;
		}
	};
private:
	//volatile general registers (integer).
	uint32_t i;
	uint32_t j;
	uint32_t k;
	int32_t s;

	//volatile time register
	struct tm t;

	//volatile string register
	string str_t;
	string str_s;

	//volatile Value register
	Value dbl_t;

	//volatile char* register
	char * p;

	//volatile char array register
	char * str_buf;

	//volatile EPath register
	EPath path_t;

	//volatile Status register
	Status status_t;

	//program counter
	Bytecode * pc;

	//invariants.
	const User* m_user;
	EMemoryContext* m_mem_context;
	Environment* m_env;

	//stack pointers
	Value * sp_dbl;
	string * sp_str;

	//handle of preallocated mem
	size_t handle;

	double dummy41;

	//string buffer
	char str_bp[256];

	//registers holding non-volatile values
	Value dbl_0;
	string str_0;

	//placeholders for return values
	Value * ptrValue;
	Status * ptrStatus;

	struct machine_state {
		EPath path;
		ERule* rule;
		Value defValue;
		Status defStatus;
		uint8_t is_base;
		Bytecode * pc;
		Value* sp_dbl;
		string* sp_str;
		bytecode_generator::WM_OPCODES return_pos;
	};

	class machine_stack : private std::vector<machine_state> {
	private:
		static const unsigned int MAX_STACK_SIZE = 50 * 1024;
	public:
		bool is_empty()
		{
			return empty();
		}
		void push_state(EPath path, ERule* rule, Value defValue, Status defStatus, uint8_t is_base, Bytecode *pc, bytecode_generator::WM_OPCODES return_pos, Value* sp_dbl, string* sp_str)
		{
			machine_state state;
			for (size_t i = 0; i < rule->cube->nrDimensions; i++) {
				state.path[i] = path[i];
			}
			state.rule = rule;
			state.defValue = defValue;
			state.defStatus = defStatus;
			state.is_base = is_base;
			state.pc = pc;
			state.return_pos = return_pos;
			state.sp_dbl = sp_dbl;
			state.sp_str = sp_str;

			push_back(state);
		}

		Bytecode pop_state(EPath path, ERule** rule, Value* defValue, Status* defStatus, uint8_t* is_base, Bytecode **pc, Value** sp_dbl, string** sp_str)
		{
			bytecode_generator::WM_OPCODES return_pos;
			machine_state state = back();
			for (size_t i = 0; i < state.rule->cube->nrDimensions; i++) {
				path[i] = state.path[i];
			}
			*rule = state.rule;
			*defValue = state.defValue;
			*defStatus = state.defStatus;
			*is_base = state.is_base;
			*pc = state.pc;
			return_pos = state.return_pos;
			*sp_dbl = state.sp_dbl;
			*sp_str = state.sp_str;

			pop_back();
			return return_pos;
		}

	};

	machine_stack m_stack;

#define PUSH( ret_pos )  \
                            m_stack.push_state( path, rule, defValue, defStatus, is_base, pc, ret_pos, sp_dbl, sp_str ); \
                            m_mem_context->m_recursion_stack.push( rule, path );

#define RETURN() \
                        if ( m_stack.is_empty() ) { return; } \
                        else { \
                            ret_pc = m_stack.pop_state( path, &rule, &defValue, &defStatus, &is_base, &pc, &sp_dbl, &sp_str  ); \
                            m_mem_context->m_recursion_stack.pop(); \
                            continue; }

private:
#define HANDLE_ISERROR_DBL(n,m) if ( *pc == bytecode_generator::FUNC_ISERROR_DBL ) { \
							*sp_dbl++ = dbl_0; sp_dbl -= (n); dbl_0 = *--sp_dbl; \
							*sp_str++ = str_0; sp_str -= (m); str_0 = *--sp_str; \
							*sp_dbl++ = dbl_0; dbl_0 = 1.0; \
							pc++; }

#define HANDLE_ISERROR_STR(n,m) if ( *pc == bytecode_generator::FUNC_ISERROR_STR ) { \
							*sp_dbl++ = dbl_0; sp_dbl -= (n); dbl_0 = *--sp_dbl; \
							*sp_str++ = str_0; sp_str -= (m); str_0 = *--sp_str; \
							*sp_dbl++ = dbl_0; dbl_0 = 1.0; \
							pc++; }
#define RETURN_ERROR return

	long StringToInt(const std::string& str)
	{
		char *p;

		if (str.empty())
			return 0;

		long i = strtol(str.c_str(), &p, 10);

		if (0 != *p)
			return 0;
		return i;
	}

	inline bool is_consolidation(EPath& path, ECube& cube)
	{
		bool result = false;
		/* check if consolidated                                               */
		for (uint32_t d = 0; d < cube.nrDimensions; d++) {
			char ElemType = cube.dimensions[d]->type[path[d]];
			if (ElemType == 'C') {
				result = true;
			} else if (ElemType == 'S') {
				result = false;
				break;
			}
		}

		return result;
	}

	inline bool is_string(EPath& path, ECube& cube)
	{
		/* check if consolidated                                               */
		for (uint32_t d = 0; d < cube.nrDimensions; d++) {
			if (cube.dimensions[d]->type[path[d]] == 'S') {
				return true;
			}
		}

		return false;
	}

	ERule* get_matching_rule(EElementId* a_path, TypeList* a_list)
	{
		/* loop over the rules                                             */
		for (ERule * crt_rule = (ERule*)FirstItemList( a_list ); crt_rule != 0; crt_rule = (ERule*)NextItemList( a_list, crt_rule )) {
			if (IsBasePathInArea(crt_rule->dest_area, a_path)) {
				return crt_rule;
			}
		}

		return NULL;
	}

public:
	void compute(EPath path, ERule* rule, Value defValue, Status defStatus, uint8_t is_base);

	virtual_machine(Environment* env, EMemoryContext* mem_context, Value* ptrValue, Status* ptrStatus) :
		m_user(env->auser), m_mem_context(mem_context), m_env(env), dbl_0(0.0), str_0(""), ptrValue(ptrValue), ptrStatus(ptrStatus)

	{
		const preallocated_stacks::preallocated_stack_type& stack = preallocated_stacks::get_instance().get(handle);

		sp_dbl = stack.first;
		sp_str = stack.second;

		str_buf = str_bp;
	}

	~virtual_machine()
	{
		preallocated_stacks::get_instance().release(handle);
	}

};
}

#endif
