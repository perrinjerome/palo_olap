////////////////////////////////////////////////////////////////////////////////
/// @brief simple cache
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Radu Racariu radu@yalos-solutions.com
/// @author Radu Ialovoi ialovoi@yalos-solutions.com
////////////////////////////////////////////////////////////////////////////////


#include "Cache/SimpleCache.h"
#include "Cache/AreaIterator.h"
#include "Olap/CellPath.h"
#include "Olap/HashAreaStorage.h"
#include "Olap/Element.h"
#include "Thread/ReadLocker.h"
#include "Thread/WriteLocker.h"
#include "Thread/UpgradeLocker.h"

namespace palo {

bool SimpleCache::getValue(const CellPath& path, cache_value_type& value)
{
	READ_LOCK();

	if (!valid) {
		UPGRADE_LOCK()
		m_cache.clear();
		key_list.clear();
		valid = true;
		return false;
	}

	const_cache_type_iterator val = m_cache.find(*path.getPathIdentifier());
	if (m_cache.end() != val) {
		value = val->second;
		return true;
	}
	return false;
}

bool SimpleCache::getValues(const vector<CellPath*>& sourceArea, vector<CellPath*>& nonCachedArea, cache_block& storage)
{
	READ_LOCK();

	if (!valid) {
		UPGRADE_LOCK()
		m_cache.clear();
		key_list.clear();
		nonCachedArea = sourceArea;
		valid = true;
		return false;
	}

	const_cache_type_iterator end = m_cache.end();
	bool result = true;

	for (vector<CellPath*>::const_iterator i = sourceArea.begin(), e = sourceArea.end(); i != e; i++) {
		const IdentifiersType* path = (*i)->getPathIdentifier();
		const_cache_type_iterator val = m_cache.find(*path);
		if (end != val) {
			storage[*path] = val->second;
		} else {
			result = false;
			nonCachedArea.push_back((*i));
		}
	}

	return result;
}

bool SimpleCache::getValues(const vector<vector<Element*> >& sourceArea, vector<vector<Element*> >& nonCachedArea, cache_block& storage)
{
	READ_LOCK();

	const_cache_type_iterator end = m_cache.end();
	bool result = true;

	area_iterator crt_path(sourceArea);
	area_iterator end_area = area_iterator::end();
	size_t dim_size = crt_path.size();

	if (!valid) {
		UPGRADE_LOCK()
		m_cache.clear();
		key_list.clear();
		nonCachedArea = sourceArea;
		valid = true;
		return end_area == crt_path;
	}

	nonCachedArea.resize(dim_size);

	while (end_area != crt_path) {
		const IdentifiersType& path = *crt_path;

		const_cache_type_iterator val = m_cache.find(path);
		if (end != val) {
			storage[path] = val->second;
		} else {
			result = false;
			for (size_t i = 0; i < dim_size; i++) {
				if (nonCachedArea[i].end() != std::find(nonCachedArea[i].begin(), nonCachedArea[i].end(), crt_path.at(i)))
					continue;
				nonCachedArea[i].push_back(crt_path.at(i));
			}
		}

		++crt_path;
	}

	return result;
}

void SimpleCache::putValue(const vector<vector<Element*> >& area, HashAreaStorage& storage)
{
	if (!cacheBarrier) {
		return;
	}

	WRITE_LOCK();

	if (!valid) {
		m_cache.clear();
		key_list.clear();
		valid = true;
	}

	if (area.size()  > cacheBarrier) {
		// TODO: -jj- here we should just cache first cacheBarrier CALCULATED cells from area and exit
		return;
	}

	TEST()
	area_iterator crt_path(area);
	area_iterator end = area_iterator::end();

	double value;
	uint16_t status;
	size_t cacheCount = 0;

	while (end != crt_path) {
		const IdentifiersType& path = *crt_path;

		if (!storage.getValue(path, value, status))
			status = NOT_FOUND;

		if (0 == (++cacheCount % cacheClearBarrierCells) && key_list.size() > cacheBarrier) {
			purgeCache();
			cacheCount = 0;
		}
		// if not found. insert key here
		if (m_cache.end() == m_cache.find(path))
			key_list.push_back(path);

		m_cache[path] = cache_value_type(value, status);

		++crt_path;
	}

}

const void SimpleCache::putValue(const IdentifiersType* path, double value, uint16_t stat)
{
	if (!cacheBarrier) {
		return;
	}

	WRITE_LOCK();

	if (!valid) {
		m_cache.clear();
		key_list.clear();
		valid = true;
	}

	TEST()
	// if not found. insert key here
	if (m_cache.end() == m_cache.find(*path)) {
		key_list.push_back(*path);
	}

	m_cache[*path] = cache_value_type(value, stat);

	return;
}

const void SimpleCache::putValue(const CellPath& path, double value, uint16_t stat)
{
	return putValue(path.getPathIdentifier(), value, stat);
}

void SimpleCache::invalidate()
{
	WRITE_LOCK();
	valid = false;
}

void SimpleCache::purgeCache()
{
	// made sure in configCacheParams settings are correct cacheClearBarrierCells < m_cache.size()
	for (size_t i = 0; i < key_list.size() - (cacheBarrier - cacheClearBarrierCells); i++) {
		m_cache.erase(key_list.front());
		key_list.pop_front();
	}
}

void SimpleCache::configCacheParams(size_t cBarrier, size_t cClearBarrierCells)
{
	// silently ignore invalid settings; use defaults
	if (cBarrier && cBarrier <= cClearBarrierCells) {
		return;
	}
	WRITE_LOCK();
	cacheBarrier = cBarrier;
	cacheClearBarrierCells = cClearBarrierCells;
}

IdentifierType SimpleCache::ruleFromStat(const uint16_t& stat)
{
	if (IsRuleStatus( stat )) {
		return (stat) & RuleNumberMask;
	} else {
		return Rule::NO_RULE;
	}
}

uint16_t SimpleCache::statFromRule(IdentifierType rule)
{
	if (Rule::NO_RULE != rule) {
		return RuleStatus | rule;
	} else {
		return BaseStatus;
	}
}

void SimpleCache::dispatch(const cache_value_type& val, IdentifierType& idRule, unsigned int& errorNumber)
{
	uint16_t status = val.second;

	if (IsErrorStatus( status )) {
		idRule = Rule::NO_RULE;
		errorNumber = AreaResultStorage::getErrorType(&status);
	} else if (IsEmptyStatus( status )) {
		idRule = Rule::NO_RULE;
		errorNumber = 0;
	} else if (IsRuleStatus( status )) {
		idRule = (status) & RuleNumberMask;
		errorNumber = 0;
	}
	// isOK
	else {
		idRule = Rule::NO_RULE;
		errorNumber = 0;
	}
	return;
}

}
