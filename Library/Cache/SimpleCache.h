////////////////////////////////////////////////////////////////////////////////
/// @brief simple cache
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Radu Racariu radu@yalos-solutions.com
/// @author Radu Ialovoi ialovoi@yalos-solutions.com
////////////////////////////////////////////////////////////////////////////////

#ifndef SIMPLE_CACHE_H
#define SIMPLE_CACHE_H

#include "Thread/ReadWriteLock.h"

namespace palo {
class HashAreaStorage;
class CellPath;
class Element;
class SimpleCache {
private:
	static const size_t CACHE_BARRIER_SIZE = 10000;
	static const size_t CACHE_TRESHHOLD_SIZE = 1000;
	size_t cacheBarrier;
	size_t cacheClearBarrierCells;
public:
	typedef std::pair<double, uint16_t> cache_value_type;
	typedef IdentifiersType cache_key_type;
	typedef std::map<cache_key_type, cache_value_type> cache_type;
	typedef std::map<cache_key_type, cache_value_type>::iterator cache_type_iterator;
	typedef std::map<cache_key_type, cache_value_type>::const_iterator const_cache_type_iterator;
	typedef std::map<cache_key_type, cache_value_type> cache_block;
	typedef std::pair<IdentifierType, IdentifierType> query_cache_key;
	typedef std::map<query_cache_key, cache_block> query_cache_type;
	typedef std::map<query_cache_key, cache_block>::iterator query_cache_iterator;
private:
#define     READ_LOCK()       ReadLocker  readLock( &lock );
#define     WRITE_LOCK()      WriteLocker  writeLock( &lock );
#define     UPGRADE_LOCK()    UpgradeLocker  writeLock( &lock );
#define     TEST()            if ( cacheBarrier <= m_cache.size() ) purgeCache();
	const cache_value_type cacheDefaultVal;
	bool valid;
	ReadWriteLock lock;

	cache_block m_cache;
	std::deque<cache_key_type> key_list;
	void purgeCache();
public:
	static const uint16_t NOT_FOUND = 0xF000;

	SimpleCache() :
		cacheBarrier(CACHE_BARRIER_SIZE), cacheClearBarrierCells(CACHE_TRESHHOLD_SIZE), cacheDefaultVal(0.0, 0), valid(true)
	{
	}

	bool getValue(const CellPath& path, cache_value_type& value);
	bool getValues(const vector<CellPath*>& sourceArea, vector<CellPath*>& nonCachedArea, cache_block& storage);
	bool getValues(const vector<vector<Element*> >& sourceArea, vector<vector<Element*> >& nonCachedArea, cache_block& storage);

	const void putValue(const IdentifiersType* path, double value, uint16_t stat);
	const void putValue(const CellPath& path, double value, uint16_t stat);
	void putValue(const vector<vector<Element*> >& area, HashAreaStorage& storage);

	void invalidate();
	void configCacheParams(size_t cacheBarrier, size_t cacheClearBarrierCells);

	static IdentifierType ruleFromStat(const uint16_t& stat);
	static uint16_t statFromRule(IdentifierType rule);
	static void dispatch(const cache_value_type& val, IdentifierType& idRule, unsigned int& errorNumber);

};

}

#endif
