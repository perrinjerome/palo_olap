////////////////////////////////////////////////////////////////////////////////
/// @brief iterates on an area
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Radu Racariu radu@yalos-solutions.com
/// @author Radu Ialovoi ialovoi@yalos-solutions.com
////////////////////////////////////////////////////////////////////////////////


#ifndef AREA_ITERATOR_H
#define AREA_ITERATOR_H

#include "Olap/Element.h"

namespace palo {
class area_iterator {
private:
	vector<vector<Element*>::const_iterator> begin_iterators;
	vector<vector<Element*>::const_iterator> end_iterators;
	vector<vector<Element*>::const_iterator> crt_iterators;

	IdentifiersType value;

	size_t dim_size;
	bool valid;

	area_iterator()
	{
		valid = false;
	}
public:
	static area_iterator end()
	{
		return area_iterator();
	}

	area_iterator(const vector<vector<Element*> >& raw_area)
	{
		valid = true;
		dim_size = raw_area.size();
		begin_iterators.resize(dim_size);
		end_iterators.resize(dim_size);
		crt_iterators.resize(dim_size);
		value.resize(dim_size);

		for (size_t i = 0; i < dim_size; i++) {
			begin_iterators[i] = raw_area[i].begin();
			end_iterators[i] = raw_area[i].end();
			if (begin_iterators[i] == end_iterators[i]) {
				valid = false;
				return;
			}
			crt_iterators[i] = begin_iterators[i];
		}
	}

	Element* at(size_t pos)
	{
		if (!valid)
			return NULL;
		return *(crt_iterators[pos]);
	}

	size_t size()
	{
		return dim_size;
	}

	void operator++()
	{
		if (!valid)
			return;
		for (size_t i = dim_size - 1; i >= 0; i--) {
			crt_iterators[i]++;

			if (crt_iterators[i] == end_iterators[i]) {
				if (0 == i) {
					valid = false;
					return;
				}

				crt_iterators[i] = begin_iterators[i];
			} else
				return;
		}
	}

	const IdentifiersType& operator*()
	{
		for (size_t i = 0; i < dim_size; i++) {
			value[i] = (*crt_iterators[i])->getIdentifier();
		}

		return value;
	}

	bool operator ==(const area_iterator& other)
	{
		if (this == &other)
			return true;
		if (valid != other.valid)
			return false;
		if (!valid)
			return true;
		if (dim_size != other.dim_size)
			return false;

		for (size_t i = 0; i < dim_size; i++) {
			if (crt_iterators[i] != other.crt_iterators[i])
				return false;
		}

		return true;
	}

	bool operator !=(const area_iterator& other)
	{
		return !(*this == other);
	}
};
}

#endif
