////////////////////////////////////////////////////////////////////////////////
/// @brief Area
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "PaloJobs/AreaJob.h"

#include "Olap/Engine.h"

#include "Olap/ExportStorage.h"

#include "InputOutput/Statistics.h"

namespace palo {

// allow area to grow this factor
const double AreaJob::MAXIMAL_FACTOR = 1.5;

// ignore grow factor up to this size;
uint64_t AreaJob::MAXIMAL_SIZE = 200000;

size_t AreaJob::s_max_cell_count;

HashAreaStorage* AreaJob::computeDoubleArea(const vector<Dimension*> * dimensions, vector<vector<Element*> >& numericAreaElements, size_t _numCompute)
{
	size_t numDimension = dimensions->size();
	size_t numCompute = _numCompute;

	vector<map<Element*, uint32_t> > hashMapping;
	vector<CellPath> foundRulePaths;
	vector<Cube::CellValueType> foundRuleValues;
	vector<CellPath> notFoundRulePaths;
	vector<CellPath> consolidatedPaths;
	vector<vector<Element*> > increasedNumericElements;

	if (cube->sizeFilledNumericCells() == 0 && (!cube->hasRule() || !memory_context.calcRules())) {
		// do not compute values for an empty cube
		vector<map<Element*, uint32_t> > hashMapping;
		AreaResultStorage::computeAreaParameters(cube, numericAreaElements, hashMapping);
		return new HashAreaStorage(cube, hashMapping);
	}


	{
	vector<vector<Element*> > newNumericElements = eliminateDirectRules(numericAreaElements, numCompute, foundRulePaths, foundRuleValues, notFoundRulePaths, consolidatedPaths);

	increasedNumericElements = computeIncreasedArea(newNumericElements, numCompute);

	vector<vector<Element*> > allElements(increasedNumericElements);
	for (size_t i = 0; i < numDimension; i++) {
		if (newNumericElements[i].size() != numericAreaElements[i].size()) {
			set<Element*> all;
			all.insert(increasedNumericElements[i].begin(), increasedNumericElements[i].end());
			all.insert(numericAreaElements[i].begin(), numericAreaElements[i].end());
			allElements[i].clear();
			allElements[i].insert(allElements[i].end(), all.begin(), all.end());
		}
	}

	AreaResultStorage::computeAreaParameters(cube, allElements, hashMapping);
	}

	HashAreaStorage* nonCachedStorage = new HashAreaStorage(cube, hashMapping);


	{
	// fill double storage
	if (numCompute > 0) {
		fillDoubleResultStorage(dimensions, nonCachedStorage, increasedNumericElements);
	}

	if (hasConsolidatedValues(numericAreaElements)) {
		processConsolidatedValues(nonCachedStorage, foundRulePaths, foundRuleValues, notFoundRulePaths, consolidatedPaths, numDimension);
	}

	if (memory_context.calcRules()) {
		cube->cache.putValue(numericAreaElements, *nonCachedStorage);
	}
	}

	return nonCachedStorage;

}

void AreaJob::fillDoubleResultStorage(const vector<Dimension*>* dimensions, HashAreaStorage* doubleStorage, vector<ElementsType>& numericAreaElements)
{
	// Now create a vector that contains no cons elements, whose basic elements of
	// All is included in the Area:
	vector<vector<Element*> > adjustedNumericArea(numericAreaElements.size());
	vector<vector<vector<pair<IdentifierType, double> > > > consMapping(numericAreaElements.size());
	bool hasCons = computeAdjustedNumericArea(dimensions, numericAreaElements, adjustedNumericArea, consMapping);

	cube->fillHashAreaStorage2(doubleStorage, &adjustedNumericArea, &memory_context);

	if (hasCons) {
		doubleStorage->computeConsolidations(numericAreaElements, consMapping);
	}
}

bool AreaJob::computeAdjustedNumericArea(const vector<Dimension*>* dimensions, vector<vector<Element*> >& numericAreaElements, vector<vector<Element*> >& adjustedNumericArea, vector<vector<vector<pair<IdentifierType, double> > > >& consIdentifiers)
{
	bool hasCons = false;

	for (size_t i = 0; i < numericAreaElements.size(); i++) {
		vector<Element*>& dimElements = numericAreaElements[i];
		vector<vector<pair<IdentifierType, double> > >& cons2Children = consIdentifiers[i];
		cons2Children.resize(dimElements.size());

		// copy element identifiers to set
		set<IdentifierType> dimIdentifiers;
		for (vector<Element*>::iterator ei = dimElements.begin(); ei != dimElements.end(); ei++) {
			dimIdentifiers.insert((*ei)->getIdentifier());
		}

		Dimension* dimension = dimensions->at(i);
		size_t num = 0;
		for (vector<Element*>::iterator ei = dimElements.begin(); ei != dimElements.end(); ei++) {
			vector<pair<IdentifierType, double> >& childrenPairs = cons2Children.at(num++);

			Element* element = *ei;
			if (element->getElementType() == CONSOLIDATED) {
				bool foundAll = true;
				const map<IdentifierType, double>* children = element->getBaseElements(dimension);
				map<IdentifierType, double>::const_iterator childIter = children->begin();
				for (; childIter != children->end(); childIter++) {
					IdentifierType childId = childIter->first;
					set<IdentifierType>::iterator find = dimIdentifiers.find(childId);
					if (find == dimIdentifiers.end()) {
						// base element not found
						foundAll = false;
						break;
					}
				}
				if (foundAll) {
					childIter = children->begin();
					for (; childIter != children->end(); childIter++) {
						childrenPairs.push_back(make_pair(childIter->first, childIter->second));
					}
					hasCons = true;
				} else {
					adjustedNumericArea[i].push_back(element);
				}
			} else {
				adjustedNumericArea[i].push_back(element);
			}
		}
	}
	return hasCons;
}

size_t AreaJob::hashCalc(size_t lasty)
{
	size_t m = 6905;
	size_t a = 2134;
	size_t b = 2160;

	return (a * lasty + b) % m;
}

vector<vector<vector<Element*> > > AreaJob::computeSubAreas(vector<vector<Element*> >& adjustedNumericArea, size_t maxAreas, bool useFirstDimensions)
{

	//TODO: Validate the split algorithm

	vector<vector<vector<Element*> > > result;

	// get number of dimension
	size_t numDim = 0;
	size_t numElements = 0;
	size_t length = adjustedNumericArea.size();

	if (useFirstDimensions) {
		// select first or second dimension
		if (adjustedNumericArea[0].size() > adjustedNumericArea[1].size()) {
			numDim = 0;
			numElements = adjustedNumericArea[0].size();
		} else {
			numDim = 1;
			numElements = adjustedNumericArea[1].size();
		}
	} else {
		// select largest dimension
		for (size_t i = 0; i < length; i++) {
			if (adjustedNumericArea[i].size() > numElements) {
				numDim = i;
				numElements = adjustedNumericArea[i].size();
			}
		}
	}

	size_t numAreas = maxAreas;
	if (maxAreas > numElements) {
		numAreas = numElements;
	}

	result.resize(numAreas);

	size_t y = 3778;
	for (size_t i = 0; i < length; i++) {

		if (i == numDim) {
			for (size_t idArea = 0; idArea < numAreas; idArea++) {
				vector<Element*> newDimension;
				result[idArea].push_back(newDimension);
			}
			vector<Element*> dimElements = adjustedNumericArea[numDim];

			for (size_t x = numElements; x > 0; x--) {
				size_t idArea = x % numAreas;
				y = hashCalc(y);
				size_t step = y % x;
				vector<Element*>::iterator next = (dimElements.begin() + step);

				result[idArea][numDim].push_back(*next);
				dimElements.erase(next);
			}
		} else {
			for (size_t idArea = 0; idArea < numAreas; idArea++) {
				result[idArea].push_back(adjustedNumericArea[i]);
			}
		}
	}

	return result;
}

bool AreaJob::hasConsolidatedValues(vector<vector<Element*> >& area)
{
	size_t length = area.size();

	for (size_t i = 0; i < length; i++) {
		for (vector<Element*>::iterator ei = area[i].begin(); ei != area[i].end(); ei++) {
			Element* element = *ei;
			if (element->getElementType() == CONSOLIDATED) {
				return true;
			}
		}
	}

	return false;
}

bool AreaJob::processConsolidatedValues(HashAreaStorage* doubleStorage, vector<CellPath>& foundRulePaths, vector<Cube::CellValueType>& foundRuleValues, vector<CellPath>& notFoundRulePaths, vector<CellPath>& consolidatedPaths, size_t numDimension)
{
	// add found rule values
	vector<CellPath>::iterator ri = foundRulePaths.begin();
	vector<Cube::CellValueType>::iterator vi = foundRuleValues.begin();
	for (; ri != foundRulePaths.end(); ri++, vi++) {
		if (IsErrorStatus( vi->rule )) {
			doubleStorage->setValue(ri->getPathIdentifier(), vi->doubleValue, vi->rule);
		} else {
			doubleStorage->setValue(ri->getPathIdentifier(), vi->doubleValue, RuleStatus | vi->rule);
		}
	}

	// add not found rule values
	ri = notFoundRulePaths.begin();
	for (; ri != notFoundRulePaths.end(); ri++) {
		doubleStorage->setValue(ri->getPathIdentifier(), 0.0, 0);
	}

//	if (!cube->hasRule() || !memory_context.calcRules())
//		return false;
//
//	EMemoryContext ctxt;
//	IdentifierType* buf = (IdentifierType*)alloca(sizeof(IdentifierType) * numDimension);
//	ri = consolidatedPaths.begin();
//	for (; ri != consolidatedPaths.end(); ri++) {
//		const IdentifiersType * path_ids = ri->getPathIdentifier();
//		for (size_t i = 0; i < numDimension; i++) {
//			buf[i] = path_ids->at(i);
//		}
//		Cube::CellValueType cellValue;
//		try {
//			if (ExportStorage::findIndirectRuleMatch(cube, cube->getRules(0), buf, cellValue, &ctxt)) {
//				doubleStorage->setValue(path_ids, cellValue.doubleValue, BaseStatus);
//			}
//		} catch (ErrorException& e) {
//			if ((e.getErrorType() == ErrorException::ERROR_RULE_HAS_CIRCULAR_REF) || (e.getErrorType() == ErrorException::ERROR_INTERNAL)) {
//				//mask
//				doubleStorage->setValue(path_ids, 123456789, ErrorStatus + ErrorCircularRule);
//			} else {
//				throw e;
//			}
//		}
//
//	}

	return false;
}

void AreaJob::addBaseElements(Dimension* dimension, vector<Element*>& elements, set<Element*>& addedBaseElements)
{

	for (vector<Element*>::iterator i = elements.begin(); i != elements.end(); i++) {
		Element* element = *i;

		addedBaseElements.insert(element);

		const map<IdentifierType, double>* children = element->getBaseElements(dimension);
		map<IdentifierType, double>::const_iterator childIter = children->begin();

		for (; childIter != children->end(); childIter++) {
			IdentifierType childId = childIter->first;
			Element* children = dimension->lookupElement(childId);
			if (children) {
				addedBaseElements.insert(children);
			}
		}
	}
}

struct CompareFactor : public std::binary_function<const pair<double, size_t>&, const pair<double, size_t>&, bool> {
	CompareFactor()
	{
	}

	bool operator ()(const pair<double, size_t>& left, const pair<double, size_t>& right)
	{
		return left.first < right.first;
	}
};

vector<vector<Element*> > AreaJob::computeIncreasedArea(vector<vector<Element*> >& numericAreaElements, size_t& numCompute)
{

	size_t numDimensions = numericAreaElements.size();
	vector<vector<Element*> > result(numericAreaElements);

	vector<set<Element*> > increasedElementsSet(numDimensions);
	const vector<Dimension*> * dimensions = cube->getDimensions();

	vector<pair<double, size_t> > factor2pos;
	for (size_t i = 0; i < numDimensions; i++) {

		addBaseElements(dimensions->at(i), result[i], increasedElementsSet[i]);

		if (increasedElementsSet[i].size() > result[i].size()) {
			double dimensionFactor = double(increasedElementsSet[i].size()) / double(result[i].size());
			factor2pos.push_back(make_pair(dimensionFactor, i));
		}
	}

	if (factor2pos.size() > 0) {
		CompareFactor compare;
		sort(factor2pos.begin(), factor2pos.end(), compare);

		double factor = 1.0;
		while (!factor2pos.empty()) {

			double dimensionFactor = factor2pos[0].first;
			size_t pos = factor2pos[0].second;

			double f = factor * dimensionFactor;

			if (f <= MAXIMAL_FACTOR || numCompute * f <= MAXIMAL_SIZE) {
				if (Logger::isDebug()) {
					Logger::debug << "moving dimension #" << pos << " to fixed list" << endl;
				}

				result[pos].clear();
				result[pos].insert(result[pos].end(), increasedElementsSet[pos].begin(), increasedElementsSet[pos].end());

				factor2pos.erase(factor2pos.begin());
				factor = f;
			} else {
				break;
			}
		}

	}

	numCompute = 1;
	for (size_t i = 0; i < numDimensions; i++) {
		numCompute *= result[i].size();
	}

	if (Logger::isDebug()) {
		Logger::debug << "   increased area size = " << numCompute << endl;
	}

	return result;
}

vector<vector<Element*> > AreaJob::eliminateDirectRules(vector<vector<Element*> >& numericAreaElements, size_t& numCompute, vector<CellPath>& foundRulePaths, vector<Cube::CellValueType>& foundRuleValues, vector<CellPath>& notFoundRulePaths, vector<CellPath>& consolidatedPaths)
{

	Statistics::Timer timer("AreaJob::eliminateDirectRules");

	if (!memory_context.calcRules() || !cube->hasRule() || !EngineAreaHasRule(&numericAreaElements, cube, 0, &memory_context)) {

		size_t length = numericAreaElements.size();
		vector<size_t> combinations(length);
		bool eval = true;
		vector<Element*> path(length);
		for (size_t i = 0; i < length;) {
			if (eval) {
				bool isCon = false;
				for (size_t j = 0; j < length; j++) {
					Element * e = numericAreaElements[j][combinations[j]];
					path[j] = e;
					if (e->getElementType() == CONSOLIDATED) {
						isCon = true;
					}
				}
				if (isCon) {
					CellPath cp(cube, &path);
					consolidatedPaths.push_back(cp);
				}
			}
			size_t position = combinations[i];
			const vector<Element*>& dim = numericAreaElements[i];
			if (position + 1 < dim.size()) {
				combinations[i] = (int)position + 1;
				i = 0;
				eval = true;
			} else {
				i++;
				for (size_t k = 0; k < i; k++) {
					combinations[k] = 0;
				}
				eval = false;
			}
		}

		return numericAreaElements;
	}

	size_t length = numericAreaElements.size();

	vector<vector<Element*> > result(length);

	vector<set<Element*> > newElementsSet(length);

	vector<size_t> combinations(length);
	bool eval = true;
	vector<Element*> path(length);
	// bool found;

	Cube::CellValueType cellValue;

	// loop over area
	for (size_t i = 0; i < length;) {

		if (eval) {
			bool isCon = false;

			// construct path and look for consolidated elements
			for (size_t j = 0; j < length; j++) {
				Element * e = numericAreaElements[j][combinations[j]];
				path[j] = e;

				if (e->getElementType() == CONSOLIDATED) {
					isCon = true;
				}
			}

			if (isCon) {
				// found a consolidated path
				CellPath cp(cube, &path);
				Value value = 0.0;
				Status status = BaseStatus;
				IdentifierType epath[MAX_DIMENSIONS];
				/* EPath epath; */
				for (size_t j = 0; j < length; j++) {
					epath[j] = path[j]->getIdentifier();
				}
				if (EngineCellHasRule(&value, &status, epath, cube, 0, &memory_context)) {
					if (IsErrorStatus( status )) {
						foundRulePaths.push_back(cp);
						cellValue.type = NUMERIC;
						cellValue.doubleValue = value;
						cellValue.rule = status;
						foundRuleValues.push_back(cellValue);
					} else {
						foundRulePaths.push_back(cp);
						cellValue.type = NUMERIC;
						cellValue.doubleValue = value;
						cellValue.rule = status & RuleNumberMask;
						foundRuleValues.push_back(cellValue);
					}
				} else {
					consolidatedPaths.push_back(cp);
					// add path elements to newElementsSet
					for (size_t j = 0; j < length; j++) {
						newElementsSet[j].insert(path[j]);
					}
				}
			} else {
				// add path elements to newElementsSet
				for (size_t j = 0; j < length; j++) {
					newElementsSet[j].insert(path[j]);
				}
			}
		}

		size_t position = combinations[i];
		const vector<Element*>& dim = numericAreaElements[i];

		if (position + 1 < dim.size()) {
			combinations[i] = (int)position + 1;
			i = 0;
			eval = true;
		} else {
			i++;

			for (size_t k = 0; k < i; k++) {
				combinations[k] = 0;
			}

			eval = false;
		}
	}

	numCompute = 1;
	for (size_t j = 0; j < length; j++) {
		result[j].insert(result[j].end(), newElementsSet[j].begin(), newElementsSet[j].end());
		numCompute *= result[j].size();
	}

	return result;
}
}
