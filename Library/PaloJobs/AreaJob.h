////////////////////////////////////////////////////////////////////////////////
/// @brief area
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
/// @author Tobias Lauer, Jedox AG, Freiburg, Germany
/// @author Zurab Khadikov, Jedox AG, Freiburg, Germany
/// @author Christoffer Anselm, Jedox AG, Freiburg, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_AREA_JOB_H
#define PALO_JOBS_AREA_JOB_H 1

#include "palo.h"

#include "Exceptions/ErrorException.h"
#include "Olap/CellPath.h"
#include "Olap/Rule.h"
#include "Olap/Cube.h"
#include "Olap/AreaStringResultStorage.h"
#include "PaloDispatcher/DirectPaloJob.h"
#include "PaloJobs/AreaSubJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief area
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS AreaJob : public DirectPaloJob {

public:

	// allow area to grow this factor
	static const double MAXIMAL_FACTOR;

	// ignore grow factor up to this size;
	static uint64_t MAXIMAL_SIZE;

	static size_t s_max_cell_count;

	static void setMaximalAreaGrowSize(uint64_t size)
	{
		MAXIMAL_SIZE = size;
	}

	static void setMaxCellCount(size_t size)
	{
		s_max_cell_count = size;
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	AreaJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest),max_cell_count(s_max_cell_count)
	{
	}

protected:
	size_t max_cell_count;
	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructs a result storage for numeric areas
	/// The hash mapping is filled here
	////////////////////////////////////////////////////////////////////////////////

	HashAreaStorage* computeDoubleArea(const vector<Dimension*> * dimensions, vector<vector<Element*> >& numericAreaElements, size_t _numCompute);



	void fillDoubleResultStorage(const vector<Dimension*>* dimensions, HashAreaStorage* doubleStorage, vector<vector<Element*> >& numericAreaElements);

	bool computeAdjustedNumericArea(const vector<Dimension*>* dimensions, vector<vector<Element*> >& numericAreaElements, vector<vector<Element*> >& adjustedNumericArea, vector<vector<vector<pair<IdentifierType, double> > > >& consIdentifiers);

	size_t hashCalc(size_t lasty);

	bool hasConsolidatedValues(vector<vector<Element*> >& area);

	vector<vector<vector<Element*> > > computeSubAreas(vector<vector<Element*> >& adjustedNumericArea, size_t maxAreas, bool useFirstDimensions);

	bool processConsolidatedValues(HashAreaStorage* doubleStorage, vector<CellPath>& foundRulePaths, vector<Cube::CellValueType>& foundRuleValues, vector<CellPath>& notFoundRulePaths, vector<CellPath>& consolidatedPaths, size_t numDimension);

	void addBaseElements(Dimension* dimension, vector<Element*>& elements, set<Element*>& addedBaseElements);

	vector<vector<Element*> > computeIncreasedArea(vector<vector<Element*> >& numericAreaElements, size_t& numCompute);

	vector<vector<Element*> > eliminateDirectRules(vector<vector<Element*> >& numericAreaElements, size_t& numCompute, vector<CellPath>& foundRulePaths, vector<Cube::CellValueType>& foundRuleValues, vector<CellPath>& notFoundRulePaths, vector<CellPath>& consolidatedPaths);

	void appendError(StringBuffer* sb, ErrorException::ErrorType type, IdentifierType idRule, bool showRule, bool showLockInfo)
	{

		sb->appendCsvInteger((int32_t)99);
		sb->appendCsvInteger((int32_t)type);
		sb->appendCsvString(StringUtils::escapeString(ErrorException::getDescriptionErrorType(type)));

		if (showRule) {
			if (idRule != Rule::NO_RULE && idRule != 0) {
				sb->appendCsvInteger((uint32_t)idRule);
			} else {
				sb->appendChar(';');
			}
		}
		if (showLockInfo) {
			sb->appendCsvInteger(0);
		}

		sb->appendEol();
	}

	void appendDouble(StringBuffer* sb, CellPath* path, double value, bool found, IdentifierType idRule, bool showRule, bool showLockInfo, IdentifierType userId)
	{

		sb->appendCsvInteger((int32_t)1);

		if (found) {
			sb->appendCsvString("1");
			sb->appendCsvDouble(value);
		} else {
			sb->appendCsvString("0;");
		}

		if (showRule) {
			if (idRule != Rule::NO_RULE && idRule != 0) {
				sb->appendCsvInteger((uint32_t)idRule);
			} else {
				sb->appendChar(';');
			}
		}

		if (showLockInfo) {
			Cube::CellLockInfo lockInfo = 0;
			lockInfo = cube->getCellLockInfo(path, userId);
			sb->appendCsvInteger((uint32_t)lockInfo);
		}

		sb->appendEol();
	}

	void appendString(StringBuffer* sb, CellPath* path, string* value, bool found, IdentifierType idRule, bool showRule, bool showLockInfo, IdentifierType userId)
	{

		sb->appendCsvInteger((int32_t)2);

		if (found) {
			sb->appendCsvString("1");
			sb->appendCsvString(StringUtils::escapeString(*value));
		} else {
			sb->appendCsvString("0;");
		}

		if (showRule) {
			if (idRule != Rule::NO_RULE) {
				sb->appendCsvInteger((uint32_t)idRule);
			} else {
				sb->appendChar(';');
			}
		}

		if (showLockInfo) {
			Cube::CellLockInfo lockInfo = 0;
			lockInfo = cube->getCellLockInfo(path, userId);
			sb->appendCsvInteger((uint32_t)lockInfo);
		}

		sb->appendEol();
	}

};

}

#endif
