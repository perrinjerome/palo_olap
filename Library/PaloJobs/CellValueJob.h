////////////////////////////////////////////////////////////////////////////////
/// @brief cell value job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_CELL_VALUE_JOB_H
#define PALO_JOBS_CELL_VALUE_JOB_H 1

#include "palo.h"
#include "Olap/PaloSession.h"

#include "PaloJobs/AreaJob.h"

#include "Cache/SimpleCache.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cell value job
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CellValueJob : public AreaJob {
public:
	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new CellValueJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CellValueJob(Server* server, PaloJobRequest* jobRequest) :
		AreaJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return READ_JOB;
	}

	void compute()
	{
		value.type = UNDEFINED;
		value.rule = NO_IDENTIFIER;
		bool found = false;

		findPath();

		checkToken(cube);

		checkPathPermission(cellPath, RIGHT_READ);

		cube->resetCacheCounter();

		SimpleCache::cache_value_type cachedValue;
		if (cube->cache.getValue(*cellPath, cachedValue)) {
			value.doubleValue = cachedValue.first;
			value.type = NUMERIC;
			if (SimpleCache::NOT_FOUND == cachedValue.second) {
				value.rule = Rule::NO_RULE;
				found = false;
			} else {
				value.rule = SimpleCache::ruleFromStat(cachedValue.second);
				found = true;
			}
		} else {
			set<pair<Rule*, IdentifiersType> > ruleHistory;
			bool haveValue = false;

			if (CONSOLIDATED == cellPath->getPathType() && cube->hasRule()) {
				haveValue = cube->getCellValueFromDirectRule(cellPath, value, &found, &ruleHistory, &memory_context);

//				if (!haveValue) {
//					haveValue = cube->getCellValueFromIndirectRule(cellPath, value, &found, &ruleHistory, &memory_context);
//				}
			}

			if (!haveValue) {
				value = cube->getCellValueNew(cellPath, &haveValue, &ruleHistory, &memory_context, true);
			}

			found = haveValue;

			if (NUMERIC == value.type && haveValue) {
				cube->cache.putValue(cellPath->getPathIdentifier(), value.doubleValue, SimpleCache::statFromRule(value.rule));
			}

		}

		Cube::checkZero(value);

		generateCellValueResponse(value, found);
	}

private:

	vector<set<Element*> > computeNumericAreaElements(const vector<Dimension*> * dimensions, const IdentifiersType& area)
	{
		size_t numDimensions = dimensions->size();
		vector<set<Element*> > numericAreaElements;

		for (size_t i = 0; i < numDimensions; i++) {

			set<Element*> crt;

			Element* element = (*dimensions)[i]->lookupElement(area[i]);
			if (element) {
				crt.insert(element);
			}

			numericAreaElements.push_back(crt);
		}
		return numericAreaElements;
	}

private:
	Cube::CellValueType value;
};

}

#endif
