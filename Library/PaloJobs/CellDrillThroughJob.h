////////////////////////////////////////////////////////////////////////////////
/// @brief cell drillthrough
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Developed by Marko Stijak, Banja Luka on behalf of Jedox AG.
/// Copyright and exclusive worldwide exploitation right has
/// Jedox AG, Freiburg.
///
/// @author Marko Stijak, Banja Luka, Bosnia and Herzegovina
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_CELL_DRILLTHROUGH_JOB_H
#define PALO_JOBS_CELL_DRILLTHROUGH_JOB_H 1

#include "palo.h"

#include "Worker/LoginWorker.h"
#include "PaloDispatcher/DirectPaloJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief login to a palo server
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CellDrillThroughJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new CellDrillThroughJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CellDrillThroughJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return SPECIAL_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findPath();

		if (jobRequest->mode < 1 || jobRequest->mode > 2)
			throw new ParameterException(ErrorException::ERROR_INVALID_MODE, "unsupported cell/drillthrough mode", PaloRequestHandler::ID_MODE, jobRequest->mode);

		if (user && user->getRoleCellDataRight() < RIGHT_SPLASH) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "insufficient access rights for path", "user", user->getIdentifier());
		}

		LoginWorker* loginWorker = server->getLoginWorker();

		if (loginWorker) {
			cellDrillThrough(jobRequest->mode);
		} else {
			throw ErrorException(ErrorException::ERROR_INTERNAL, "no login worker");
		}
	}

private:
	void cellDrillThrough(IdentifierType modeId)
	{
		LoginWorker* loginWorker = server->getLoginWorker();

		vector<string> result;

		ResultStatus status = loginWorker->cellDrillThrough(modeId, database->getIdentifier(), cube->getIdentifier(), cellPath, result);

		try {
			if (status == RESULT_FAILED) {
				throw ErrorException(ErrorException::ERROR_INTERNAL, "login worker error");
			}

			if (result.size() < 2) {
				throw ErrorException(ErrorException::ERROR_INTERNAL, "login worker error");
			}
		} catch (ErrorException e) {
			response = new HttpResponse(HttpResponse::BAD);

			StringBuffer& body = response->getBody();

			body.appendCsvInteger((int32_t)e.getErrorType());
			body.appendCsvString(StringUtils::escapeString(ErrorException::getDescriptionErrorType(e.getErrorType())));
			body.appendCsvString(StringUtils::escapeString(e.getMessage()));
			body.appendEol();

			return;
		}

		response = new HttpResponse(HttpResponse::OK);
		StringBuffer& sb = response->getBody();

		for (std::vector<std::string>::iterator i = result.begin(); i != result.end(); i++) {
			sb.appendCsvString(StringUtils::escapeString(i->c_str()));
			sb.appendEol();
		}
	}
};
}

#endif
