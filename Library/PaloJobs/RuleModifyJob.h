////////////////////////////////////////////////////////////////////////////////
/// @brief rule modify job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_RULE_MODIFY_JOB_H
#define PALO_JOBS_RULE_MODIFY_JOB_H 1

#include "palo.h"

#include "PaloDispatcher/DirectPaloJob.h"
#include "Parser/RuleParserDriver.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief rule modify
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS RuleModifyJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new RuleModifyJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	RuleModifyJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findCube();

		string definition = "";
		if (jobRequest->definition) {
			definition = *(jobRequest->definition);
		}

		if (definition != "") {
			RuleParserDriver driver;

			driver.parse(definition);
			RuleNode* r = driver.getResult();

			if (r) {

				// find rule to replace
				findRule();

				// validate parse tree
				string errorMsg;
				bool ok = r->validate(server, database, cube, errorMsg);

				if (!ok) {
					delete r;
					throw ParameterException(ErrorException::ERROR_PARSING_RULE, errorMsg, PaloRequestHandler::DEFINITION, definition);
				}

				string externalIdentifier = "";
				if (jobRequest->externalIdentifier) {
					externalIdentifier = *(jobRequest->externalIdentifier);
				}
				string comment = "";
				if (jobRequest->comment) {
					comment = *(jobRequest->comment);
				}

				if (!cube->modifyRule(rule, r, externalIdentifier, comment, user)) {
					throw ParameterException(ErrorException::ERROR_PARSING_RULE, "Cannot compute marker.", "", "");
				}

				cube->activateRule(rule, jobRequest->activate, user, true);

				server->invalidateCache();

				return generateRuleResponse(rule, jobRequest->useIdentifier);
			} else {

				// got no parse tree
				throw ParameterException(ErrorException::ERROR_PARSING_RULE, driver.getErrorMessage(), PaloRequestHandler::DEFINITION, definition);
			}
		} else {
			// find rule to replace
			findRule();

			cube->activateRule(rule, jobRequest->activate, user);

			server->invalidateCache();

			generateRuleResponse(rule, jobRequest->useIdentifier);
		}
	}
};

}

#endif
