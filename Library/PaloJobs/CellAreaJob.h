////////////////////////////////////////////////////////////////////////////////
/// @brief cell area
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_CELL_AREA_JOB_H
#define PALO_JOBS_CELL_AREA_JOB_H 1

#include "palo.h"

#include "Exceptions/ErrorException.h"
#include "Olap/CellPath.h"
#include "Olap/Rule.h"
#include "Olap/Cube.h"
#include "Olap/AreaStringResultStorage.h"
#include "PaloJobs/AreaJob.h"
#include "Cache/AreaIterator.h"



namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cell area
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CellAreaJob : public AreaJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new CellAreaJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CellAreaJob(Server * server, PaloJobRequest* jobRequest) :
		AreaJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return READ_JOB;
	}

protected:
	vector<IdentifiersType> requestedArea;
	vector<vector<Element *> > numericAreaElements;
	vector<vector<Element *> > numericAreaElementsForString;
	vector<vector<Element *> > numericRequestedAreaElements;
	vector<set<IdentifierType> > stringIdentifiers;
	vector<vector<Element *> > stringElements;
	vector<set<IdentifierType> > unknownIdentifiers;
	vector<set<IdentifierType> > noPermisssionIdentifiers;
	vector<set<IdentifierType> > consolidationIdentifiers;

	SimpleCache::cache_block cached_area;

public:
	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findCube();

		const vector<Dimension*> * dimensions = cube->getDimensions();

		size_t numResult = 0;
		bool computeData = true;

		if (jobRequest->area) {
			computeData = computeAreaIdentifiers(dimensions, numResult, jobRequest->baseOnly);
		} else {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "area is empty", PaloRequestHandler::ID_AREA, "");
		}

		// build result storages for string values
		vector<AreaStringResultStorage*> stringStorages;
		vector<vector<vector<Element*> > > stringAreas;
		buildStringResultStorages(numericRequestedAreaElements, stringElements, stringStorages, stringAreas);

		// fill string storages
		fillStringResultStorages(stringStorages, stringAreas);

		// build result storage for double values
		HashAreaStorage* doubleStorage = NULL;
		if (computeData) {
			doubleStorage = computeDoubleArea(dimensions, numericAreaElements, numResult);
		}

		response = new HttpResponse(HttpResponse::OK);
		setToken(cube);

		IdentifierType userId = user ? user->getIdentifier() : 0;

		if (numResult) {
			loop(&requestedArea, cube, doubleStorage, &stringIdentifiers, &unknownIdentifiers, &noPermisssionIdentifiers, &stringStorages, &response->getBody(), jobRequest->showRule, jobRequest->showLockInfo, userId);
		}
		
		size_t max = stringStorages.size();
		for (size_t numStorage = 0; numStorage < max; numStorage++) {
			AreaStringResultStorage* storage = stringStorages[numStorage];
			delete storage;
		}
		if (doubleStorage) {
			delete doubleStorage;
		}

	}

protected:

	void buildStringResultStorages(vector<vector<Element*> >& numericAreaElements, vector<vector<Element*> >& stringElements, vector<AreaStringResultStorage*>& stringStorages, vector<vector<vector<Element*> > >& stringAreas)
	{

		size_t maxDims = numericAreaElements.size();
		for (size_t i = 0; i < maxDims; i++) {
			vector<vector<Element*> > area(maxDims);

			bool foundEmptyDimension = false;

			for (size_t pos = 0; pos < maxDims; pos++) {
				vector<Element*>& dim = area.at(pos);

				if (pos < i) {
					dim.insert(dim.end(), numericAreaElements[pos].begin(), numericAreaElements[pos].end());
				} else if (pos > i) {
					dim.insert(dim.end(), numericAreaElements[pos].begin(), numericAreaElements[pos].end());
					dim.insert(dim.end(), stringElements[pos].begin(), stringElements[pos].end());
				} else {
					dim.insert(dim.end(), stringElements[pos].begin(), stringElements[pos].end());
				}
				if (dim.size() == 0) {
					foundEmptyDimension = true;
					break;
				}
			}

			if (!foundEmptyDimension) {
				vector<map<Element*, uint32_t> > hashMapping;
				if (AreaResultStorage::computeAreaParameters(cube, area, hashMapping)) {
					stringStorages.push_back(new AreaStringResultStorage(hashMapping));
					stringAreas.push_back(area);
				}

			}
		}
	}

	void fillStringResultStorages(vector<AreaStringResultStorage*>& stringStorages, vector<vector<vector<Element*> > >& stringAreas)
	{

		if (stringStorages.size() == 0) {
			return;
		}

		size_t max = stringStorages.size();
		for (size_t numStorage = 0; numStorage < max; numStorage++) {

			AreaStringResultStorage* storage = stringStorages[numStorage];
			vector<vector<Element*> >& area = stringAreas[numStorage];

			int length = (int)area.size();
			vector<size_t> combinations(length);
			bool eval = true;
			PathType path(length);

			for (int i = 0; i < length;) {

				if (eval) {
					// construct path
					for (int j = 0; j < length; j++) {
						path[j] = area[j][combinations[j]];
					}

					CellPath cp(cube, &path);
					bool found = false;

					try {
						set<pair<Rule*, IdentifiersType> > ruleHistory;
						Cube::CellValueType value = cube->getCellValueNew(&cp, &found, &ruleHistory, &memory_context);
						if (found && value.type == STRING) {
							storage->setValue(cp.getPathIdentifier(), value.charValue, value.rule);
						}
					}
					// Error
					catch (ErrorException ee) {
						storage->setErrorValue(cp.getPathIdentifier(), ee.getErrorType());
					}
				}

				size_t position = combinations[i];
				const vector<Element*>& dim = area[i];

				if (position + 1 < dim.size()) {
					combinations[i] = (int)position + 1;
					i = 0;
					eval = true;
				} else {
					i++;

					for (int k = 0; k < i; k++) {
						combinations[k] = 0;
					}

					eval = false;
				}
			}

		}
	}

	bool computeAreaIdentifiers(const vector<Dimension*> * dimensions, size_t& numResult, bool useBaseOnly, vector<vector<IdentifierType> > *area = NULL)
	{
		if (!area) {
			area = jobRequest->area;
		}

		// check area size
		if (dimensions->size() != area->size()) {
			throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of area elements", PaloRequestHandler::ID_AREA, "");
		}

		bool checkPermissions = false;
		RightsType cubePermission = RIGHT_NONE;

		if (cube->getMinimumAccessRight(user) == RIGHT_NONE) {
			checkPermissions = true;
			cubePermission = cube->getCubeAccessRight(user);
		}

		size_t numDimensions = dimensions->size();

		requestedArea.clear();
		requestedArea.resize(numDimensions);
		numericAreaElements.clear();
		numericAreaElements.resize(numDimensions);
		numericAreaElementsForString.clear();
		numericAreaElementsForString.resize(numDimensions);
		stringIdentifiers.clear();
		stringIdentifiers.resize(numDimensions);
		stringElements.clear();
		stringElements.resize(numDimensions);
		unknownIdentifiers.clear();
		unknownIdentifiers.resize(numDimensions);
		noPermisssionIdentifiers.clear();
		noPermisssionIdentifiers.resize(numDimensions);
		consolidationIdentifiers.clear();
		consolidationIdentifiers.resize(numDimensions);

		numResult = 1;

		for (size_t i = 0; i < numDimensions; i++) {
			vector<Element*>& crtDimElementsForString = numericAreaElementsForString[i];
			vector<Element*>& crtDimElementsForNumeric = numericAreaElements[i];

			IdentifiersType dimensionElementIdentifiers;
			Dimension* dim = dimensions->at(i);

			// empty dimension (add all elements of the dimension to the area)
			if (area->at(i).size() == 0) {
				vector<Element*> elements = dim->getElements(0);

				for (vector<Element*>::iterator j = elements.begin(); j != elements.end(); j++) {
					Element * e = *j;

					// >MDo< next "if" line should be probably removed to make algorithm same as in "else (notEmpty)" case,
					// to include string consolidation or string element with normal consolidation in path
					if (!(useBaseOnly && e->getElementType() == CONSOLIDATED)) {
						dimensionElementIdentifiers.push_back(e->getIdentifier());

						bool bRightsOk = true;
						if (checkPermissions) {
							RightsType elementPermission = min(cubePermission, cube->getElementAccessRight(user, dim, e));
							if (elementPermission < RIGHT_READ) {
								set<IdentifierType>& noI = noPermisssionIdentifiers[i];
								noI.insert(e->getIdentifier());
								bRightsOk = false;
							}
						}

						if (bRightsOk) {
							if (e->getElementType() == STRING || dim->isStringConsolidation(e)) {
								stringIdentifiers[i].insert(e->getIdentifier());
								stringElements[i].push_back(e);
							} else {
								if (e->getElementType() == CONSOLIDATED) {
									consolidationIdentifiers[i].insert(e->getIdentifier());
								}
								if (crtDimElementsForString.end() == std::find(crtDimElementsForString.begin(), crtDimElementsForString.end(), e)) {
									crtDimElementsForString.push_back(e);
								}
								// next line commented out to leave algorithm as it was before for non cell/export use
//								if (!useBaseOnly || e->getElementType() == NUMERIC) {
									if (crtDimElementsForNumeric.end() == std::find(crtDimElementsForNumeric.begin(), crtDimElementsForNumeric.end(), e)) {
										crtDimElementsForNumeric.push_back(e);
									}
//								}
							}
						}
					}
				}
			}
			// not empty
			else {
				for (size_t j = 0; j < area->at(i).size(); j++) {
					IdentifierType id = area->at(i).at(j);
					dimensionElementIdentifiers.push_back(id);

					// check for unknown identifiers
					try {
						Element* e = (*dimensions)[i]->findElement(id, 0);

						bool bRightsOk = true;
						if (checkPermissions) {
							RightsType elementPermission = min(cubePermission, cube->getElementAccessRight(user, dim, e));
							if (elementPermission < RIGHT_READ) {
								noPermisssionIdentifiers[i].insert(id);
								bRightsOk = false;
							}
						}

						if (bRightsOk) {
							if (e->getElementType() == STRING || dim->isStringConsolidation(e)) {
								stringIdentifiers[i].insert(e->getIdentifier());
								stringElements[i].push_back(e);
							} else {
								if (e->getElementType() == CONSOLIDATED) {
									consolidationIdentifiers[i].insert(id);
								}
								if (crtDimElementsForString.end() == std::find(crtDimElementsForString.begin(), crtDimElementsForString.end(), e)) {
									crtDimElementsForString.push_back(e);
								}
								if (!useBaseOnly || e->getElementType() == NUMERIC) {
									if (crtDimElementsForNumeric.end() == std::find(crtDimElementsForNumeric.begin(), crtDimElementsForNumeric.end(), e)) {
										crtDimElementsForNumeric.push_back(e);
									}
								}
							}
						}
					} catch (ParameterException e) {
						unknownIdentifiers[i].insert(id);
					}

				}
			}

			numResult *= dimensionElementIdentifiers.size();
			requestedArea[i] = dimensionElementIdentifiers;

		}

		bool computeData = false;
		vector<vector<Element*> > nonCachedElements;
		numericRequestedAreaElements = numericAreaElements;
		if (numResult) {
			if (memory_context.calcRules()) {
				if (!cube->cache.getValues(numericAreaElements, nonCachedElements, cached_area)) {
					computeData = true;
					numericAreaElements = nonCachedElements;
				}
			} else {
				area_iterator crt_path(numericAreaElements);
				if (crt_path != area_iterator::end()) {
					computeData = true;
				}
			}
		}

		return computeData;

	}

private:

	void loop(const vector<IdentifiersType>* requestedArea, Cube* cube, HashAreaStorage* doubleStorage, vector<set<IdentifierType> >* stringIdentifiers, vector<set<IdentifierType> >* unknownIdentifiers, vector<set<IdentifierType> >* noPermisssionIdentifiers, vector<AreaStringResultStorage*>* stringStorages, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId)
	{

		size_t total_lines = 0;

		bool eval = true;
		int length = (int)requestedArea->size();
		vector<size_t> combinations(length);

		bool found;
		IdentifierType idRule;
		unsigned int idError;

		for (int i = 0; i < length;) {
			if (eval) {
				// construct path
				IdentifiersType path;
				bool containsStringIdentifier = false;
				bool containsUnknownIdentifier = false;
				bool containsNoPermisssionIdentifier = false;

				for (size_t j = 0; j < requestedArea->size(); j++) {
					IdentifierType id = requestedArea->at(j)[combinations[j]];

					set<IdentifierType>::iterator find = stringIdentifiers->at(j).find(id);
					if (find != stringIdentifiers->at(j).end()) {
						containsStringIdentifier = true;
					}

					find = unknownIdentifiers->at(j).find(id);
					if (find != unknownIdentifiers->at(j).end()) {
						containsUnknownIdentifier = true;
					}

					find = noPermisssionIdentifiers->at(j).find(id);
					if (find != noPermisssionIdentifiers->at(j).end()) {
						containsNoPermisssionIdentifier = true;
					}

					path.push_back(id);
				}

				if (containsUnknownIdentifier) {

					if (total_lines >= max_cell_count) {
						appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
						Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
						break;
					}
					total_lines++;

					appendError(path, sb, ErrorException::ERROR_ELEMENT_NOT_FOUND, showRule, showLockInfo);
				} else if (containsNoPermisssionIdentifier) {

					if (total_lines >= max_cell_count) {
						appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
						Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
						break;
					}
					total_lines++;

					appendError(path, sb, ErrorException::ERROR_NOT_AUTHORIZED, showRule, showLockInfo);

				} else if (containsStringIdentifier) {
					vector<AreaStringResultStorage*>::iterator storeIter = stringStorages->begin();

					for (; storeIter != stringStorages->end(); storeIter++) {
						if ((*storeIter)->containsPath(&path)) {
							string* value = (*storeIter)->getValue(&path, &idRule, &idError, &found);

							if (idError > 0) {

								if (total_lines >= max_cell_count) {
									appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
									Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
									break;
								}
								total_lines++;

								appendError(path, sb, (ErrorException::ErrorType)idError, showRule, showLockInfo);
							} else {

								if (total_lines >= max_cell_count) {
									appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
									Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
									break;
								}
								total_lines++;

								appendString(cube, path, value, found, idRule, sb, showRule, showLockInfo, userId);
								break;
							}
						}
					}
				} else {
					SimpleCache::const_cache_type_iterator val = cached_area.find(path);
					if (val != cached_area.end()) {
						if (SimpleCache::NOT_FOUND == val->second.second) {
							if (total_lines >= max_cell_count) {
								appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
								Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
								break;
							}
							total_lines++;

							appendDouble(cube, path, 0, false, Rule::NO_RULE, sb, showRule, showLockInfo, userId);
						} else {
							SimpleCache::dispatch(val->second, idRule, idError);
							if (idError > 0) {

								if (total_lines >= max_cell_count) {
									appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
									Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
									break;
								}
								total_lines++;

								appendError(path, sb, (ErrorException::ErrorType)idError, showRule, showLockInfo);
							} else {

								if (total_lines >= max_cell_count) {
									appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
									Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
									break;
								}
								total_lines++;

								double value = val->second.first;
								Cube::checkZero(value);
								appendDouble(cube, path, value, true, idRule, sb, showRule, showLockInfo, userId);
							}
						}
					} else if (doubleStorage) {
						double* value = doubleStorage->getValue(&path, &idRule, &idError, &found);

						if (idError > 0) {

							if (total_lines >= max_cell_count) {
								appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
								Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
								break;
							}
							total_lines++;

							appendError(path, sb, (ErrorException::ErrorType)idError, showRule, showLockInfo);
						} else {

							if (total_lines >= max_cell_count) {
								appendError(path, sb, ErrorException::ERROR_MAX_CELL_REACHED, showRule, showLockInfo);
								Logger::warning << "Area job: Reached maximum number of returnable cells: " << max_cell_count << endl;
								break;
							}
							total_lines++;

							Cube::checkZero(*value);
							appendDouble(cube, path, *value, found, idRule, sb, showRule, showLockInfo, userId);
						}
					}
				}
			}

			size_t position = combinations[i];
			const IdentifiersType& dim = requestedArea->at(i);

			if (position + 1 < dim.size()) {
				combinations[i] = (int)position + 1;
				i = 0;
				eval = true;
			} else {
				i++;

				for (int k = 0; k < i; k++) {
					combinations[k] = 0;
				}

				eval = false;
			}
		}
	}

	virtual void appendError(const IdentifiersType& path, StringBuffer* sb, ErrorException::ErrorType type, bool showRule, bool showLockInfo)
	{

		sb->appendCsvInteger((int32_t)99);
		sb->appendCsvInteger((int32_t)type);
		sb->appendCsvString(StringUtils::escapeString(ErrorException::getDescriptionErrorType(type)));

		for (size_t i = 0; i < path.size(); i++) {
			if (0 < i) {
				sb->appendChar(',');
			}
			sb->appendInteger(path[i]);
		}

		sb->appendChar(';');

		if (showRule) {
			sb->appendChar(';');
		}
		if (showLockInfo) {
			sb->appendCsvInteger(0);
		}

		sb->appendEol();
	}

	virtual void appendDouble(Cube * cube, const IdentifiersType& path, double value, bool found, IdentifierType idRule, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId)
	{

		sb->appendCsvInteger((int32_t)1);

		if (found) {
			sb->appendCsvString("1");
			sb->appendCsvDouble(value);
		} else {
			sb->appendCsvString("0;");
		}

		for (size_t i = 0; i < path.size(); i++) {
			if (0 < i) {
				sb->appendChar(',');
			}
			sb->appendInteger(path[i]);
		}

		sb->appendChar(';');

		if (showRule) {
			if (idRule != Rule::NO_RULE) {
				sb->appendCsvInteger((uint32_t)idRule);
			} else {
				sb->appendChar(';');
			}
		}

		if (showLockInfo) {
			Cube::CellLockInfo lockInfo = 0;
			CellPath cp(cube, &path);
			lockInfo = cube->getCellLockInfo(&cp, userId); //safe to call getCellLockInfo because cp is valid path
			sb->appendCsvInteger((uint32_t)lockInfo);
		}

		sb->appendEol();
	}

	virtual void appendString(Cube * cube, const IdentifiersType& path, string* value, bool found, IdentifierType idRule, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId)
	{

		sb->appendCsvInteger((int32_t)2);

		if (found) {
			sb->appendCsvString("1");
			sb->appendCsvString(StringUtils::escapeString(*value));
		} else {
			sb->appendCsvString("0;");
		}

		for (size_t i = 0; i < path.size(); i++) {
			if (0 < i) {
				sb->appendChar(',');
			}
			sb->appendInteger(path[i]);
		}

		sb->appendChar(';');

		if (showRule) {
			if (idRule != Rule::NO_RULE) {
				sb->appendCsvInteger((uint32_t)idRule);
			} else {
				sb->appendChar(';');
			}
		}

		if (showLockInfo) {
			Cube::CellLockInfo lockInfo = 0;
			CellPath cp(cube, &path);
			lockInfo = cube->getCellLockInfo(&cp, userId); //safe to call getCellLockInfo because cp is valid path
			sb->appendCsvInteger((uint32_t)lockInfo);
		}

		sb->appendEol();
	}

};

}

#endif
