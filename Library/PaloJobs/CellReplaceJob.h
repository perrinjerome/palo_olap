////////////////////////////////////////////////////////////////////////////////
/// @brief cell replace
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_CELL_REPLACE_JOB_H
#define PALO_JOBS_CELL_REPLACE_JOB_H 1

#include "palo.h"

#include <iostream>

#include "Exceptions/ParameterException.h"
#include "Olap/CellPath.h"
#include "Olap/Dimension.h"
#include "Olap/Lock.h"
#include "Olap/PaloSession.h"
#include "Olap/Server.h"
#include "PaloDispatcher/DirectPaloJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cell replace
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CellReplaceJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new CellReplaceJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CellReplaceJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findPath();

		// get splash mode
		Cube::SplashMode mode = PaloJob::splashMode(jobRequest->splash);

		// get "add" flag, true means add value instead of set
		bool addFlag = jobRequest->add; // getBoolean(request, ADD, false);

		if (addFlag) {
			if (mode != Cube::DISABLED && mode != Cube::ADD_BASE && mode != Cube::DEFAULT) {
				throw ParameterException(ErrorException::ERROR_INVALID_SPLASH_MODE, "add=1 requires splash mode DRFAULT, DISABLED, or ADD", PaloRequestHandler::SPLASH, (int)mode);
			}
		}

		// get event-process flag, false means to circumvent the processor (right will be checked by cube)
		bool eventProcessor = jobRequest->eventProcess;

		// try to change the cell value
		bool withinEvent = Server::isBlocking();

		PaloSession* session = PaloSession::findSession(jobRequest->session);

		if (!withinEvent && session->isWorker()) {
			throw ParameterException(ErrorException::ERROR_NOT_WITHIN_EVENT, "worker cell/replace requires an event/begin", "session", session->getEncodedIdentifier());
		}

		// do not use the supervision event processor if within event or requested by user
		bool checkArea = eventProcessor && !withinEvent;

		if (cellPath->getPathType() == STRING) {
			string value = "";

			if (jobRequest->value) {
				value = *(jobRequest->value);
			}

			for (size_t i = 0; i < value.length(); i++) {
				if (value[i] >= 0 && value[i] < 32 && value[i] != 9 && value[i] != 10 && value[i] != 13) { 
					// only \n \r \t are allowed from special characters
					throw ParameterException(ErrorException::ERROR_INVALID_STRING, "string value contains an illegal character", "value", value);
				}
			}

			cube->setCellValue(cellPath, value, user, session, checkArea, !eventProcessor, Lock::checkLock);
		} else {
			double value = 0.0;

			if (jobRequest->value) {
				value = StringUtils::stringToDouble(*(jobRequest->value));
			}
			cube->setCellValue(cellPath, value, user, session, checkArea, !eventProcessor, addFlag, mode, Lock::checkLock);
		}

		server->invalidateCache();

		generateOkResponse(cube);
	}

};

}

#endif
