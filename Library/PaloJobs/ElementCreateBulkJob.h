////////////////////////////////////////////////////////////////////////////////
/// @brief element create bulk job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Marko Stijak, Banja Luka, Bosnia and Herzegovina
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_ELEMENT_CREATE_BULK_JOB_H
#define PALO_JOBS_ELEMENT_CREATE_BULK_JOB_H 1

#include "palo.h"

#include "PaloDispatcher/DirectPaloJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief element create bulk
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS ElementCreateBulkJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new ElementCreateBulkJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	ElementCreateBulkJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findDimension();

		if (jobRequest->types != 0) {
			computeMultipleType();
		} else {
			computeSingleType();
		}
	}

private:

	// /////////////////////////////////////////////////////////////////////////////
	// create elements of same type
	// /////////////////////////////////////////////////////////////////////////////

	void computeSingleType()
	{

		// example url:
		// element/create_bulk?sid=NvUl&database=3&dimension=11&name_elements=abc,def&type=4&name_children=a,b,c:d,e,f,r&weights=1,1,2:1,2,1
		ElementType elementType = elementTypeByIdentifier(jobRequest->type);

		if (!jobRequest->elementsName) {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing element names", PaloRequestHandler::NAME_ELEMENTS, "");
		}

		vector<ElementsWeightType> children;

		if (elementType == CONSOLIDATED) {
			bool hasWeight = false;

			if (jobRequest->weights && jobRequest->weights->size() > 0) {
				hasWeight = true;
			}

			vector<vector<Element*> > childrenElements;

			if (jobRequest->children) {
				if (jobRequest->children->size() < jobRequest->elementsName->size()) {
					throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing children", PaloRequestHandler::ID_CHILDREN, "");
				}
				for (size_t i = 0; i < jobRequest->elementsName->size(); i++) {
					vector<Element*> ch;
					for (size_t j = 0; j < jobRequest->children->at(i).size(); j++) {
						Element* e = dimension->findElement(jobRequest->children->at(i).at(j), user);
						ch.push_back(e);
					}
					childrenElements.push_back(ch);
				}
			} else if (jobRequest->childrenName) {
				if (jobRequest->childrenName->size() < jobRequest->elementsName->size()) {
					throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing children", PaloRequestHandler::ID_CHILDREN, "");
				}
				for (size_t i = 0; i < jobRequest->elementsName->size(); i++) {
					vector<Element*> ch;
					for (size_t j = 0; j < jobRequest->childrenName->at(i).size(); j++) {
						Element* e = dimension->findElementByName(jobRequest->childrenName->at(i).at(j), user);
						ch.push_back(e);
					}
					childrenElements.push_back(ch);
				}
			} else {
				throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing children", PaloRequestHandler::ID_CHILDREN, "");
			}

			if (hasWeight) {
				if (childrenElements.size() > jobRequest->weights->size()) {
					throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing weight", PaloRequestHandler::WEIGHTS, "");
				}
			}

			for (size_t i = 0; i < jobRequest->elementsName->size(); i++) {

				if (hasWeight) {
					if (childrenElements.at(i).size() > jobRequest->weights->at(i).size()) {
						throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing weight", PaloRequestHandler::WEIGHTS, "");
					}
				}

				ElementsWeightType elementsWeightTypes;

				for (size_t j = 0; j < childrenElements.at(i).size(); j++) {
					if (hasWeight) {
						elementsWeightTypes.push_back(pair<Element*, double> (childrenElements.at(i).at(j), jobRequest->weights->at(i).at(j)));
					} else {
						elementsWeightTypes.push_back(pair<Element*, double> (childrenElements.at(i).at(j), 1.0));
					}
				}

				children.push_back(elementsWeightTypes);
			}

		}

		//everything seems ok, now create elements
		vector<Element*> createdElement;

		try {
			createdElement.reserve(jobRequest->elementsName->size());

			for (size_t i = 0; i < jobRequest->elementsName->size(); ++i) {
				Element* element = dimension->addElement(jobRequest->elementsName->at(i), elementType, user);
				if (elementType == CONSOLIDATED)
					dimension->addChildren(element, &children[i], user);
				createdElement.push_back(element);
			}
		} catch (...) {
			for (size_t i = 0; i < createdElement.size(); i++)
				dimension->deleteElement(createdElement[i], 0);
			throw ;
		}

		generateOkResponse(dimension);
	}

	// /////////////////////////////////////////////////////////////////////////////
	// create elements of different types
	// /////////////////////////////////////////////////////////////////////////////

	void computeMultipleType()
	{

		// example url:
		// element/create_bulk?sid=NvUl&database=3&dimension=11&name_elements=abc,def&type=4&name_children=a,b,c:d,e,f,r&weights=1,1,2:1,2,1

		if (!jobRequest->elementsName) {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing element names", PaloRequestHandler::NAME_ELEMENTS, "");
		}

		if (jobRequest->elementsName->size() != jobRequest->types->size()) {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing types", PaloRequestHandler::ID_TYPES, "expecting " + StringUtils::convertToString((uint32_t)jobRequest->elementsName->size()));
		}

		vector<ElementType> elementTypes;

		for (vector<uint32_t>::const_iterator i = jobRequest->types->begin(); i != jobRequest->types->end(); i++) {
			ElementType elementType = elementTypeByIdentifier(*i);

			elementTypes.push_back(elementType);
		}

		// check for children and weights
		bool hasWeight = false;

		if (jobRequest->weights && jobRequest->weights->size() > 0) {
			hasWeight = true;
		}

		vector<vector<string> > childrenElements;

		// children by identifier
		if (jobRequest->children) {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "names of children are required", PaloRequestHandler::NAME_CHILDREN, "");
		}

		// children by name
		else if (jobRequest->childrenName) {
			if (jobRequest->childrenName->size() < jobRequest->elementsName->size()) {
				throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing children", PaloRequestHandler::ID_CHILDREN, "");
			}

			for (size_t i = 0; i < jobRequest->elementsName->size(); i++) {
				vector<string> ch;

				for (size_t j = 0; j < jobRequest->childrenName->at(i).size(); j++) {
					ch.push_back(jobRequest->childrenName->at(i).at(j));
				}

				childrenElements.push_back(ch);
			}
		}

		// no children
		else {
			if (hasWeight) {
				throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing children", PaloRequestHandler::ID_CHILDREN, "");
			}

			childrenElements.resize(elementTypes.size());
		}

		// check weights
		if (hasWeight) {
			if (childrenElements.size() > jobRequest->weights->size()) {
				throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing weight", PaloRequestHandler::WEIGHTS, "");
			}
		}

		// everything seems ok, now create elements
		vector<Element*> createdElement;

		try {
			createdElement.reserve(jobRequest->elementsName->size());

			for (size_t i = 0; i < jobRequest->elementsName->size(); ++i) {
				ElementType elementType = elementTypes.at(i);
				Element* element = dimension->addElement(jobRequest->elementsName->at(i), elementType, user);

				createdElement.push_back(element);
			}
		} catch (...) {
			for (size_t i = 0; i < createdElement.size(); i++)
				dimension->deleteElement(createdElement[i], 0);
			throw ;
		}

		// now create the children
		try {
			for (size_t i = 0; i < jobRequest->elementsName->size(); i++) {

				// check weight list
				if (hasWeight) {
					if (childrenElements.at(i).size() > jobRequest->weights->at(i).size()) {
						throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing weight", PaloRequestHandler::WEIGHTS, "");
					}
				}

				// construct elements and weights of children
				ElementsWeightType elementsWeightTypes;

				for (size_t j = 0; j < childrenElements.at(i).size(); j++) {
					Element* e = dimension->findElementByName(childrenElements.at(i).at(j), user);

					if (hasWeight) {
						elementsWeightTypes.push_back(pair<Element*, double> (e, jobRequest->weights->at(i).at(j)));
					} else {
						elementsWeightTypes.push_back(pair<Element*, double> (e, 1.0));
					}
				}

				// and add children
				ElementType elementType = elementTypes.at(i);

				if (elementType == CONSOLIDATED) {
					dimension->addChildren(createdElement.at(i), &elementsWeightTypes, user);
				} else if (!elementsWeightTypes.empty()) {
					throw ParameterException(ErrorException::ERROR_INVALID_ELEMENT_TYPE, "expecting no children for non-consolidated element", "element", createdElement.at(i)->getName());
				}
			}
		} catch (...) {
			for (size_t i = 0; i < createdElement.size(); i++)
				dimension->deleteElement(createdElement[i], 0);
			throw ;
		}

		generateOkResponse(dimension);
	}
};

}

#endif
