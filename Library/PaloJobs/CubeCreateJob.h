////////////////////////////////////////////////////////////////////////////////
/// @brief cube create job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_CUBE_CREATE_JOB_H
#define PALO_JOBS_CUBE_CREATE_JOB_H 1

#include "palo.h"

#include "PaloDispatcher/DirectPaloJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cube create
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CubeCreateJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new CubeCreateJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CubeCreateJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findDatabase();

		string name;

		if (jobRequest->newName) {
			name = *(jobRequest->newName);
		} else {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "name missing", PaloRequestHandler::NEW_NAME, "");
		}

		bool isInfo = false;

		if (jobRequest->type == 0) {
		} else if (jobRequest->type == 3) {
			isInfo = true;
		} else {
			throw ParameterException(ErrorException::ERROR_INVALID_TYPE, "wrong cube type", PaloRequestHandler::ID_TYPE, jobRequest->type);
		}

		vector<Dimension*> dimensions;

		if (jobRequest->dimensions) {
			for (size_t i = 0; i < jobRequest->dimensions->size(); i++) {
				Dimension* dimension = database->findDimension(jobRequest->dimensions->at(i), user);
				dimensions.push_back(dimension);
			}
		} else if (jobRequest->dimensionsName) {
			for (size_t i = 0; i < jobRequest->dimensionsName->size(); i++) {
				Dimension* dimension = database->findDimensionByName(jobRequest->dimensionsName->at(i), user);
				dimensions.push_back(dimension);
			}
		} else {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "dimension missing", PaloRequestHandler::ID_DIMENSIONS, "");
		}

		cube = database->addCube(name, &dimensions, user, isInfo);

		CubeWorker* cubeWorker = cube->getCubeWorker();

		if (cubeWorker != 0) {

			bool ok = cubeWorker->start();

			if (!ok) {
				throw ErrorException(ErrorException::ERROR_WORKER_MESSAGE, "cannot start worker");
			}
		}

		generateCubeResponse(cube);
	}
};

}

#endif
