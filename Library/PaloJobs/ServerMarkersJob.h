////////////////////////////////////////////////////////////////////////////////
/// @brief server info job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_bi_suite.txt">
///   http://www.jedox.com/license_palo_bi_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// @author Radu Ialovoi
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_SERVER_MARKERS_JOB_H
#define PALO_JOBS_SERVER_MARKERS_JOB_H 1

#include "palo.h"

#include "PaloDispatcher/DirectPaloJob.h"
#include "PaloDispatcher/PaloJobRequest.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief server info job
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS ServerMarkersJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new ServerMarkersJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	ServerMarkersJob(Server* server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief initializes job
	////////////////////////////////////////////////////////////////////////////////

	bool initialize()
	{
		PaloJob::initialize();

		encryption = server->getEncryptionType();
		https = jobRequest->httpsPort;

		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		checkToken(server);

		Server::setMarkerCalculationState(this->jobRequest->activate);

		generateOkResponse(server);
	}

private:
	Encryption_e encryption;
	int https;
};
}

#endif
