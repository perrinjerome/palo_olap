////////////////////////////////////////////////////////////////////////////////
/// @brief dimension element job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Martin Jakl, qBicon s.r.o., Prague, Czech Republic
////////////////////////////////////////////////////////////////////////////////

#include "PaloJobs/CellAreaJob.h"
#include "PaloJobs/DimensionDFilterJob.h"
#include "InputOutput/Condition.h"

#include <list>
#include <limits>

namespace palo {

enum DataFilterFlag {
	DATA_MIN = 0x1, DATA_MAX = 0x2, DATA_SUM = 0x4, DATA_AVERAGE = 0x8, DATA_ANY = 0x10, DATA_ALL = 0x20, DATA_STRING = 0x40, ONLY_CONSOLIDATED = 0x80, ONLY_LEAVES = 0x100, UPPER_PERCENTAGE = 0x200, LOWER_PERCENTAGE = 0x400, MID_PERCENTAGE = 0x800, TOP = 0x1000, NORULES = 0x2000
};

bool queryFlag(long a, long b) {
	return (a & b) != 0;
}

class SingleCellAccumulator : public CellArrayAccumulator {
public:
	virtual void addVal(double d)
	{
		m_accumulation = d;
	}

	virtual bool finalize()
	{
		return m_op->check(m_accumulation);
	}
};

class SumAccumulator : public CellArrayAccumulator {
public:
	virtual void addVal(double d)
	{
		m_accumulation += d;
	}

	virtual bool finalize()
	{
		return m_op->check(m_accumulation);
	}
};

class MinAccumulator : public CellArrayAccumulator {
public:
	MinAccumulator()
	{
		m_accumulation = std::numeric_limits<double>::max();
	}

	virtual void addVal(double d)
	{
		if (d < m_accumulation) {
			m_accumulation = d;
		}
	}

	virtual bool finalize()
	{
		return m_op->check(m_accumulation);
	}

	virtual void reset()
	{
		m_accumulation = std::numeric_limits<double>::max();
	}
};

class MaxAccumulator : public CellArrayAccumulator {
public:
	MaxAccumulator()
	{
		m_accumulation = -std::numeric_limits<double>::max();
	}

	virtual void addVal(double d)
	{
		if (d > m_accumulation)
			m_accumulation = d;
	}
	virtual bool finalize()
	{
		return m_op->check(m_accumulation);
	}

	virtual void reset()
	{
		m_accumulation = -std::numeric_limits<double>::max();
	}
};

class AverageAccumulator : public CellArrayAccumulator {
public:
	AverageAccumulator() : counter(0) {}

	virtual void addVal(double d)
	{
		m_accumulation += d;
		counter++;
	}

	virtual bool finalize()
	{
		m_accumulation /= counter;
		return m_op->check(m_accumulation);
	}

	virtual void reset()
	{
		this->CellArrayAccumulator::reset();
		counter = 0;
	}

private:
	int counter;
};

class AnyAccumulator : public CellArrayAccumulator {
public:
	AnyAccumulator() : m_found(false) {}

	virtual void addVal(double d)
	{
		if (m_op->check(d)) {
			m_found = true;
			m_accumulation = d;
		}
	}

	virtual bool finalize()
	{
		return m_found;
	}

	virtual void reset()
	{
		this->CellArrayAccumulator::reset();
		m_found = false;
	}

private:
	bool m_found;
};

class AllAccumulator : public CellArrayAccumulator {
public:
	AllAccumulator() : m_all_true(true) {}

	virtual void addVal(double d)
	{
		if (!m_op->check(d)) {
			m_all_true = false;
		} else {
			m_accumulation = d;
		}
	}

	virtual void addVal(const string &s)
	{
		m_all_true = false;
	}

	virtual bool finalize()
	{
		return m_all_true;
	}

	virtual void reset()
	{
		this->CellArrayAccumulator::reset();
		m_all_true = true;
	}

private:
	bool m_all_true;
};

class StringAccumulator : public CellArrayAccumulator {
public:
	StringAccumulator() : m_found(false) {}

	virtual void addVal(const string &s)
	{
		if (m_op->check(s)) {
			m_found = true;
			str = s;
		}
	}

	virtual bool finalize()
	{
		return m_found;
	}

	virtual void reset()
	{
		this->CellArrayAccumulator::reset();
		m_found = false;
	}

	virtual string getString() {return str;}

private:
	bool m_found;
	string str;
};

CellArrayAccumulator *CellArrayAccumulator::create(long flag, boost::shared_ptr<Condition> op)
{
	CellArrayAccumulator *ret = 0;
	if (queryFlag(flag, DATA_SUM)) {
		ret = new SumAccumulator;
	} else if (queryFlag(flag, DATA_MIN)) {
		ret = new MinAccumulator;
	} else if (queryFlag(flag, DATA_MAX)) {
		ret = new MaxAccumulator;
	} else if (queryFlag(flag, DATA_AVERAGE)) {
		ret = new AverageAccumulator;
	} else if (queryFlag(flag, DATA_ANY)) {
		ret = new AnyAccumulator;
	} else if (queryFlag(flag, DATA_ALL)) {
		ret = new AllAccumulator;
	} else if (queryFlag(flag, DATA_STRING)) {
		ret = new StringAccumulator;
	} else {
		ret = new SingleCellAccumulator;
	}
	ret->setOperator(op);
	return ret;
}

void DimensionDFilterJob::compute()
{
	if ((queryFlag(jobRequest->mode, UPPER_PERCENTAGE) && queryFlag(jobRequest->mode, DATA_STRING)) || (queryFlag(jobRequest->mode, LOWER_PERCENTAGE) && queryFlag(jobRequest->mode, DATA_STRING)) || (queryFlag(jobRequest->mode, MID_PERCENTAGE) && queryFlag(jobRequest->mode, DATA_STRING))) {
		throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "It is not possible to use percentage parameters with string data", PaloRequestHandler::ID_MODE, "");
	}
	if (!jobRequest->area) {
		throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "No area defined", PaloRequestHandler::ID_AREA, "");
	}
	boost::shared_ptr<Condition> op(Condition::parseCondition(*jobRequest->condition));
	findDimension();
	findCube();
	const vector<Dimension*> *dims = cube->getDimensions();
	if (dims->size() != jobRequest->area->size()) {
		throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of area elements", PaloRequestHandler::ID_AREA, "");
	}
	pos = 0;
	for (vector<Dimension*>::const_iterator it = dims->begin(); it != dims->end(); it++, pos++) {
		if ((*it) == dimension) {
			break;
		}
	}
	if (pos == dims->size()) {
		throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "Dimension not in cube.", PaloRequestHandler::ID_DIMENSION, "");	\
	}

	list<Element*> subset;

	for (vector<IdentifierType>::iterator it = jobRequest->area->at(pos).begin(); it != jobRequest->area->at(pos).end(); it++) {
		if ((*it) > dimension->getMaximalIdentifier()) {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "Wrong area.", PaloRequestHandler::ID_AREA, "");	\
		}
		subset.push_back(dimension->findElement(*it, user));
	}

	if (queryFlag(jobRequest->mode, ONLY_LEAVES) && queryFlag(jobRequest->mode, ONLY_CONSOLIDATED)) {
		throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "Bad flag combination.", PaloRequestHandler::ID_MODE, "");	\
	} else if (queryFlag(jobRequest->mode, ONLY_LEAVES)) {
		jobRequest->area->at(pos).clear();
		for (list<Element*>::iterator it = subset.begin(); it != subset.end(); it++) {
			if (dimension->getChildren(*it).empty()) {
				jobRequest->area->at(pos).push_back((*it)->getIdentifier());
			}
		}
	} else if (queryFlag(jobRequest->mode, ONLY_CONSOLIDATED)) {
		jobRequest->area->at(pos).clear();
		for (list<Element*>::iterator it = subset.begin(); it != subset.end(); it++) {
			if (!dimension->getChildren(*it).empty()) {
				jobRequest->area->at(pos).push_back((*it)->getIdentifier());
			}
		}
	}

	for (vector<IdentifierType>::iterator it = jobRequest->area->at(pos).begin(); it != jobRequest->area->at(pos).end(); it++) {
		accumulators.insert(pair<IdentifierType, boost::shared_ptr<CellArrayAccumulator> >(*it, boost::shared_ptr<CellArrayAccumulator>(CellArrayAccumulator::create(jobRequest->mode, op))));
	}

	if (queryFlag(jobRequest->mode, DATA_SUM)) {
		jobRequest->skipEmpty = true;
	}
	if (queryFlag(jobRequest->mode, NORULES)) {
		memory_context.noRules();
	}
	vector<Cube::CellValueType> vals;
	max_cell_count = numeric_limits<size_t>::max();
	CellAreaJob::compute();
	for (list<Element*>::iterator it = subset.begin(); it != subset.end();) {
		map<IdentifierType, boost::shared_ptr<CellArrayAccumulator> >::iterator acc_it = accumulators.find((*it)->getIdentifier());
		if (acc_it != accumulators.end()) {
			boost::shared_ptr<CellArrayAccumulator> acc = acc_it->second;
			if (!acc->finalize()) {
				it = subset.erase(it);
			} else {
				it++;
				Cube::CellValueType v;
				if (queryFlag(jobRequest->mode, DATA_STRING)) {
					v.type = STRING;
					v.charValue = acc->getString();
					v.doubleValue = 0;
				} else {
					v.type = NUMERIC;
					v.doubleValue = acc->getAccumulation();
				}
				vals.push_back(v);
			}
		} else {
			Cube::CellValueType v;
			v.type = UNDEFINED;
			v.doubleValue = 0;
			vals.push_back(v);
			it++;
		}
	}

	int top_num = 0;
	double u = 0, l = 0;
	if (jobRequest->values) {
		if (jobRequest->values->size()) {
			top_num = StringUtils::stringToInteger(jobRequest->values->at(0));
			if (jobRequest->values->size() > 1) {
				u = StringUtils::stringToDouble(jobRequest->values->at(1));
				if (jobRequest->values->size() > 2) {
					l = StringUtils::stringToDouble(jobRequest->values->at(2));
				}
			}
		}
	}

	if (queryFlag(jobRequest->mode, TOP) && (top_num > 0)) {
		top(subset, vals, top_num);
	} else if (queryFlag(jobRequest->mode, UPPER_PERCENTAGE)) {
		upperPer(subset, vals, u);
	} else if (queryFlag(jobRequest->mode, LOWER_PERCENTAGE)) {
		lowerPer(subset, vals, l);
	} else if (queryFlag(jobRequest->mode, MID_PERCENTAGE)) {
		middlePer(subset, vals, u, l);
	}

	generateElementsValuesResponse(dimension, &subset, &vals);
}

void DimensionDFilterJob::appendError(const IdentifiersType& path, StringBuffer* sb, ErrorException::ErrorType type, bool showRule, bool showLockInfo)
{
}

void DimensionDFilterJob::appendDouble(Cube * cube, const IdentifiersType& path, double value, bool found, IdentifierType idRule, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId)
{
	accumulators.find(path[pos])->second->addVal(value);
}

void DimensionDFilterJob::appendString(Cube * cube, const IdentifiersType& path, string* value, bool found, IdentifierType idRule, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId)
{
	accumulators.find(path[pos])->second->addVal(*value);
}

struct greater {
  bool operator() (const Cube::CellValueType& lhs, const Cube::CellValueType& rhs) const {return lhs.doubleValue > rhs.doubleValue;}
};

struct less {
  bool operator() (const Cube::CellValueType& lhs, const Cube::CellValueType& rhs) const {return lhs.doubleValue < rhs.doubleValue;}
};

template<class T> void subs2map(const list<Element*> &subset, const vector<Cube::CellValueType> &vals, multimap<Cube::CellValueType, Element*, T> &sort)
{
	list<Element*>::const_iterator eit;
	vector<Cube::CellValueType>::const_iterator vit;
	for (eit = subset.begin(), vit = vals.begin(); eit != subset.end(); eit++, vit++) {
		sort.insert(pair<Cube::CellValueType, Element*>(*vit, *eit));
	}
}

template<class T> void map2subs(list<Element*> &subset, vector<Cube::CellValueType> &vals, const multimap<Cube::CellValueType, Element*, T> &sort, int top_num)
{
	int i = 0;
	for (typename multimap<Cube::CellValueType, Element*, T>::const_iterator it = sort.begin(); i < top_num && it != sort.end(); i++, it++) {
		subset.push_back(it->second);
		vals.push_back(it->first);
	}
}

void DimensionDFilterJob::top(list<Element*> &subset, vector<Cube::CellValueType> &vals, int top_num)
{
	multimap<Cube::CellValueType, Element*, greater> sort;
	subs2map<greater>(subset, vals, sort);
	subset.clear();
	vals.clear();
	map2subs<greater>(subset, vals, sort, top_num);
}

template<class T> DimensionDFilterJob::PercentageAccumulator<T>::PercentageAccumulator(DimensionDFilterJob *df, multimap<Cube::CellValueType, Element*, T> &sort, double percentage) :
	m_sum(0), m_limit(0)
{
	double total = 0;

	for (typename multimap<Cube::CellValueType, Element*, T>::iterator it_beg = sort.begin(); it_beg != sort.end();) {
		if (queryFlag(df->jobRequest->mode, ONLY_LEAVES)) {
			if (!df->dimension->getChildren(it_beg->second).empty()) {
				m_swaplist.insert(*it_beg);
				sort.erase(it_beg++);
			} else {
				total += (it_beg->first.doubleValue * (((double)percentage) / 100.0));
				++it_beg;

			}
		} else if (queryFlag(df->jobRequest->mode, ONLY_CONSOLIDATED)) {
			if (df->dimension->getChildren(it_beg->second).empty()) {
				m_swaplist.insert(*it_beg);
				sort.erase(it_beg++);
			} else {
				total += (it_beg->first.doubleValue * (((double)percentage) / 100.0));
				++it_beg;
			}
		} else {
			total += (it_beg->first.doubleValue * (((double)percentage) / 100.0));
			++it_beg;
		}
	}
	m_limit = total;
}

template<class T> bool DimensionDFilterJob::PercentageAccumulator<T>::check(typename multimap<Cube::CellValueType, Element*, T>::iterator it)
{
	if (m_sum < m_limit) {
		if (it->first.doubleValue < ((m_limit - m_sum) * 2.0)) {
			m_sum += it->first.doubleValue;
			return false;
		} else {
			m_sum = m_limit;
			return true;
		}
	} else {
		return true;
	}
}

template<class T> multimap<Cube::CellValueType, Element*, T>& DimensionDFilterJob::PercentageAccumulator<T>::get_swaplist()
{
	return m_swaplist;
}

template<class T> DimensionDFilterJob::PercentageNegAccumulator<T>::PercentageNegAccumulator(DimensionDFilterJob *df, multimap<Cube::CellValueType, Element*, T> &sort, double percentage) :
	PercentageAccumulator<T>(df, sort, percentage)
{
}

template<class T> bool DimensionDFilterJob::PercentageNegAccumulator<T>::check(typename multimap<Cube::CellValueType, Element*, T>::iterator it)
{
	return !PercentageAccumulator<T>::check(it);
}

template<class T> void DimensionDFilterJob::PercentageAccumulator<T>::removeif(multimap<Cube::CellValueType, Element*, T> &sort)
{
	for (typename multimap<Cube::CellValueType, Element*, T>::iterator it = sort.begin(); it != sort.end();) {
		if (check(it)) {
			sort.erase(it++);
		} else {
			it++;
		}
	}
}

void DimensionDFilterJob::upperPer(list<Element*> &subset, vector<Cube::CellValueType> &vals, double u)
{
	multimap<Cube::CellValueType, Element*, greater> sort;
	subs2map<greater>(subset, vals, sort);

	PercentageAccumulator<greater> p(this, sort, u);
	p.removeif(sort);
	subset.clear();
	vals.clear();
	map2subs<greater>(subset, vals, sort, numeric_limits<int>::max());
	map2subs<greater>(subset, vals, p.get_swaplist(), numeric_limits<int>::max());
}

void DimensionDFilterJob::lowerPer(list<Element*> &subset, vector<Cube::CellValueType> &vals, double l)
{
	multimap<Cube::CellValueType, Element*, less> sort;
	subs2map<less>(subset, vals, sort);

	PercentageAccumulator<less> p(this, sort, l);
	p.removeif(sort);
	subset.clear();
	vals.clear();
	map2subs<less>(subset, vals, sort, numeric_limits<int>::max());
	map2subs<less>(subset, vals, p.get_swaplist(), numeric_limits<int>::max());
}

void DimensionDFilterJob::middlePer(list<Element*> &subset, vector<Cube::CellValueType> &vals, double u, double l)
{
	multimap<Cube::CellValueType, Element*, greater> sort1;
	subs2map<greater>(subset, vals, sort1);
	PercentageNegAccumulator<greater> p1(this, sort1, u);
	p1.removeif(sort1);

	multimap<Cube::CellValueType, Element*, less> sort2;
	subs2map<less>(subset, vals, sort2);
	PercentageNegAccumulator<less> p2(this, sort2, l);
	p2.removeif(sort2);

	map<Element *, pair<int, Cube::CellValueType> > tmp;
	map<Element *, pair<int, Cube::CellValueType> > res;
	for (multimap<Cube::CellValueType, Element*, greater>::iterator it = sort1.begin(); it != sort1.end(); it++) {
		map<Element *, pair<int, Cube::CellValueType> >::iterator iti = tmp.find(it->second);
		if (iti == tmp.end()) {
			tmp.insert(pair<Element *, pair<int, Cube::CellValueType> >(it->second, pair<int, Cube::CellValueType>(1, it->first)));
		} else {
			iti->second.first++;
		}
	}
	for (multimap<Cube::CellValueType, Element*, less>::iterator it = sort2.begin(); it != sort2.end(); it++) {
		map<Element *, pair<int, Cube::CellValueType> >::iterator iti = res.find(it->second);
		if (iti == res.end()) {
			res.insert(pair<Element *, pair<int, Cube::CellValueType> >(it->second, pair<int, Cube::CellValueType>(1, it->first)));
		} else {
			iti->second.first++;
		}
	}

	subset.clear();
	vals.clear();
	for (map<Element *, pair<int, Cube::CellValueType> >::iterator it = res.begin(); it != res.end(); it++) {
		map<Element *, pair<int, Cube::CellValueType> >::iterator iti = tmp.find(it->first);
		if (iti != tmp.end()) {
			int c = iti->second.first > it->second.first ? it->second.first : iti->second.first;
			for (int i = 0; i < c; i++) {
				subset.push_back(it->first);
				vals.push_back(it->second.second);
			}
		}
	}

	map2subs<greater>(subset, vals, p1.get_swaplist(), numeric_limits<int>::max());
	map2subs<less>(subset, vals, p2.get_swaplist(), numeric_limits<int>::max());
}

}
