////////////////////////////////////////////////////////////////////////////////
/// @brief cell export
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_CELL_EXPORT_JOB_H
#define PALO_JOBS_CELL_EXPORT_JOB_H 1

#include "palo.h"

#include "InputOutput/Condition.h"
#include "Olap/CubeLooper.h"
#include "PaloDispatcher/DirectPaloJob.h"
#include "PaloJobs/AreaJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cell export
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CellExportJob : public CellAreaJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob * create(Server *server, PaloJobRequest *jobRequest)
	{
		return new CellExportJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CellExportJob(Server *server, PaloJobRequest *jobRequest) :
		CellAreaJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findCube();

		// condition
		Condition *condition;
		if (jobRequest->condition) {
			condition = Condition::parseCondition(*(jobRequest->condition));
		} else {
			condition = 0;
		}

		// setup dimensions
		const vector<Dimension *> *dimensions = cube->getDimensions();

		// begin after
		const IdentifiersType *beginAfter = 0;
		bool useBeginAfter = false;
		if (jobRequest->path || jobRequest->pathName) {
			findPath();
			beginAfter = cellPath->getPathIdentifier();
			useBeginAfter = true;
		}

		if (!jobRequest->useRules) {
			memory_context.noRules();
		}

		bool bComputeData = true;
		size_t numResult = 0;
		max_cell_count = numeric_limits<size_t>::max();

		if (!jobRequest->area) {
			// whole cube will be exported or areaName used, prepare vector for ids
			jobRequest->area = new vector<vector<IdentifierType> >;
			jobRequest->area->resize(dimensions->size());
		}

		// area defined by element names
		if (jobRequest->areaName) {
			jobRequest->area->resize(jobRequest->areaName->size());
			for (uint32_t i = 0; i < jobRequest->areaName->size(); i++) {
				for (uint32_t j = 0; j < jobRequest->areaName->at(i).size(); j++) {
					IdentifierType id = (*dimensions)[i]->findElementByName(jobRequest->areaName->at(i).at(j), 0)->getIdentifier();
					jobRequest->area->at(i).push_back(id);
				}
			}
		}

		if (jobRequest->area->size() != dimensions->size()) {
			throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "wrong number of path elements", PaloRequestHandler::ID_AREA, "");
		}

		IdentifierType userId = user ? user->getIdentifier() : 0;

		long maxAreaSize = min((uint32_t)350000, jobRequest->blockSize);
		if (jobRequest->skipEmpty && maxAreaSize <= 1000) {
			maxAreaSize *= 3; // magic constant
		}

		uint64_t linesPrinted = 0;
		uint64_t progressOfExport = 0;
		uint64_t totalElements = 1;
		// fill *,*,* area with elements
		for (size_t i = 0; i < jobRequest->area->size(); i++) {
			if (jobRequest->area->at(i).size() == 0) {
				vector<Element *> allElems;
				if (jobRequest->baseOnly && (jobRequest->type == 1 || (cube->sizeFilledStringCells() == 0 && (!cube->hasRule() || !jobRequest->useRules)))) {
					// baseOnly and (numericOnly or noStrings)
					allElems = dimensions->at(i)->getBaseElements(user);
				}
				else {
					allElems = dimensions->at(i)->getElements(user);
				}
				for (vector<Element *>::iterator it = allElems.begin(); it != allElems.end(); it++) {
					jobRequest->area->at(i).push_back((*it)->getIdentifier());
				}
			}
			else if (user && database->getHideElements()) {
				//hide elements which user should not see
				vector<IdentifierType> filteredElems;
				size_t dimSize = jobRequest->area->at(i).size();
				filteredElems.reserve(dimSize);
				for (size_t elemIndex = 0; elemIndex < dimSize; elemIndex++) {
					if (user->getDimensionDataRight(database, dimensions->at(i), jobRequest->area->at(i)[elemIndex]) > RIGHT_NONE) {
						filteredElems.push_back(jobRequest->area->at(i)[elemIndex]);
					}
				}
				jobRequest->area->at(i) = filteredElems;
			}

			totalElements *= jobRequest->area->at(i).size();
			std::sort(jobRequest->area->at(i).begin(), jobRequest->area->at(i).end());
		}

		vector<uint64_t> subAreaSizes;
		subAreaSizes.resize(jobRequest->area->size() + 1);
		subAreaSizes[jobRequest->area->size()] = 1;
		for (int i = (int)jobRequest->area->size() - 1; i >= 0; i--) {
			subAreaSizes[i] = subAreaSizes[i + 1] * jobRequest->area->at(i).size();
		}

		int iterStart = 0;
		int iterEnd = (int)dimensions->size() - 1;
		int areaToComputeSize = 1;
		// find max area to fit in blockSize
		int iterMax = 0;
		if ((!jobRequest->useRules || !cube->hasRule()) && jobRequest->skipEmpty) {
			iterMax = 3;
		}
		while (iterEnd >= iterMax) {
			int dimSize = (int)jobRequest->area->at(iterEnd).size();
			if (areaToComputeSize * dimSize <= maxAreaSize) {
				areaToComputeSize *= dimSize;
				iterEnd--;
			}
			else {
				break;
			}
		}

		vector<vector<IdentifierType> > subArea;
		subArea.resize(jobRequest->area->size());
		for (uint32_t i = iterEnd+1; i < jobRequest->area->size(); i++) {
			subArea[i] = jobRequest->area->at(i);
		}

		response = new HttpResponse(HttpResponse::OK);
		setToken(cube);
		StringBuffer *body = &response->getBody();

		if (totalElements == 0) {
			body->appendCsvDouble(0);
			body->appendCsvDouble(0);
			body->appendEol();

			delete condition;
		}
		else {
			vector<IdentifierType> lastIndexes;
			lastIndexes.resize(iterEnd+1);
			bool bWork = true;
			bool bCut = true;

			// skip elements already computed (beginAfter) or set iterators to start from 0
			for (int i = iterStart; i <= iterEnd; i++) {
				if (!useBeginAfter) {
					lastIndexes[i] = 0;
				}
				else {
					if (bCut) {
						while (jobRequest->area->at(i).size() > lastIndexes[i] + 1 && jobRequest->area->at(i)[lastIndexes[i]] < (*beginAfter)[i]) {
							lastIndexes[i]++;
							progressOfExport += subAreaSizes[i + 1];
						}
						if (jobRequest->area->at(i)[lastIndexes[i]] < (*beginAfter)[i]) {
							// start from element greater then maximum possible in this dim
							if (i == 0) {
								//nothing to do
								bWork = false;
								bCut = false;
								progressOfExport = totalElements;
								break;
							}
							else {
								// start with next combination from dim i-1
								bCut = false;
								progressOfExport += subAreaSizes[i + 1] * (jobRequest->area->at(i).size() - lastIndexes[i]);
								lastIndexes[i] = 0;
								lastIndexes[i - 1]++;
							}
						}
						else if (jobRequest->area->at(i)[lastIndexes[i]] > (*beginAfter)[i]) {
							bCut = false;
						}
					}
					else {
						lastIndexes[i] = 0;
					}
				}
			}

			// main loop
			while (bWork) {

				// go directly into pages
				if (iterEnd >= 2 && (!jobRequest->useRules || !cube->hasRule()) && jobRequest->skipEmpty) {

					bool bThroughPagesAllowed = true;
					CubePage *pageString = NULL;
					CubePage *pageDouble = NULL;
					uint32_t id0 = jobRequest->area->at(0)[lastIndexes[0]];
					uint32_t id1 = jobRequest->area->at(1)[lastIndexes[1]];

					if (jobRequest->type == 2 || jobRequest->type == 0) {
						pageString = cube->getStorageString()->lookupCubePage(id0, id1);
					}
					if (jobRequest->type == 1 || jobRequest->type == 0) {
						pageDouble = cube->getStorageDouble()->lookupCubePage(id0, id1);

						if (!jobRequest->baseOnly) { // we need consolidations too
							// if page found, we need to go classic area way
							if (pageDouble) {
								bThroughPagesAllowed = false;
							}
							else {
								// page not found but id0 || id1 are consolidated
								if (((*dimensions)[0]->findElement(id0, 0)->getElementType() == CONSOLIDATED || (*dimensions)[1]->findElement(id1, 0)->getElementType() == CONSOLIDATED)
										&& (cube->sizeFilledNumericCells() > 0)) {
									bThroughPagesAllowed = false;
								}
								// else id0 and id1 are numeric base elements and page not found, this whole area can be ignored
							}
						}
					}

					if (bThroughPagesAllowed) {
						IdentifiersType lastPath;
						if (pageDouble && !pageString) {
							loopPage(pageDouble, &response->getBody(), jobRequest->showRule, jobRequest->showLockInfo, userId, condition, linesPrinted, bWork, lastPath, true, beginAfter);
						}
						else if (pageString && !pageDouble) {
							loopPage(pageString, &response->getBody(), jobRequest->showRule, jobRequest->showLockInfo, userId, condition, linesPrinted, bWork, lastPath, false, beginAfter);
						}
						else if (pageString && pageDouble) {
							loopPages(pageDouble, pageString, &response->getBody(), jobRequest->showRule, jobRequest->showLockInfo, userId, condition, linesPrinted, bWork, lastPath, false, beginAfter);
						}

						if (!bWork) {
							// blocksize reached
							for (uint32_t i = 2; i < jobRequest->area->size(); i++) {
								vector<IdentifierType>::iterator it = std::find(jobRequest->area->at(i).begin(), jobRequest->area->at(i).end(), lastPath[i]);
								uint64_t numUsed = it - jobRequest->area->at(i).begin();
								progressOfExport += subAreaSizes[i + 1] * numUsed;
							}
						}
						else if (lastIndexes[1] < jobRequest->area->at(1).size() - 1) {
							progressOfExport += subAreaSizes[2];
							lastIndexes[1]++;
						}
						else if (lastIndexes[0] < jobRequest->area->at(0).size() - 1) {
							progressOfExport += subAreaSizes[2]; /* *(jobRequest->area->at(1).size() - lastIndexes[1]);*/
							lastIndexes[1] = 0;
							lastIndexes[0]++;
						}
						else {
							progressOfExport += subAreaSizes[2];
							bWork = false;
						}
						for (int i = 2; i < iterEnd; i++) {
							lastIndexes[i] = 0;
						}
						continue;
					}
				}

				// if you are here, do it "classic" area way

				for (int i = iterStart; i <= iterEnd; i++) {
					subArea[i].clear();
					subArea[i].push_back(jobRequest->area->at(i)[lastIndexes[i]]);
				}

				// add as many elements as possible to create area to fit to maxAreaSize
				int currAreaSize = areaToComputeSize;
				if (iterEnd >= 0) {
					lastIndexes[iterEnd]++; // prepare for next round

					while (currAreaSize + areaToComputeSize <= maxAreaSize) { // more elements can be computed
						if (lastIndexes[iterEnd] < jobRequest->area->at(iterEnd).size()) { // not the last in dim
							subArea[iterEnd].push_back(jobRequest->area->at(iterEnd)[lastIndexes[iterEnd]]);
							currAreaSize += areaToComputeSize;
							lastIndexes[iterEnd]++; // prepare for next round
						}
						else {
							break; // dim complete, stop adding
						}
					}
					if (lastIndexes[iterEnd] >= jobRequest->area->at(iterEnd).size()) { // end of dim
						lastIndexes[iterEnd] = 0;
						if (iterEnd == 0) {
							bWork = false;
						}
						else {
							for (int nextIndex = iterEnd - 1; nextIndex >= 0; nextIndex--) {
								if (lastIndexes[nextIndex] + 1 < jobRequest->area->at(nextIndex).size()) {
									lastIndexes[nextIndex]++;
									break;
								}
								else {
									if (nextIndex == 0) {
										bWork = false;
									}
									lastIndexes[nextIndex] = 0;
								}
							}
						}
					}
				}
				else {
					// everything will be done in one loop
					bWork = false;
				}

				bComputeData = computeAreaIdentifiers(dimensions, numResult, jobRequest->baseOnly, &subArea);

				for (uint32_t i = 0; i < requestedArea.size(); i++) {
					std::sort(requestedArea[i].begin(), requestedArea[i].end());
				}

				// build result storages for string values
				vector<AreaStringResultStorage *> stringStorages;
				vector<vector<vector<Element *> > > stringAreas;

				if (jobRequest->type == 0 || jobRequest->type == 2) {
					buildStringResultStorages(numericAreaElementsForString, stringElements, stringStorages, stringAreas);
					// fill string storages
					fillStringResultStorages(stringStorages, stringAreas);
				}

				// build result storage for double values
				HashAreaStorage *doubleStorage = NULL;
				if (jobRequest->type == 0 || jobRequest->type == 1) {
					if (bComputeData) {
						doubleStorage = computeDoubleArea(dimensions, numericAreaElements, numResult);
					}
				}

				loop(&requestedArea, cube, doubleStorage, &stringStorages, &response->getBody(),
						jobRequest->showRule, jobRequest->showLockInfo, userId, condition, linesPrinted, progressOfExport, beginAfter);

				if (linesPrinted >= jobRequest->blockSize) {
					bWork = false;
				}

				size_t max = stringStorages.size();
				for (size_t numStorage = 0; numStorage < max; numStorage++) {
					AreaStringResultStorage *storage = stringStorages[numStorage];
					delete storage;
				}
				if (doubleStorage) {
					delete doubleStorage;
				}
			}
		}

		delete condition;

		body->appendCsvInteger(progressOfExport);
		body->appendCsvInteger(totalElements);
		body->appendEol();
	}

public:


	void loopPage(CubePage *page, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId, Condition *condition,
			uint64_t &linesPrinted, bool &bWork, IdentifiersType &lastPath, bool bIsDouble = true, const IdentifiersType *beginAfter = NULL)
	{
		size_t row_size = page->getRowSize();
		CubePage::buffer_t page_beg = page->begin();
		CubePage::element_t row;
		IdentifierType *path;
		size_t val_size;
		if (bIsDouble) {
			val_size = cube->getStorageDouble()->getValueSize();
		}
		else {
			val_size = cube->getStorageString()->getValueSize();
		}
		bool bCheckBeginAfter = (beginAfter != NULL);
		bool bSkip = false;

		page->sortExportL();
		ReadLocker lock(page->getLock());

		// loop over all rows in the page
		row = page_beg;
		size_t count = page->getUsedElements();
		for (size_t i = 0; i < count; i++, row += row_size) {
			path = (IdentifierType *)(row + val_size);
			double val = 0;
			char **pStr;
			string str;

			if (bIsDouble) {
				val = *(double *)row;
			}
			else {
				pStr = (char **)row;
				str = *pStr;
			}

			lastPath.clear();

			bSkip = false;
			for (uint32_t i = 0; i < jobRequest->area->size(); i++) {
				if (bCheckBeginAfter) {
					if ((*beginAfter)[i] < *path) {
						bCheckBeginAfter = false;
					}
					else if ((*beginAfter)[i] > *path) {
						bSkip = true;
						break;
					}
					else if (/*==*/ i == jobRequest->area->size() - 1) {
						bCheckBeginAfter = false;
						bSkip = true;
						break;
					}
				}
				if (i >= 2) { // apply restrictions
					if (std::find(jobRequest->area->at(i).begin(), jobRequest->area->at(i).end(), *path) == jobRequest->area->at(i).end()) {
						bSkip = true;
						break;
					}
				}
				lastPath.push_back(*path);
				path++;
			}
			if (bSkip) {
				continue;
			}

			if (bIsDouble) {
				if (!condition || condition->check(val)) {
					appendDouble(cube, lastPath, val, true, Rule::NO_RULE, sb, showRule, showLockInfo, userId);
					linesPrinted++;
				}
			}
			else {
				appendString(cube, lastPath, &str, true, Rule::NO_RULE, sb, showRule, showLockInfo, userId);
				linesPrinted++;
			}
			if (linesPrinted >= jobRequest->blockSize) {
				bWork = false;
				break;
			}
		}
	}

	void loopPages(CubePage *pageDouble, CubePage *pageString, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId, Condition *condition,
			uint64_t &linesPrinted, bool &bWork, IdentifiersType &lastPath, bool bIsDouble = true, const IdentifiersType *beginAfter = NULL)
	{
		size_t row_sizeD = 0;
		size_t row_sizeS = 0;
		CubePage::element_t rowD;
		CubePage::element_t rowS;
		bool bCheckBeginAfter = (beginAfter != NULL);
		bool bSkip = false;
		IdentifierType *pathD;
		IdentifierType *pathS;
		IdentifierType *path;
		size_t val_sizeD = cube->getStorageDouble()->getValueSize();
		size_t val_sizeS = cube->getStorageString()->getValueSize();

		row_sizeD = pageDouble->getRowSize();
		CubePage::buffer_t page_begD = pageDouble->begin();
		pageDouble->sortExportL();
		ReadLocker lockD(pageDouble->getLock());
		rowD = page_begD;

		row_sizeS = pageString->getRowSize();
		CubePage::buffer_t page_begS = pageString->begin();
		pageString->sortExportL();
		ReadLocker lockS(pageString->getLock());
		rowS = page_begS;

		size_t countD = pageDouble->getUsedElements();
		size_t countS = pageString->getUsedElements();
		size_t indexD = 0;
		size_t indexS = 0;
		pathD = (IdentifierType *)(rowD + val_sizeD);
		pathS = (IdentifierType *)(rowS + val_sizeS);
		double val;
		char **pStr;
		string str;

		while (indexD < countD || indexS < countS) {

			bool bDoubleFirst = true;
			if (indexD < countD && indexS < countS) {
				for (uint32_t i = 0; i < jobRequest->area->size(); i++) {
					if (pathS[i] < pathD[i]) {
						bDoubleFirst = false;
						break;
					}
					else if (pathS[i] > pathD[i]) {
						break;
					}
				}
			}
			else if (indexS < countS) {
				bDoubleFirst = false;
			}

			lastPath.clear();
			if (bDoubleFirst) {
				path = pathD;
			}
			else {
				path = pathS;
			}

			bSkip = false;
			for (uint32_t i = 0; i < jobRequest->area->size(); i++) {
				if (bCheckBeginAfter) {
					if ((*beginAfter)[i] < *path) {
						bCheckBeginAfter = false;
					}
					else if ((*beginAfter)[i] > *path) {
						bSkip = true;
						break;
					}
					else if (/*==*/ i == jobRequest->area->size() - 1) {
						bCheckBeginAfter = false;
						bSkip = true;
						break;
					}
				}
				if (i >= 2) { // apply restrictions
					if (std::find(jobRequest->area->at(i).begin(), jobRequest->area->at(i).end(), *path) == jobRequest->area->at(i).end()) {
						bSkip = true;
						break;
					}
				}
				lastPath.push_back(*path);
				path++;
			}
			if (bSkip) {
				if (bDoubleFirst) {
					indexD++;
					rowD += row_sizeD;
					pathD = (IdentifierType *)(rowD + val_sizeD);
				}
				else {
					indexS++;
					rowS += row_sizeS;
					pathS = (IdentifierType *)(rowS + val_sizeS);
				}
				continue;
			}

			if (bDoubleFirst) {
				val = *(double *)rowD;
				if (!condition || condition->check(val)) {
					appendDouble(cube, lastPath, val, true, Rule::NO_RULE, sb, showRule, showLockInfo, userId);
					indexD++;
					rowD += row_sizeD;
					pathD = (IdentifierType *)(rowD + val_sizeD);
					linesPrinted++;
				}
			}
			else {
				pStr = (char **)rowS;
				str = *pStr;
				appendString(cube, lastPath, &str, true, Rule::NO_RULE, sb, showRule, showLockInfo, userId);
				indexS++;
				rowS += row_sizeS;
				pathS = (IdentifierType *)(rowS + val_sizeS);
				linesPrinted++;
			}
			if (linesPrinted >= jobRequest->blockSize) {
				bWork = false;
				break;
			}
		}
	}

	void loop(const vector<IdentifiersType> *requestedArea, Cube *cube, HashAreaStorage *doubleStorage,
			vector<AreaStringResultStorage *> *stringStorages, StringBuffer *sb, bool showRule, bool showLockInfo, IdentifierType userId,
			Condition *condition, uint64_t &linesPrinted, uint64_t &progressOfExport, const IdentifiersType *beginAfter = NULL)
	{
		int length = (int)requestedArea->size();
		vector<size_t> combinations(length);

		bool found;
		IdentifierType idRule;
		unsigned int idError;
		bool bJustMoveIndex = false;
		bool bCheckBeginAfter = (beginAfter != NULL);

		int changingDimIndex = length - 1;
		IdentifiersType path;
		path.resize(length);
		while (true) {
			bool containsStringIdentifier = false;
			bool containsUnknownIdentifier = false;
			bool containsNoPermisssionIdentifier = false;
			bool containsConsolidatedIdentifier = false;

			if (!bJustMoveIndex) {
				// construct path
				progressOfExport++;

				for (size_t j = 0; j < requestedArea->size(); j++) {
					IdentifierType id = requestedArea->at(j)[combinations[j]];

					if (bCheckBeginAfter) {
						if ((*beginAfter)[j] > id) {
							bJustMoveIndex = true; // starting element still not found, skip to next path
							break;
						}
						else if ((*beginAfter)[j] == id) {
							if (j == requestedArea->size() - 1) {
								bJustMoveIndex = true;
								bCheckBeginAfter = false; // all next will be ok, this is last not to compute
								break;
							}
							else {
								// still same, check next dim
							}
						}
						else /*if ((*beginAfter)[j] < id)*/ {
							bCheckBeginAfter = false; // first path found, all next paths are ok too
						}
					}

					set<IdentifierType>::iterator find = stringIdentifiers.at(j).find(id);
					if (find != stringIdentifiers.at(j).end()) {
						containsStringIdentifier = true;
					}

					find = unknownIdentifiers.at(j).find(id);
					if (find != unknownIdentifiers.at(j).end()) {
						containsUnknownIdentifier = true;
					}

					find = noPermisssionIdentifiers.at(j).find(id);
					if (find != noPermisssionIdentifiers.at(j).end()) {
						containsNoPermisssionIdentifier = true;
					}

					find = consolidationIdentifiers.at(j).find(id);
					if (find != consolidationIdentifiers.at(j).end()) {
						containsConsolidatedIdentifier = true;
					}

					path[j] = id;
				}
			}

			if (!bJustMoveIndex) {
				if (containsUnknownIdentifier) {
					appendError(path, sb, ErrorException::ERROR_ELEMENT_NOT_FOUND, showRule, showLockInfo);
					linesPrinted++;
				}
				else if (containsNoPermisssionIdentifier) {
					appendError(path, sb, ErrorException::ERROR_NOT_AUTHORIZED, showRule, showLockInfo);
					linesPrinted++;
				}
				else if (containsStringIdentifier && (jobRequest->type == 0 || jobRequest->type == 2)) {
					vector<AreaStringResultStorage*>::iterator storeIter = stringStorages->begin();

					for (; storeIter != stringStorages->end(); storeIter++) {
						if ((*storeIter)->containsPath(&path)) {
							string *value = (*storeIter)->getValue(&path, &idRule, &idError, &found);

							if (!jobRequest->skipEmpty || found) {
								if (idError > 0) {
									appendError(path, sb, (ErrorException::ErrorType)idError, showRule, showLockInfo);
									linesPrinted++;
								} else {
									appendString(cube, path, value, found, idRule, sb, showRule, showLockInfo, userId);
									linesPrinted++;
									break;
								}
							}
						}
					}
				} else if (!containsStringIdentifier && (jobRequest->type == 0 || jobRequest->type == 1)
						&& (!containsConsolidatedIdentifier || !jobRequest->baseOnly)) {
					SimpleCache::const_cache_type_iterator val = cached_area.find(path);
					if (val != cached_area.end()) {
						if (SimpleCache::NOT_FOUND == val->second.second) {
							if (!jobRequest->skipEmpty && (!condition || condition->check(0))) {
								appendDouble(cube, path, 0, false, Rule::NO_RULE, sb, showRule, showLockInfo, userId);
								linesPrinted++;
							}
						} else {
							SimpleCache::dispatch(val->second, idRule, idError);
							if (idError > 0) {
								appendError(path, sb, (ErrorException::ErrorType)idError, showRule, showLockInfo);
								linesPrinted++;
							} else {
								double value = val->second.first;
								Cube::checkZero(value);
								if (!condition || condition->check(value)) {
									appendDouble(cube, path, value, true, idRule, sb, showRule, showLockInfo, userId);
									linesPrinted++;
								}
							}
						}
					} else if (doubleStorage && (!containsConsolidatedIdentifier || !jobRequest->baseOnly)) {
						double *value = doubleStorage->getValue(&path, &idRule, &idError, &found);

						if (idError > 0) {
							appendError(path, sb, (ErrorException::ErrorType)idError, showRule, showLockInfo);
							linesPrinted++;
						}
						else if ((!jobRequest->skipEmpty || found) && (!condition || condition->check(*value))) {
							Cube::checkZero(*value);
							appendDouble(cube, path, *value, found, idRule, sb, showRule, showLockInfo, userId);
							linesPrinted++;
						}
					}
				}
			}

			if (linesPrinted >= jobRequest->blockSize) {
				break;
			}

			size_t position = combinations[changingDimIndex];
			const IdentifiersType& dim = requestedArea->at(changingDimIndex);

			if (position + 1 < dim.size()) {
				combinations[changingDimIndex] = (int)position + 1;
				changingDimIndex = length - 1;
				bJustMoveIndex = false;
			} else {
				if (changingDimIndex == 0) {
					// all done
					break;
				}

				changingDimIndex--;
				for (int k = length - 1; k > changingDimIndex; k--) {
					combinations[k] = 0;
				}
				bJustMoveIndex = true;
			}
		}
	}

	virtual void appendError(const IdentifiersType& path, StringBuffer* sb, ErrorException::ErrorType type, bool showRule, bool showLockInfo)
	{

		sb->appendCsvInteger((int32_t)99);
		sb->appendCsvInteger((int32_t)type);
		sb->appendCsvString(StringUtils::escapeString(ErrorException::getDescriptionErrorType(type)));

		for (size_t i = 0; i < path.size(); i++) {
			if (0 < i) {
				sb->appendChar(',');
			}
			sb->appendInteger(path[i]);
		}

		sb->appendChar(';');

		if (showRule) {
			sb->appendChar(';');
		}
		if (showLockInfo) {
			sb->appendCsvInteger(0);
		}

		sb->appendEol();
	}

	virtual void appendDouble(Cube * cube, const IdentifiersType& path, double value, bool found, IdentifierType idRule, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId)
	{

		sb->appendCsvInteger((int32_t)1);

		if (found) {
			sb->appendCsvString("1");
			sb->appendCsvDouble(value);
		} else {
			sb->appendCsvString("0;");
		}

		for (size_t i = 0; i < path.size(); i++) {
			if (0 < i) {
				sb->appendChar(',');
			}
			sb->appendInteger(path[i]);
		}

		sb->appendChar(';');

		if (showRule) {
			if (idRule != Rule::NO_RULE) {
				sb->appendCsvInteger((uint32_t)idRule);
			} else {
				sb->appendChar(';');
			}
		}

		if (showLockInfo) {
			Cube::CellLockInfo lockInfo = 0;
			CellPath cp(cube, &path);
			lockInfo = cube->getCellLockInfo(&cp, userId); //safe to call getCellLockInfo because cp is valid path
			sb->appendCsvInteger((uint32_t)lockInfo);
		}

		sb->appendEol();
	}

	virtual void appendString(Cube * cube, const IdentifiersType& path, string* value, bool found, IdentifierType idRule, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId)
	{

		sb->appendCsvInteger((int32_t)2);

		if (found) {
			sb->appendCsvString("1");
			sb->appendCsvString(StringUtils::escapeString(*value));
		} else {
			sb->appendCsvString("0;");
		}

		for (size_t i = 0; i < path.size(); i++) {
			if (0 < i) {
				sb->appendChar(',');
			}
			sb->appendInteger(path[i]);
		}

		sb->appendChar(';');

		if (showRule) {
			if (idRule != Rule::NO_RULE) {
				sb->appendCsvInteger((uint32_t)idRule);
			} else {
				sb->appendChar(';');
			}
		}

		if (showLockInfo) {
			Cube::CellLockInfo lockInfo = 0;
			CellPath cp(cube, &path);
			lockInfo = cube->getCellLockInfo(&cp, userId); //safe to call getCellLockInfo because cp is valid path
			sb->appendCsvInteger((uint32_t)lockInfo);
		}

		sb->appendEol();
	}

};

}

#endif
