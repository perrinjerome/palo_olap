////////////////////////////////////////////////////////////////////////////////
/// @brief dimension element job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Martin Jakl, qBicon s.r.o., Prague, Czech Republic
////////////////////////////////////////////////////////////////////////////////

#ifndef DIMENSIONDFILTERJOB_H_
#define DIMENSIONDFILTERJOB_H_

#include "palo.h"

#include "PaloDispatcher/DirectPaloJob.h"

#include <boost/shared_ptr.hpp>

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief dimension dfilter
////////////////////////////////////////////////////////////////////////////////
class CellArrayAccumulator {
protected:
	double m_accumulation;
	boost::shared_ptr<Condition> m_op;
public:
	CellArrayAccumulator() : m_accumulation(0) {}
	virtual void addVal(double d) {};
	virtual void addVal(const string &s) {};
	virtual bool finalize() = 0;
	void setOperator(boost::shared_ptr<Condition> op) {m_op = op;}
	double getAccumulation()
	{
		return m_accumulation;
	}

	virtual string getString() {return "";}

	virtual void reset()
	{
		m_accumulation = 0;
	}

	static CellArrayAccumulator *create(long flag, boost::shared_ptr<Condition> op);
};

class SERVER_CLASS DimensionDFilterJob : public CellAreaJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new DimensionDFilterJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	DimensionDFilterJob(Server * server, PaloJobRequest* jobRequest) :
			CellAreaJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return READ_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute();

private:
	virtual void appendError(const IdentifiersType& path, StringBuffer* sb, ErrorException::ErrorType type, bool showRule, bool showLockInfo);
	virtual void appendDouble(Cube * cube, const IdentifiersType& path, double value, bool found, IdentifierType idRule, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId);
	virtual void appendString(Cube * cube, const IdentifiersType& path, string* value, bool found, IdentifierType idRule, StringBuffer* sb, bool showRule, bool showLockInfo, IdentifierType userId);

	void top(list<Element*> &subset, vector<Cube::CellValueType> &vals, int top_num);
	void upperPer(list<Element*> &subset, vector<Cube::CellValueType> &vals, double u);
	void lowerPer(list<Element*> &subset, vector<Cube::CellValueType> &vals, double l);
	void middlePer(list<Element*> &subset, vector<Cube::CellValueType> &vals, double u, double l);

	size_t pos;
	map<IdentifierType, boost::shared_ptr<CellArrayAccumulator> > accumulators;

	template<class T> class PercentageAccumulator {
	private:
		long double m_sum;
		long double m_limit;
		multimap<Cube::CellValueType, Element*, T> m_swaplist;

	public:
		PercentageAccumulator(DimensionDFilterJob *df, multimap<Cube::CellValueType, Element*, T> &sort, double percentage);
		virtual bool check(typename multimap<Cube::CellValueType, Element*, T>::iterator it);
		multimap<Cube::CellValueType, Element*, T>& get_swaplist();
		void removeif(multimap<Cube::CellValueType, Element*, T> &sort);
	};

	template<class T> class PercentageNegAccumulator : public PercentageAccumulator<T> {
	public:
		PercentageNegAccumulator(DimensionDFilterJob *df, multimap<Cube::CellValueType, Element*, T> &sort, double percentage);
		virtual bool check(typename multimap<Cube::CellValueType, Element*, T>::iterator it);
	};
};

}

#endif /* DIMENSIONDFILTERJOB_H_ */
