////////////////////////////////////////////////////////////////////////////////
/// @brief cells values
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Jiri Junek, qBicon s.r.o., Prague, Czech Republic
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_CELL_VALUES_JOB_H
#define PALO_JOBS_CELL_VALUES_JOB_H 1

#include "palo.h"

#include "Olap/AreaStorage.h"
#include "Olap/HashAreaStorage.h"
#include "Olap/Rule.h"
#include "Olap/SubCubeList.h"
#include "PaloDispatcher/DirectPaloJob.h"
#include "PaloJobs/AreaJob.h"



namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cells value
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CellValuesJob : public AreaJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new CellValuesJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CellValuesJob(Server * server, PaloJobRequest* jobRequest) :
		AreaJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return READ_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////
private:

	struct NumericResult {
		double value;
		unsigned int idError;
		IdentifierType idRule;
		bool found;
		bool filled;
	};

	struct PathCmp {
		bool operator()(const IdentifiersType *v1, const IdentifiersType *v2) const
		{
			size_t dimCount = v1->size();
			for (size_t i = 0; i < dimCount; i++) {
				if ((*v1)[i] != (*v2)[i]) {
					return (*v1)[i] < (*v2)[i];
				}
			}
			return false;
		}
	};
	map<const IdentifiersType *, NumericResult, PathCmp> orderedPaths; //key: cell path (coordinate), value: result of calculation

	const vector<Dimension*>* dimensions;
	vector<CellPath*> requestedCellPaths;
	vector<CellPath*> nonCachedCellPaths;
	vector<vector<Element*> > numericAreaElements;

	set<size_t> stringPaths;
	set<size_t> invalidPaths;
	set<size_t> unknownIdentifiersPaths;
	set<size_t> noPermisssionPaths;

	SimpleCache::cache_block cached_area;

	vector<string> stringValues;
	vector<IdentifierType> stringRules;
	vector<int> stringErrors;


public:

	void compute()
	{
		size_t numResult;
		bool computeData = true;

		findCube();

		dimensions = cube->getDimensions();

		if (jobRequest->paths) {
			computeData = computeAreaIdentifiers(numResult);
		} else {
			throw ParameterException(ErrorException::ERROR_INVALID_COORDINATES, "path is empty, list of element identifiers is missing", PaloRequestHandler::ID_PATHS, "");
		}

		if (Logger::isDebug()) {
			Logger::debug << "cell/values size = " << numResult << endl;
		}

		computeStringValues();

		StringBuffer sbResponse;
		sbResponse.initialize();



		//{
		//	CellPath *pNextCellPath = 0;

		//	vector<CellPath*>::const_iterator pathit = nonCachedCellPaths.begin();

		//	do {
		//		buildAreaElements(pathit, pNextCellPath);

		//		// build result storage for double values
		//		HashAreaStorage* doubleStorage = NULL;

		//		if (computeData) {
		//			doubleStorage = computeDoubleArea(dimensions, numericAreaElements, numResult);
		//		}

		//		loop(&sbResponse, doubleStorage, iLoop, pNextCellPath, loopCountStrings);
		//
		//		if (NULL != doubleStorage) {
		//			delete doubleStorage;
		//		}
		//		doubleStorage = NULL;
		//	} while (pNextCellPath);
		//}

		{
			if (computeData) {
				SubCubeList *subCubes = new SubCubeList();
				buildSubCubes(subCubes, nonCachedCellPaths);

				size_t size = subCubes->size();
				subCubes->reset();
				for (size_t i = 0; i < size; i++) {
					SubCube &cube = *subCubes->next();

					HashAreaStorage *doubleStorage = 0;
					try {
						buildAreaElementsFromSubCube(cube);

						doubleStorage = computeDoubleArea(dimensions, numericAreaElements, numResult);

						loopOverSubCube(cube, doubleStorage);
					} catch (...) {
						delete doubleStorage;
						delete subCubes;
						for (vector<CellPath*>::iterator i = requestedCellPaths.begin(); i != requestedCellPaths.end(); i++) {
							CellPath* cp = *i;
							if (cp) {
								delete cp;
							}
						}
						throw ;
					}

					delete doubleStorage;
				}
				delete subCubes;
			}

			response = new HttpResponse(HttpResponse::OK);
			setToken(cube);
			//response->getBody().appendText(sbResponse);
			loop2(&response->getBody());   
		}

		for (vector<CellPath*>::iterator i = requestedCellPaths.begin(); i != requestedCellPaths.end(); i++) {
			CellPath* cp = *i;
			if (cp) {
				delete cp;
			}
		}
	}

	void computeStringValues()
	{
		stringValues.resize(stringPaths.size());
		stringRules.resize(stringPaths.size());
		stringErrors.resize(stringPaths.size());

		size_t countStrings = 0;

		size_t max = requestedCellPaths.size();
		for (size_t i = 0; i < max; i++) {
			CellPath* cellPath = requestedCellPaths[i];

			if (cellPath) {
				set<size_t>::iterator isString = stringPaths.find(i);
				if (isString != stringPaths.end()) {
					bool found = false;

					try {
						set<pair<Rule*, IdentifiersType> > ruleHistory;
						Cube::CellValueType value = cube->getCellValueNew(cellPath, &found, &ruleHistory, &memory_context);
						if (found && value.type == STRING) {
							stringValues[countStrings] = value.charValue;
							stringRules[countStrings] = value.rule;
							stringErrors[countStrings] = 0;
						}
					}
					// Error
					catch (ErrorException ee) {
						stringErrors[countStrings] = ee.getErrorType();
					}

					countStrings++;
				}
			}
		}
	}

	bool computeAreaIdentifiers(size_t& numResult)
	{
		numResult = 0;
		vector<CellPath*> numericCellPaths;

		bool checkPermissions = false;
		RightsType cubePermission = RIGHT_NONE;

		if (cube->getMinimumAccessRight(user) == RIGHT_NONE) {
			checkPermissions = true;
			cubePermission = cube->getCubeAccessRight(user);
		}

		size_t numDimensions = dimensions->size();

		numResult = (jobRequest->paths)->size();
		vector<Element*> pathElements(numDimensions);

		// loop over all requested paths
		for (size_t i = 0; i < numResult; i++) {
			// check length of path
			vector<IdentifierType>& requestPath = jobRequest->paths->at(i);
			if (requestPath.size() != numDimensions) {
				invalidPaths.insert(i);
				requestedCellPaths.push_back(0);
				continue;
			}

			CellPath* cp = 0;
			for (size_t pos = 0; pos < numDimensions; pos++) {
				Dimension* dim = dimensions->at(pos);

				Element* element = dim->lookupElement(requestPath[pos]);
				if (element) {
					// check rights
					if (checkPermissions) {
						RightsType elementPermission = min(cubePermission, cube->getElementAccessRight(user, dim, element));
						if (elementPermission < RIGHT_READ) {
							noPermisssionPaths.insert(i);
							break;
						}
					}

					// add element to path
					pathElements[pos] = element;
					if (pos == numDimensions - 1) {
						cp = new CellPath(cube, &pathElements);
					}
				} else {
					// element not found
					unknownIdentifiersPaths.insert(i);
					break;
				}
			}

			requestedCellPaths.push_back(cp);

			if (cp) {
				// check for string path
				if (STRING == cp->getPathType()) {
					stringPaths.insert(i);
				} else {
					numericCellPaths.push_back(cp);
				}
			}

		}

		cube->cache.getValues(numericCellPaths, nonCachedCellPaths, cached_area);

		return !nonCachedCellPaths.empty();
	}

	//	void buildAreaElements(vector<CellPath*>::const_iterator &pathit, CellPath *&pNextCellPath)
	//	{
	//		size_t AreaSize = 0;
	//
	//		size_t numDimensions = dimensions->size();
	//		numericAreaElements.clear();
	//		numericAreaElements.resize(numDimensions);
	//		vector<CellPath*>::const_iterator endit = nonCachedCellPaths.end();
	//
	//		for (; pathit != endit; pathit++) {
	//			const PathType *pathElements = (*pathit)->getPathElements();
	//			size_t pos = 0;
	//
	//			vector<Element*> pathNewElems(numDimensions);
	//
	//			size_t NewAreaSize = AreaSize;
	//
	//			for (pos = 0; pos < numDimensions; pos++) {
	//				vector<Element*>& crt = numericAreaElements[pos];
	//				if (crt.end() != std::find(crt.begin(), crt.end(), (*pathElements)[pos])) {
	//					pathNewElems[pos] = 0;
	//					continue;
	//				}
	//
	//				if (!crt.empty()) {
	//					NewAreaSize = NewAreaSize / crt.size() * (crt.size() + 1);
	//					if (NewAreaSize > 10000) {
	//						// do not build Areas bigger than 10k cells
	//						break;
	//					}
	//				}
	//				pathNewElems[pos] = (*pathElements)[pos];
	//			}
	//			if (pos < numDimensions) {
	//				// do not expand area - break here
	//				break;
	//			}
	//			// extend area for the current cell
	//			for (pos = 0; pos < numDimensions; pos++) {
	//				if (pathNewElems[pos]) {
	//					numericAreaElements[pos].push_back(pathNewElems[pos]);
	//				}
	//			}
	//			if (!AreaSize) {
	//				AreaSize = 1;
	//			} else {
	//				AreaSize = NewAreaSize;
	//			}
	//		}
	//		if (pathit == endit) {
	//			pNextCellPath = 0;
	//		} else {
	//			pNextCellPath = *pathit;
	//		}
	//	}


	void loop(StringBuffer *sb, HashAreaStorage *doubleStorage, size_t &iLoop, CellPath *pNextCellPath, size_t &loopCountStrings)
	{
		IdentifierType userId = user ? user->getIdentifier() : 0;
		bool found;
		IdentifierType idRule;
		unsigned int idError;


		size_t iEnd = requestedCellPaths.size();
		for (; iLoop < iEnd; iLoop++) {
			CellPath* cellPath = requestedCellPaths[iLoop];

			if (!cellPath) {
				set<size_t>::iterator error = invalidPaths.find(iLoop);
				if (error != invalidPaths.end()) {
					appendError(sb, ErrorException::ERROR_INVALID_COORDINATES, 0, jobRequest->showRule, jobRequest->showLockInfo);
					continue;
				}

				error = unknownIdentifiersPaths.find(iLoop);
				if (error != unknownIdentifiersPaths.end()) {
					appendError(sb, ErrorException::ERROR_INVALID_COORDINATES, 0, jobRequest->showRule, jobRequest->showLockInfo);
					continue;
				}

				error = noPermisssionPaths.find(iLoop);
				if (error != noPermisssionPaths.end()) {
					appendError(sb, ErrorException::ERROR_NOT_AUTHORIZED, 0, jobRequest->showRule, jobRequest->showLockInfo);
					continue;
				}
			} else {
				set<size_t>::iterator isString = stringPaths.find(iLoop);
				if (isString != stringPaths.end()) {

					if (stringErrors[loopCountStrings] > 0) {
						appendError(sb, (ErrorException::ErrorType)stringErrors[loopCountStrings], 0, jobRequest->showRule, jobRequest->showLockInfo);
					} else {
						bool found = stringValues[loopCountStrings].length() > 0;
						appendString(sb, cellPath, &stringValues[loopCountStrings], found, stringRules[loopCountStrings], jobRequest->showRule, jobRequest->showLockInfo, userId);
					}
					loopCountStrings++;
				} else {
					SimpleCache::const_cache_type_iterator val = cached_area.find(*cellPath->getPathIdentifier());
					if (val != cached_area.end()) {
						if (SimpleCache::NOT_FOUND == val->second.second) {
							appendDouble(sb, cellPath, 0, false, Rule::NO_RULE, jobRequest->showRule, jobRequest->showLockInfo, userId);
						} else {
							SimpleCache::dispatch(val->second, idRule, idError);

							if (idError > 0) {
								appendError(sb, (ErrorException::ErrorType)idError, 0, jobRequest->showRule, jobRequest->showLockInfo);
							} else {
								double value = val->second.first;
								Cube::checkZero(value);
								appendDouble(sb, cellPath, value, true, idRule, jobRequest->showRule, jobRequest->showLockInfo, userId);
							}
						}
					} else if (doubleStorage) {
						if (pNextCellPath == cellPath) {
							// end of the batch -> return;
							break;
						}

						double* value = doubleStorage->getValue(cellPath->getPathIdentifier(), &idRule, &idError, &found);

						if (idError > 0) {
							appendError(sb, (ErrorException::ErrorType)idError, idRule, jobRequest->showRule, jobRequest->showLockInfo);
						} else {
							Cube::checkZero(*value);
							appendDouble(sb, cellPath, *value, found, idRule, jobRequest->showRule, jobRequest->showLockInfo, userId);
						}

					}
				}
			}
		}
	}

	void buildSubCubes(SubCubeList *subCubes, vector<CellPath *> &cellPaths)
	{
		//sort cellPaths
		NumericResult res;
		res.filled = false;
		for (vector<CellPath *>::iterator it = cellPaths.begin(); it != cellPaths.end(); it++) {
			orderedPaths.insert(pair<const IdentifiersType *, NumericResult>((*it)->getPathIdentifier(), res));
		}

		for (map<const IdentifiersType *, NumericResult, PathCmp>::iterator it = orderedPaths.begin(); it != orderedPaths.end(); it++) {
			subCubes->addCell(it->first);
		}
	}

	void buildAreaElementsFromSubCube(SubCube &cube)
	{
		size_t dimCount = cube.size();
		numericAreaElements.clear();
		numericAreaElements.resize(dimCount);

		for (size_t i = 0; i < dimCount; i++) {
			SetID &setID = *cube[i];
			for (SetID::iterator it = setID.begin(); it != setID.end(); it++) {
				Element *elem = (*dimensions)[i]->lookupElement(*it);
				if (elem) {
					numericAreaElements[i].push_back(elem);
				} else {
					throw ErrorException(ErrorException::ERROR_INTERNAL, "in CellValuesJob::buildAreaElementsFromSubCube");
				}
			}
		}
	}

	void loopOverSubCube(SubCube &cube, HashAreaStorage *doubleStorage)
	{
		size_t dimCount = cube.size();

		IdentifiersType path(dimCount);
		vector<SetID::iterator> vit(dimCount);
		for (size_t i = 0; i < dimCount; i++) {
			vit[i] = (*(cube[i])).begin();
		}

		do {
			//fill path
			for (size_t i = 0; i < dimCount; i++) {
				path[i] = *(vit[i]);
			}

			//read result
			unsigned int idError;
			IdentifierType idRule;
			bool found;

			double *value = doubleStorage->getValue(&path, &idRule, &idError, &found);

			//store result
			map<const IdentifiersType *, NumericResult, PathCmp>::iterator it = orderedPaths.find(&path);
			if (it == orderedPaths.end()) {
				throw ErrorException(ErrorException::ERROR_INTERNAL, "in CellValuesJob::loopOverSubCube, no iterator");
			} else {
				NumericResult &numRes = it->second;
				if (numRes.filled) {
					throw ErrorException(ErrorException::ERROR_INTERNAL, "in CellValuesJob::loopOverSubCube, same cell");
				} else {
					numRes.value = *value;
					numRes.idError = idError;
					numRes.idRule = idRule;
					numRes.found = found;
					numRes.filled = true;
				}
			}

			//get next position
			int pos;
			for (pos = (int)dimCount - 1; pos >= 0; pos--) {
				vit[pos]++;
				if (vit[pos] != (*(cube[pos])).end()) {
					break;
				}
			}
			if (pos < 0) {
				break;
			} else {
				for (size_t i = pos + 1; i < dimCount; i++) {
					vit[i] = (*(cube[i])).begin();
				}
			}
		}while (true);
	}

	void loop2(StringBuffer* sb)
	{
		size_t countStrings = 0;
		IdentifierType userId = user ? user->getIdentifier() : 0;
		bool found;
		IdentifierType idRule;
		unsigned int idError;

		size_t max = requestedCellPaths.size();
		for (size_t i = 0; i < max; i++) {
			CellPath* cellPath = requestedCellPaths[i];

			if (!cellPath) {
				set<size_t>::iterator error = invalidPaths.find(i);
				if (error != invalidPaths.end()) {
					appendError(sb, ErrorException::ERROR_INVALID_COORDINATES, 0, jobRequest->showRule, jobRequest->showLockInfo);
					continue;
				}

				error = unknownIdentifiersPaths.find(i);
				if (error != unknownIdentifiersPaths.end()) {
					appendError(sb, ErrorException::ERROR_INVALID_COORDINATES, 0, jobRequest->showRule, jobRequest->showLockInfo);
					continue;
				}

				error = noPermisssionPaths.find(i);
				if (error != noPermisssionPaths.end()) {
					appendError(sb, ErrorException::ERROR_NOT_AUTHORIZED, 0, jobRequest->showRule, jobRequest->showLockInfo);
					continue;
				}
			} else {
				set<size_t>::iterator isString = stringPaths.find(i);
				if (isString != stringPaths.end()) {

					if (stringErrors[countStrings] > 0) {
						appendError(sb, (ErrorException::ErrorType)stringErrors[countStrings], 0, jobRequest->showRule, jobRequest->showLockInfo);
					} else {
						bool found = stringValues[countStrings].length() > 0;
						appendString(sb, cellPath, &stringValues[countStrings], found, stringRules[countStrings], jobRequest->showRule, jobRequest->showLockInfo, userId);
					}
					countStrings++;
				} else {
					SimpleCache::const_cache_type_iterator val = cached_area.find(*cellPath->getPathIdentifier());
					if (val != cached_area.end()) {
						if (SimpleCache::NOT_FOUND == val->second.second) {
							appendDouble(sb, cellPath, 0, false, Rule::NO_RULE, jobRequest->showRule, jobRequest->showLockInfo, userId);
						} else {
							SimpleCache::dispatch(val->second, idRule, idError);

							if (idError > 0) {
								appendError(sb, (ErrorException::ErrorType)idError, 0, jobRequest->showRule, jobRequest->showLockInfo);
							} else {
								double value = val->second.first;
								Cube::checkZero(value);
								appendDouble(sb, cellPath, value, true, idRule, jobRequest->showRule, jobRequest->showLockInfo, userId);
							}
						}
					} else {
						double *value;

						map<const IdentifiersType *, NumericResult, PathCmp>::iterator it = orderedPaths.find(cellPath->getPathIdentifier());
						if (it == orderedPaths.end()) {
							throw ErrorException(ErrorException::ERROR_INTERNAL, "in CellValuesJob::loop2, no iterator");
						} else {
							NumericResult &numRes = it->second;
							if (!numRes.filled) {
								throw ErrorException(ErrorException::ERROR_INTERNAL, "in CellValuesJob::loop2, empty result");
							} else {
								value = &numRes.value;
								idError = numRes.idError;
								idRule = numRes.idRule;
								found = numRes.found;
							}
						}

						if (idError > 0) {
							appendError(sb, (ErrorException::ErrorType)idError, idRule, jobRequest->showRule, jobRequest->showLockInfo);
						} else {
							Cube::checkZero(*value);
							appendDouble(sb, cellPath, *value, found, idRule, jobRequest->showRule, jobRequest->showLockInfo, userId);
						}
					}
				}
			}
		}
	}

};
}

#endif
