////////////////////////////////////////////////////////////////////////////////
/// @brief element replace job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_ELEMENT_REPLACE_JOB_H
#define PALO_JOBS_ELEMENT_REPLACE_JOB_H 1

#include "palo.h"

#include "PaloDispatcher/DirectPaloJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief element replace
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS ElementReplaceJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new ElementReplaceJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	ElementReplaceJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findDimension();

		bool hasChildren = false;
		bool isCreated = false;

		string name = "";

		if (jobRequest->newName != 0) {
			name = *jobRequest->newName;
		} else if (jobRequest->elementName != 0) {
			name = *jobRequest->elementName;
		}

		ElementType elementType = elementTypeByIdentifier(jobRequest->type);

		try {
			findElement();
			if (element->getElementType() != elementType) {
				dimension->changeElementType(element, elementType, user, false);
			}
		} catch (ParameterException e) {
			// create element
			element = dimension->addElement(name, elementType, user);
			isCreated = true;
		}

		// add children
		if (elementType == CONSOLIDATED) {
			ElementsWeightType children;
			set<Element*> childrenSet;

			try {
				// get weights
				vector<double>* weights = 0;

				if (jobRequest->weights) {
					if (jobRequest->weights->size() == 1 && jobRequest->weights->at(0).size() > 0) {
						weights = &jobRequest->weights->at(0);
					}
				}

				// get children
				if (jobRequest->children != 0) {
					if (jobRequest->children->size() > 1) {
						throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "too many children", PaloRequestHandler::ID_CHILDREN, "");
					} else if (jobRequest->children->size() == 1) {
						vector<IdentifierType>& childrenIds = jobRequest->children->at(0);

						if (weights && childrenIds.size() > weights->size()) {
							throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing weight", PaloRequestHandler::WEIGHTS, "");
						}

						for (size_t i = 0; i < childrenIds.size(); i++) {
							double weight = 1.0;

							Element * child = dimension->findElement(childrenIds.at(i), user);

							if (weights) {
								weight = weights->at(i);
							}

							children.push_back(pair<Element*, double> (child, weight));
							childrenSet.insert(child);

							hasChildren = true;
						}
					}
				} else if (jobRequest->childrenName != 0) {
					if (jobRequest->childrenName->size() > 1) {
						throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "too many children", PaloRequestHandler::NAME_CHILDREN, "");
					} else if (jobRequest->children->size() == 1) {
						vector<string>& childrenNames = jobRequest->childrenName->at(0);

						if (weights && childrenNames.size() > weights->size()) {
							throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing weight", PaloRequestHandler::WEIGHTS, "");
						}

						for (size_t i = 0; i < childrenNames.size(); i++) {
							double weight = 1.0;

							Element * child = dimension->findElementByName(childrenNames.at(i), user);

							if (weights) {
								weight = weights->at(i);
							}

							children.push_back(pair<Element*, double> (child, weight));
							childrenSet.insert(child);

							hasChildren = true;
						}
					}
				} else {
					if (weights && weights->size() != 0) {
						throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "too many weights", PaloRequestHandler::WEIGHTS, (int)weights->size());
					}
				}
			} catch (...) {

				// error found, we have to delete the new element
				if (isCreated) {
					dimension->deleteElement(element, 0);
				}

				throw ;
			}

			const ElementsWeightType oldChildren = dimension->getChildren(element);

			if (!isCreated) {

				// we cannot remove all children, because this might change the parent order
				// therefore remove only unknown childs
				dimension->removeChildrenNotIn(user, element, &childrenSet);
			}

			if (hasChildren) {
				try {
					dimension->addChildren(element, &children, user);
				} catch (...) {
					dimension->addChildren(element, &oldChildren, 0);
					throw ;
				}
			}
		}

		server->invalidateCache();

		generateElementResponse(dimension, element);
	}
};

}

#endif
