////////////////////////////////////////////////////////////////////////////////
/// @brief element replace bulk job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// @author Martin Jakl, qBicon s.r.o., Prague, Czech Republic
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_ELEMENT_REPLACE_BULK_JOB_H
#define PALO_JOBS_ELEMENT_REPLACE_BULK_JOB_H 1

#include "palo.h"

#include "PaloDispatcher/DirectPaloJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief element bulk replace
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS ElementReplaceBulkJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new ElementReplaceBulkJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	ElementReplaceBulkJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findDimension();
		ElementType elementType = elementTypeByIdentifier(jobRequest->type);

		size_t elemcount = 0;
		if (jobRequest->elements) {
			elemcount = jobRequest->elements->size();
		} else if (jobRequest->elementsName) {
			elemcount = jobRequest->elementsName->size();
		} else {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing elements", PaloRequestHandler::ID_ELEMENTS, "");
		}
		if (elementType == CONSOLIDATED) {
			if (!jobRequest->children || jobRequest->children->size() != elemcount) {
				throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing children", PaloRequestHandler::ID_CHILDREN, "");
			}
			if (!jobRequest->weights || jobRequest->weights->size() != elemcount) {
				throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing weights", PaloRequestHandler::WEIGHTS, "");
			}
			vector<vector<IdentifierType> >::const_iterator itc;
			vector<vector<double> >::const_iterator itw;
			for (itc = jobRequest->children->begin(), itw = jobRequest->weights->begin(); itc != jobRequest->children->end(); itc++, itw++) {
				if (itc->size() > itw->size()) {
					throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing weights", PaloRequestHandler::WEIGHTS, "");
				}
			}
		}

		vector<pair<Element *, bool> > elemsToReplace;
		map<Element *, pair<ElementType, ElementsWeightType> > oldTypes;

		if (jobRequest->elements) {
			for (vector<IdentifierType>::const_iterator it = jobRequest->elements->begin(); it != jobRequest->elements->end(); it++) {
				elemsToReplace.push_back(pair<Element *, bool> (dimension->findElement(*it, user), false));
			}
		} else {
			for (vector<string>::const_iterator it = jobRequest->elementsName->begin(); it != jobRequest->elementsName->end(); it++) {
				Element *e;
				bool created = false;
				try {
					e = dimension->findElementByName(*it, user);
				} catch (ParameterException ex) {
					e = dimension->addElement(*it, elementType, user);
					created = true;
				}
				elemsToReplace.push_back(pair<Element *, bool> (e, created));
			}
		}
		try {
			// prepare vector<vector<Element *> > - children to be assigned to each element, so no recomputation of user rights cubes
			// will be done in each dimension->findElement below (tokens will be changed often (removechildren etc.), new rights objects needed)
			vector<vector<Element *> > vChildren;
			if (elementType == CONSOLIDATED) {
				vChildren.resize(jobRequest->children->size());
				for (size_t i = 0; i != jobRequest->children->size(); i++) {
					for (vector<IdentifierType>::const_iterator itc = jobRequest->children->at(i).begin(); itc != jobRequest->children->at(i).end(); itc++) {
						Element *e = dimension->findElement(*itc, user);
						vChildren[i].push_back(e);
					}
				}
			}

			size_t i = 0;
			for (vector<pair<Element *, bool> >::iterator it = elemsToReplace.begin(); it != elemsToReplace.end(); it++, i++) {
				Element *e = it->first;
				ElementType et = e->getElementType();
				ElementsWeightType tmp;
				ElementsWeightType echcpy;
				const ElementsWeightType *ech = &tmp;
				if (!it->second) {
					if (et == CONSOLIDATED) {
						echcpy = dimension->getChildren(e);
						ech = &echcpy;
					}
					oldTypes.insert(pair<Element *, pair<ElementType, ElementsWeightType> > (e, pair<ElementType, ElementsWeightType> (et, *ech)));
					if (et != elementType) {
						dimension->changeElementType(e, elementType, user, false);
					}
				}
				if (elementType == CONSOLIDATED) {
					set<Element *> keep;
					ElementsWeightType add;
					vector<double>::const_iterator itw;
					int child = 0;
					for (itw = jobRequest->weights->at(i).begin(); itw != jobRequest->weights->at(i).end(); child++, itw++) {
						Element *el = vChildren[i][child];
						if (!it->second) {
							keep.insert(el);
						}
						add.push_back(pair<Element *, double> (el, *itw));
					}

					if (!it->second) {
						dimension->removeChildrenNotIn(user, e, &keep);
					}
					dimension->addChildren(e, &add, user);
				}
			}
		} catch (...) {
			rollback(elemsToReplace, oldTypes);
			throw ;
		}

		server->invalidateCache();
		generateOkResponse(dimension);
	}

	void rollback(vector<pair<Element *, bool> > &elemsToReplace, map<Element *, pair<ElementType, ElementsWeightType> > &oldTypes)
	{
		try {
			vector<Element *> del;
			for (vector<pair<Element *, bool> >::iterator it = elemsToReplace.begin(); it != elemsToReplace.end(); it++) {
				if (it->second) {
					del.push_back(it->first);
				} else {
					Element * e = it->first;
					map<Element *, pair<ElementType, ElementsWeightType> >::iterator itm = oldTypes.find(e);
					if (itm != oldTypes.end()) {
						ElementType elementType = itm->second.first;
						if (e->getElementType() != elementType) {
							dimension->changeElementType(e, elementType, user, false);
						}
						if (elementType == CONSOLIDATED) {
							set<Element *> keep;
							for (ElementsWeightType::iterator itch = itm->second.second.begin(); itch != itm->second.second.end(); itch++) {
								keep.insert(itch->first);
							}
							dimension->removeChildrenNotIn(0, e, &keep);
							dimension->addChildren(e, &itm->second.second, 0);
						}
					}
				}
			}
			dimension->deleteElements(del, 0);
		} catch (...) {
		}

	}
};

}

#endif
