////////////////////////////////////////////////////////////////////////////////
/// @brief cube convert job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Zurab Khadikov, Jedox AG, Freiburg, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef CUBE_CONVERT_JOB_H
#define	CUBE_CONVERT_JOB_H 1

#include "palo.h"

#ifdef ENABLE_GPU_SERVER 
    #include "Olap/gpuCube.h"
#endif

#include "Olap/NormalCube.h"

#include "PaloDispatcher/DirectPaloJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cube convert
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CubeConvertJob : DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new CubeConvertJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CubeConvertJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

    void compute()
    {
        if ( !server->isEnableGpu() )
        {
            throw ErrorException(ErrorException::ERROR_GPU_SERVER_NOT_ENABLED, "cube convert is not possible");
        }

        findCube();

        if(cube->sizeFilledNumericCells() == 0)
        {
            throw ErrorException (ErrorException::ERROR_CUBE_EMPTY, "convert empty cube is not possible");
        }

        if (cube -> hasRule())
        {
            throw ErrorException (ErrorException::ERROR_UNKNOWN, "convert cube with rules is not possible");
        }

        uint32_t targetType = jobRequest->type;

        vector<Dimension*> dimensions;
        const vector<Dimension*>* dims = cube -> getDimensions();

        if (dims->size() != 0) {
            for (uint32_t i = 0; i < dims->size(); i++) {
                Dimension* dimension = database->findDimension(dims->at(i)->getIdentifier(), user);
                dimensions.push_back(dimension);
            }
        } else {
            throw ErrorException(ErrorException::ERROR_DIMENSION_NOT_FOUND, "dimensions missing");
        }


        if ( targetType == 0 && cube->getType() == GPUTYPE)
        {
            server->invalidateCache();

            // convert gpu type cube to normal cube
            cube = database->changeCubeType(cube,&dimensions,targetType,user);
        } 
        else
        {
            if (targetType == 4 && cube->getType() == NORMAL)
            {
                server->invalidateCache();

                // convert normal cube to gpu type cube
                cube = database->changeCubeType(cube,&dimensions,targetType,user);
            }
            else
            {
                throw ParameterException(ErrorException::ERROR_INVALID_TYPE, "wrong cube type or target cube type", PaloRequestHandler::ID_TYPE, targetType);
            }
        }

        CubeWorker* cubeWorker = cube->getCubeWorker();

        if (cubeWorker != 0) {

            bool ok = cubeWorker->start();

            if (!ok) {
                throw ErrorException(ErrorException::ERROR_WORKER_MESSAGE, "cannot start worker");
            }
        }

        // load cube with new type
        database->loadCube(cube, user);

        generateCubeResponse(cube);
    }

};
}

#endif // CUBE_CONVERT_JOB_H
