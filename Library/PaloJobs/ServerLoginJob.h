////////////////////////////////////////////////////////////////////////////////
/// @brief server login job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_SERVER_LOGIN_JOB_H
#define PALO_JOBS_SERVER_LOGIN_JOB_H 1

#include "palo.h"

#include "Olap/PaloSession.h"
#include "PaloDispatcher/DirectPaloJob.h"
#include "PaloDispatcher/PaloJobRequest.h"
#include "PaloHttpServer/PaloRequestHandler.h"
#include "Worker/LoginWorker.h"
#include "Worker/Worker.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief server login
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS ServerLoginJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new ServerLoginJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	ServerLoginJob(Server* server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief initializes job
	////////////////////////////////////////////////////////////////////////////////

	bool initialize()
	{
		bool ok = PaloJob::initialize();

		if (!ok) {
			return false;
		}

		try {
			if (server->isLoginRequired()) {
				if (server->getLoginType() == WORKER_NONE) {
					worker = 0;
					type = READ_JOB;
				} else {
					worker = server->getLoginWorker();

					if (worker == 0) {
						type = READ_JOB;
					} else {
						checkToken(server);
						type = SPECIAL_JOB;
					}
				}
			} else {
				worker = 0;
				type = READ_JOB;
			}
		} catch (const ErrorException& e) {
			response = new HttpResponse(HttpResponse::BAD);

			StringBuffer& body = response->getBody();

			body.appendCsvInteger((int32_t)e.getErrorType());
			body.appendCsvString(StringUtils::escapeString(ErrorException::getDescriptionErrorType(e.getErrorType())));
			body.appendCsvString(StringUtils::escapeString(e.getMessage()));
			body.appendEol();

			Logger::warning << "error code: " << (int32_t)e.getErrorType() << " description: " << ErrorException::getDescriptionErrorType(e.getErrorType()) << " message: " << e.getMessage() << endl;

			return false;
		}

		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief gets job type
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return type;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief start working
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		checkToken(server);
		PaloSession * session = 0;
		bool useMd5 = true;

		if (!server->isLoginRequired()) {
			session = PaloSession::createSession(server, 0, false, server->getDefaultTtl());
		} else {

			// extract and check username
			username = jobRequest->user;

			if (username == 0) {
				throw ParameterException(ErrorException::ERROR_AUTHORIZATION_FAILED, "missing username", PaloRequestHandler::NAME_USER, "");
			}

			if (username->empty()) {
				throw ParameterException(ErrorException::ERROR_AUTHORIZATION_FAILED, "empty username", PaloRequestHandler::NAME_USER, "");
			}

			// extract and check password
			password = jobRequest->password;

			if (worker != 0 && server->getLoginType() == WORKER_AUTHENTICATION) {
				if (jobRequest->externPassword != 0) {
					password = jobRequest->externPassword;
				}
			} else if (worker == 0) {
				if (password == 0 && jobRequest->externPassword != 0) {
					password = jobRequest->externPassword;
					useMd5 = false;
				}
			}

			if (password == 0) {
				throw ParameterException(ErrorException::ERROR_AUTHORIZATION_FAILED, "missing password", PaloRequestHandler::PASSWORD, "");
			}

			if (password->empty()) {
				throw ParameterException(ErrorException::ERROR_AUTHORIZATION_FAILED, "empty password", PaloRequestHandler::PASSWORD, "");
			}

			// login using username and password
			if (worker == 0) {
				session = loginInternal(useMd5);
			} else {
				if (server->getLoginType() == WORKER_INFORMATION) {
					session = loginExternalInformation();
				} else if (server->getLoginType() == WORKER_AUTHENTICATION) {
					session = loginExternalAuthentication();
				} else if (server->getLoginType() == WORKER_AUTHORIZATION) {
					session = loginExternalAuthorization();
				} else {
					throw ParameterException(ErrorException::ERROR_AUTHORIZATION_FAILED, "unknown login type", "login type", server->getLoginType());
				}
			}
		}

		generateLoginResponse(session);
	}

private:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief login internally
	////////////////////////////////////////////////////////////////////////////////

	PaloSession* loginInternal(bool useMd5)
	{
		SystemDatabase* sd = server->getSystemDatabase();

		if (sd == 0) {
			throw ErrorException(ErrorException::ERROR_DATABASE_NOT_FOUND, "system database not found");
		}

		User* user = sd->getUser(*username, *password, useMd5);

		if (user == 0 || !user->canLogin()) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "login error", PaloRequestHandler::NAME_USER, *username);
		}

		return PaloSession::createSession(server, user, false, server->getDefaultTtl());
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief login using external worker for information
	////////////////////////////////////////////////////////////////////////////////

	PaloSession* loginExternalInformation()
	{
		SystemDatabase* sd = server->getSystemDatabase();

		if (sd == 0) {
			throw ErrorException(ErrorException::ERROR_DATABASE_NOT_FOUND, "system database not found");
		}

		User* user = sd->getUser(*username, *password);

		if (user == 0 || !user->canLogin()) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "login error", PaloRequestHandler::NAME_USER, *username);
		}

		ResultStatus status = worker->loginInformation(*username, *password);

		if (status != RESULT_OK) {
			throw ParameterException(ErrorException::ERROR_AUTHORIZATION_FAILED, "worker failed", "username", *username);
		}

		return PaloSession::createSession(server, user, false, server->getDefaultTtl());
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief login using external worker for authentication
	////////////////////////////////////////////////////////////////////////////////

	PaloSession* loginExternalAuthentication()
	{
		SystemDatabase* sd = server->getSystemDatabase();

		if (sd == 0) {
			throw ErrorException(ErrorException::ERROR_DATABASE_NOT_FOUND, "system database not found");
		}

		bool canLogin;

		ResultStatus status = worker->authenticateUser(*username, *password, &canLogin);

		if (status != RESULT_OK) {
			throw ParameterException(ErrorException::ERROR_AUTHORIZATION_FAILED, "worker failed", "username", *username);
		}

		if (!canLogin) {
			throw ParameterException(ErrorException::ERROR_WORKER_AUTHORIZATION_FAILED, "login error", PaloRequestHandler::NAME_USER, *username);
		}

		User* user = sd->getUser(*username);

		if (user == 0 || !user->canLogin()) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "login error", PaloRequestHandler::NAME_USER, *username);
		}

		return PaloSession::createSession(server, user, false, server->getDefaultTtl());
	}

	////////////////////////////////////////////////////////////////////////////////
	/// @brief login using external worker for authorization
	////////////////////////////////////////////////////////////////////////////////

	PaloSession* loginExternalAuthorization()
	{
		SystemDatabase* sd = server->getSystemDatabase();

		if (sd == 0) {
			throw ErrorException(ErrorException::ERROR_DATABASE_NOT_FOUND, "system database not found");
		}

		bool canLogin;
		vector<string> groups;

		ResultStatus status = worker->authorizeUser(*username, *password, &canLogin, &groups);

		if (status != RESULT_OK) {
			throw ParameterException(ErrorException::ERROR_AUTHORIZATION_FAILED, "worker failed", "username", *username);
		}

		if (!canLogin) {
			throw ParameterException(ErrorException::ERROR_WORKER_AUTHORIZATION_FAILED, "login error", PaloRequestHandler::NAME_USER, *username);
		}

		User* user = sd->getExternalUser(*username, &groups);

		if (user == 0) {
			throw ParameterException(ErrorException::ERROR_NOT_AUTHORIZED, "login error", PaloRequestHandler::NAME_USER, *username);
		}

		return PaloSession::createSession(server, user, false, server->getDefaultTtl());
	}

private:
	LoginWorker * worker;
	JobType type;
	string* username;
	string* password;
};
}

#endif
