////////////////////////////////////////////////////////////////////////////////
/// @brief area sub job
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_AREA_SUB_JOB_H
#define PALO_JOBS_AREA_SUB_JOB_H 1

#include "palo.h"

#include "Olap/Cube.h"
#include "Olap/HashAreaStorage.h"
#include "Dispatcher/SubJob.h"

namespace palo {

class EMemoryContext;
////////////////////////////////////////////////////////////////////////////////
/// @brief cell area sub job
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS AreaSubJob : public SubJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	AreaSubJob(Job* job, Cube*cube, vector<vector<Element*> >* subArea, HashAreaStorage* doubleStorage, EMemoryContext* mem_context) :
		SubJob(job), cube(cube), subArea(subArea), doubleStorage(doubleStorage), mem_context(mem_context)
	{
	}

	~AreaSubJob()
	{
	}

public:
	void operator ()()
	{
		cube->fillHashAreaStorage2(doubleStorage, subArea, mem_context);
		delete this;
	}

	void compute()
	{

		cube->fillHashAreaStorage2(doubleStorage, subArea, mem_context);

		/*
		 vector< map<Element*, uint32_t> > hashMapping;
		 vector< vector< vector< pair<uint32_t, double> > > > numericMapping;
		 vector<uint32_t> hashSteps;
		 vector<uint32_t> lengths;
		 if (AreaResultStorage::computeAreaParameters(cube, *subArea, hashMapping, numericMapping, hashSteps, lengths)) {
		 HashAreaStorage tmpStorage(hashMapping);
		 cube->fillHashAreaStorage2( &tmpStorage, subArea );
		 tmpStorage.copy(doubleStorage, hashSteps, lengths);
		 }
		 */
	}

private:
	Cube* cube;
	vector<vector<Element*> >* subArea;
	HashAreaStorage* doubleStorage;
	EMemoryContext* mem_context;
};

}

#endif
