////////////////////////////////////////////////////////////////////////////////
/// @brief cell replace bulk
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PALO_JOBS_CELL_REPLACE_BULK_JOB_H
#define PALO_JOBS_CELL_REPLACE_BULK_JOB_H 1

#include "palo.h"

#include "Exceptions/CommunicationException.h"
#include "Olap/Lock.h"
#include "Olap/PaloSession.h"
#include "PaloDispatcher/DirectPaloJob.h"

namespace palo {

////////////////////////////////////////////////////////////////////////////////
/// @brief cell replace bulk
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS CellReplaceBulkJob : public DirectPaloJob {
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief factory method
	////////////////////////////////////////////////////////////////////////////////

	static PaloJob* create(Server* server, PaloJobRequest* jobRequest)
	{
		return new CellReplaceBulkJob(server, jobRequest);
	}

public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	CellReplaceBulkJob(Server * server, PaloJobRequest* jobRequest) :
		DirectPaloJob(server, jobRequest)
	{
	}

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	JobType getType()
	{
		return WRITE_JOB;
	}

	////////////////////////////////////////////////////////////////////////////////
	/// {@inheritDoc}
	////////////////////////////////////////////////////////////////////////////////

	void compute()
	{
		findPaths();
		assertParameter(PaloRequestHandler::VALUES, jobRequest->values);

		// get splash mode
		Cube::SplashMode mode = splashMode(jobRequest->splash);

		// get "add" flag, true means add value instead of set
		bool addFlag = jobRequest->add;

		if (addFlag) {
			if (mode != Cube::DISABLED && mode != Cube::ADD_BASE && mode != Cube::DEFAULT) {
				throw ParameterException(ErrorException::ERROR_INVALID_SPLASH_MODE, "add=1 requires splash mode DEFAULT, DISABLED, or ADD", PaloRequestHandler::SPLASH, (int)mode);
			}
		}

		// get event-process flag, false means to circumvent the processor (right will be checked by cube)
		bool eventProcessor = jobRequest->eventProcess;

		// check size of values
		if (cellPaths->size() > jobRequest->values->size()) {
			throw ParameterException(ErrorException::ERROR_PARAMETER_MISSING, "missing values", PaloRequestHandler::VALUES, (int)jobRequest->values->size());
		}

		// set cell value one-by-one
		PaloSession * session = PaloSession::findSession(getSession());

		for (size_t i = 0; i < cellPaths->size(); i++) {

			// get path
			IdentifiersType& path = cellPaths->at(i);

			// construct cell path
			CellPath cp(cube, &path);

			bool withinEvent = Server::isBlocking();

			if (session->isWorker()) {
				if (!withinEvent || getSession() != server->getActiveSession()) {
					throw ParameterException(ErrorException::ERROR_NOT_WITHIN_EVENT, "worker cell/replace_bulk requires an event/begin", "session", getSession());
				}
			}

			ResultStatus status = RESULT_FAILED;

			if (cp.getPathType() == STRING) {
				string value = jobRequest->values->at(i);

				status = cube->setCellValue(&cp, value, user, session, eventProcessor, !withinEvent, Lock::checkLock);
			} else {
				double value = StringUtils::stringToDouble(jobRequest->values->at(i));

				status = cube->setCellValue(&cp, value, user, session, eventProcessor, !withinEvent, addFlag, mode, Lock::checkLock);
			}

			if (status != RESULT_OK) {
				throw CommunicationException(ErrorException::ERROR_INVALID_WORKER_REPLY, "cube worker returned invalid answer");
			}
		}

		server->invalidateCache();

		generateOkResponse(cube);
	}
};

}

#endif
