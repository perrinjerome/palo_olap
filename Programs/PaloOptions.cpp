////////////////////////////////////////////////////////////////////////////////
/// @brief palo options
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
/// @author Zurab Khadikov, Jedox AG, Freiburg, Germany
////////////////////////////////////////////////////////////////////////////////

#include "PaloOptions.h"

#include <iostream>

#include "Logger/Logger.h"
#include "Olap/PaloSession.h"
#include "Olap/RollbackStorage.h"
#include "Olap/Server.h"
#include "Options/OptionsFileIterator.h"
#include "Worker/LoginWorker.h"
#include "PaloJobs/AreaJob.h"
#include "Olap/Engine.h"

namespace palo {
using namespace std;

// /////////////////////////////////////////////////////////////////////////////
// list of all options
// /////////////////////////////////////////////////////////////////////////////

// build options parser
static const char * AllowedOptions[] = {"?|help",
		"a+admin                 <address> <port>",
		"A|auto-load",
		"b:cache-barrier         <maximum of number_of_cells to store in each Cube cache>",
		"B|auto-commit",
		"C|chdir",
		"d:data-directory        <directory>",
		"D|add-new-databases",
		"e:clear-cache           <number_of_cache_invalidations>",
		"E:extensions            <directory>",
		"-f|fake-session-id",
#if defined(_MSC_VER)
		"F:friendly-service-name <service-name>",
#endif
		"g:clear-cache-cells     <number_of_cells to purge from cache>",
		"G:ignore-rule-error     <arg>",
		"h+http                  <address> <port>",
		"H+https                 <port>",
		"i:init-file             <init-file>",
		"I+initial-thread-pool   <initial-pool-size>",
		"K+key-files             <ca> <private> <dh>",
		"l:maximum-return-cells  <maximum_return_cells>",
		"L+splash-limit          <error> <warning> <info>",
		"M:session-timeout      <seconds>",
		"m:undo-memory-size      <undo_memory_size_in_bytes_per_lock>",
		"n|load-init-file",
		"o:log                   <logfile>",
		"O:maxgrow               <maximal-area-grow>",
		"p:password              <private-password>",
#if defined(_MSC_VER)
		"q+service-description   <service-description>",
#endif
		"Q+autosave              <mode> <hour>:<minute>",
		"R|user-login",
#if defined(_MSC_VER)
		"s|start-service",
		"S:service-name          <service-name>",
#endif
		"t:template-directory    <directory>",
#ifdef ENABLE_TRACE_OPTION
		"-T:trace                <trace-file>",
#endif
		"u:undo-file-size        <undo_file_size_in_bytes_per_lock>",
		"-U|ignore-cell-data",
		"v:verbose               <level>",
		"V|version",
		"w+worker                <worker-executable> <argument1> <argument2> <argumentX>",
		"x:workerlogin           <worker-login-type>",
		"X:encryption            <encryption-type>",
		"y|enable-drillthrough",
		"Y|use-cube-worker",
		"z:goalseek-timeout      <miliseconds>",
		"Z:goalseek-limit        <number_of_cells>",



		// old obsolete options:
		"c:cache-size            <total_cache_size_in_bytes>",
		" +processors-cores      <number-processors> <number-cores>",
		"J+max-subjobs           <max-subjobs-size>",
		"k|keep-trying",
		" |parallel-computation", 
		"r:rule-cache-size       <total_cache_size_in_bytes>",
		0};

/****************************************************************************
 **

 *defines rule error behaviour. affects results for division by zero
 */
bool ignoreRuleError;

// /////////////////////////////////////////////////////////////////////////////
// constructors
// /////////////////////////////////////////////////////////////////////////////

PaloOptions::PaloOptions(int argc, char** argv) :
	arguments(argc - 1, argv + 1), options(*argv, AllowedOptions)
{
	options.setControls(OptionSpecification::NOGUESSING);

	ignoreRuleError = true;
	autoAddDb = true;
	autoCommit = true;
	autoLoadDb = true;
	cacheBarrier = 5000.0;
	cacheClearBarrierCells = 1000.0;
	changeDirectory = true;
	dataDirectory = "./Data";
	defaultTtl = -1;
	drillThroughEnabled = false;
	encryptionType = ENC_NONE;
	extensionsDirectory = "../Modules";
	friendlyServiceName = "PALO 3.0 Server Service";
	goalseekCellLimit = 1000;
	goalseekTimeout = 10000;
	ignoreCellData = false;
	initFile = "palo.ini";
	logFile = "-";
	logLevel = "error";
	loginType = WORKER_NONE;
	requireUserLogin = false;
	serviceName = "PALO3ServerService";
	showUsage = false;
	showVersion = false;
	startAsService = false;
	templateDirectory = "../Api";
	undoFileSize = 50 * 1024 * 1024;
	undoMemorySize = 10 * 1024 * 1024;
	useCubeWorkers = false;
	useFakeSession = false;
	useInitFile = true;

	externalHttpInterface = 0;
	externalHttpsInterface = 0;
	externalJobAnalyser = 0;

	maximalAreaGrowSize = 200000;

	initialThreadPoolSize = 10;
	service_description = "Palo MOLAP Server Service";
	maximumReturnCells = 20000;


}

// /////////////////////////////////////////////////////////////////////////////
// public methods
// /////////////////////////////////////////////////////////////////////////////

void PaloOptions::parseOptions()
{
	parseOptions(arguments);
}

void PaloOptions::parseOptions(const string& filename)
{
	OptionsFileIterator file(filename);

	parseOptions(file);
}

void PaloOptions::checkOptions()
{

	// check key files
	if (!keyFiles.empty()) {
		if (keyFiles.size() % 3 != 0) {
			cout << "expecting three key files, not " << keyFiles.size() << "\n" << endl;
			usage();
		}

		if (keyFiles.size() > 3) {
			keyFiles.erase(keyFiles.begin(), keyFiles.end() - 3);
		}
	}

	// splash limits
	if (!splashLimits.empty()) {
		if (splashLimits.size() % 3 != 0) {
			cout << "expecting three splash limits, not " << splashLimits.size() << "\n" << endl;
			usage();
		}

		if (splashLimits.size() > 3) {
			splashLimits.erase(splashLimits.begin(), splashLimits.end() - 3);
		}
	}

	// check http host/port pairs
	if (httpPorts.size() % 2 == 1) {
		cout << "not enough arguments to --http\n" << endl;
		usage();
	}

	// check https ports
	if (!httpsPorts.empty() && httpsPorts.size() != httpPorts.size() / 2) {
		cout << "got " << httpsPorts.size() << " https ports, but " << (httpPorts.size() / 2) << " http addresses\n" << endl;
		usage();
	}

	if (encryptionType == ENC_REQUIRED) {
		if (!httpPorts.empty() && httpsPorts.empty()) {
			cout << "encryption type 'REQUIRED', but no https ports given\n" << endl;
			usage();
		}
	} else if (encryptionType == ENC_NONE && !httpsPorts.empty()) {
		cout << "encryption type 'NONE', but https ports given\n" << endl;
		usage();
	}

	// check admin host/port pairs
	if (adminPorts.size() % 2 == 1) {
		cout << "not enough arguments to --admin\n" << endl;
		usage();
	}

	if (httpPorts.empty() && adminPorts.empty()) {
		cout << "need at least one --http or --admin option\n" << endl;
		usage();
	}

	// check undo size
	if (undoMemorySize < 1024 * 1024) {
		undoMemorySize = 1024 * 1024;
		Logger::error << "undo memory size too small, using a size of 1MB" << endl;
	}
}

void PaloOptions::updateGlobals(Server* server)
{
	if (defaultTtl != -1) {
		Server::setDefaultTtl(defaultTtl);
		Logger::debug << "setting default session timeout to " << defaultTtl << endl;
	}

	if (splashLimits.size() == 3) {
		Cube::splashLimit1 = splashLimits[0];
		Cube::splashLimit2 = splashLimits[1];
		Cube::splashLimit3 = splashLimits[2];
	}

	Logger::debug << "using splash limits " << Cube::splashLimit1 << ", " << Cube::splashLimit2 << ", " << Cube::splashLimit3 << endl;

	Server::setLoginRequired(requireUserLogin);
	Server::setLoginType(loginType);
	Server::setDrillThroughEnabled(drillThroughEnabled);


	Cube::setCacheBarrier(cacheBarrier);
	Cube::setCacheClearBarrierCells(cacheClearBarrierCells);
	Cube::setGoalseekCellLimit(goalseekCellLimit);
	Cube::setGoalseekTimeout(goalseekTimeout);
	Cube::setIgnoreCellData(ignoreCellData);

	CubeWorker::setUseCubeWorker(useCubeWorkers);

	RollbackStorage::setMaximumMemoryRollbackSize(undoMemorySize);
	RollbackStorage::setMaximumFileRollbackSize(undoFileSize);

	AreaJob::setMaximalAreaGrowSize(maximalAreaGrowSize);
	AreaJob::setMaxCellCount(maximumReturnCells);

	if (loginType != WORKER_NONE || useCubeWorkers) {
		Worker::setExecutable(workerProgram);
		Worker::setArguments(workerProgramArguments);

		string arg;

		for (vector<string>::iterator i = workerProgramArguments.begin(); i != workerProgramArguments.end(); i++) {
			arg += " \'" + *i + "'";
		}

		Logger::info << "worker executable: '" << workerProgram << "'" << endl;
		Logger::info << "worker arguments: " << arg << endl;

		if (loginType != WORKER_NONE) {
			PaloSession* session = PaloSession::createSession(server, 0, true, 0);
			LoginWorker* worker = new LoginWorker(session->getEncodedIdentifier());

			server->setLoginWorker(worker);
		}
	}

	palo::ignoreRuleError = this->ignoreRuleError;
}

void PaloOptions::version()
{
	cout << "Palo Server Version " << Server::getVersionRevision() << endl;
	exit(0);
}

void PaloOptions::usage()
{
	if (showUsage) {
		cout << "Palo Server Version " << Server::getVersionRevision() << endl;
	}

	cout << options.usage("") << "\n"
#if defined(ENABLE_HTTPS)
	        << "<encryption-type> can be 'none', 'optional', or 'required'\n"
#endif
	        << "<worker-login-type> can be 'information', 'authentication', or\n" << "'authorization'. Setting <address> to \"\" binds the port to any\n" << "address. By default palo will change into the data directory before\n" << "opening the log file.\n" << endl;

	if (showUsage) {
		cout << "data-directory:        " << dataDirectory << "\n"
		<< "chdir on startup:      " << (changeDirectory ? "true" : "false") << "\n"
		<< "init-file:             " << initFile << "\n"
		<< "require user login:    " << (requireUserLogin ? "true" : "false") << "\n"
		<< "use init-file:         " << (useInitFile ? "true" : "false") << "\n"
		<< "auto-load databases:   " << (autoLoadDb ? "true" : "false") << "\n"
		<< "auto-add databases:    " << (autoAddDb ? "true" : "false") << "\n"
		<< "auto-commit on exit:   " << (autoCommit ? "true" : "false") << "\n"
		<< "use cube workers:      " << (useCubeWorkers ? "true" : "false") << "\n"
		<< "drillthrough enabled:  " << (drillThroughEnabled ? "true" : "false") << "\n"
		<< "maximal area grow size:" << maximalAreaGrowSize << "\n"
		<< "cache-barrier:         " << cacheBarrier << "\n"
		<< "clear-cache-cells:     " << cacheClearBarrierCells << "\n"

        ;
		cout << "\n"
		<< "The server side cache is configured to 5000 cells per cube\n"
		<< "by default. In order to deactivate the cache completely set both\n"
		<< "<cache-barrier> and <clear-cache-cells> to 0.\n";

		cout << "\n"
		<< "In a locked cube area it is possible to undo changes. Each lock can use\n"
		<< "<undo_memory_size_in_bytes_per_lock> bytes in memory and\n"
		<< "<undo_file_size_in_bytes_per_lock> bytes in files for storing changes:\n"
		<< "\n"
		<< "undo memory size in byte per lock: " << undoMemorySize << " (" << int(undoMemorySize / 1024 / 1024) << "MB)\n"
		<< "undo file size in byte per lock:   " << undoFileSize << " (" << int(undoFileSize / 1024 / 1024) << "MB)\n"
		<< "\n"
		<< "goalseek cell limit:   " << goalseekCellLimit << "\n"
		<< "goalseek timeout:   " << goalseekTimeout << "ms\n"

#if defined(_MSC_VER)
		        << "\n"
		        << "Or: " << options.getProgramName() << " install_srv <data-directory>\n"
		        << "    " << options.getProgramName() << " delete_srv\n\n"
		        << "to install or delete a Windows XP service. You can change the service\n"
		        << "name with --service-name. The default is '" << serviceName << "'\n"
#endif
		        << endl;
	}

	exit(showUsage ? 0 : 1);
}

// /////////////////////////////////////////////////////////////////////////////
// private methods
// /////////////////////////////////////////////////////////////////////////////

void PaloOptions::parseOptions(OptionsIterator& iter)
{
	options.clear();

	int optchar;
	string optarg;
	bool seenWorker = false;

	while ((optchar = options(iter, optarg)) != Options::OPTIONS_END_OPTIONS) {
		if (optchar == 'w') {
			if (!seenWorker) {
				workerProgram = "";
				workerProgramArguments.clear();
			}

			seenWorker = true;
		} else {
			seenWorker = false;
		}

		switch (optchar) {
		case 'A':
			autoLoadDb = !autoLoadDb;
			break;

		case 'a':
			adminPorts.push_back(optarg);
			break;

		case 'B':
			autoCommit = !autoCommit;
			break;

		case 'b':
			cacheBarrier = StringUtils::stringToDouble(optarg);
			break;

		case 'C':
			changeDirectory = !changeDirectory;
			break;

		case 'D':
			autoAddDb = !autoAddDb;
			break;

		case 'd':
			dataDirectory = optarg;
			break;

		case 'E':
			extensionsDirectory = optarg;
			break;

		case 'f':
			useFakeSession = !useFakeSession;
			break;

#if defined(_MSC_VER)
			case 'F':
			friendlyServiceName = optarg;
			break;
#endif

		case 'g':
			cacheClearBarrierCells = StringUtils::stringToDouble(optarg);
			break;

		case 'G':
			ignoreRuleError = optarg == "true";
			break;

		case 'h':
			httpPorts.push_back(optarg);
			break;

#if defined(ENABLE_HTTPS)
		case 'H':
			httpsPorts.push_back(optarg);
			break;
#endif

		case 'i':
			initFile = optarg;
			break;

		case 'I':
			initialThreadPoolSize = StringUtils::stringToInteger(optarg);
			break;

		case 'K':
			keyFiles.push_back(optarg);
			break;

		case 'L':
			splashLimits.push_back(StringUtils::stringToDouble(optarg));
			break;

		case 'l':
			maximumReturnCells = StringUtils::stringToInteger(optarg);
			break;

		case 'm':
			undoMemorySize = StringUtils::stringToInteger(optarg);
			break;

		case 'M':
			defaultTtl = StringUtils::stringToInteger(optarg);
			break;

		case 'n':
			useInitFile = !useInitFile;
			break;

		case 'o':
			logFile = optarg;
			break;

		case 'O':
			maximalAreaGrowSize = StringUtils::stringToInteger(optarg);
			break;

		case 'p':
			keyFilePassword = optarg;
			break;

#if defined(_MSC_VER)
			case 'q':
			service_description = optarg;
			break;
#endif
		case 'Q':
			autosave_definition.push(optarg);
			break;

		case 'R':
			requireUserLogin = !requireUserLogin;
			break;

#if defined(_MSC_VER)
			case 's':
			startAsService = ! startAsService;
			break;

			case 'S':
			serviceName = optarg;
			break;
#endif

		case 't':
			templateDirectory = optarg;
			break;

		case 'T':
			traceFile = optarg;
			break;

		case 'u':
			undoFileSize = StringUtils::stringToInteger(optarg);
			break;

		case 'U':
			ignoreCellData = !ignoreCellData;
			break;

		case 'v':
			logLevel = optarg;
			break;

		case 'V':
			showVersion = !showVersion;
			break;

		case 'w':
			if (workerProgram == "") {
				workerProgram = optarg;
			} else {
				workerProgramArguments.push_back(optarg);
			}
			break;

		case 'x':
			loginType = convertWorkerLoginType(optarg);
			break;

		case 'X':
			encryptionType = convertEncryptionType(optarg);
			break;

		case 'y':
			drillThroughEnabled = true;
			break;

		case 'Y':
			useCubeWorkers = !useCubeWorkers;
			break;

		case 'z':
			goalseekTimeout = StringUtils::stringToInteger(optarg);
			break;

		case 'Z':
			goalseekCellLimit = StringUtils::stringToInteger(optarg);
			break;


		case '?':
			showUsage = !showUsage;
			break;

		case Options::OPTIONS_POSITIONAL:
		case Options::OPTIONS_MISSING_VALUE:
		case Options::OPTIONS_EXTRA_VALUE:
		case Options::OPTIONS_AMBIGUOUS:
		case Options::OPTIONS_BAD_KEYWORD:
		case Options::OPTIONS_BAD_CHAR:
			cout << "\n";
			usage();
			break;

		// old obsolete arguments:
		case 'c':
		case 'e':
		case 'J':
		case 'k':
		case 'r':
			Logger::warning << "obsolete argument used: " << (char)optchar << endl;
			break;

		default:
			cout << "Oops! Got " << optchar << " '" << (char)optchar << "'\n" << endl;
			usage();
			break;
		}
	}
}

WorkerLoginType PaloOptions::convertWorkerLoginType(const string& type)
{
	string t = StringUtils::tolower(type);

	if (t == "information") {
		return WORKER_INFORMATION;
	} else if (t == "authentication") {
		return WORKER_AUTHENTICATION;
	} else if (t == "authorization") {
		return WORKER_AUTHORIZATION;
	} else {
		Logger::error << "unknown worker login type '" << type << "'" << endl;
		throw "unknown worker type";
	}
}

Encryption_e PaloOptions::convertEncryptionType(const string& type)
{
	string t = StringUtils::tolower(type);

	if (t == "none") {
		return ENC_NONE;
	} else if (t == "optional") {
		return ENC_OPTIONAL;
	} else if (t == "required") {
		return ENC_REQUIRED;
	} else {
		Logger::error << "unknown encryption type '" << type << "'" << endl;
		throw "unknown encryption type";
	}
}
}
