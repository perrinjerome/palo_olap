////////////////////////////////////////////////////////////////////////////////
/// @brief palo loader
///
/// @file deffer worker creation till the server is able to handle them
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// @author Vali Nitu, Yalos Solutions, Bucharest, Romania
////////////////////////////////////////////////////////////////////////////////

#ifndef PROGRAMS_WORKER_CREATOR_H
#define PROGRAMS_WORKER_CREATOR_H 1

#include <boost/thread/thread.hpp>
#include <boost/thread/thread_time.hpp>

#include "Olap/Server.h"

typedef void(*worker_callback)();

namespace palo {
class WorkersCreator {
private:
	Server* server;
	worker_callback sink;
public:
	WorkersCreator(Server* server, worker_callback sink) :
		server(server), sink(sink)
	{
		boost::thread(*this);
	}

	void operator()()
	{
		bool had_some_workers = false;
		boost::this_thread::sleep(boost::posix_time::seconds(1));

		if (NULL != server->getLoginWorker()) {
			had_some_workers = true;
		}

		vector<Database*> databases = server->getDatabases(0);

		for (vector<Database*>::iterator i = databases.begin(); i != databases.end(); i++) {
			Database* database = *i;

			vector<Cube*> cubes = database->getCubes(0);

			for (vector<Cube*>::iterator j = cubes.begin(); j != cubes.end(); j++) {
				Cube* cube = *j;

				CubeWorker* worker = cube->getCubeWorker();

				if (worker != 0) {
					Logger::trace << "trying to start cube worker for cube '" << cube->getName() << "'" << endl;

					bool ok = worker->start();

					Logger::trace << "status = " << (ok ? "ok" : "FAILED") << endl;

					had_some_workers = true;
				}
			}
		}

		if (had_some_workers) {
			Logger::info << "All Workers intialized." << endl;
		}

		if (NULL != sink) {
			sink(); //give a chance to the caller to update state
		}
	}
};

}
;

#endif
