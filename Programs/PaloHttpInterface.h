////////////////////////////////////////////////////////////////////////////////
/// @brief palo http/https interface
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PROGRAMS_PALO_HTTP_INTERFACE_H
#define PROGRAMS_PALO_HTTP_INTERFACE_H 1

#include "palo.h"
#include "Thread/ReadWriteLock.h"
#include "Thread/WriteLocker.h"
#include "extension.h"

namespace palo {
class PaloOptions;
class HttpServer;
class HttpServerTask;
class Server;
class PaloHttpServer;

////////////////////////////////////////////////////////////////////////////////
/// @brief palo http/https interface
////////////////////////////////////////////////////////////////////////////////

class SERVER_CLASS PaloHttpInterface {
protected:
	typedef std::vector<HttpServer*> server_list_type;
	typedef std::vector<HttpServerTask*> task_list_type;
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief constructor
	////////////////////////////////////////////////////////////////////////////////

	PaloHttpInterface(PaloOptions*, JobAnalyser*);
	virtual ~PaloHttpInterface()
	{
	}
public:

	////////////////////////////////////////////////////////////////////////////////
	/// @brief adds servers
	////////////////////////////////////////////////////////////////////////////////

	void addServers();

	////////////////////////////////////////////////////////////////////////////////
	/// @brief run the servers
	////////////////////////////////////////////////////////////////////////////////
	virtual void run();
	virtual void handleShutdown();
	void setPaloServer(Server* server)
	{
		this->server = server;
	}

	void commitAndSave();
private:
	void addHttpServer(const string& address, int port, int httpsPort, bool admin);

	void addHttpsServer(const string& address, int port);

protected:
	virtual void EnablePaloInterface(PaloHttpServer *paloHttpServer);

private:
	const string templateDirectory;
	const bool requireUserLogin;
	const Encryption_e encryptionType;
	const string traceFile;

	set<pair<string, int> > seen;

	int traceFileCounter;

	const vector<string> adminPorts;
	const vector<string> httpPorts;
	const vector<string> httpsPorts;
	const vector<string> keyFiles;
	const string keyFilePassword;

	InitHttpInterface_fptr externalHttpInterface;
	InitHttpsInterface_fptr externalHttpsInterface;

	JobAnalyser* analyser;
public:
	vector<HttpServer*> servers;
protected:
	bool runnable;
	ReadWriteLock master_lock;
	Server* server;
};

}

#endif
