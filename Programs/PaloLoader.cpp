////////////////////////////////////////////////////////////////////////////////
/// @brief palo loader
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#include "Programs/PaloLoader.h"

#include "Exceptions/FileFormatException.h"
#include "Exceptions/FileOpenException.h"
#include "Olap/PaloSession.h"
#include "Olap/Server.h"

namespace palo {

// /////////////////////////////////////////////////////////////////////////////
// constructors and destructors
// /////////////////////////////////////////////////////////////////////////////

PaloLoader::PaloLoader(Server* server, bool autoCommit, const string& dataDirectory) :
	server(server), autoCommit(autoCommit), dataDirectory(dataDirectory)
{
	lastResort = new char[1000 * 1000];
}

PaloLoader::~PaloLoader()
{
	if (lastResort != 0) {
		delete[] lastResort;
	}

	//    if (autoCommit) {
	//      commitChanges();
	//    }
}

// /////////////////////////////////////////////////////////////////////////////
// public methods
// /////////////////////////////////////////////////////////////////////////////

void PaloLoader::load(bool autoLoadDb, bool autoAddDb)
{

	loadServer();

	// add any new database found in the path
	if (autoLoadDb && autoAddDb) {
		addNewDatabases();
	}

	// autoload other databases
	if (autoLoadDb) {
		autoLoadDatabases();
	}
}

void PaloLoader::finalize(bool useFakeSession)
{

	// rebuild markers
	server->rebuildAllMarkers();

	// sort all unsorted cube pages
	server->sortAllCubePages();

	// create a fake session id
	if (useFakeSession) {
		PaloSession::createSession(PaloSession::FAKE_SESSION, server, 0, false, 0);
	}
}

// /////////////////////////////////////////////////////////////////////////////
// private methods
// /////////////////////////////////////////////////////////////////////////////

void PaloLoader::loadServer()
{
	bool loaded = false;

	// load palo server from disk
	try {
		server->loadServer(0);
		loaded = true;
	} catch (const FileFormatException& e) {
		Logger::error << "file format error: " << e.getMessage() << endl;
		Logger::error << "server file is corrupted, giving up!" << endl;
		throw "cannot load server";
	} catch (const FileOpenException& e) {
		loaded = false;
		Logger::warning << "failed to load server '" << e.getMessage() << "' (" << e.getDetails() << ")" << endl;
	} catch (const ErrorException& e) {
		Logger::error << "cannot load server, giving up! '" << e.getMessage() << "' (" << e.getDetails() << ")" << endl;
		throw "cannot load server";
	}

	// create a new server
	if (!loaded) {
		try {
			server->saveServer(0);
		} catch (const FileFormatException& e) {
			Logger::error << "file format error: " << e.getMessage() << endl;
			Logger::error << "detail: " << e.getDetails() << endl;
			Logger::error << "server file is corrupted, giving up!" << endl;
			throw "cannot load server";
		} catch (const FileOpenException&) {
			Logger::error << "cannot save server file, giving up!" << endl;
			throw "cannot load server";
		} catch (const ErrorException& e) {
			Logger::error << "cannot save server file, giving up! '" << e.getMessage() << "' (" << e.getDetails() << ")" << endl;
			throw "cannot load server";
		}
	}

	// load or generate system databases
	try {
		server->addSystemDatabase();
	} catch (const FileFormatException& e) {
		Logger::error << "file format error: " << e.getMessage() << endl;
		Logger::error << "detail: " << e.getDetails() << endl;
		Logger::error << "system database file is corrupted, giving up!" << endl;
		throw "cannot load server";
	} catch (const FileOpenException&) {
		Logger::error << "cannot load system database file, giving up!" << endl;
		throw "cannot load server";
	} catch (const ErrorException& e) {
		Logger::error << "cannot load system database file, giving up! '" << e.getMessage() << "' (" << e.getDetails() << ")" << endl;
		throw "cannot load server";
	}
}

void PaloLoader::autoLoadDatabases()
{
	Database* systemDB = server->getSystemDatabase();
	vector<Database*> databases = server->getDatabases(0);

	for (vector<Database*>::iterator i = databases.begin(); i != databases.end(); i++) {
		Database * database = *i;

		if (database == systemDB) {
			continue;
		}

		try {
			Logger::info << "auto loading database '" << database->getName() << "'" << endl;
			server->loadDatabase(database, 0);

			vector<Cube*> cubes = database->getCubes(0);

			for (vector<Cube*>::iterator j = cubes.begin(); j != cubes.end(); j++) {
				Cube * cube = *j;

				database->loadCube(cube, 0);
			}

			// go on and purge the orphan elements
			for (vector<Cube*>::iterator i = cubes.begin(); i < cubes.end(); i++) {
				Cube * cube = *i;

				if (NULL != cube) {
					if (!cube->purgedList.empty()) {
						for (vector<std::pair<Dimension*, IdentifierType> >::const_iterator p = cube->purgedList.begin(); p < cube->purgedList.end(); p++) {
							cube->deleteElement("system", "purged", p->first, p->second, true, true, true, NULL, true);
						}
						cube->purgedList.clear();
						database->saveCube(cube, NULL);
					}
				}
			}
		} catch (std::bad_alloc) {
			delete[] lastResort;
			lastResort = 0;

			Logger::error << "running out of memory, please reduce the number of loaded databases" << endl;
			throw "out of memory";
		} catch (const ErrorException& e) {
			Logger::warning << "cannot load database '" << database->getName() << "'" << endl;
			Logger::warning << "message 1: " << e.getMessage() << endl;
			Logger::warning << "message 2: " << e.getDetails() << endl;
		}
	}
}

void PaloLoader::addNewDatabases()
{
	vector<string> files = FileUtils::listFiles(dataDirectory);

	for (vector<string>::iterator i = files.begin(); i != files.end(); i++) {
		string dbName = *i;
		string file = dataDirectory + "/" + dbName;

		if (FileUtils::isReadable(FileName(file, "database", "csv"))) {
			if (dbName.find_first_not_of(Server::VALID_DATABASE_CHARACTERS) != string::npos) {
				Logger::warning << "directory '" << dbName << "' of database contains an illegal character, skipping" << endl;
			} else if (dbName[0] == '.') {
				Logger::warning << "directory '" << dbName << "' of database begins with a dot character, skipping" << endl;
			} else {
				if (server->lookupDatabaseByName(dbName) == 0) {
					Logger::info << "added new directory as database '" << dbName << "'" << endl;

					try {
						server->addDatabase(dbName, 0);
					} catch (const ErrorException& e) {
						Logger::warning << "cannot load database '" << dbName << "'" << endl;
						Logger::warning << "message 1: " << e.getMessage() << endl;
						Logger::warning << "message 2: " << e.getDetails() << endl;
						throw "cannot load database";
					}
				}
			}
		}
	}
}

void PaloLoader::commitChanges()
{
	try {
		server->saveServer(0);

		vector<Database*> databases = server->getDatabases(0);

		for (vector<Database*>::iterator i = databases.begin(); i != databases.end(); i++) {
			Database * database = *i;
			if (NULL == database)
				continue;

			if (database->getStatus() == Database::CHANGED) {
				Logger::info << "auto commiting changes to database '" << database->getName() << "'" << endl;
				server->saveDatabase(database, 0);
			}

			vector<Cube*> cubes = database->getCubes(0);

			for (vector<Cube*>::iterator j = cubes.begin(); j != cubes.end(); j++) {
				Cube * cube = *j;
				if (NULL == cube)
					continue;

				if (cube->getStatus() == Cube::CHANGED) {
					Logger::info << "auto commiting changes to cube '" << cube->getName() << "'" << endl;
					database->saveCube(cube, 0);
				}
			}
		}
	} catch (const ErrorException& e) {
		Logger::warning << "cannot commit data file '" << e.getMessage() << "' (" << e.getDetails() << ")" << endl;
	}
}
}
