////////////////////////////////////////////////////////////////////////////////
/// @brief main program
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
/// @author Marek Pikulski, marek@pikulski.net
/// @author Zurab Khadikov, Jedox AG, Freiburg, Germany
////////////////////////////////////////////////////////////////////////////////

#include "palo.h"

#include "Logger/Logger.h"
#include "Olap/Server.h"
#include "PaloDispatcher/PaloJobAnalyser.h"

#include "PaloHttpInterface.h"
#include "PaloLoader.h"
#include "PaloOptions.h"
#include "AutosaveTimer.h"
#include "WorkersCreator.h"

#include "extension.h"

typedef void(*callback)();
static int startPalo(palo::PaloOptions& options, callback sink);
static void initiatePaloStop();

#if defined(_MSC_VER)
#include "Win32ServiceWrapper.h"
#endif

using namespace std;
using namespace palo;

////////////////////////////////////////////////////////////////////////////////
/// @brief the one and only interface
////////////////////////////////////////////////////////////////////////////////
PaloHttpInterface* iface = NULL;

////////////////////////////////////////////////////////////////////////////////
/// @ctrl-c handlers
////////////////////////////////////////////////////////////////////////////////
#if defined(_MSC_VER)

SERVICE_STATUS_HANDLE Palo_Win32_Service::service_status;
PaloOptions* Palo_Win32_Service::PaloServiceOptions;
Win32StatusUpdater Palo_Win32_Service::StatusUpdater;

BOOL WINAPI signalHandler (DWORD dwCtrlType)
{

	initiatePaloStop();
	return TRUE;
}

#else

static void signalHandler(int signal)
{
	initiatePaloStop();
	Logger::info << "signaled to close" << endl;
}

#endif

static void initiatePaloStop()
{
	if (NULL != iface) {
		iface->handleShutdown();
	}
}

////////////////////////////////////////////////////////////////////////////////
/// @brief starts the server and blocks on select
////////////////////////////////////////////////////////////////////////////////
static int startPalo(PaloOptions& options, callback sink)
{
	int result = 0;
	JobAnalyser* analyser = NULL;
	Server* server = NULL;

	try {
		Server::setEncryptionType(options.encryptionType);
		// create server
		server = new Server("", "-1", "0", "2147483647", FileName(options.dataDirectory, "palo", "csv"));

		// update global settings
		options.updateGlobals(server);




		// check for external modules
		vector<ServerInfo_t*> extensions = palo::OpenExternalModules(server, options.extensionsDirectory);

		for (vector<ServerInfo_t*>::iterator i = extensions.begin(); i != extensions.end(); i++) {
			ServerInfo_t * info = *i;

			if (NULL != info->httpInterface) {
				options.externalHttpInterface = info->httpInterface;
			}

			if (NULL != info->httpsInterface) {
				options.externalHttpsInterface = info->httpsInterface;
			}

			if (NULL != info->jobAnalyser) {
				options.externalJobAnalyser = info->jobAnalyser;
			}
		}

		// create job analyser
		if (options.externalJobAnalyser) {
			analyser = (*options.externalJobAnalyser)(server);
		} else {
			analyser = new PaloJobAnalyser(server);
		}

		if (NULL == analyser) {
			Logger::error << "cannot construct job analyser, giving up!" << endl;
			throw "missing job analyser";
		}

		// load server from disk
		PaloLoader loader(server, options.autoCommit, options.dataDirectory);

		loader.load(options.autoLoadDb, options.autoAddDb);
		loader.finalize(options.useFakeSession);

		// wait for something to happen
		if (Server::isLoginRequired()) {
			Logger::info << "user login is required" << endl;
		}

		// create http interface
		iface = new PaloHttpInterface(&options, analyser);

		iface->addServers();
		iface->setPaloServer(server);

		//if ( NULL != sink ) { sink(); }//give a chance to the caller to update state

		AutosaveTimer autosave_timer(options.autosave_definition, iface);
		WorkersCreator creator(server, sink);

		Logger::info << "starting to listen" << endl;

		//block on run
		iface->run();

		//starting the shutdown sequence

		Logger::info << "sockets closed. shutting down continues..." << endl;
		autosave_timer.stop();
		//		server->beginShutdown(0);
		if (options.autoCommit) {
			server->commitAndSave();
		}

		if (NULL != sink) {
			sink(); //give a chance to the caller to update state
		}

		Logger::info << "finished" << endl;

	} catch (const ErrorException& e) {
		Logger::error << "got exception '" << e.getMessage() << "' (" << e.getDetails() << "), giving up!" << endl;
		result = 1;
	} catch (std::bad_alloc) {
		Logger::error << "running out of memory, please reduce the number of loaded databases or cache sizes" << endl;
		result = 1;
	} catch (...) {
		Logger::error << "got exception, giving up!" << endl;
		result = 1;
	}

	if (NULL != iface) {
		delete iface;
	}
	if (NULL != analyser) {
		delete analyser;
	}
	if (NULL != server) {
		delete server;
	}

	palo::CloseExternalModules();

	return result;
}

////////////////////////////////////////////////////////////////////////////////
/// @brief runs palo
////////////////////////////////////////////////////////////////////////////////
static int runPalo(int argc, char* argv[])
{
	// initialize random module
	srand((int)time(0));

	// initialize logger
	Logger::setLogLevel("error");

	// parse options
	PaloOptions options(argc, argv);

	try {
		// handle command line options and init file
		options.parseOptions();

		if (options.showUsage) {
			options.usage();
		}

		if (options.showVersion) {
			options.version();
		}

		// change into data directory
		if (options.changeDirectory) {
			if (0 != chdir(options.dataDirectory.c_str()))
#if !defined(_MSC_VER)
				// on a case sensitive FS also try lower & upper case dir names
				if (0 != chdir(StringUtils::tolower(options.dataDirectory).c_str()))
					if (0 != chdir(StringUtils::toupper(options.dataDirectory).c_str())) {
#else
						{
#endif
						Logger::error << "cannot change into directory '" << options.dataDirectory << "'" << endl;
						throw "missing directory";
					}

			Logger::info << "changed into directory '" << options.dataDirectory << "'" << endl;

			options.dataDirectory = ".";
		} else {
			if (!options.initFile.empty()) {
				if (options.initFile[0] != '/' && options.initFile[0] != '\\') {
					options.initFile = options.dataDirectory + "/" + options.initFile;
				}
			}
		}

		// load init file
		if (options.useInitFile && !options.initFile.empty()) {
			options.parseOptions(options.initFile);
		}

		options.checkOptions();

		Logger::setLogFile(options.logFile);
		Logger::setLogLevel(options.logLevel);

		// log version number
		Logger::info << "starting Palo " << Server::getVersionRevision() << endl;

#if defined(_MSC_VER)
		// start as service or start as normal program
		if (options.startAsService)
		{
			return Palo_Win32_Service::startPaloService(options);
		}
		else
		{
			SetConsoleCtrlHandler(&signalHandler, TRUE);
			return startPalo(options, NULL);
		}
#else
		return startPalo(options, NULL);
#endif
	} catch (const ErrorException& e) {
		Logger::error << "got exception '" << e.getMessage() << "' (" << e.getDetails() << "), giving up!" << endl;
		return 1;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// @brief main
////////////////////////////////////////////////////////////////////////////////

#if defined(_MSC_VER)

extern "C" SERVER_FUNC int PaloMain (int argc, char * argv[])
{
	int ret_code = 0;
	_setmaxstdio(2048); // this is a hard limit in windows

	if (argc >= 3 && strcmp(argv[argc-2], "install_srv") == 0)
	{
		PaloOptions options(argc - 2, argv);
		options.parseOptions();

		Palo_Win32_Service::InstallService(options, argv[argc-1]);
	}
	else if (argc >= 2 && strcmp(argv[argc-1], "delete_srv") == 0)
	{
		PaloOptions options(argc - 1, argv);
		options.parseOptions();

		Palo_Win32_Service::DeleteService(options);
	}
	else
	{
		WSADATA wsaData;
		WSAStartup(MAKEWORD(1,1), &wsaData);

		ret_code = runPalo(argc, argv);

		WSACleanup();
	}

	return ret_code;
}

#else

int main(int argc, char * argv[])
{
	signal(SIGABRT, &signalHandler);
	signal(SIGTERM, &signalHandler);
	signal(SIGQUIT, &signalHandler);
	signal(SIGINT, &signalHandler);

	return runPalo(argc, argv);
}

#endif
