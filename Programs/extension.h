////////////////////////////////////////////////////////////////////////////////
/// @brief support for dynamic extensions
///
/// @file
///
/// Copyright (C) 2006-2010 Jedox AG
///
/// This program is free software; you can redistribute it and/or modify it
/// under the terms of the GNU General Public License (Version 2) as published
/// by the Free Software Foundation at http://www.gnu.org/copyleft/gpl.html.
///
/// This program is distributed in the hope that it will be useful, but WITHOUT
/// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
/// more details.
///
/// You should have received a copy of the GNU General Public License along with
/// this program; if not, write to the Free Software Foundation, Inc., 59 Temple
/// Place, Suite 330, Boston, MA 02111-1307 USA
///
/// You may obtain a copy of the License at
///
/// <a href="http://www.jedox.com/license_palo_suite.txt">
///   http://www.jedox.com/license_palo_suite.txt
/// </a>
///
/// If you are developing and distributing open source applications under the
/// GPL License, then you are free to use Palo under the GPL License.  For OEMs,
/// ISVs, and VARs who distribute Palo with their products, and do not license
/// and distribute their source code under the GPL, Jedox provides a flexible
/// OEM Commercial License.
///
/// Portions of the code developed by triagens GmbH, Koeln on behalf of Jedox
/// AG. Intellectual property rights for these portions has triagens GmbH,
/// Koeln, or othervise Jedox AG, Freiburg. Exclusive worldwide exploitation
/// right (commercial copyright) has Jedox AG, Freiburg.
///
/// @author Frank Celler, triagens GmbH, Cologne, Germany
/// @author Achim Brandt, triagens GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

#ifndef PROGRAMS_EXTENSION_H
#define PROGRAMS_EXTENSION_H 1

#include "palo.h"

namespace palo {
class HttpServer;
class LegacyServer;
class Server;
class JobAnalyser;

////////////////////////////////////////////////////////////////////////////////
/// @brief init function for an http interface
////////////////////////////////////////////////////////////////////////////////

typedef HttpServer* (*InitHttpInterface_fptr)(const string& templateDirectory);

////////////////////////////////////////////////////////////////////////////////
/// @brief init function for an https interface
////////////////////////////////////////////////////////////////////////////////

typedef HttpServer* (*InitHttpsInterface_fptr)(const string& rootfile, const string& keyfile, const string& password, const string& dhfile, const string& templateDirectory);

////////////////////////////////////////////////////////////////////////////////
/// @brief init function for a job analyser
////////////////////////////////////////////////////////////////////////////////

typedef JobAnalyser* (*InitJobAnalyser_fptr)(Server*);

////////////////////////////////////////////////////////////////////////////////
/// @brief close extension function
////////////////////////////////////////////////////////////////////////////////

typedef void (*CloseExtension_fptr)();

////////////////////////////////////////////////////////////////////////////////
/// @brief information about the server
////////////////////////////////////////////////////////////////////////////////

struct ServerInfo_t {
	const char * revision; // input
	const char * version; // input
	Server * server; // input
	const char * modules; // input

#if defined(_MSC_VER)
	HINSTANCE handle; // input (handle to DLL)
#else
	void * handle; // input (handle to shared library)
#endif

	char * description; // output
	InitHttpInterface_fptr httpInterface; // output
	InitHttpsInterface_fptr httpsInterface; // output
	InitJobAnalyser_fptr jobAnalyser; // output
	CloseExtension_fptr closeExtension; // output
};

////////////////////////////////////////////////////////////////////////////////
/// @brief init function
////////////////////////////////////////////////////////////////////////////////

#if defined(_MSC_VER)
#define INIT_EXTENSION(a) extern "C" __declspec(dllexport) InitExtension_fptr InitExtension = &a
#else
#define INIT_EXTENSION(a) InitExtension_fptr InitExtension = &a
#endif

typedef bool (*InitExtension_fptr)(ServerInfo_t*);

////////////////////////////////////////////////////////////////////////////////
/// @brief checks the existance of external modules
////////////////////////////////////////////////////////////////////////////////

vector<ServerInfo_t*> OpenExternalModules(Server* server, const string& directory);

////////////////////////////////////////////////////////////////////////////////
/// @brief checks the existance of external modules
////////////////////////////////////////////////////////////////////////////////

void CloseExternalModules();
}

#endif
